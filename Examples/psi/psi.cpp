// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/core.h>
#include <script/script.h>
#include <core/Application.h>
#include <core/Console.h>
#include <core/Exception.h>
#include <core/Log.h>
#include <io/FileStream.h>
#include <io/FileSystem.h>
#include <io/FileUtils.h>
#include <io/Path.h>
#include <io/Stream.h>
#include <reflection/Assembly.h>
#include <reflection/PolkAPI.h>
#include <script/Executable.h>
#include <script/ExecutionTrack.h>
#include <script/ScriptList.h>
#include <script/StringSlice.h>

using namespace polk::core;
using namespace polk::collections;
using namespace polk::io;
using namespace polk::reflection;
using namespace polk::script;

static CLog* g_log = nullptr;
static CExecutionTrack* g_track = nullptr;
static int g_anonExeId = 0;
static CArrayList<CAssembly*>* g_assemblies = nullptr;

    // ****************************
    //       Source registry.
    // ****************************

class CSourceRegistryImpl: public CSourceRegistry
{
private:
	Auto<const CString> m_rootPath;
    PolkAPI m_polkAPI;

public:
	CSourceRegistryImpl(const CString* scriptPath)
	{
		Auto<const CString> fullScriptPath (Path::GetFullPath(scriptPath));
		m_rootPath.SetPtr(Path::GetParent(fullScriptPath));

        InitializePolkAPI(&m_polkAPI);
	}

	virtual const CString* LoadSource(const CString* path) override
	{
		Auto<const CString> finalPath (Path::Combine(m_rootPath, path));

        const CString* source = LoadAsAssembly(finalPath);
        if(source) {
            return source;
        }
		
        return LoadAsPolkScript(finalPath);
	}
    
private:
    const CString* LoadAsAssembly(const CString* path)
    {
        Auto<const CString> asmName (CAssembly::GetAssemblyName(path));
        if (asmName) { // a valid assembly name => load as an assembly
            try {

                Auto<CAssembly> assembly (CAssembly::Load(path));  
                g_assemblies->Add(assembly); // pins to avoid data unload

                typedef void (*EntryPointFunction)(PolkAPI*);
                EntryPointFunction fn = assembly->GetFunction<EntryPointFunction>("RegisterClasses");
                fn(&m_polkAPI);

                // returns a dummy
                return CString::CreateEmptyString();

            } catch (const SException&) {
                return nullptr;
            }
        } else {
            return nullptr;
        }
    }
    
    const CString* LoadAsPolkScript(const CString* path)
    {
		try {
			return FileUtils::ReadAllText(path);
		} catch (const SException&) {
			return nullptr;
		}
    }
};

    // ****************************
    //    Custom native methods.
    // ****************************

static SScriptValue eval(const CString* code)
{
    try {
        
        Auto<const CString> anon (CString::Format("command%d", ++g_anonExeId));

        SStringSlice codeSlice (code);
        Auto<CExecutable> exe (CExecutable::Compile(codeSlice,
                                                    anon,
                                                    g_track->TopExecutable()->Metadata(),
                                                    g_log));
        if(!exe)
            return SScriptValue::FromNothing();

        Auto<CExecutionTrack> track (new CExecutionTrack(exe, g_track->VariableContext()));
		track->SetSourceRegistry(g_track->SourceRegistry());

        while(track->Resume()) { }

        return track->ReturnValue();

    } catch (const SException& e) {

        return SScriptValue::FromNothing();

    }
}

METAMETHODIMPL(Shell_Eval_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const codeArg = &args[0];
    VERIFY_ARGOBJ(codeArg, "String")

    METAGUARD_BEGIN
        const CString* const code = METAVALUE_TO_STRINGPTR(codeArg);

        if(!CString::IsNullOrEmpty(code)) {
            SScriptValue ret (eval(code));

            *out_ret = *ret.MetaValue();
            RefMetaValue(out_ret);
        }
    METAGUARD_END
}

METAMETHODIMPL(Shell_GetGlobals_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        auto r = new CScriptList();

        Auto<CHashMap<SStringSlice, SScriptValue> > vars (g_track->GetVariables());
        SHashMapEnumerator<SStringSlice, SScriptValue> varEnum (vars);
        SStringSlice k;
        while(varEnum.MoveNext(&k, nullptr)) {
            Auto<const CString> str (k.ToString());
            if(str->Length() > 0 && str->Chars()[0] == POLK_CHAR('_'))
                continue;
            SScriptValue value;
            value.SetObject("String", (intptr_t)(void*)static_cast<const CObject*>(str.Ptr()));
            r->Add(value);
        }

        *out_ret = MetaValueFromPtr("List", (void*)static_cast<CObject*>(r));
    METAGUARD_END
}

// TODO should be moved to FileSystem.cpp
METAMETHODIMPL(Shell_ListFiles_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        auto r = new CScriptList();

        Auto<CArrayList<const CString*> > files (FileSystem::ListFiles(METAVALUE_TO_STRINGPTR(pathArg), false));
        for(int i = 0; i < files->Count(); i++) {
            const CString* file = files->Array()[i];

            SScriptValue value;
            value.SetObject("String", (intptr_t)(void*)static_cast<const CObject*>(file));
            r->Add(value);
        }

        *out_ret = MetaValueFromPtr("List", (void*)static_cast<CObject*>(r));
    METAGUARD_END
}

static void __InitMetaShell()
{
    BEGIN_VTABLE(3)
        DEFINE_VMETHOD("Eval", Shell_Eval_impl, 0)
        DEFINE_VMETHOD("GetGlobals", Shell_GetGlobals_impl, 0)
        DEFINE_VMETHOD("ListFiles", Shell_ListFiles_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "Shell";
    metaClass.flags = METACLASSFLAGS_STATIC;

    RegisterMetaClass(&metaClass);
}

    // ****************
    //      misc
    // ****************

static bool isNewer(const CString* a, const CString* b)
{
    if(!FileSystem::FileExists(a) || !FileSystem::FileExists(b))
        return false;

    Auto<CFileSystemInfo> aInfo (FileSystem::GetFileSystemInfo(a));
    Auto<CFileSystemInfo> bInfo (FileSystem::GetFileSystemInfo(b));

    const auto aDate = aInfo->LastWriteTimeUtc();
    const auto bDate = bInfo->LastWriteTimeUtc();

    return aDate.IsAfter(bDate);
}

    // ****************
    //      main
    // ****************
    
static bool mainImpl()
{
    Auto<const CString> scriptPath (Application::GetCommandLineArgs());
    Auto<const CString> statePath (Path::ChangeExtension(scriptPath, ".state"));

    Auto<CLog> log (new CLog());
    Auto<CLogHandler> logHandler (CLogHandler::CreateForConsole());
    log->AddLogHandler(E_LOGPRIORITY_ALL, "Core;Script", logHandler);
    g_log = log;

    try {

        // If scriptPath is newer than statePath, then statePath is stale data.
        if(isNewer(scriptPath, statePath)) {
            log->Write(E_LOGPRIORITY_INFO, "Script", "Previous execution discarded.");
            printf("\n");

            FileSystem::DeleteFile(statePath);
        }

        Auto<CExecutable> exe;
        Auto<CExecutionTrack> track;

        if(FileSystem::FileExists(statePath)) {

            // .state file found => deserialize our previous track from it.
            Auto<CStream> stateStream (CFileStream::Open(statePath, E_FILEACCESS_READ));
            track.SetPtr(CExecutionTrack::Deserialize(stateStream, log));

            log->Write(E_LOGPRIORITY_INFO, "Script", "Execution resumed.");
            printf("\n");

        } else {

            // No .state file found => creates a new track.
            Auto<const CString> code (CString::IsNullOrEmpty(scriptPath)? CString::FromUtf8("Console WriteLine \"Polk interpreter. Usage: psi path-to-file\"")
                                                                        : FileUtils::ReadAllText(scriptPath));
            exe.SetPtr(CExecutable::Compile(code, scriptPath, log));
            track.SetPtr(new CExecutionTrack(exe));

			Auto<CSourceRegistry> sourceRegistry (new CSourceRegistryImpl(scriptPath));
			track->SetSourceRegistry(sourceRegistry);
        }

        g_track = track;

        if(track->Resume()) {

            // We're here if a `yield` operator was executed.
            // We simulate yielding in this interpreter by dumping track data
            // to a temporary file. This allows us to play with serialization
            // and check its consistency.

            Auto<CStream> stateStream (CFileStream::Open(statePath, E_FILEACCESS_WRITE));
            track->Serialize(stateStream, log);

            printf("\n");
            log->Write(E_LOGPRIORITY_INFO, "Script", "Execution suspended.");

        } else {

            // We're here after the track finished naturally. So we delete the
            // state file.
            if(FileSystem::FileExists(statePath)) {
                FileSystem::DeleteFile(statePath);

                printf("\n");
                log->Write(E_LOGPRIORITY_INFO, "Script", "Execution finished.");
            }

        }

    } catch (const SException& e) {

        if(e.Message()) {
            log->Write(E_LOGPRIORITY_ERROR, "Script", "Error: '%s'\n", e.Message());
        } else {
            log->Write(E_LOGPRIORITY_ERROR, "Script", "Error code: '%d'\n", e.Code());
        }

        return false;

    }
    
    return true;
}

int main()
{
    InitCore();
    InitScript();
    __InitMetaShell();
    g_assemblies = new CArrayList<CAssembly*>();

    bool success = mainImpl();
    
    g_assemblies->Unref();
    g_assemblies = nullptr;
    DeinitScript();
    DeinitCore();
    return success? 0: 1;
}
