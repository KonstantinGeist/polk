Polk Standalone Interpreter. A standalone version.
Supports `yield` statements across process executions.

See also ./scripts for some examples to play with.