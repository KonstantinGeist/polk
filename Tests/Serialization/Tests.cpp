// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/core.h>
#include <core/Contract.h>
#include <core/Exception.h>
#include <core/Log.h>
#include <core/Object.h>
#include <core/Random.h>
#include <core/String.h>
#include <core/Variant.h>
#include <reflection/IdTable.h>
#include <reflection/MetaObject.h>
#include <io/BinaryReader.h>
#include <io/BinaryWriter.h>
#include <io/FileStream.h>
#include <io/MemoryStream.h>

#define NODECLASSNAME "idtableproto.Node"

using namespace polk::core;
using namespace polk::reflection;
using namespace polk::io;

static CLog* g_log = nullptr;
#include "../TestFramework.h"

class CNode final: public CObject
{
public:
    SVariant Value1, Value2;

    virtual bool Equals(const CObject* obj) const override
    {
        auto const other = dynamic_cast<const CNode*>(obj);
        if(!other)
            return false;

        return this->Value1.Equals(other->Value1)
            && this->Value2.Equals(other->Value2);
    }
};

#define GENTYPE_CONST 7 	// leads to 4 nodes per tree on average
//#define GENTYPE_CONST 9  	// leads to 15 nodes per tree on average

static CNode* genNode(int level, CRandom* random);

static int genType(CRandom* random)
{
    int type = random->NextInt(0, GENTYPE_CONST);
    if(type > METATYPE_BOOL) // clamps to METATYPE_OBJECT to make Node a very popular type
        type = METATYPE_OBJECT;
    return type;
}

#define MAX_LEVEL 20
static SVariant genValue(int level, CRandom* random)
{
    // NOTE limits nesting to avoid stack overflow
    int type = (level < MAX_LEVEL)? genType(random): METATYPE_NULL;

    SVariant r;

    if(type == METATYPE_NULL) {
        // nothing
    } else if(type == METATYPE_NUMBER) {
        r.SetFloat((float)((random->NextDouble() * 10.0) - 20.0f));
    } else if(type == METATYPE_BOOL) {
        r.SetBool(random->NextInt(0, 1) == 0);
    } else if(type == METATYPE_OBJECT) {
        Auto<CNode> node (genNode(level + 1, random));
        r.SetObject(node);
    } else {
        POLK_THROW(EC_NOT_IMPLEMENTED);
    }

    return r;
}

static CNode* genNode(int level, CRandom* random)
{
    CNode* const r = new CNode();
    r->Value1 = genValue(level, random);
    r->Value2 = genValue(level, random);
    return r;
}

static CNode* generateRandomTree(int seed = 0)
{
    Auto<CRandom> random (new CRandom(seed));

    return genNode(0, random);
}

static void serializeDataTree(CNode* node, SBinaryWriter& bw)
{
    METAVALUE value = MetaValueFromPtr(NODECLASSNAME, node);

    Auto<CIdTable> idTable (CIdTable::CreateInstance());
    METAIDTABLE pIdTable;
    idTable->ToMetaIdTable(&pIdTable);

    CStream* const baseStream = bw.Stream();
    METASTREAM pBaseStream;
    baseStream->ToMetaStream(&pBaseStream);

    const polk_long idTableOffsetRefPos = baseStream->GetPosition();
    bw.WriteInt(0); // allocates space for the offset to the id table

    METARESULT res = SerializeMetaValue(&value, &pBaseStream, &pIdTable);
    assert(res == METARESULT_SUCCESS);

    const int idTableOffsetPos = (int)baseStream->GetPosition();
    idTable->Serialize(bw);

    const polk_long oldPos = baseStream->GetPosition();
    baseStream->SetPosition(idTableOffsetRefPos);
    bw.WriteInt(idTableOffsetPos);
    baseStream->SetPosition(oldPos);
}

static CNode* deserializeDataTree(SBinaryReader& br)
{
    CStream* const baseStream = br.Stream();
    METASTREAM pBaseStream;
    baseStream->ToMetaStream(&pBaseStream);    

    const int tableOffsetPos = br.ReadInt();
    const polk_long oldPos = baseStream->GetPosition();
    baseStream->SetPosition(tableOffsetPos);

    Auto<CIdTable> idTable (CIdTable::Deserialize(br));
    METAIDTABLE pIdTable;
    idTable->ToMetaIdTable(&pIdTable);
    baseStream->SetPosition(oldPos);

    METAVALUE metaValue;
    METARESULT res = DeserializeMetaValue(&pBaseStream, &metaValue, &pIdTable);
    assert(res == METARESULT_SUCCESS);
    assert(metaValue.type == METATYPE_OBJECT);
    assert(strcmp(metaValue.u.asObject.klass->name, NODECLASSNAME) == 0);

    return static_cast<CNode*>((CObject*)metaValue.u.asObject.idptr);
}

METACTORIMPL(Node_ctor)
{
    METAGUARD_BEGIN

        auto const r = new CNode();
        *out_ret = OBJECT_TO_IDPTR(r);

    METAGUARD_END
}

static void serializeVariant(const SVariant& variant, METASTREAM* stream, METAIDTABLE* idTable)
{
    METAVALUE metaValue;

    switch(variant.Type()) {
        case E_VARIANTTYPE_NOTHING:
            metaValue = MetaValueFromNull();
            break;

        case E_VARIANTTYPE_BOOL:
            metaValue = MetaValueFromBoolean(variant.BoolValue());
            break;

        case E_VARIANTTYPE_FLOAT:
            metaValue = MetaValueFromNumber(variant.FloatValue());
            break;

        case E_VARIANTTYPE_OBJECT:
            metaValue = MetaValueFromPtr(NODECLASSNAME, dynamic_cast<CNode*>(variant.ObjectValue()));
            break;

        default:
            POLK_THROW(EC_NOT_IMPLEMENTED);
    }

    METARESULT res = SerializeMetaValue(&metaValue, stream, idTable);
    assert(res == METARESULT_SUCCESS);
}

static SVariant deserializeVariant(METASTREAM* stream, METAIDTABLE* idTable)
{
    METAVALUE metaValue;
    METARESULT res = DeserializeMetaValue(stream, &metaValue, idTable);
    assert(res == METARESULT_SUCCESS);

    SVariant r;

    switch(metaValue.type) {
        case METATYPE_NULL:
            // Nothing.
            break;

        case METATYPE_NUMBER:
            r.SetFloat(metaValue.u.asNumber);
            break;

        case METATYPE_BOOL:
            r.SetBool(metaValue.u.asBool);
            break;

        case METATYPE_OBJECT:
            {
                assert(strcmp(metaValue.u.asObject.klass->name, NODECLASSNAME) == 0);
                auto const node = dynamic_cast<CNode*>((CObject*)metaValue.u.asObject.idptr);
                assert(node);
                r.SetObject(node);
            }
            break;

        default:
            POLK_THROW(EC_NOT_IMPLEMENTED);
    }

    bool deleted;
    res = UnrefMetaValue(&metaValue, &deleted);
    assert(res == METARESULT_SUCCESS);

    return r;
}

METASERIALIMPL(Node_serialize)
{
    METAGUARD_BEGIN
        auto const self = static_cast<CNode*>((CObject*)(void*)(obj->idptr));
        
        serializeVariant(self->Value1, stream, idTable);
        serializeVariant(self->Value2, stream, idTable);
    METAGUARD_END
}

METADESERIALIMPL(Node_deserialize)
{
    METAGUARD_BEGIN
        const SVariant value1 (deserializeVariant(stream, idTable));
        const SVariant value2 (deserializeVariant(stream, idTable));

        auto const node = new CNode();
        node->Value1 = value1;
        node->Value2 = value2;
        *out_ret = MetaValueFromPtr(NODECLASSNAME, node);
    METAGUARD_END
}

void __InitMetaNode()
{
    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    metaClass.name = NODECLASSNAME;
    metaClass.ctor = Node_ctor;
    metaClass.serialimpl = Node_serialize;
    metaClass.deserialimpl = Node_deserialize;
    metaClass.parent = GetMetaClassByName("Object");
    POLK_REQ_PTR(metaClass.parent);

    RegisterMetaClass(&metaClass);
}

static bool verifyDataTreeSerialization()
{
    Auto<CStream> stream (new CMemoryStream());
    SBinaryWriter bw (stream);

    Auto<CNode> node (generateRandomTree());

    serializeDataTree(node, bw);

    // *******************************

    stream->SetPosition(0);
    SBinaryReader br (stream);

    Auto<CNode> deserialized (deserializeDataTree(br));

    return deserialized->Equals(node);
}

// Randomly generates a lot of trees and compares the original to the deserialized
// object.
static bool verifyRandomizedTrees()
{
    for(int i = 0; i < 10000; i++) {
        if(!verifyDataTreeSerialization())
            return false;
    }

    return true;
}

// Validates that the same reference mentioned several times does not end up as
// duplicate objects.
static bool testReferenceEquality()
{
    Auto<CNode> node1 (generateRandomTree(5));
    Auto<CNode> node2 (generateRandomTree(5));

    // Same object added twice.
    node1->Value1.SetObject(node2);
    node1->Value2.SetObject(node2);

    Auto<CMemoryStream> mem (new CMemoryStream());
    SBinaryWriter bw (mem);
    SBinaryReader br (mem);

    serializeDataTree(node1, bw);

    mem->SetPosition(0);
    Auto<CNode> deserialized (deserializeDataTree(br));

    return deserialized->Value1.ObjectValue() == deserialized->Value2.ObjectValue();
}

static void performTests()
{
    TEST(verifyRandomizedTrees);
    TEST(testReferenceEquality);
}

int main()
{
    InitCore();
    __InitMetaNode();

    g_log = new CLog();
    Auto<const CString> logPath (CString::FromUtf8("Serialization.log"));
    Auto<CLogHandler> logHandler (CLogHandler::CreateFromFile(logPath));
    g_log->AddLogHandler(E_LOGPRIORITY_ALL, "Core", logHandler);
    logHandler.SetPtr(CLogHandler::CreateForConsole());
    g_log->AddLogHandler(E_LOGPRIORITY_ALL, "Core", logHandler);

    performTests();

    g_log->Unref();
    g_log = nullptr;

    DeinitCore();
    return 0;
}
