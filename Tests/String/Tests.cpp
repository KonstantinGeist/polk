// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/core.h>
#include <core/Log.h>
#include <core/String.h>
#include <core/StringBuilder.h>
#include <collections/ArrayList.h>

using namespace polk::core;
using namespace polk::collections;

static CLog* g_log = nullptr;

#include "../TestFramework.h"

static bool String_CreateBuffer()
{
    polk_char16* chars;
    Auto<const CString> str (CString::CreateBuffer(3, &chars));
    chars[0] = POLK_CHAR('a');
    chars[1] = POLK_CHAR('b');
    chars[2] = POLK_CHAR('c');

    return str->EqualsASCII("abc");
}

static void String_CreateBuffer_Failure1()
{
    polk_char16* chars;
    Auto<const CString> str (CString::CreateBuffer(-1, &chars));
}

static void String_CreateBuffer_Failure2()
{
    Auto<const CString> str (CString::CreateBuffer(1, nullptr));
}

static bool String_Length()
{
    Auto<const CString> str (CString::FromUtf8("12345"));
    return str->Length() == 5;
}

static bool String_Chars()
{
    Auto<const CString> str (CString::FromUtf8("12345"));
    return str->Chars()[2] == POLK_CHAR('3');
}

static bool String_IsNullOrEmpty1()
{
    return CString::IsNullOrEmpty(nullptr);
}

static bool String_IsNullOrEmpty2()
{
    Auto<const CString> empty (CString::CreateEmptyString());
    return CString::IsNullOrEmpty(empty);
}

static bool String_IsNullOrEmpty3()
{
    Auto<const CString> str (CString::FromUtf8("12345"));
    return !CString::IsNullOrEmpty(str);
}

static bool String_FromChar()
{
    Auto<const CString> str (CString::FromChar(POLK_CHAR('z')));
    return str->EqualsASCII("z");
}

static bool String_FromUtf8()
{
    Auto<const CString> str (CString::FromUtf8("hel5"));
    if(str->Length() != 4)
        return false;
    const polk_char16* chars = str->Chars();

    if(chars[0] != POLK_CHAR('h'))
        return false;
    if(chars[1] != POLK_CHAR('e'))
        return false;
    if(chars[2] != POLK_CHAR('l'))
        return false;
    if(chars[3] != POLK_CHAR('5'))
        return false;

    return true;
}

static bool String_FromASCII()
{
    Auto<const CString> str (CString::FromASCII("Hello, World! :) 123"));
    return str->EqualsASCII("Hello, World! :) 123");
}

static bool String_ToUtf8()
{
    Auto<const CString> str1 (CString::FromUtf8("hel5"));
    Utf8Auto str2 (str1->ToUtf8());

    return str1->EqualsASCII(str2);
}

static bool String_CloneUtf8()
{
    char* clone = CString::CloneUtf8("Hello, world!");
    if(strcmp(clone, "Hello, world!") != 0)
        return false;
    CString::FreeUtf8(clone);
    return true;
}

static bool String_Concat1()
{
    Auto<const CString> str1 (CString::FromUtf8("Hello, "));
    Auto<const CString> str2 (CString::FromUtf8("world!"));
    Auto<const CString> r (str1->Concat(str2));
    return r->EqualsASCII("Hello, world!");
}

static bool String_Concat2()
{
    Auto<const CString> str1 (CString::FromUtf8("Hello, "));
    Auto<const CString> str2 (CString::FromUtf8("world"));
    Auto<const CString> str3 (CString::FromUtf8("!"));
    Auto<const CString> r (str1->Concat(str2, str3));
    return r->EqualsASCII("Hello, world!");
}

static bool String_Substring1()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    Auto<const CString> substr (str->Substring(4));

    return substr->EqualsASCII("o, world!");
}

static bool String_Substring2()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    Auto<const CString> substr (str->Substring(4, 3));

    return substr->EqualsASCII("o, ");
}

static void String_Substring_Failure1()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    Auto<const CString> substr (str->Substring(-1));
}

static void String_Substring_Failure2()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    Auto<const CString> substr (str->Substring(100));
}

static void String_Substring_Failure3()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    Auto<const CString> substr (str->Substring(4, 20));
}

static void String_Substring_Failure4()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    Auto<const CString> substr (str->Substring(4, -1));
}

static void String_Substring_Failure5()
{
    Auto<const CString> str (CString::CreateEmptyString());
    Auto<const CString> substr (str->Substring(4, 1));
}

static bool String_Remove1()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    str.SetPtr(str->Remove(POLK_CHAR('o')));
    return str->EqualsASCII("Hell, wrld!");
}

static bool String_Remove2()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    str.SetPtr(str->Remove(POLK_CHAR('z')));
    return str->EqualsASCII("Hello, world!");
}

static bool String_Replace1()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    str.SetPtr(str->Replace(POLK_CHAR('o'), POLK_CHAR('z')));
    return str->EqualsASCII("Hellz, wzrld!");
}

static bool String_Replace2()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    str.SetPtr(str->Replace(POLK_CHAR('z'), POLK_CHAR('y')));
    return str->EqualsASCII("Hello, world!");
}

static bool String_PadRight()
{
    Auto<const CString> str (CString::FromUtf8("abz"));
    str.SetPtr(str->PadRight(3));
    return str->EqualsASCII("abz   ");
}

static void String_PadRight_Failure()
{
    Auto<const CString> str (CString::FromUtf8("abz"));
    str.SetPtr(str->PadRight(-3));
}

static bool String_StartsWith()
{
    Auto<const CString> str (CString::FromUtf8("abz"));
    return str->StartsWith("ab");
}

static bool String_StartsWithASCII()
{
    Auto<const CString> str (CString::FromUtf8("abz"));
    return str->StartsWithASCII("ab");
}

static bool String_EndsWith()
{
    Auto<const CString> str (CString::FromUtf8("abz"));
    return str->EndsWith("bz");
}

static bool String_EndsWithASCII()
{
    Auto<const CString> str (CString::FromUtf8("abz"));
    return str->EndsWithASCII("bz");
}

static bool String_GetHashCode()
{
    Auto<const CString> str1 (CString::FromUtf8("abc"));
    Auto<const CString> str2 (CString::FromUtf8("abd"));

    return str1->GetHashCode() != str2->GetHashCode();
}

static bool String_FindChar1()
{
    Auto<const CString> str (CString::FromUtf8("baca"));
    return str->FindChar(POLK_CHAR('a') == 1);
}

static bool String_FindChar2()
{
    Auto<const CString> str (CString::FromUtf8("baca"));
    return str->FindChar(POLK_CHAR('z') == -1);
}

static void String_FindChar_Failure1()
{
    Auto<const CString> str (CString::FromUtf8("baca"));
    str->FindChar(POLK_CHAR('z'), -5);
}

static void String_FindChar_Failure2()
{
    Auto<const CString> str (CString::FromUtf8("baca"));
    str->FindChar(POLK_CHAR('z'), 0, 10);
}

static bool String_FindLastChar1()
{
    Auto<const CString> str (CString::FromUtf8("baca"));
    return str->FindLastChar(POLK_CHAR('a') == 3);
}

static bool String_FindLastChar2()
{
    Auto<const CString> str (CString::FromUtf8("baca"));
    return str->FindLastChar(POLK_CHAR('z') == -1);
}

static void String_FindLastChar_Failure1()
{
    Auto<const CString> str (CString::FromUtf8("baca"));
    str->FindLastChar(POLK_CHAR('z'), -5);
}

static void String_FindLastChar_Failure2()
{
    Auto<const CString> str (CString::FromUtf8("baca"));
    str->FindLastChar(POLK_CHAR('z'), 0, 10);
}

static bool String_FindSubstring1()
{
    Auto<const CString> str1 (CString::FromUtf8("Hello, world!"));
    Auto<const CString> str2 (CString::FromUtf8("world"));
    return str1->FindSubstring(str2) == 7;
}

static bool String_FindSubstring2()
{
    Auto<const CString> str1 (CString::FromUtf8("Hello, world!"));
    Auto<const CString> str2 (CString::FromUtf8("hell"));
    return str1->FindSubstring(str2) == -1;
}

static bool String_FindSubstring3()
{
    Auto<const CString> str1 (CString::FromUtf8("Hello, world!"));
    Auto<const CString> str2 (CString::FromUtf8("Hello"));
    return str1->FindSubstring(str2) == 0;
}

static bool String_FindSubstring4()
{
    Auto<const CString> str1 (CString::FromUtf8("Hello, world!"));
    Auto<const CString> str2 (CString::FromUtf8("Hello, world!ABC"));
    return str1->FindSubstring(str2) == -1;
}

static void String_FindSubstring_Failure1()
{
    Auto<const CString> str1 (CString::FromUtf8("Hello, world!"));
    Auto<const CString> str2 (CString::FromUtf8("Hello, world!ABC"));
    str1->FindSubstring(str2, -1);
}

static void String_FindSubstring_Failure2()
{
    Auto<const CString> str1 (CString::FromUtf8("Hello, world!"));
    Auto<const CString> str2 (CString::FromUtf8("Hello, world!ABC"));
    str1->FindSubstring(str2, 100);
}

static bool String_FindSubstringASCII()
{
    Auto<const CString> str1 (CString::FromUtf8("Hello, world!"));
    return str1->FindSubstringASCII("world") == 7;
}

static bool String_ToLowerCase()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    str.SetPtr(str->ToLowerCase());
    return str->EqualsASCII("hello, world!");
}

static bool String_ToUpperCase()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    str.SetPtr(str->ToUpperCase());
    return str->EqualsASCII("HELLO, WORLD!");
}

static bool String_Equals()
{
    Auto<const CString> str1 (CString::FromUtf8("Hello, world!"));
    Auto<const CString> str2 (CString::FromUtf8("Hello, world!"));
    return str1->Equals(str2);
}

static bool String_EqualsASCII()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    return str->EqualsASCII("Hello, world!");
}

static bool String_ToString()
{
    Auto<const CString> str1 (CString::FromUtf8("Hello, world!"));
    Auto<const CString> str2 (str1->ToString());
    return str1->Equals(str2);
}

static bool String_Format()
{
    Auto<const CString> str (CString::FromUtf8("Hello, world!"));
    Auto<const CString> formatted (CString::Format("b<%o %d %f %s%%",
                                                   static_cast<const CObject*>(str.Ptr()),
                                                   666,
                                                   13.5f,
                                                   "yep"));
    return formatted->EqualsASCII("b<Hello, world! 666 13.5 yep%%");
}

static bool String_TryParseString1()
{
    Auto<const CString> str1 (CString::FromUtf8(" \"Hello, world!\"  "));
    const CString* str2;
    if(!str1->TryParseString(&str2))
        return false;

    bool b = str2->EqualsASCII("Hello, world!");
    str2->Unref();
    return b;
}

static bool String_TryParseString2()
{
    Auto<const CString> str1 (CString::FromUtf8(" \"Hello, world!\"  s"));
    const CString* str2;
    if(str1->TryParseString(&str2)) {
        str2->Unref();
        return false;
    } else {
        return true;
    }
}

static bool String_TryParseInt1()
{
    Auto<const CString> str (CString::FromUtf8("78905"));
    int i;
    if(str->TryParseInt(&i)) {
        return i == 78905;
    } else {
        return false;
    }
}

static bool String_TryParseInt2()
{
    Auto<const CString> str (CString::FromUtf8("-78905555"));
    int i;
    if(str->TryParseInt(&i)) {
        return i == -78905555;
    } else {
        return false;
    }
}

static bool String_TryParseInt3()
{
    Auto<const CString> str (CString::FromUtf8("-789055b55"));
    int i;
    if(str->TryParseInt(&i)) {
        return false;
    } else {
        return true;
    }
}

static bool String_TryParseInt4()
{
    Auto<const CString> str (CString::FromUtf8("132"));
    int i;
    if(str->TryParseInt(&i, -1)) {
        return false;
    } else {
        return true;
    }
}

static bool String_TryParseInt5()
{
    Auto<const CString> str (CString::FromUtf8("132"));
    int i;
    if(str->TryParseInt(&i, 5)) {
        return false;
    } else {
        return true;
    }
}

static bool String_TryParseInt6()
{
    Auto<const CString> str (CString::FromUtf8("132"));
    int i;
    if(str->TryParseInt(&i, 1, 8)) {
        return false;
    } else {
        return true;
    }
}

static bool String_TryParseInt7()
{
    Auto<const CString> str (CString::FromUtf8("78905"));
    int i;
    if(str->TryParseInt(&i, 0, 3)) {
        return i == 789;
    } else {
        return false;
    }
}

static bool String_TryParseFloat1()
{
    Auto<const CString> str (CString::FromUtf8("1.0"));
    float f;
    if(str->TryParseFloat(&f)) {
        return f == 1.0f;
    } else {
        return false;
    }
}

static bool String_TryParseFloat2()
{
    Auto<const CString> str (CString::FromUtf8("z1.0"));
    float f;
    if(str->TryParseFloat(&f)) {
        return false;
    } else {
        return true;
    }
}

static bool String_TryParseBool1()
{
    Auto<const CString> str (CString::FromUtf8("true"));
    bool b;
    if(str->TryParseBool(&b)) {
        return b == true;
    } else {
        return false;
    }
}

static bool String_TryParseBool2()
{
    Auto<const CString> str (CString::FromUtf8("True"));
    bool b;
    if(str->TryParseBool(&b)) {
        return b == true;
    } else {
        return false;
    }
}

static bool String_TryParseBool3()
{
    Auto<const CString> str (CString::FromUtf8("false"));
    bool b;
    if(str->TryParseBool(&b)) {
        return b == false;
    } else {
        return false;
    }
}

static bool String_TryParseBool4()
{
    Auto<const CString> str (CString::FromUtf8("False"));
    bool b;
    if(str->TryParseBool(&b)) {
        return b == false;
    } else {
        return false;
    }
}

static bool String_TryParseBool5()
{
    Auto<const CString> str (CString::FromUtf8("random"));
    bool b;
    if(str->TryParseBool(&b)) {
        return false;
    } else {
        return true;
    }
}

static bool String_Clone()
{
    Auto<const CString> str1 (CString::FromUtf8("random"));
    Auto<const CString> str2 (str1->Clone());
    return str1->Equals(str2) && (str1.Ptr() != str2.Ptr());
}

static bool String_Split1()
{
    Auto<const CString> str (CString::FromUtf8("abc def gh  ikjlmn"));
    Auto<CArrayList<const CString*> > r (str->Split(POLK_CHAR(' ')));
    if(r->Count() != 5)
        return false;
    if(!r->Array()[0]->EqualsASCII("abc"))
        return false;
    if(!r->Array()[1]->EqualsASCII("def"))
        return false;
    if(!r->Array()[2]->EqualsASCII("gh"))
        return false;
    if(!CString::IsNullOrEmpty(r->Array()[3]))
        return false;
    if(!r->Array()[4]->EqualsASCII("ikjlmn"))
        return false;

    return true;
}

static bool String_Split2()
{
    Auto<const CString> str (CString::FromUtf8("abcXYZdefXYZghXYZXYZikjlmn"));
    Auto<const CString> del (CString::FromUtf8("XYZ"));
    Auto<CArrayList<const CString*> > r (str->Split(del));
    if(r->Count() != 5)
        return false;
    if(!r->Array()[0]->EqualsASCII("abc"))
        return false;
    if(!r->Array()[1]->EqualsASCII("def"))
        return false;
    if(!r->Array()[2]->EqualsASCII("gh"))
        return false;
    if(!CString::IsNullOrEmpty(r->Array()[3]))
        return false;
    if(!r->Array()[4]->EqualsASCII("ikjlmn"))
        return false;

    return true;
}

static bool String_CompareTo()
{
    Auto<const CString> str1 (CString::FromUtf8("abc"));
    Auto<const CString> str2 (CString::FromUtf8("abc"));

    return str1->CompareTo(str2) == 0;
}

static bool String_Trim1()
{
    Auto<const CString> str (CString::FromUtf8("  abc"));
    str.SetPtr(str->Trim());
    return str->EqualsASCII("abc");
}

static bool String_Trim2()
{
    Auto<const CString> str (CString::FromUtf8("  abc "));
    str.SetPtr(str->Trim());
    return str->EqualsASCII("abc");
}

static bool String_Trim3()
{
    Auto<const CString> str (CString::FromUtf8("abc  "));
    str.SetPtr(str->Trim());
    return str->EqualsASCII("abc");
}

static bool String_CreateEmptyString()
{
    Auto<const CString> str (CString::CreateEmptyString());
    return CString::IsNullOrEmpty(str);
}

// TODO UTF32

static void StringBuider_ctor_Failure()
{
    Auto<CStringBuilder> sb (new CStringBuilder(-5));
}

static bool StringBuider_Capacity()
{
    Auto<CStringBuilder> sb (new CStringBuilder(32));
    return sb->Capacity() == 32;
}

static bool StringBuilder_Length()
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    if(sb->Length() != 0)
        return false;

    sb->Append("abc");
    if(sb->Length() != 3)
        return false;

    sb->SetLength(2);
    if(sb->Length() != 2)
        return false;

    return true;
}

static bool StringBuilder_Chars()
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    sb->Append("abc");
    polk_char16* chars = sb->Chars();

    if(chars[0] != POLK_CHAR('a'))
        return false;
    if(chars[1] != POLK_CHAR('b'))
        return false;
    if(chars[2] != POLK_CHAR('c'))
        return false;

    return true;
}

static bool StringBuider_Append()
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    sb->Append("abc");
    Auto<const CString> str (CString::FromUtf8("def"));
    sb->Append(str);
    sb->Append(str, 1, 1);
    SVariant v;
    v.SetInt(666);
    sb->Append(v);
    sb->Append(777);
    sb->Append(1.0f);
    sb->Append(POLK_CHAR('Z'));
    sb->AppendASCII("abc");
    sb->AppendFormat("%d %s", 13, "abc");
    Auto<const CString> r (sb->ToString());
    return r->EqualsASCII("abcdefe6667771Zabc13 abc");
}

static void StringBuider_Append_Failure1()
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    Auto<const CString> str (CString::Format("random"));
    sb->Append(str, -1);
}

static void StringBuider_Append_Failure2()
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    Auto<const CString> str (CString::Format("random"));
    sb->Append(str, 1, 10);
}

static bool StringBuider_Clear()
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    sb->Append("abc");
    sb->Clear();
    return sb->Length() == 0;
}

static bool StringBuider_Remove()
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    sb->Append("abc");
    sb->Remove(1, 1);
    Auto<const CString> r (sb->ToString());
    return r->EqualsASCII("ac");
}

static void StringBuider_Remove_Failure1()
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    sb->Append("abc");
    sb->Remove(-1, 1);
}

static void StringBuider_Remove_Failure2()
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    sb->Append("abc");
    sb->Remove(0, -3);
}

static void StringBuider_Remove_Failure3()
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    sb->Append("abc");
    sb->Remove(1, 5);
}

static bool StringBuider_Insert1()
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    sb->Append("abc");
    sb->Insert(1, POLK_CHAR('z'));
    Auto<const CString> r (sb->ToString());
    return r->EqualsASCII("azbc");
}

static bool StringBuider_Insert2()
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    sb->Append("abc");
    Auto<const CString> str (CString::Format("random"));
    sb->Insert(1, str);
    Auto<const CString> r (sb->ToString());
    return r->EqualsASCII("arandombc");
}

// No need for CString since it's tested multiple times in most tests.

static void performTests()
{
    TEST(String_CreateBuffer)
    TEST_FAILURE(String_CreateBuffer_Failure1)
    TEST_FAILURE(String_CreateBuffer_Failure2)
    TEST(String_Length)
    TEST(String_Chars)
    TEST(String_IsNullOrEmpty1)
    TEST(String_IsNullOrEmpty2)
    TEST(String_IsNullOrEmpty3)
    TEST(String_FromChar)
    TEST(String_FromUtf8)
    TEST(String_ToUtf8)
    TEST(String_FromASCII)
    TEST(String_CloneUtf8)
    TEST(String_Concat1)
    TEST(String_Concat2)
    TEST(String_Substring1)
    TEST(String_Substring2)
    TEST_FAILURE(String_Substring_Failure1)
    TEST_FAILURE(String_Substring_Failure2)
    TEST_FAILURE(String_Substring_Failure3)
    TEST_FAILURE(String_Substring_Failure4)
    TEST_FAILURE(String_Substring_Failure5)
    TEST(String_Remove1)
    TEST(String_Remove2)
    TEST(String_Replace1)
    TEST(String_Replace2)
    TEST(String_PadRight)
    TEST_FAILURE(String_PadRight_Failure)
    TEST(String_StartsWith)
    TEST(String_StartsWithASCII)
    TEST(String_EndsWith)
    TEST(String_EndsWithASCII)
    TEST(String_GetHashCode)
    TEST(String_FindChar1)
    TEST(String_FindChar2)
    TEST_FAILURE(String_FindChar_Failure1)
    TEST_FAILURE(String_FindChar_Failure2)
    TEST(String_FindLastChar1)
    TEST(String_FindLastChar2)
    TEST_FAILURE(String_FindLastChar_Failure1)
    TEST_FAILURE(String_FindLastChar_Failure2)
    TEST(String_FindSubstring1)
    TEST(String_FindSubstring2)
    TEST(String_FindSubstring3)
    TEST(String_FindSubstring4)
    TEST_FAILURE(String_FindSubstring_Failure1)
    TEST_FAILURE(String_FindSubstring_Failure2)
    TEST(String_FindSubstringASCII)
    TEST(String_ToLowerCase)
    TEST(String_ToUpperCase)
    TEST(String_Equals)
    TEST(String_EqualsASCII)
    TEST(String_ToString)
    TEST(String_Format)
    TEST(String_TryParseString1)
    TEST(String_TryParseString2)
    TEST(String_TryParseInt1)
    TEST(String_TryParseInt2)
    TEST(String_TryParseInt3)
    TEST(String_TryParseInt4)
    TEST(String_TryParseInt5)
    TEST(String_TryParseInt6)
    TEST(String_TryParseInt7)
    TEST(String_TryParseFloat1)
    TEST(String_TryParseFloat2)
    TEST(String_TryParseBool1)
    TEST(String_TryParseBool2)
    TEST(String_TryParseBool3)
    TEST(String_TryParseBool4)
    TEST(String_TryParseBool5)
    TEST(String_Clone)
    TEST(String_Split1)
    TEST(String_Split2)
    TEST(String_CompareTo)
    TEST(String_Trim1)
    TEST(String_Trim2)
    TEST(String_Trim3)
    TEST(String_CreateEmptyString)

    TEST_FAILURE(StringBuider_ctor_Failure)
    TEST(StringBuider_Capacity)
    TEST(StringBuilder_Length)
    TEST(StringBuilder_Chars)
    TEST(StringBuider_Append)
    TEST_FAILURE(StringBuider_Append_Failure1)
    TEST_FAILURE(StringBuider_Append_Failure2)
    TEST(StringBuider_Clear)
    TEST(StringBuider_Remove)
    TEST_FAILURE(StringBuider_Remove_Failure1)
    TEST_FAILURE(StringBuider_Remove_Failure2)
    TEST_FAILURE(StringBuider_Remove_Failure3)
    TEST(StringBuider_Insert1)
    TEST(StringBuider_Insert2)
}

// *****************************************************************************
// *****************************************************************************

int main()
{
    InitCore();

    g_log = new CLog();
    Auto<const CString> logPath (CString::FromUtf8("String.log"));
    Auto<CLogHandler> logHandler (CLogHandler::CreateFromFile(logPath));
    g_log->AddLogHandler(E_LOGPRIORITY_ALL, "Core", logHandler);
    logHandler.SetPtr(CLogHandler::CreateForConsole());
    g_log->AddLogHandler(E_LOGPRIORITY_ALL, "Core", logHandler);

    performTests();

    g_log->Unref();
    g_log = nullptr;

    DeinitCore();
    return 0;
}
