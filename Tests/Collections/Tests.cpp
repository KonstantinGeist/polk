// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/core.h>
#include <core/CoreUtils.h>
#include <core/Log.h>
#include <collections/ArrayList.h>
#include <collections/HashMap.h>
#include <collections/LinkedList.h>
#include <collections/Queue.h>
#include <collections/Stack.h>

using namespace polk::core;
using namespace polk::collections;

static CLog* g_log = nullptr;

#include "../TestFramework.h"

static void HashMap_ctor_Failure()
{
    Auto<CHashMap<int, int> > map (new CHashMap<int, int>(-4));
}

static bool HashMap_GetSetEnumerator()
{
    Auto<CHashMap<int, int> > map (new CHashMap<int, int>());
    for(int i = 0; i < 100; i++) {
        map->Set(i, i * 2);
    }

    SHashMapEnumerator<int, int> mapEnum (map);
    int k;
    int v;
    while(mapEnum.MoveNext(&k, &v)) {
        if(k * 2 != v) {
            return false;
        }
    }

    return true;
}

static bool HashMap_Remove1()
{
    Auto<CHashMap<int, int> > map (new CHashMap<int, int>());
    map->Set(5, 4);
    if(!map->Remove(5))
        return false;
    return map->Size() == 0;
}

static bool HashMap_TryGet1()
{
    Auto<CHashMap<int, int> > map (new CHashMap<int, int>());
    map->Set(666, 15);
    int v;
    if(!map->TryGet(666, &v))
        return false;
    return v == 15;
}

static bool HashMap_TryGet2()
{
    Auto<CHashMap<int, int> > map (new CHashMap<int, int>());
    map->Set(666, 15);
    int v;
    return !map->TryGet(667, &v);
}

static bool HashMap_Contains1()
{
    Auto<CHashMap<int, int> > map (new CHashMap<int, int>());
    map->Set(20, 21);
    return map->Contains(20);
}

static bool HashMap_Contains2()
{
    Auto<CHashMap<int, int> > map (new CHashMap<int, int>());
    map->Set(20, 21);
    return !map->Contains(21);
}

static bool HashMap_Clear()
{
    Auto<CHashMap<int, int> > map (new CHashMap<int, int>());
    map->Set(20, 21);
    map->Set(-34, 56);
    map->Clear();
    return map->Size() == 0;
}

static bool HashMap_Import()
{
    Auto<CHashMap<int, int> > map1 (new CHashMap<int, int>());
    Auto<CHashMap<int, int> > map2 (new CHashMap<int, int>());
    map1->Set(20, 21);
    map1->Set(-34, 56);
    map1->Import(map2);
    return map1->Contains(20) && map1->Contains(-34);
}

static bool HashMap_big()
{
    Auto<CHashMap<int, int> > map (new CHashMap<int, int>());
    for(int i = 0; i < 100000; i++) {
        map->Set(i, i);
    }
    return (map->Size() == 100000) && (map->Item(100) + map->Item(10000) == 100 + 10000);
}

static bool HashMap_big2()
{
    Auto<CHashMap<int, int> > map (new CHashMap<int, int>());

    for(int k = 0; k < 5; k++) {
        for(int i = 0; i < 100000; i++) {
            map->Set(i, i);
        }

        for(int i = 0; i < 100000; i++) {
            
            int z = 0;
            if(!map->TryGet(i, &z)) {
                return false;
            }

            if(i != z)
                return false;

            if(!map->Contains(i))
                return false;

            bool success = map->Remove(i);
            if(!success)
                return false;

            if(map->Contains(i))
                return false;
        }
    }

    return map->Size() == 0;
}

static bool HashMap_Size()
{
    Auto<CHashMap<int, int> > map (new CHashMap<int, int>());
    for(int i = 0; i < 100; i++) {
        map->Set(i, i);
        if(map->Size() != i + 1)
            return false;
    }
    map->Remove(0);
    return map->Size() == 99;
}

static bool LinkedList_AddSize()
{
    Auto<CLinkedList<int> > list (new CLinkedList<int>());
    list->Add(0);
    if(list->Count() != 1)
        return false;
    list->Add(2);
    if(list->Count() != 2)
        return false;
    return true;
}

static bool LinkedList_FirstLastNode()
{
    Auto<CLinkedList<int> > list (new CLinkedList<int>());
    list->Add(13);
    if(!list->FirstNode())
        return false;
    if(list->FirstNode()->Value != 13)
        return false;
    if(list->FirstNode() != list->LastNode())
        return false;
    list->Add(666);
    if(list->LastNode()->Value != 666)
        return false;
    return list->FirstNode() != list->LastNode();
}

static bool LinkedList_FindNode()
{
    Auto<CLinkedList<int> > list (new CLinkedList<int>());
    list->Add(13);
    list->Add(14);
    list->Add(15);
    list->Add(16);
    auto node = list->FindNode(15);
    if(!node)
        return false;
    return node->Value == 15;
}

static void LinkedList_Add_Failure()
{
    Auto<CLinkedList<int> > list (new CLinkedList<int>());
    list->Add(13);
    list->Add(14);
    list->Add(15);
    SLinkedListEnumerator<int> listEnum (list);
    int v;
    while(listEnum.MoveNext(&v)) {
        list->Add(16);
    }
}

static bool LinkedList_InsertAfter1()
{
    Auto<CLinkedList<int> > list (new CLinkedList<int>());
    list->Add(13);  
    list->InsertAfter(list->LastNode(), 14);
    return list->LastNode()->Value == 14;
}

static bool LinkedList_InsertAfter2()
{
    Auto<CLinkedList<int> > list (new CLinkedList<int>());
    list->Add(13);  
    list->InsertAfter(list->FirstNode(), 14);
    return list->FirstNode()->Next->Value == 14;
}

static bool LinkedList_InsertBefore1()
{
    Auto<CLinkedList<int> > list (new CLinkedList<int>());
    list->Add(13);  
    list->InsertBefore(list->FirstNode(), 14);
    return list->FirstNode()->Value == 14;
}

static bool LinkedList_InsertBefore2()
{
    Auto<CLinkedList<int> > list (new CLinkedList<int>());
    list->Add(13);  
    list->InsertBefore(list->LastNode(), 14);
    return list->LastNode()->Prev->Value == 14;
}

static bool LinkedList_Remove1()
{
    Auto<CLinkedList<int> > list (new CLinkedList<int>());
    list->Add(13);
    if(!list->Remove(13))
        return false;
    return list->Count() == 0;
}

static bool LinkedList_Remove2()
{
    Auto<CLinkedList<int> > list (new CLinkedList<int>());
    list->Add(13);
    return !list->Remove(14);
}

static bool LinkedList_Remove3()
{
    Auto<CLinkedList<int> > list (new CLinkedList<int>());
    list->Add(1);
    list->Add(2);
    list->Add(3);
    auto node = list->FindNode(2);
    if(!node)
        return false;
    list->Remove(node);
    return list->Count() == 2;
}

static bool LinkedList_enumerator()
{
    Auto<CLinkedList<int> > list (new CLinkedList<int>());
    int sum1 = 0;
    for(int i = 0; i < 10; i++) {
        list->Add(i);
        sum1 += i;
    }
    SLinkedListEnumerator<int> listEnum (list);
    int v;
    int sum2 = 0;
    while(listEnum.MoveNext(&v)) {
        sum2 += v;
    }
    return sum1 == sum2;
}

static void Queue_ctor_Failure()
{
    Auto<CQueue<int> > queue (new CQueue<int>(-5));
}

static bool Queue_EnqueueDequeueCount()
{
    Auto<CQueue<int> > queue (new CQueue<int>(5));
    queue->Enqueue(5);
    queue->Enqueue(10);
    queue->Enqueue(15);
    if(queue->Dequeue() != 5)
        return false;
    if(queue->Count() != 2)
        return false;
    if(queue->Dequeue() != 10)
        return false;
    if(queue->Count() != 1)
        return false;
    if(queue->Dequeue() != 15)
        return false;
    if(!queue->IsEmpty())
        return false;
    return true;
}

static bool Queue_Peek()
{
    Auto<CQueue<int> > queue (new CQueue<int>(5));
    queue->Enqueue(5);
    queue->Enqueue(10);
    if(queue->Count() != 2)
        return false;
    if(queue->Peek() != 5)
        return false;
    return queue->Count() == 2;
}

static bool Queue_Clear()
{
    Auto<CQueue<int> > queue (new CQueue<int>(5));
    queue->Enqueue(5);
    queue->Enqueue(10);
    queue->Clear();
    return queue->Count() == 0;
}

static bool Queue_big()
{
    Auto<CQueue<int> > queue (new CQueue<int>());
    for(int i = 0; i < 10000; i++) {
        queue->Enqueue(i);
    }
    for(int i = 0; i < 10000; i++) {
        if(queue->Dequeue() != i)
            return false;
    }
    return true;
}

static bool Queue_enumerator()
{
    Auto<CQueue<int> > queue (new CQueue<int>());
    int sum1 = 0;
    for(int i = 0; i < 10; i++) {
        queue->Enqueue(i);
        sum1 += i;
    }
    int sum2 = 0;
    SQueueEnumerator<int> queueEnum (queue);
    int value;
    while(queueEnum.MoveNext(&value)) {
        sum2 += value;
    }
    return sum1 == sum2;
}

static void Queue_Dequeue_Failure()
{
    Auto<CQueue<int> > queue (new CQueue<int>());
    queue->Enqueue(1);
    queue->Dequeue();
    queue->Dequeue();
}

static void ArrayList_ctor_Failure()
{
    Auto<CArrayList<int> > list (new CArrayList<int>(-5));
}

static bool ArrayList_Contains1()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    return list->Contains(10);
}

static bool ArrayList_Contains2()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    return !list->Contains(11);
}

static bool ArrayList_Clone()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    Auto<CArrayList<int> > clone (list->Clone());
    if(clone->Count() != 2)
        return false;
    if(clone->Item(0) != 1)
        return false;
    if(clone->Item(1) != 10)
        return false;
    return true;
}

static bool ArrayList_Clear()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    for(int i = 0; i < 10; i++) {
        list->Add(i);
    }
    if(list->Count() != 10)
        return false;
    list->Clear();
    return list->Count() == 0;
}

static bool ArrayList_SetItem()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    for(int i = 0; i < 10; i++) {
        list->Add(i);
    }
    list->Set(5, 6);
    return list->Item(5) == 6;
}

static void ArrayList_Set_Failure1()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Set(-5, 5);   
}

static void ArrayList_Set_Failure2()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Set(2, 5);   
}

static bool ArrayList_Insert1()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    list->Insert(1, 2);
    return list->Item(1) == 2;
}

static bool ArrayList_Insert2()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    list->Insert(3, 2);
    if(list->Item(2) != 100)
        return false;
    if(list->Item(3) != 2)
        return false;
    return true;
}

static void ArrayList_Insert_Failure1()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    list->Insert(-1, 2);
}

static void ArrayList_Insert_Failure2()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    list->Insert(4, 2);
}

static bool ArrayList_AddRange()
{
    Auto<CArrayList<int> > list1 (new CArrayList<int>());
    Auto<CArrayList<int> > list2 (new CArrayList<int>());
    list1->Add(1);
    list1->Add(10);
    list1->Add(100);
    list2->Add(1000);
    list1->AddRange(list2);
    return list1->Count() == 4 && list1->Item(1) == 10 && list1->Item(3) == 1000;
}

static bool ArrayList_AddUnsafeRange1()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);

    list->AddUnsafeRange(nullptr, 0);

    return list->Count() == 3;
}

static bool ArrayList_AddUnsafeRange2()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);

    int buf[3] = {
        1000,
        10000,
        100000
    };

    list->AddUnsafeRange(&buf[0], 3);

    if(list->Count() != 6)
        return false;
    if(list->Item(3) != 1000)
        return false;
    if(list->Item(4) != 10000)
        return false;
    if(list->Item(5) != 100000)
        return false;

    return true;
}

static void ArrayList_AddUnsafeRange3_Failure()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);

    int buf[3] = {
        1000,
        10000,
        1000000
    };

    list->AddUnsafeRange(&buf[0], -1);
}

static bool ArrayList_Array()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    int* r = list->Array();
    if(r[0] != 1)
        return false;
    if(r[1] != 10)
        return false;
    if(r[2] != 100)
        return false;
    return true;
}

static bool ArrayList_RemoveAt1()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    if(!list->RemoveAt(1))
        return false;
    if(list->Count() != 2)
        return false;
    return list->Item(1) == 100;
}

static bool ArrayList_RemoveAt2()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    return !list->RemoveAt(-1);
}

static bool ArrayList_Remove1()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    if(!list->Remove(10))
        return false;
    if(list->Count() != 2)
        return false;
    return list->Item(1) == 100;
}

static bool ArrayList_Remove2()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    return !list->Remove(567);
}

static bool ArrayList_Expand()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Expand(10, 2);
    if(list->Count() != 11)
        return false;
    int sum1 = 1;
    for(int i = 0; i < 10; i++) {
        sum1 += 2;
    }
    int sum2 = 0;
    for(int i = 0; i < list->Count(); i++) {
        sum2 += list->Array()[i];
    }
    return sum1 == sum2;
}

static bool ArrayList_FindItem1()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    return list->FindItem(10) == 1;
}

static bool ArrayList_FindItem2()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(1);
    list->Add(10);
    list->Add(100);
    return list->FindItem(11) == -1;
}

static bool ArrayList_Sort()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    list->Add(100);
    list->Add(1);
    list->Add(10);
    list->Sort(CoreUtils::IntCompareFunction);
    if(list->Item(0) != 1)
        return false;
    if(list->Item(1) != 10)
        return false;
    if(list->Item(2) != 100)
        return false;
    return true;
}

static bool ArrayList_enumerator()
{
    Auto<CArrayList<int> > list (new CArrayList<int>());
    int sum1 = 0;
    for(int i = 0; i < 10; i++) {
        list->Add(i);
        sum1 += i;
    }
    int sum2 = 0;
    SArrayListEnumerator<int> listEnum (list);
    int value;
    while(listEnum.MoveNext(&value)) {
        sum2 += value;
    }
    return sum1 == sum2;
}

static void Stack_ctor_Failure()
{
    Auto<CStack<int> > stack (new CStack<int>(-1));
}

static bool Stack_PushCount()
{
    Auto<CStack<int> > stack (new CStack<int>(10));
    if(stack->Count() != 0)
        return false;
    stack->Push(0);
    if(stack->Count() != 1)
        return false;
    return true;
}

static bool Stack_PopIsEmpty()
{
    Auto<CStack<int> > stack (new CStack<int>(10));
    stack->Push(1);
    stack->Push(10);
    stack->Push(100);
    if(stack->Pop() != 100)
        return false;
    if(stack->Pop() != 10)
        return false;
    if(stack->Pop() != 1)
        return false;
    return stack->IsEmpty();
}

static bool Stack_Peek()
{
    Auto<CStack<int> > stack (new CStack<int>());
    stack->Push(1);
    stack->Push(10);
    if(stack->Count() != 2)
        return false;
    if(stack->Peek() != 10)
        return false;
    if(stack->Count() != 2)
        return false;    
    return true;
}

static void Stack_Pop_Failure()
{
    Auto<CStack<int> > stack (new CStack<int>());
    stack->Push(1);
    stack->Pop();
    stack->Pop();
}

static bool Stack_Clear()
{
    Auto<CStack<int> > stack (new CStack<int>());
    stack->Push(1);
    stack->Push(7);
    stack->Clear();
    return stack->Count() == 0;
}

// TODO OrderedHashMap
// TODO LruCache

static void performTests()
{
    // ***********
    //   HashMap
    // ***********

    TEST_FAILURE(HashMap_ctor_Failure)
    TEST(HashMap_GetSetEnumerator)
    TEST(HashMap_Remove1)
    TEST(HashMap_TryGet1)
    TEST(HashMap_TryGet2)
    TEST(HashMap_Contains1)
    TEST(HashMap_Contains2)
    TEST(HashMap_Clear)
    TEST(HashMap_Import)
    TEST(HashMap_big)
    TEST(HashMap_big2)
    TEST(HashMap_Size)

    // **************
    //   LinkedList
    // **************

    TEST(LinkedList_AddSize)
    TEST(LinkedList_FirstLastNode)
    TEST(LinkedList_FindNode)
    TEST_FAILURE(LinkedList_Add_Failure)
    TEST(LinkedList_InsertAfter1)
    TEST(LinkedList_InsertAfter2)
    TEST(LinkedList_InsertBefore1)
    TEST(LinkedList_InsertBefore2)
    TEST(LinkedList_Remove1)
    TEST(LinkedList_Remove2)
    TEST(LinkedList_Remove3)
    TEST(LinkedList_enumerator)

    // ***********
    //    Queue
    // ***********

    TEST_FAILURE(Queue_ctor_Failure)
    TEST(Queue_EnqueueDequeueCount)
    TEST(Queue_Peek)
    TEST(Queue_Clear)
    TEST(Queue_big)
    TEST(Queue_enumerator)
    TEST_FAILURE(Queue_Dequeue_Failure)

    // ***********
    //  ArrayList
    // ***********

    TEST_FAILURE(ArrayList_ctor_Failure)
    TEST(ArrayList_Contains1)
    TEST(ArrayList_Contains2)
    TEST(ArrayList_Clone)
    TEST(ArrayList_Clear)
    TEST(ArrayList_SetItem)
    TEST_FAILURE(ArrayList_Set_Failure1)
    TEST_FAILURE(ArrayList_Set_Failure2)
    TEST(ArrayList_Insert1)
    TEST(ArrayList_Insert2)
    TEST_FAILURE(ArrayList_Insert_Failure1)
    TEST_FAILURE(ArrayList_Insert_Failure2)
    TEST(ArrayList_AddRange)
    TEST(ArrayList_AddUnsafeRange1)
    TEST(ArrayList_AddUnsafeRange2)
    TEST_FAILURE(ArrayList_AddUnsafeRange3_Failure)
    TEST(ArrayList_Array)
    TEST(ArrayList_RemoveAt1)
    TEST(ArrayList_RemoveAt2)
    TEST(ArrayList_Remove1)
    TEST(ArrayList_Remove2)
    TEST(ArrayList_Expand)
    TEST(ArrayList_FindItem1)
    TEST(ArrayList_FindItem2)
    TEST(ArrayList_Sort)
    TEST(ArrayList_enumerator)

    // **************
    //     Stack
    // **************

    TEST_FAILURE(Stack_ctor_Failure)
    TEST(Stack_PushCount)
    TEST(Stack_PopIsEmpty)
    TEST(Stack_Peek)
    TEST_FAILURE(Stack_Pop_Failure)
    TEST(Stack_Clear)
}

// *****************************************************************************
// *****************************************************************************

int main()
{
    InitCore();

    g_log = new CLog();
    Auto<const CString> logPath (CString::FromUtf8("Collections.log"));
    Auto<CLogHandler> logHandler (CLogHandler::CreateFromFile(logPath));
    g_log->AddLogHandler(E_LOGPRIORITY_ALL, "Core", logHandler);
    logHandler.SetPtr(CLogHandler::CreateForConsole());
    g_log->AddLogHandler(E_LOGPRIORITY_ALL, "Core", logHandler);

    performTests();

    g_log->Unref();
    g_log = nullptr;

    DeinitCore();
    return 0;
}
