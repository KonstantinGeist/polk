How to run the tests.
*Disclaimer:* works only on Windows so far.

1. Compile TestRunner by calling one of the cmake_build_XXX files.
2. cd ./Bin
3. Launch the TestRunner binary.
4. The rest should happen automatically. It may take a while.

Expects CMake/GCC to be installed & configured.