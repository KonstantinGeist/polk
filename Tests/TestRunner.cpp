// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/core.h>
#include <core/Application.h>
#include <core/Console.h>
#include <core/CoreUtils.h>
#include <core/String.h>
#include <core/Thread.h>
#include <collections/ArrayList.h>
#include <io/FileSystem.h>
#include <io/FileUtils.h>
#include <io/Path.h>

// IMPORTANT Expects CMAKE & Co. to be installed.
// TODO Windows-specific.

using namespace polk::core;
using namespace polk::collections;
using namespace polk::io;

int main()
{
    InitCore();

    // *********************************************************
    //   Looks for test directories and compiles test suites.
    // *********************************************************

    Auto<const CString> curDir (FileSystem::GetCurrentDirectory());
    Auto<const CString> parentDir (Path::GetParent(curDir));

    Auto<const CString> cmd (CString::FromUtf8("cmd.exe"));
    Auto<const CString> cmdArgFormat (CString::FromUtf8("/c \"%o/cmake_build_win64.bat\""));

    Auto<CArrayList<const CString*> > shortPathDirs (FileSystem::ListDirectories(parentDir, false));
    for(int i = 0; i < shortPathDirs->Count(); i++) {
        const CString* shortPathDir = shortPathDirs->Array()[i];

        if(shortPathDir->EqualsASCII("Bin"))
            continue;
        if(shortPathDir->StartsWithASCII("tmp_"))
            continue;

        Auto<const CString> fullPathDir (Path::Combine(parentDir, shortPathDir));
        Auto<const CString> cmdArg (CString::Format(cmdArgFormat, static_cast<const CString*>(fullPathDir.Ptr())));

        try {

            FileSystem::SetCurrentDirectory(fullPathDir);

            SLaunchOptions launchOptions;
            launchOptions.InheritConsole = true;
            launchOptions.WaitForExit = true;
            Application::Launch(cmd, cmdArg, launchOptions);

        } catch (const SException& e) {
            Utf8Auto cShortPathDir (shortPathDir->ToUtf8());
            printf("Error compiling '%s' (error code: %d).\n", cShortPathDir.Ptr(), (int)e.Code());
            exit(1);
        }

        Auto<const CString> inputBin (CString::Format("%o/tmp_build_mingw64d/Test_%o.exe",
                                                      static_cast<const CString*>(fullPathDir.Ptr()),
                                                      static_cast<const CString*>(shortPathDir)));
        Auto<const CString> outputBin (CString::Format("%o/Test_%o.exe",
                                                       static_cast<const CString*>(curDir.Ptr()),
                                                       static_cast<const CString*>(shortPathDir)));

		try {
			FileSystem::CopyFile(inputBin, outputBin);
		} catch (const SException& e) {
            Utf8Auto cInputBin (inputBin->ToUtf8());
            printf("Error copying '%s' (error code: %d).\n", cInputBin.Ptr(), (int)e.Code());
            exit(1);
		}
    }

    // *****************************
    //    Invokes the test suites.
    // *****************************
    
    FileSystem::SetCurrentDirectory(curDir);

    Auto<CArrayList<const CString*> > shortPaths (FileSystem::ListFiles(curDir, false));
    for(int i = 0; i < shortPaths->Count(); i++) {
        const CString* shortPathExe = shortPaths->Array()[i];
        if(!shortPathExe->EndsWithASCII(".exe"))
            continue;
        if(!shortPathExe->StartsWithASCII("Test_"))
            continue;

        SLaunchOptions launchOptions;
        launchOptions.InheritConsole = true;
        launchOptions.WaitForExit = true;
        Application::Launch(shortPathExe, nullptr, launchOptions);
    }

    // *******************************************
    //    Parses log files to check what failed.
    // *******************************************
    
    CThread::Sleep(500); // just in case
    
    Auto<const CString> failedStr (CString::FromUtf8("failed"));
    Auto<const CString> passedStr (CString::FromUtf8("passed"));

    Auto<CArrayList<const CString*> > failedList (new CArrayList<const CString*>());
    int passedCount = 0;

    shortPaths.SetPtr(FileSystem::ListFiles(curDir, false));
    for(int i = 0; i < shortPaths->Count(); i++) {
        const CString* shortPathLog = shortPaths->Array()[i];
        if(!shortPathLog->EndsWithASCII(".log"))
            continue;

        Auto<CArrayList<const CString*> > lines (FileUtils::ReadAllLines(shortPathLog));
        for(int j = 0; j < lines->Count(); j++) {
            const CString* line = lines->Array()[j];

            if(line->FindSubstring(failedStr) != -1) {
                failedList->Add(line);
            } else if(line->FindSubstring(passedStr) != -1) {
                passedCount++;
            }
        }
    }

    // *******************************************
    //       Conclusion.
    // *******************************************

    printf("\n\n\n");

    printf("Tests passed: %d.\n", passedCount);
    printf("Tests failed: %d.\n", failedList->Count());
    Console::SetForeColor(E_CONSOLECOLOR_RED);
    for(int i = 0; i < failedList->Count(); i++) {
        Console::WriteLine(failedList->Array()[i]);
    }

    printf("\nPress ENTER to continue.\n");
    Auto<const CString> dummyInput (Console::ReadLine());

    DeinitCore();
    return 0;
}
