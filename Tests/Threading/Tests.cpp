// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/core.h>
#include <core/Application.h>
#include <core/CoreUtils.h>
#include <core/Log.h>
#include <core/Mutex.h>
#include <core/Random.h>
#include <core/String.h>
#include <core/Thread.h>
#include <core/WaitObject.h>
#include <collections/HashMap.h>
#include <collections/Queue.h>

using namespace polk::core;
using namespace polk::collections;

static CLog* g_log = nullptr;

#include "../TestFramework.h"

static bool Thread_StartAndJoin()
{
    static polk_atomic_int g_value1;
    CoreUtils::AtomicWrite(&g_value1, 0);

    class CTestThread1 final: public CThread
    {
    protected:
        virtual void OnStart()
        {
            CoreUtils::AtomicWrite(&g_value1, 153);
        }
    };

    Auto<CThread> thread (new CTestThread1());
    thread->Start();
    CThread::Join(thread);

    return CoreUtils::AtomicRead(&g_value1) == 153;
}

static bool Thread_Abort()
{
    class CTestThread2 final: public CThread
    {
    protected:
        virtual void OnStart() override
        {
            while(this->State() != E_THREADSTATE_ABORT_REQUESTED) { }
        }
    };

    Auto<CThread> thread (new CTestThread2());
    thread->Start();
    thread->Abort();
    CThread::Join(thread);

    return true;
}

// Several threads simultanously try to write to buf under a mutex.
static bool Mutex_test()
{
    Auto<CMutex> mutex (new CMutex());
    static char buf[4028*16];
    memset(buf, 13, sizeof(buf));

    class CTestThread3 final: public CThread
    {
    private:
        Auto<CMutex> m_mutex;
        int m_value;

    public:
        CTestThread3(CMutex* mutex, int value)
            : m_value(value)
        {
            m_mutex.SetVal(mutex);
        }

    protected:
        virtual void OnStart() override
        {
            // TODO is this correct?
            std::atomic_thread_fence(std::memory_order_acquire);

            while(this->State() != E_THREADSTATE_ABORT_REQUESTED) {
                POLK_LOCK(m_mutex)
                {
                    memset(buf, m_value, sizeof(buf));
                }
                POLK_END_LOCK(m_mutex);
            }
            
            // TODO is this correct?
            std::atomic_thread_fence(std::memory_order_release);
        }
    };

    Auto<CThread> thread1 (new CTestThread3(mutex, 0));
    Auto<CThread> thread2 (new CTestThread3(mutex, 1));
    Auto<CThread> thread3 (new CTestThread3(mutex, 2));
    Auto<CThread> thread4 (new CTestThread3(mutex, 3));

    thread1->Start();
    thread2->Start();
    thread3->Start();
    thread4->Start();

    CThread::Sleep(3000);

    thread1->Abort();
    thread2->Abort();
    thread3->Abort();
    thread4->Abort();
    CThread::Join(thread1);
    CThread::Join(thread2);
    CThread::Join(thread3);
    CThread::Join(thread4);

    std::atomic_thread_fence(std::memory_order_acquire);

    int sum = 0;
    for(size_t i = 0; i < sizeof(buf); i++) {
        sum += buf[i];
    }

    return sum == 0 || sum == sizeof(buf) || sum == sizeof(buf) * 2 || sum == sizeof(buf) * 3;
}

static bool WaitObject_test()
{
    #define WAITOBJECTTEST_CYCLECOUNT 3000000
    
    class CWorkerThread final: public CThread
    {
    private:
        Auto<CWaitObject> m_waitObject;
        Auto<CMutex> m_mutex;
        Auto<CQueue<const CString*> > m_queue;

    public:
        int Sum;

    protected:
        virtual void OnStart() override
        {
            bool iterate = true;
            
            while(iterate) {
                CThread::Wait(m_waitObject, 100);

                if(!m_queue->IsEmpty()) {

                    POLK_LOCK(m_mutex)
                    {
                        while(!m_queue->IsEmpty()) {
                            Auto<const CString> str (m_queue->Dequeue());
                            int i;
                            if(str->TryParseInt(&i)) {
                                this->Sum += i;

                                if(i == WAITOBJECTTEST_CYCLECOUNT)
                                    iterate = false;
                            }
                        }
                    }
                    POLK_END_LOCK(m_mutex);

                }
            }
        }

    public:
        CWorkerThread()
            : m_waitObject(new CWaitObject()),
              m_mutex(new CMutex()),
              m_queue(new CQueue<const CString*>()),
              Sum(0)
        {
        }

        void Enqueue(const CString* str)
        {
            POLK_LOCK(m_mutex)
            {
                Auto<const CString> clone (str->Clone());
                m_queue->Enqueue(clone);
            }
            POLK_END_LOCK(m_mutex);

            m_waitObject->Pulse();
        }
    };

    Auto<CWorkerThread> thread (new CWorkerThread());
    thread->Start();

    for(int i = 0; i <= WAITOBJECTTEST_CYCLECOUNT; i++) {
        Auto<const CString> str (CoreUtils::IntToString(i));
        thread->Enqueue(str);
    }

    int sum = 0;
    for(int i = 0; i <= WAITOBJECTTEST_CYCLECOUNT; i++) {
        sum += i;
    }
    
    CThread::Join(thread);

    return sum == thread->Sum;
}

static bool HashMapMutex_test()
{
    Auto<CHashMap<int, int> > map (new CHashMap<int, int>());
    Auto<CRandom> random (new CRandom());
    Auto<CMutex> mutex (new CMutex());

    class CWorkerThread final: public CThread
    {
    private:
        CHashMap<int, int>* m_map;
        CRandom* m_random;
        CMutex* m_mutex;
        bool m_isWriter;
        bool m_threwException;

        void write()
        {
            int v;

            POLK_LOCK(m_mutex)
            {
                v = m_random->NextInt(0, 50);
            }
            POLK_END_LOCK(m_mutex);

            POLK_LOCK(m_mutex)
            {
                m_map->Set(v, v);
            }
            POLK_END_LOCK(m_mutex);
        }

        void read()
        {
            int v;

            POLK_LOCK(m_mutex)
            {
                v = m_random->NextInt(0, 50);
            }
            POLK_END_LOCK(m_mutex);

            POLK_LOCK(m_mutex)
            {
                int v2;
                if(m_map->TryGet(v, &v2)) {
                    if(v != v2) {
                        POLK_THROW(EC_CONTRACT_UNSATISFIED);
                    }
                }

                m_map->Remove(v);
            }
            POLK_END_LOCK(m_mutex);
        }

    public:
        CWorkerThread(CHashMap<int, int>* map, CRandom* random, CMutex* mutex, bool isWriter)
            : m_map(map), m_random(random), m_mutex(mutex), m_isWriter(isWriter),
              m_threwException(false)
        {
        }

        bool ThrewException() const
        {
            return m_threwException;
        }

    protected:
        virtual void OnStart() override
        {
            try {

                if(m_isWriter) {
                    while(this->State() != E_THREADSTATE_ABORT_REQUESTED) {
                        write();
                    }
                } else {
                    while(this->State() != E_THREADSTATE_ABORT_REQUESTED) {
                        read();
                    }                    
                }

            } catch (const SException& e) {
                m_threwException = true;
            }
        }
    };

    Auto<CWorkerThread> thread1 (new CWorkerThread(map, random, mutex, false));
    Auto<CWorkerThread> thread2 (new CWorkerThread(map, random, mutex, true));

    thread1->Start();
    thread2->Start();

    // Gives the threads some time.
    volatile polk_long startTicks = Application::TickCount();
    while(Application::TickCount() - startTicks < 10000) {
    }

    thread1->Abort();
    thread2->Abort();
    CThread::Join(thread1);
    CThread::Join(thread2);

    return !thread1->ThrewException() && !thread2->ThrewException();
}

static bool TestMemorySafety()
{   
    class CWorkerThread final: public CThread
    {
    protected:
        virtual void OnStart() override
        {
            Auto<CRandom> random (new CRandom());

            for(int i = 0; i < 10000000; i++) {
                volatile auto tmp = new int[random->NextInt(1, 256)];
                delete [] tmp;
            }
        }
    };

    Auto<CWorkerThread> thread1 (new CWorkerThread());
    Auto<CWorkerThread> thread2 (new CWorkerThread());

    thread1->Start();
    thread2->Start();

    CThread::Join(thread1);
    CThread::Join(thread2);

    return true;
}

// ****************************************************************

static void performTests()
{
    TEST(TestMemorySafety)
    TEST(Thread_StartAndJoin)
    TEST(Thread_Abort)
    TEST(Mutex_test)
    TEST(WaitObject_test)
    TEST(HashMapMutex_test)
    testFailure("hello", nullptr);
}

// *****************************************************************************
// *****************************************************************************

int main()
{
    InitCore();

    g_log = new CLog();
    Auto<const CString> logPath (CString::FromUtf8("Threading.log"));
    Auto<CLogHandler> logHandler (CLogHandler::CreateFromFile(logPath));
    g_log->AddLogHandler(E_LOGPRIORITY_ALL, "Core", logHandler);
    logHandler.SetPtr(CLogHandler::CreateForConsole());
    g_log->AddLogHandler(E_LOGPRIORITY_ALL, "Core", logHandler);

    performTests();

    g_log->Unref();
    g_log = nullptr;

    DeinitCore();
    return 0;
}
