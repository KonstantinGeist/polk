// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

// To be included by Tests.cpp in respective directories.

// Should return true if passed, false if not.
typedef bool(*TestFunc)();
typedef void(*TestFailureFunc)();

static void test(const char* name, TestFunc testFunc)
{
    if(!testFunc)
        return;

    try {

        if(testFunc()) {
            POLK_LOG_INFO(g_log, "Core", "Test '%s' passed.", name);
        } else {
            POLK_LOG_ERROR(g_log, "Core", "Test '%s' failed.", name);
        }

    } catch (const SException& e) {
        POLK_LOG_ERROR(g_log, "Core", "Test '%s' failed with exception code %d.", name, (int)e.Code());
    }
}

static void testFailure(const char* name, TestFailureFunc testFunc)
{
    if(!testFunc)
        return;

    try {

        testFunc();
        POLK_LOG_ERROR(g_log, "Core", "Test '%s' failed.", name);

    } catch (const SException& e) {
        POLK_LOG_INFO(g_log, "Core", "Test '%s' passed.", name);
    }
}

#define TEST(x) test(#x, x);
#define TEST_FAILURE(x) testFailure(#x, x);
