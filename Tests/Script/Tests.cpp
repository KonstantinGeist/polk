// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/core.h>
#include <core/Console.h>
#include <core/CoreUtils.h>
#include <core/Log.h>
#include <core/String.h>
#include <reflection/IdTable.h>
#include <io/BinaryReader.h>
#include <io/BinaryWriter.h>
#include <io/MemoryStream.h>
#include <script/Executable.h>
#include <script/ExecutionTrack.h>
#include <script/script.h>
#include <script/ScriptList.h>
#include <script/ScriptValue.h>
#include <script/StringSlice.h>

using namespace polk::core;
using namespace polk::reflection;
using namespace polk::io;
using namespace polk::script;

static CLog* g_log = nullptr;

static void test(const char* _code, const char* _name, const SScriptValue& expected)
{
    Auto<const CString> code (CString::FromUtf8(_code));
    Auto<const CString> name (CString::FromUtf8(_name));

    Auto<CExecutable> exe (CExecutable::Compile(code, name, g_log));
    if(!exe) {
        POLK_LOG_ERROR(g_log,
                    "Script",
                    "Test '%s' failed with syntax error.",
                    _name);
        return;
    }

    /*Auto<const CString> dis (exe->Disassemble());
    Console::WriteLine(dis);*/

    Auto<CExecutionTrack> exeTrack (new CExecutionTrack(exe));

    try {

        while(exeTrack->Resume()) { }
        
        if(expected.Equals(exeTrack->ReturnValue())) {

            Console::SetForeColor(E_CONSOLECOLOR_YELLOW);
            POLK_LOG_INFO(g_log,
                       "Script",
                       "Test '%s' passed.",
                       _name);

        } else {

            Console::SetForeColor(E_CONSOLECOLOR_RED);
            
            Auto<const CString> expectedAsStr (expected.ToString());
            Auto<const CString> returnedValueAsStr (exeTrack->ReturnValue().ToString());

            Auto<const CString> msg (CString::Format("Test '%s': expected '%o', got '%o'.",
                                                     _name,
                                                     static_cast<const CObject*>(expectedAsStr.Ptr()),
                                                     static_cast<const CObject*>(returnedValueAsStr.Ptr())));

            Console::WriteLine(msg);

        }

    } catch(const SException& e) {

        Console::SetForeColor(E_CONSOLECOLOR_RED);
        POLK_LOG_ERROR(g_log, "Script",
                    "Test '%s' failed with:\n%s.", _name, e.Message());

    }
}

static void testFailure(const char* _code, const char* _name)
{   
    Auto<const CString> code (CString::FromUtf8(_code));
    Auto<const CString> name (CString::FromUtf8(_name));

    bool exceptionCaught = false;

    try {

        Auto<CExecutable> exe (CExecutable::Compile(code, name, g_log));

        if(exe == nullptr) {
            exceptionCaught = true;
        } else {
            Auto<CExecutionTrack> exeTrack (new CExecutionTrack(exe));
            while(exeTrack->Resume()) { }
        }

    } catch (SException& e) {
        exceptionCaught = true;
    }

    if(exceptionCaught) {
        Console::SetForeColor(E_CONSOLECOLOR_YELLOW);
        POLK_LOG_INFO(g_log, "Script", "%s passed.", _name);
    } else {
        Console::SetForeColor(E_CONSOLECOLOR_RED);
        POLK_LOG_ERROR(g_log, "Script", "%s failed to raise an error.", _name);
    }
}

static void basicReturn()
{
    test("return 13.5", "BasicNumberReturn", SScriptValue::FromNumber(13.5f));
    test("return true", "BasicBoolReturn", SScriptValue::FromBool(true));
    test("return false", "BasicBoolReturn2", SScriptValue::FromBool(false));
    test("return (false or true)", "BasicBoolReturn3", SScriptValue::FromBool(true));
}

static void setVariable()
{
    test(
        "local a = 10\n"
        "local b = (5 + a)\n"
        "return (b / 3)\n"
,"setVariables1", SScriptValue::FromNumber(5));

    test(
        "local a = 1\n"
        "local b = 2\n"
        "local c = 3; local d = 4;\n"
        "local e = 5\n"
        "local f = 6\n"
        "return (((((a + b) + c) + d) + e) + f)\n"
, "setVariables2", SScriptValue::FromNumber(21));
}

static void bools()
{
    test(
        "return (false or true)\n"
, "Bool1", SScriptValue::FromBool(true));

    test(
        "return (false or false)\n"
, "Bool2", SScriptValue::FromBool(false));

    test(
        "return (true or true)\n"
, "Bool3", SScriptValue::FromBool(true));

    test(
        "return (true and true)\n"
, "Bool4", SScriptValue::FromBool(true));

    test(
        "return (true and false)\n"
, "Bool5", SScriptValue::FromBool(false));

    test(
        "return (false and false)\n"
, "Bool6", SScriptValue::FromBool(false));

    test(
        "local a = false\n"
        "local b = true\n"
        "local c = false\n"
        "return ((a or b) and c)\n"
, "Bool7", SScriptValue::FromBool(false));
}

static void math()
{
    test(
        "local a = 1\n"
        "local b = 2\n"
        "return ((((a + b) * 3) / 2) - 4)\n"
, "Math1", SScriptValue::FromNumber(0.5f));

    testFailure(
        "return (0 / 0)\n"
, "DivisionByZero");
}

static void testSyntaxFailures()
{
    testFailure(
        "return (3 / 3\n"
, "UnmatchedParenthesis1.");

    testFailure(
        "return 3 / 3)\n"
, "UnmatchedParenthesis2.");

    testFailure(
        "local a = {\n"
, "UnmatchedCurly1.");

    testFailure(
        "local a = }\n"
, "UnmatchedCurly2.");
}

static void conditionals()
{
    test(
        "local r = 0\n"
        "if { true } {\n"
        "  local r = 13\n"
        "}\n"
        "if { false } {\n"
        "  local r = 666\n"
        "}\n"
        "return r\n"
, "Cond1", SScriptValue::FromNumber(13));

    test(
        "local r = 0\n"
        "if { false } {\n"
        "  local r = 13\n"
        "}\n"
        "if { true } {\n"
        "  local r = 666\n"
        "}\n"
        "return r\n"
, "Cond2", SScriptValue::FromNumber(666));

    test(
        "local a = 0\n"
        "local r = 2;\n"

        "while { a < 5 } {\n"
        "  local r = r * 2\n"
        "  local a = a + 1\n"
        "}\n"

        "return r\n"
, "Cond3", SScriptValue::FromNumber(64));
}

static void callB()
{
    test(
        "local a = 1\n"
        "local b = 2.5\n"
        "local sum = { a + b }\n"
        "return (sum!)\n"
, "CallB", SScriptValue::FromNumber(3.5f));
}

static void args()
{
    testFailure(
        "local sum = {| a b | a + b}\n"
        "local r = (sum! 1 2 3)\n"
, "Arg1");

    test(
        "local sum = {| a b | a + b}\n"
        "return (sum! 1.5 2.5)\n"
, "Arg2", SScriptValue::FromNumber(4));

    test(
        "local sum = {| a b | a + b}\n"
        "local mul = {| a b | a * b}\n"
        "return (mul! (sum! 1.5 2.5) 2)\n"
, "Arg3", SScriptValue::FromNumber(8));

    test(
"local funcOuter = {| arg |\n"

"   local funcInner = {| arg |\n"
"      arg\n"
"   }\n"

"   funcInner! arg\n"
"}\n"
"return (funcOuter! 13.3)\n"

, "Arg4", SScriptValue::FromNumber(13.3f));
    

    // ******************************************

    testFailure(
        "local func = { |arg2 arg2|\n"
        "    arg1 * arg2\n"
        "}\n"

    , "NameCollision1");

    // ******************************************

    test(
        "local func = {|a b| a - b }\n"
        "return (func! 15 3)\n"
, "Arg5", SScriptValue::FromNumber(12));
}

static void misc()
{
    test(
    "    local for = {|a b c|\n"
    "        local for_t = a\n"
    "\n"
    "        while { for_t < b } {\n"
    "            c! for_t\n"
    "\n"
    "            local for_t = for_t + 1\n"
    "        }\n"
    "    }\n"
    "\n"
    "    local apply = {|l f|\n"
    "        for! 0 (l Count) {|t|\n"
    "            l Set t (f! (l Get t))\n"
    "        }\n"
    "    }\n"
    "\n"
    "    local l = new List\n"
    "\n"
    "    for! 0 10 {|t|\n"
    "        l Add t\n"
    "    }\n"
    "\n"
    "    apply! l {|t| t * 2}\n"
    "\n"
    "    l Get 7\n"

    , "Misc1", SScriptValue::FromNumber(14));
}


static void body()
{
                test(
        "local a = 13\n"
        "local body = { local a = a + 1 }\n"

        "body!\n"
        "return a\n"
    , "BodySimple", SScriptValue::FromNumber(14));

                test(
        "local a = 13\n"

        "local bodyOuter = {\n"
            "local bodyInner = {\n"
                "local a = \"Hack!\"\n"
            "}\n"

            "bodyInner!\n"
        "}\n"

        "bodyOuter!\n"
        "return a\n"
    , "BodyLayered", SScriptValue::FromString("Hack!"));

    test("local b = 13; local call = {|a| a! }; { call! { {local b = 666}! } } !; b", "Convoluted", SScriptValue::FromNumber(666));

                test(
        "local f = {|a b c d| a!; b!; c!; d!; }\n"

        "local a = 0\n"
        "f! { local a = a + 1 } { local a = a + 1 } { local a = a + 1 } { local a = a + 1 }\n"
        "return a\n"
    , "BodiesAsArgs1", SScriptValue::FromNumber(4));

                test(
    "local r = (0 - 5)\n"
    "local x = {|a b|\n"
    "    local y = {\n"
    "        local z = {\n"
    "            a! (b * 2)\n"
    "        }\n"
    "        z!\n"
    "    }\n"
    "    y!\n"
    "}\n"
    "x! {|n| local r = n } 3\n"
    "r\n"
    , "BodiesAsArgs2", SScriptValue::FromNumber(6));

                test(

    "local for = {|a b c|\n"
    "    local t = a\n"
    "    while { t < b } {\n"
    "        c! t\n"
    "        local t = t + 1\n"
    "    }\n"
    "}\n"
    "local z = 0\n"
    "for! 0 5 {|t|\n"
    "    local z = z + t\n"
    "}\n"
    "z\n"

    , "BodiesAsArgs3", SScriptValue::FromNumber(10));

                test(

    "local outer = { { 5 } }\n"
    "(outer!) !\n"

    , "ReturnInnerBodyAndCall", SScriptValue::FromNumber(5));

}

static void sleep()
{
    test(

    "local a = 10\n"
    "sleep 1000\n"
    "return a\n"

, "Sleep1", SScriptValue::FromNumber(10));
}

static void strings()
{
    test(
    "local a = \"Hello, \"\n"
    "local b = \"World!\"\n"
    "return ((a Concat b) Equals \"Hello, World!\")\n"
, "stringConcatEqual", SScriptValue::FromBool(true));
}

static void ifElse()
{
test(

    "local a = 10\n"
    "local r = 0;\n"
    "if { a == 10 } {\n"
    "    local r = 1\n"
    "} else {\n"
    "    local r = 2\n"
    "}\n"

    "r\n"

, "IfElse1", SScriptValue::FromNumber(1));

test(

    "local a = 5\n"
    "local r = 0;\n"
    "if { a == 10 } {\n"
    "    local r = 1\n"
    "} else {\n"
    "    local r = 2\n"
    "}\n"

    "r\n"

, "IfElse2", SScriptValue::FromNumber(2));
}

static void testAtomic1()
{
    Auto<const CString> code (CString::FromUtf8(
        "atomic {\n"
            "global a = 1\n"
            "yield\n"
            "global a = 2\n"
        "}\n"

        "yield\n"

));
    Auto<const CString> name (CString::FromUtf8("atomic1"));

    Auto<CExecutable> user (CExecutable::Compile(code, name, g_log));
    Auto<CExecutionTrack> track (new CExecutionTrack(user));

    track->Resume();

    SScriptValue var (track->GetVariable(SStringSlice("a")));

    if(var.Type() == METATYPE_NUMBER && var.NumberValue() == 2.0f) {
        Console::SetForeColor(E_CONSOLECOLOR_YELLOW);
        POLK_LOG_INFO_S(g_log, "Script", "testAtomic1 passed.");
    } else {
        Console::SetForeColor(E_CONSOLECOLOR_RED);
        POLK_LOG_ERROR_S(g_log, "Script", "testAtomic1 failed.");
    }
}

static void testAtomic2()
{
    Auto<const CString> code (CString::FromUtf8(

        "global a = 1\n"
        "yield\n"
        "global a = 2\n"
        "yield\n"

));
    Auto<const CString> name (CString::FromUtf8("atomic2"));

    Auto<CExecutable> user (CExecutable::Compile(code, name, g_log));
    Auto<CExecutionTrack> track (new CExecutionTrack(user));

    track->Resume();

    SScriptValue var (track->GetVariable(SStringSlice("a")));

    if(var.Type() == METATYPE_NUMBER && var.NumberValue() == 1.0f) {
        Console::SetForeColor(E_CONSOLECOLOR_YELLOW);
        POLK_LOG_INFO_S(g_log, "Script", "testAtomic2 passed.");
    } else {
        Console::SetForeColor(E_CONSOLECOLOR_RED);
        POLK_LOG_ERROR_S(g_log, "Script", "testAtomic2 failed.");
    }
}

static void testDoUndo1()
{
    Auto<const CString> code (CString::FromUtf8(

    "global a = 0\n"
    "do {\n"
        "global a = a + 1\n"
        "do {\n"
            "global a = a + 1\n"
            "if { true } {\n"
                "cancel\n"
            "}\n"
            "global a = a + 1\n"
        "} undo {\n"
            "global a = a + 1\n"
        "}\n"
        "global a = a + 1\n"
    "} undo {\n"
        "global a = a + 1\n"
        "yield\n"
    "}\n"

));
    Auto<const CString> name (CString::FromUtf8("testDoUndo1"));

    Auto<CExecutable> user (CExecutable::Compile(code, name, g_log));
    Auto<CExecutionTrack> track (new CExecutionTrack(user));

    track->Resume();

    SScriptValue var (track->GetVariable(SStringSlice("a")));

    if(var.Type() == METATYPE_NUMBER && var.NumberValue() == 4.0f) {
        Console::SetForeColor(E_CONSOLECOLOR_YELLOW);
        POLK_LOG_INFO_S(g_log, "Script", "testDoUndo1 passed.");
    } else {
        Console::SetForeColor(E_CONSOLECOLOR_RED);
        POLK_LOG_ERROR_S(g_log, "Script", "testDoUndo1 failed.");
    }
}

static void testDoUndo2()
{
    Auto<const CString> code (CString::FromUtf8(

    "global a = 0\n"
    "do {\n"
        "global a = a + 1\n"
        "do {\n"
            "global a = a + 1\n"
            "global a = a + 1\n"
        "} undo {\n"
            "global a = a + 1\n"
        "}\n"
        "global a = a + 1\n"
    "} undo {\n"
        "global a = a + 1\n"
        "yield\n"
    "}\n"

));
    Auto<const CString> name (CString::FromUtf8("testDoUndo2"));

    Auto<CExecutable> user (CExecutable::Compile(code, name, g_log));
    Auto<CExecutionTrack> track (new CExecutionTrack(user));

    track->Resume();

    SScriptValue var (track->GetVariable(SStringSlice("a")));

    if(var.Type() == METATYPE_NUMBER && var.NumberValue() == 6.0f) {
        Console::SetForeColor(E_CONSOLECOLOR_YELLOW);
        POLK_LOG_INFO_S(g_log, "Script", "testDoUndo2 passed.");
    } else {
        Console::SetForeColor(E_CONSOLECOLOR_RED);
        POLK_LOG_ERROR_S(g_log, "Script", "testDoUndo2 failed.");
    }
}

static void testDoAtomicGuarantee()
{
    Auto<const CString> code (CString::FromUtf8(

    "global a = 1\n"
    "do {\n"
        "atomic {\n"
            "global a = a + 1\n"
        "}\n"
        "global a = a + 1\n"
    "} undo {\n"
        "global a = a + 1\n"
        "yield\n"
    "}\n"

));
    Auto<const CString> name (CString::FromUtf8("testDoAtomicGuarantee"));

    Auto<CExecutable> user (CExecutable::Compile(code, name, g_log));
    Auto<CExecutionTrack> track (new CExecutionTrack(user));

    track->Cancel();
    track->Resume();

    SScriptValue var (track->GetVariable(SStringSlice("a")));

    if(var.Type() == METATYPE_NUMBER && var.NumberValue() == 3.0f) {
        Console::SetForeColor(E_CONSOLECOLOR_YELLOW);
        POLK_LOG_INFO_S(g_log, "Script", "testDoAtomicGuarantee passed.");
    } else {
        Console::SetForeColor(E_CONSOLECOLOR_RED);
        POLK_LOG_ERROR_S(g_log, "Script", "testDoAtomicGuarantee failed.");
    }
}

static void testScriptValueSerialization()
{
    Auto<const CString> inStr (CString::FromUtf8("Hello, World!"));
    intptr_t idptr = (intptr_t)((const void*)static_cast<const CObject*>(inStr.Ptr()));
    SScriptValue inVal (SScriptValue::FromIdPtr("String", idptr));

    Auto<CIdTable> idTable (CIdTable::CreateInstance());
    Auto<CMemoryStream> memStream (new CMemoryStream());
    SBinaryWriter bw (memStream);
    SBinaryReader br (memStream);

    polk_long offsetPosRef = memStream->GetPosition();
    bw.WriteInt(0); // will be filled later

    inVal.Serialize(memStream, idTable);

    int offsetPos = (int)memStream->GetPosition();
    idTable->Serialize(bw);
    memStream->SetPosition(offsetPosRef);
    bw.WriteInt(offsetPos);

    memStream->SetPosition(0);

    offsetPos = br.ReadInt();
    polk_long oldPos = memStream->GetPosition();
    memStream->SetPosition(offsetPos);

    idTable.SetPtr(CIdTable::Deserialize(br));

    memStream->SetPosition(oldPos);
    SScriptValue outVal (SScriptValue::Deserialize(memStream, idTable));

    Auto<const CString> outStr (outVal.ToString());

    if(inStr->Equals(outStr)) {
        Console::SetForeColor(E_CONSOLECOLOR_YELLOW);
        POLK_LOG_INFO_S(g_log, "Script", "testScriptValueSerialization passed.");
    } else {
        Console::SetForeColor(E_CONSOLECOLOR_RED);
        POLK_LOG_ERROR_S(g_log, "Script", "testScriptValueSerialization failed.");
    }
}

static void testFullTrackSerialization()
{
    Auto<const CString> code (CString::FromUtf8(
        "global l = new List\n"
        "local i = 0\n"
        "while { i < 10 } {\n"
            "l Add (i ToString)\n"
            "yield\n"
            "local i = i + 1\n"
        "}\n"));

    Auto<const CString> name (CString::FromUtf8("testFullTrackSerialization"));

    Auto<CExecutable> exe (CExecutable::Compile(code, name, g_log));
    Auto<CExecutionTrack> track (new CExecutionTrack(exe));

    SStringSlice varName ("l");

    int controlIndex = 0;
    while(track->Resume()) {
        Auto<CMemoryStream> memStream (new CMemoryStream());
        track->Serialize(memStream, g_log);

        memStream->SetPosition(0);

        track.SetPtr(CExecutionTrack::Deserialize(memStream, g_log));

        SScriptValue var (track->GetVariable(varName));
        const CScriptList* const list = var.TryCast<const CScriptList>("List");
        SScriptValue last (list->Item(list->Count() - 1));
        const CString* const str = last.TryCast<const CString>("String");

        Auto<const CString> controlStr (CoreUtils::FloatToString(controlIndex, 0, true));

        if(!controlStr->Equals(str)) {
            Console::SetForeColor(E_CONSOLECOLOR_RED);
            POLK_LOG_ERROR_S(g_log, "Script", "testFullTrackSerialization failed.");
            return;
        }

        controlIndex++;
    }

    Console::SetForeColor(E_CONSOLECOLOR_YELLOW);
    POLK_LOG_INFO_S(g_log, "Script", "testFullTrackSerialization passed.");
}

static void performTests()
{
    basicReturn();
    setVariable();
    bools();
    math();
    testSyntaxFailures();
    conditionals();
    callB();
    strings();
    args();
    misc();
    body();
    ifElse();
    testAtomic1();
    testAtomic2();
    testDoUndo1();
    testDoUndo2();
    testDoAtomicGuarantee();
    sleep();
    testScriptValueSerialization();
    testFullTrackSerialization();
}

int main()
{
    InitCore();
    InitScript();

    g_log = new CLog();
    Auto<const CString> logPath (CString::FromUtf8("Script.log"));
    Auto<CLogHandler> logHandler (CLogHandler::CreateFromFile(logPath));
    g_log->AddLogHandler(E_LOGPRIORITY_ALL, "Script", logHandler);
    logHandler.SetPtr(CLogHandler::CreateForConsole());
    g_log->AddLogHandler(E_LOGPRIORITY_ALL, "Script", logHandler);

    // ************
    performTests();
    // ************

    g_log->Unref();
    g_log = nullptr;

    DeinitScript();
    DeinitCore();

    return 0;
}
