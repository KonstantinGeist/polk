// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/core.h>
#include <core/Application.h>
#include <core/CoreUtils.h>
#include <core/DateTime.h>
#include <core/Log.h>
#include <core/Marshal.h>
#include <core/Random.h>
#include <core/String.h>
#include <core/Thread.h>
#include <core/Variant.h>
#include <reflection/IdTable.h>
#include <reflection/MetaObject.h>
#include <io/MemoryStream.h>

using namespace polk::core;
using namespace polk::reflection;
using namespace polk::io;

static CLog* g_log = nullptr;

#include "../TestFramework.h"

static bool Application_GetExeFileName()
{
    Auto<const CString> exeFileName (Application::GetExeFileName());
    return !CString::IsNullOrEmpty(exeFileName);
}

static bool Application_GetMemoryUsage()
{
    return Application::GetMemoryUsage() >= 0;
}

static bool Application_TickCount()
{
    polk_long tickCount = Application::TickCount();
    CThread::Sleep(100);
    return Application::TickCount() > tickCount;
}

static bool Application_GetPlatformString()
{
    const CString* platformStr;

    platformStr = Application::PlatformString(E_PLATFORMSTRING_NEWLINE);
    if(CString::IsNullOrEmpty(platformStr))
        return false;

    platformStr = Application::PlatformString(E_PLATFORMSTRING_FILE_SEPARATOR);
    if(CString::IsNullOrEmpty(platformStr))
        return false;

    return true;
}

static void Application_GetPlatformString_Failure()
{
    Application::PlatformString((EPlatformString)666);
}

// TODO Application::Launch

static bool Application_GetProcessorCount()
{
    return Application::GetProcessorCount() > 0;
}

// TODO Application::GetCommandLineArgs

static bool Application_GetSpecialFolder()
{
    Auto<const CString> str;

    str.SetPtr(Application::GetSpecialFolder(E_SPECIALFOLDER_APPDATA));
    if(CString::IsNullOrEmpty(str))
        return false;

    str.SetPtr(Application::GetSpecialFolder(E_SPECIALFOLDER_HOME));
    if(CString::IsNullOrEmpty(str))
        return false;

    return true;
}

static void Application_GetSpecialFolder_Failure()
{
    Application::GetSpecialFolder((ESpecialFolder)666);
}

static bool Application_GetOSVersion()
{
    Auto<const CString> osVersion (Application::GetOSVersion());
    return !CString::IsNullOrEmpty(osVersion);
}

static bool CoreUtils_AreObjectsEqual1()
{
    return CoreUtils::AreObjectsEqual(nullptr, nullptr);
}

static bool CoreUtils_AreObjectsEqual2()
{
    Auto<const CString> str1 (CString::FromUtf8("str1"));

    return !CoreUtils::AreObjectsEqual(str1, nullptr);
}

static bool CoreUtils_AreObjectsEqual3()
{
    Auto<const CString> str1 (CString::FromUtf8("str1"));
    Auto<const CString> str2 (CString::FromUtf8("str2"));

    return !CoreUtils::AreObjectsEqual(str1, str2);
}

static bool CoreUtils_AreObjectsEqual4()
{
    Auto<const CString> str1 (CString::FromUtf8("abc"));
    Auto<const CString> str2 (CString::FromUtf8("abc"));

    return CoreUtils::AreObjectsEqual(str1, str2);
}

// TODO CoreUtils::AtomicXXX

static bool CoreUtils_IsDigit()
{
    for(int i = 0; i < 10; i++) {
        if(!CoreUtils::IsDigit(POLK_CHAR('0') + i))
            return false;
    }

    return true;
}

static bool CoreUtils_IsWhiteSpace()
{
    if(!CoreUtils::IsWhiteSpace(POLK_CHAR(' ')))
        return false;
    if(!CoreUtils::IsWhiteSpace(POLK_CHAR('\r')))
        return false;
    if(!CoreUtils::IsWhiteSpace(POLK_CHAR('\t')))
        return false;

    if(CoreUtils::IsWhiteSpace(POLK_CHAR('a')))
        return false;

    return true;
}

static bool CoreUtils_IsLetter()
{
    if(!CoreUtils::IsLetter(POLK_CHAR('a')))
        return false;
    if(!CoreUtils::IsLetter(POLK_CHAR('T')))
        return false;
    if(CoreUtils::IsLetter(POLK_CHAR('\t')))
        return false;

    return true;
}

// TODO CoreUtils::IsControl

static bool CoreUtils_IntToString1()
{
    Auto<const CString> str (CoreUtils::IntToString(123));
    return str->EqualsASCII("123");
}

static bool CoreUtils_IntToString2()
{
    Auto<const CString> str (CoreUtils::IntToString(-321));
    return str->EqualsASCII("-321");
}

static bool CoreUtils_LongToString()
{
    Auto<const CString> str (CoreUtils::LongToString(88888888888888L));
    return str->EqualsASCII("88888888888888");
}

static bool CoreUtils_FloatToString()
{
    Auto<const CString> str (CoreUtils::FloatToString(1.0f, 0, true)); // noTrailingZeros=true
    return str->EqualsASCII("1");
}

static bool CoreUtils_BoolToString1()
{
    Auto<const CString> str (CoreUtils::BoolToString(true));
    return str->EqualsASCII("true");
}

static bool CoreUtils_BoolToString2()
{
    Auto<const CString> str (CoreUtils::BoolToString(false));
    return str->EqualsASCII("false");
}

// TODO CoreUtils::PtrToString(..) ?

static bool CoreUtils_CharToUpperCase()
{
    return CoreUtils::CharToUpperCase('a') == POLK_CHAR('A');
}

static bool CoreUtils_CharToLowerCase()
{
    return CoreUtils::CharToLowerCase('A') == POLK_CHAR('a');
}

static void CoreUtils_ValidatePath_Failure1()
{
    Auto<const CString> path;
    CoreUtils::ValidatePath(path);
}

static void CoreUtils_ValidatePath_Failure3()
{
    Auto<const CString> path (CString::FromUtf8("C:\\Windows"));
    CoreUtils::ValidatePath(path);
}

static void CoreUtils_ValidatePath_Failure4()
{
    Auto<const CString> path (CString::FromUtf8("Windows>1"));
    CoreUtils::ValidatePath(path);
}

static void CoreUtils_ValidatePath_Failure5()
{
    Auto<const CString> path (CString::FromUtf8("Windows|System32"));
    CoreUtils::ValidatePath(path);
}

static void CoreUtils_ValidatePath_Failure6()
{
    Auto<const CString> path (CString::FromUtf8("Windows\aSystem32"));
    CoreUtils::ValidatePath(path);
}

static bool CoreUtils_AllocAligned()
{
    polk_byte* mem = CoreUtils::AllocAligned(200, 32, true); // clear=true
    for(int i = 0; i < 200; i++) {
        if(mem[i]) // 
            return false;
    }

    uintptr_t pMem = (uintptr_t)(void*)mem;
    if(pMem % 32 != 0)
        return false;

    CoreUtils::FreeAligned(mem);

    return true;
}

static bool CoreUtils_MemorySizeToString()
{
    Auto<const CString> str (CoreUtils::MemorySizeToString(123));
    return !CString::IsNullOrEmpty(str);
}

static bool CoreUtils_ByteOrder()
{
    return CoreUtils::ByteOrderNetworkToHost(CoreUtils::ByteOrderHostToNetwork(13200002)) == 13200002;
}

static void DateTime_ctor_Failure()
{
    SDateTime dt ((EDateTimeKind)666, 0, 0, 0, 0, 0, 0, 0);
    dt.GetHashCode();
}

static bool Object_ReferenceCount()
{
    Auto<CObject> obj (new CObject());
    obj->Ref();

    if(obj->ReferenceCount() != 2)
        return false;
    obj->Unref();
    if(obj->ReferenceCount() != 1)
        return false;

    return true;
}

static bool Object_GetHashCode()
{
    Auto<CObject> obj1 (new CObject());
    Auto<CObject> obj2 (new CObject());

    // Not really 100% guaranteed when 64-bit pointers are truncated to 32-bit
    // hash codes, but should be true in practice because the memory allocator
    // most likely allocates memory sequentially in this test suite.
    return obj1->GetHashCode() != obj2->GetHashCode();
}

static bool BoxedStruct_MethodRedirection()
{
    struct TestStruct {
        int Field1;

        TestStruct(int field1)
            : Field1(field1)
        {
            
        }

        int GetHashCode() const {
            return this->Field1;
        }
        
        const CString* ToString() const {
            return CoreUtils::IntToString(this->Field1);
        }
    };

    TestStruct testStruct (666);
    Auto<CBoxedStruct<TestStruct> > b (new CBoxedStruct<TestStruct>(testStruct));
    if(b->GetHashCode() != 666)
        return false;

    Auto<const CString> str (b->ToString());
    if(!str->EqualsASCII("666"))
        return false;

    return true;
}

static bool Random_NextInt()
{
    Auto<CRandom> random (new CRandom());
    for(int i = 0; i < 100; i++) {
        int r = random->NextInt(-5, 8);
        if(!(r >= -5 && r < 8))
            return false;
    }
    return true;
}

static void Random_NextInt_Failure()
{
    Auto<CRandom> random (new CRandom());
    random->NextInt(9, -5);
}

static bool Random_NextDouble()
{
    Auto<CRandom> random (new CRandom());
    for(int i = 0; i < 100; i++) {
        int r = random->NextDouble();
        if(!(r >= 0.0 && r <= 1.0))
            return false;
    }
    return true;
}

static bool Variant_test()
{
    SVariant v;

    v.SetInt(5);
    if(v.IntValue() != 5)
        return false;

    v.SetFloat(1);
    if(v.FloatValue() != 1)
        return false;

    v.SetBool(true);
    if(v.BoolValue() != true)
        return false;

    Auto<const CString> str (CString::FromUtf8("test"));
    v.SetObject(str);
    if(dynamic_cast<const CString*>(v.ObjectValue()) != str.Ptr())
        return false;

    return true;
}

static bool SerializeDeserialize()
{
    Auto<CMemoryStream> memStream (new CMemoryStream());
    METASTREAM metaStream;
    memStream->ToMetaStream(&metaStream);

    Auto<CIdTable> idTable (CIdTable::CreateInstance());
    METAIDTABLE metaIdTable;
    idTable->ToMetaIdTable(&metaIdTable);

    // Checking null.

    {
        METAVALUE inValue, outValue;
        METARESULT res;
        inValue = MetaValueFromNull();
        res = SerializeMetaValue(&inValue, &metaStream, &metaIdTable);
        memStream->SetPosition(0);
        if(res != METARESULT_SUCCESS)
            return false;
        res = DeserializeMetaValue(&metaStream, &outValue, &metaIdTable);
        memStream->SetPosition(0);
        if(res != METARESULT_SUCCESS)
            return false;
        if(outValue.type != METATYPE_NULL)
            return false;
    }

    // Checking bool.

    {
        METAVALUE inValue, outValue;
        METARESULT res;
        inValue = MetaValueFromBoolean(true);
        res = SerializeMetaValue(&inValue, &metaStream, &metaIdTable);
        if(res != METARESULT_SUCCESS)
            return false;
        memStream->SetPosition(0);
        res = DeserializeMetaValue(&metaStream, &outValue, &metaIdTable);
        memStream->SetPosition(0);
        if(res != METARESULT_SUCCESS)
            return false;
        if((outValue.type != METATYPE_BOOL) || (outValue.u.asBool != true))
            return false;
    }

    {
        METAVALUE inValue, outValue;
        METARESULT res;
        inValue = MetaValueFromBoolean(false);
        res = SerializeMetaValue(&inValue, &metaStream, &metaIdTable);
        if(res != METARESULT_SUCCESS)
            return false;
        memStream->SetPosition(0);
        res = DeserializeMetaValue(&metaStream, &outValue, &metaIdTable);
        memStream->SetPosition(0);
        if(res != METARESULT_SUCCESS)
            return false;
        if((outValue.type != METATYPE_BOOL) || (outValue.u.asBool != false))
            return false;
    }

    // Checking number.

    {
        METAVALUE inValue, outValue;
        METARESULT res;
        inValue = MetaValueFromNumber(13.0f);
        res = SerializeMetaValue(&inValue, &metaStream, &metaIdTable);
        if(res != METARESULT_SUCCESS)
            return false;
        memStream->SetPosition(0);
        res = DeserializeMetaValue(&metaStream, &outValue, &metaIdTable);
        memStream->SetPosition(0);
        if(res != METARESULT_SUCCESS)
            return false;
        if((outValue.type != METATYPE_NUMBER) || (outValue.u.asNumber != 13.0f))
            return false;
    }

    {
        METAVALUE inValue, outValue;
        METARESULT res;
        inValue = MetaValueFromNumber(666.0f);
        res = SerializeMetaValue(&inValue, &metaStream, &metaIdTable);
        if(res != METARESULT_SUCCESS)
            return false;
        memStream->SetPosition(0);
        res = DeserializeMetaValue(&metaStream, &outValue, &metaIdTable);
        memStream->SetPosition(0);
        if(res != METARESULT_SUCCESS)
            return false;
        if((outValue.type != METATYPE_NUMBER) || (outValue.u.asNumber != 666.0f))
            return false;
    }

    // Checking string.

    {
        Auto<const CString> inStr (CString::FromUtf8("Hello, World!"));
        METAVALUE inValue, outValue;
        METARESULT res;
        inValue = STRINGPTR_TO_METAVALUE(inStr.Ptr());
        res = SerializeMetaValue(&inValue, &metaStream, &metaIdTable);
        if(res != METARESULT_SUCCESS)
            return false;
        memStream->SetPosition(0);
        res = DeserializeMetaValue(&metaStream, &outValue, &metaIdTable);
        memStream->SetPosition(0);
        if(res != METARESULT_SUCCESS)
            return false;
        if((outValue.type != METATYPE_OBJECT) || (strcmp(outValue.u.asObject.klass->name, "String") != 0))
            return false;
        const CString* outStr = METAVALUE_TO_STRINGPTR(&outValue);
        if(!outStr->Equals(inStr))
            return false;
        bool b;
        UnrefMetaValue(&outValue, &b);
    }

    return true;
}

static const polk_byte base64_table[67] =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=\n";

static bool IsValidBase64Char(int index, polk_char16 c)
{
    for(int i = 0; i < 67; i++) {
        if(c == (polk_char16)base64_table[i])
            return true;
    }

    return false;
}

static bool Base64EncodeDecode()
{
    const char* helloWorld = "Hello, World!";

    size_t len;
    polk_byte* cEncoded = Marshal::EncodeBase64(reinterpret_cast<const polk_byte*>(helloWorld),
                                              strlen(helloWorld), // don't forget about the null terminator
                                              &len);
    Auto<const CString> daEncoded (CString::FromUtf8(reinterpret_cast<char*>(cEncoded)));
    delete [] cEncoded;

    for(int i = 0; i < daEncoded->Length(); i++) {
        if(!IsValidBase64Char(i, daEncoded->Chars()[i]))
            return false;
    }

    Utf8Auto cInput (daEncoded->ToUtf8());

    polk_byte* cDecoded = Marshal::DecodeBase64(reinterpret_cast<polk_byte*>(cInput.Ptr()),
                                              strlen(cInput),
                                              &len);

    if(len != strlen(helloWorld))
        return false;

    for(size_t i = 0; i < len; i++) {
        if(cDecoded[i] != helloWorld[i])
            return false;
    }

    delete [] cDecoded;

    return true;
}

static void performTests()
{
    // ****************
    //   Application.
    // ****************

    TEST(Application_GetExeFileName)
    TEST(Application_GetMemoryUsage)
    TEST(Application_TickCount)
    TEST(Application_GetPlatformString)
    TEST_FAILURE(Application_GetPlatformString_Failure)
    TEST(Application_GetProcessorCount)
    TEST(Application_GetSpecialFolder)
    TEST_FAILURE(Application_GetSpecialFolder_Failure)
    TEST(Application_GetOSVersion)

    // ***************
    //    CoreUtils.
    // ***************

    TEST(CoreUtils_AreObjectsEqual1)
    TEST(CoreUtils_AreObjectsEqual2)
    TEST(CoreUtils_AreObjectsEqual3)
    TEST(CoreUtils_AreObjectsEqual4)
    TEST(CoreUtils_IsDigit)
    TEST(CoreUtils_IsWhiteSpace)
    TEST(CoreUtils_IsLetter)
    TEST(CoreUtils_IntToString1)
    TEST(CoreUtils_IntToString2)
    TEST(CoreUtils_LongToString)
    TEST(CoreUtils_FloatToString)
    TEST(CoreUtils_BoolToString1)
    TEST(CoreUtils_BoolToString2)
    TEST(CoreUtils_CharToUpperCase)
    TEST(CoreUtils_CharToLowerCase)
    TEST_FAILURE(CoreUtils_ValidatePath_Failure1)
    //TEST_FAILURE(CoreUtils_ValidatePath_Failure2)
    TEST_FAILURE(CoreUtils_ValidatePath_Failure3)
    TEST_FAILURE(CoreUtils_ValidatePath_Failure4)
    TEST_FAILURE(CoreUtils_ValidatePath_Failure5)
    TEST_FAILURE(CoreUtils_ValidatePath_Failure6)
    TEST(CoreUtils_AllocAligned)
    TEST(CoreUtils_MemorySizeToString)
    TEST(CoreUtils_ByteOrder)

    // ***************
    //    DateTime.
    // ***************

    TEST_FAILURE(DateTime_ctor_Failure)

    // ***************
    //     Object.
    // ***************

    TEST(Object_ReferenceCount)
    TEST(Object_GetHashCode)
    TEST(BoxedStruct_MethodRedirection)

    // *************
    //    Random.
    // *************

    TEST(Random_NextInt)
    TEST_FAILURE(Random_NextInt_Failure)
    TEST(Random_NextDouble)

    // *************
    //    Variant.
    // *************

    TEST(Variant_test)

    // ***************
    //   METAOBJECT
    // ***************

    TEST(SerializeDeserialize)

    // ***************
    //    Marshal
    // ***************

    TEST(Base64EncodeDecode)
}

// *****************************************************************************
// *****************************************************************************

int main()
{
    InitCore();

    g_log = new CLog();
    Auto<const CString> logPath (CString::FromUtf8("Core.log"));
    Auto<CLogHandler> logHandler (CLogHandler::CreateFromFile(logPath));
    g_log->AddLogHandler(E_LOGPRIORITY_ALL, "Core", logHandler);
    logHandler.SetPtr(CLogHandler::CreateForConsole());
    g_log->AddLogHandler(E_LOGPRIORITY_ALL, "Core", logHandler);

    performTests();

    g_log->Unref();
    g_log = nullptr;

    DeinitCore();
    return 0;
}
