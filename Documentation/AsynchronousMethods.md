Long running operations, including disk access, can block the native OS thread,
defeating the point of preemptable realtime tracks. To solve this problem,
Polk introduces the concept of asynchronous methods. Asynchronous methods
run concurrently to a track's hosting thread. From the point of view of the
scripting language, asynchronous methods do not look any different from
synchronous methods. This is achieved by the fact that on an asynchronous method
call, a track simply yields to other tracks without blocking until the asynchronous
method finishes.

Asynchronous method calls make use of a thread pool; the thread pool's size is
set by the hosting C++ code and can be anything from 1 to N worker threads. The
more there are worker threads in a thread pool, the more asynchronous methods can
run in parallel to each other (although limited by the number of CPU cores and the
scheduler of the OS).

When it comes to track cancellation, asynchronous methods can be abandoned inside
`do` blocks and outside of `atomic` blocks. The thread pool may keep executing the
method even after cancellation because it's not possible to preempt a native method,
however the track essentially abandons the method, and the result will be ignored.
Cancellation of asynchronous methods only happens during the transition to the
cancelling state; once inside `undo` blocks, asynchronous methods are never
abandoned.

Only native methods can be asynchronous methods. To make a metamethod asynchronous,
all the implementation needs is to set the metamethod's flag bit to METAMETHODFLAGS_ASYNC.
This is all which makes a method asynchronous. However, native implementations
have to face a few limitations/requirements:

1) Asynchronous methods so far only support the following argument types:
nothing, boolean, number and string. Everything else has unclear semantics
about their thread safety. The mentioned above 4 types are simply cloned before
making a call, which solves thread safety problems. As a workaround, it's possible
so far to embed additional data inside a string, and create wrappers which would
encode/decode data inside this string (similar to JSON).

2) Fortunately, asynchronous methods can return objects of any type. The requirement
is that the returned object is not shared by any other code, i.e. the reference
which it returns is unique. This requirement allows the engine to avoid cloning
it itself and solves thread safety problems which would stem from non-thread-safe
reference counting. As we eliminate the need for cloning, we can return objects
of any type.

3) As a general rule, implementing asynchronous method implementations, do
not forget that the values they access can be read/modified by other threads.

4) Asynchronous methods have a limit on the maximum number of arguments, which is
8 at this time (just like ordinary methods).

5) Implementing asynchronous method implementations, keep in mind that the original
track can be cancelled and the method abandoned, i.e. do not return data via other
objects' properties because they can be deleted by that point; rather simply return 
the value.

6) Metamethods should never throw, only return metaresults. Worker threads do
not catch exceptions; i.e. an uncaught exception will crash the whole worker thread
and possibly the entire process.
