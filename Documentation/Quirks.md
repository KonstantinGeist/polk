1. Cyclic references create memory leaks.
Why: memory management is based on reference counting for its simplificity and lack of pauses.

2. Putting a function inside a container and then retrieving it erases all its arguments. Also, the function stops being preemptable.
Why: containers are implemented using native code, and when a function is passed to native code, it is wrapped as a generic event handler
with no arguments. TODO