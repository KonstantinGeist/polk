The project is implemented in C++ but doesn't make use of STL, doesn't follow C++ semantics at all, and, in fact, reinvents many of the types. Why?

There are several reasons.

1. If we develop a scripting language with some batteries included, we need to implement our own class library for data structures, for accessing the operating system. To have a higher adoption rate, we need to introduce a familiar API. No one in his sane mind would want to adopt the C++ API for a scripting language so instead the C# base class library was chosen as inspiration because it is pretty straightforward and symmetric. So, if we have to reimplement our own types anyway, why not reuse them inside the C++ implementation to cover more cases and polish its implementation without writing a line of Polk code yet?

2. We want low-overhead interoperability between the runtime and scripts themselves. If we operate on the same types both inside the runtime and Polk scripts, then we don't have to convert objects between different type systems (which also includes the issues of memory management) -- it is trivial to expose any part of the runtime to Polk and back, as there's no clear distinction between the two. This leads us to the next point.

3. Since we are not bound to the C++ semantics and since we use the Polk's base class library for the implementation of its own runtime, it is possible in the future to bootstrap more parts of the runtime in Polk itself without rewriting code semantically. We can expose parts of the runtime internals to Polk so that it could modify the runtime dynamically as it runs. All we need is to register more metaclasses.

4. Useful for educational purposes -- one can learn how runtimes actually function, how data structure are actually implemented.

5. We want to be able to serialize full track states to memory streams at any point + successful deserialization. Coupled with the ideas of giving Polk more access to the internals, we want 100% control over our memory management down to every byte (for example, a random object state), including the internals of the runtime, as they have the potential of ending up inside serialized object trees.

In a nutshell, C++ in this project is used as nothing more than a code generation engine.