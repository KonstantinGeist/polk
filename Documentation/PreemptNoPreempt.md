The language contains two related keywords:

    preempt, nopreempt

`preempt` tells that the track can be automatically preempted from now on. It
has no effect if no quantum was set for the track (which is set externally). By
default, it is as if `preempt` was called right before the track started.

`nopreempt` tells that the track cannot be automatically preempted from now on,
even if a preemption quantum was set. As there's no automatic preemption anymore,
it's the responsibility of the programmer to add `yield` in correct places, otherwise
the script can block the thread.

Why is `nopreempt` useful? It can be useful, for example, if we want to make sure
that a certain method is always called inside each CExecutionTrack::Resume().
For example, consider the following:

    while { true } {
        local pos = ... # some code here which calculates the position
        obj SetPosition pos
    }

With automatic preemption, it can yield at any point, including right before
SetPosition is called. But consider that the host code renders a frame in a game
right after CExecutionTrack::Resume(). Since SetPosition was not called, its visual
state was not updated for the current frame. On the next frame, it can be skipped
again, due to premature preemption. When frames are skipped it can create jerky
motion.

Now consider this code:

    nopreempt

    while { true } {
        local pos = ...
        obj SetPosition pos

        yield
    }

    preempt

With no preemption, we can only suspend the track and proceed with rendering ONLY
after SetPosition was called -- which will happen on every frame.

In a nutshell, preempt/nopreempt is a dynamic alternative to atomic blocks and
which doesn't depend on scopes.