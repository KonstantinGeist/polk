Polk is designed to be suitable for soft realtime multiple agent systems,
such as game engines. This has several design implications for memory management.

There's no GC. Internal objects are reference-counted instead, to have deterministic
object lifetimes with low pauses. There are no true closures either, as capturing
variables would require heap allocation.

There are three types of variable storage: arguments, local, global.

### Arguments

    local outerFunc = {|arg|
        local innerFunc = {
            trace arg
        }
    
        innerFunc!
    }

Arguments are passed to functions and can be seen by inner functions, as long
as outer functions are still on the execution stack. If an inner function leaks
from inside its outer function, i.e. stored somewhere else and invoked long after
the parent function was destroyed or deleted from the execution stack, then
such argument variables stop being valid. To fix it would require allocation
of additional heap memory during execution, which we try to avoid.

If one wants to access an argument inside a detached callback, they'll have to
move the value from the argument into an auxiliary global: global g_arg = arg

### Locals

    local outerFunc = {
        local var = 1
    
        local innerFunc = {
            trace var
        }
    
        innerFunc!
    }

Local variables behave similar to arguments, except they can be defined anywhere.

Local variables belong to the scope where they were defined and are valid as long
as the scope is valid (i.e. found on the execution stack). If a function is
called outside of its original execution stack (e.g. used as a callback), then
naturally all locals defined in outer scopes are no longer valid. However,
the current implementation reserves "mirrors" of outer variables in every scope
where they are used. That is, if an inner function is called outside of the
original hierarchy, and if an outer variable was 1, then after the detachment,
the resolution mechanism will start referring to a mirror of this variable in the
current scope, and the value will be initialized to nothing (as it was never
written to before).

### Globals

    local outerFunc = {
        global var = 1
    }
    outerFunc!
    trace var

Global variables exist in so called variable contexts. A variable context is
an invisible concept created by the top function; all inner functions/scopes
inherit this single variable context. Changes to global variables become
immediately visible to all scopes inheriting it. Generally, each separate script
execution will have its own variable context (unless detachment happens, see
below).

The most important part about globals is that they make it possible for callbacks
to access outer variables even if the parent scope was destroyed. When an outer
function is destroyed, it notifies its children of its fate. Children in turn
create their own copies of the original variable context, i.e. children are now
all on their own (as the parent dies), with a copy of all the global variables
at the  moment of detachment. This design choice is useful for two reasons:

a) Avoids circular references in the implementation.
b) Functions are more independent and more decoupled from each other, and
therefore more lightweight. I.e. setting a callback from inside a deeply nested
function hierarchy won't extend the lifetimes of all that unnecessary garbage
which the callback might accidentally reference.

This has a major implication, as can be seen in this example:

    global sharedVar = 1
    
    obj1 SetCallback {
        global sharedVar = 2
    }
    obj2 SetCallback {
        global sharedVar = 3
    }

After the script executes, objects `obj1` and `obj2` have references to two
functions. The parent function is destroyed, and so the callbacks are detached:
each callback creates its own copy of the parent's variable context. This allows
callbacks to still refer to `sharedVar` even though the original parent function
no more exists. The implication is that the callbacks now have independent copies
of the variable context. Changes to `sharedVar` in one callback won't be visible
to the other.

The solution is a special `SharedRef` object. The insight is that while we cannot
share references themselves, we can still share data (even across separate
script executions, by the way). So the solution is to create an additional
level of indirection.

    global sharedVar = new SharedRef
    sharedVar Set 1

    obj1 SetCallback {
        sharedVar Set 2
    }
    obj2 SetCallback {
        sharedVar Set 3
    }

Global variables are also useful for communicating with the host code, they
can be accessed via CExecutionTrack::SetVariable(..) and
CExecutionTrack::GetVariable(..)