The syntax was inspired by S-expressions with some extensions to make it more palatable.

The basic function call syntax has the form:

	(object method arg1 arg2)

That is, it is a simple list where the first element is always the target of invocation, the second element is the name of the method, and the rest is arguments. This nicely unites method call syntax with arithmetics:

	(1 + 3)

This is reinterpreted as "object 1 has method 'plus' which accepts argument '3'"

Top-level method calls don't require parentheses, unlike in Lisp, and can be separated with a semicolon (which is not required):

	local i = a + 3
	Console WriteLine (i ToString)

If the first element in such a list is a reserved keyword, for example: "local", "return" etc. then interpretation of the list depends on such a keyword. Otherwise, it's a method call.

Another extension is the "executable literal" syntax, it has the following form:

	# Creates a function that takes two arguments, adds them together and returns the result.
	local sum = {|a b| a + b}

This syntax is reused to express bodies of control flow operators:

	if { a < 3 } {
		# do something
	}

That is, the "if" operator does not have some special syntactic rules just for it; instead, we have a normal S-expression that simply accepts two executable literals: the condition literal and the body literal.

This design makes the grammar easily extendable for custom DSL's, as everything you need is to introduce a new keyword that would change the semantics of an S-expression if placed as the first element.

A consequence of this oversimplification is that arithmetic operators have no priorities and they should be grouped manually using parentheses:

	local z = (a + b) + c
