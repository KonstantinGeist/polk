Executable tracks can yield manually, via the `yield` statement; or yield
automatically if a track's quantum is set. To avoid race conditions, use `atomic`
blocks which make sure the track is not preempted until the block is left:

    atomic {
        doSomething!
        yield # ignored
        doSomething!
    }

Do not place long-running loops inside atomic blocks, or they will block the
entire native thread!

Note that native functions implemented in C/C++ are always atomic.
