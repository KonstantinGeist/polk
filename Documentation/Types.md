There's no way to describe your own custom types in Polk itself. Why? Allowing to describe arbitrary types would overcomplicate the implementation of the language and assume too much.

What we do instead is rely entirely on our types being already defined somewhere in C++ code in the underlying host code/game engine (it probably already has the notion of entities, components, etc.). Polk can create new objects and manipulate them using the C++ classes that were registered within the metaobject system. For example, you can look at how it is implemented in Source/Core/src/core/Random.cpp at the end of the file.

Another reason why custom types aren't really needed is because each actor in a game is attached its own script state, and all the globals of the script are basically the actor's properties. As we can serialize/deserialize script state at any point, the entire track state serves as the actor's object model.

Auxiliary data to be passed between actors can be expressed using lists or maps, or other custom C++ classes.