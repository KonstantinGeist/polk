It's desirable to have scripts cancellable. For example, an NPC abandons a
behavior because of emergency; or the player cancels an ongoing action. And it's
desirable to allow a script to restore the environment to a predictable state,
and do it with a clear, readable syntax.

A script can be cancelled via 2 means:

1) `cancel` statement inside the script itself;
2) a call to CExecutionTrack::Cancel() by the hosting code (we can call it
"cancellation injection").

Initially, a track is marked as "cancelled", however, the scripting engine
does not immediately acknowledge it. In this context, "cancelled" means "cancellation
requested". The track can transition from the "cancelled" to the "cancelling" state
only once it reaches a `do` block (if it wasn't inside one already). This is required
because it's possible to handle cancellation in `undo` blocks:

    do {
        # Main logic.
    } undo {
        # Cleanup code.
    }

Once the scripting engine has acknowledged cancellation, the track transitions
to the "cancelling" state (which can be tested with the special "cancelling" operator).
Remember that it can happen at any point inside `do` blocks: control flow will
long jump from the current position (whatever it was) to the corresponding `undo`
block, to undo the action/clean up things. Once this `undo` block reaches its end,
control flow jumps to a higher-level `undo` block and so forth until there are
no more `undo` blocks in the chain. Note that it's possible to cancel a track
only once, further calls will have no effect.

Atomic blocks guarantee that the operations they wrap should be perceived as atomic
to the outside world. This means that cancellation handling and stack unwinding
cannot be initiated inside atomic blocks, otherwise it would leave a script
in a partial state. In the following code, `cancel` will be ignored until control
flow leaves the atomic block and emerges inside a `do` block:

    do {
        atomic {
            cancel
            trace "This is still called."
        }
        trace "This is not called."
    } undo {
        # Cleanup...
    }

It's important to realize that cancellation can happen at any point (if cancelled
via CExecutionTrack::Cancel()) therefore code must be written with a lot of caution,
expecting cancellation at any point, as if it was asynchronous. This requirement
creates a few pecularities. Consider this simple code which looks safe:

    do {
        enity LockAnimation "sit"
    } undo {
        entity UnlockAnimation
    }

However, this code is not safe. The problem is that, the first call is not atomic.
First, it loads the value of the variable "entity", then pushes the argument "sit",
and only then makes a call. But what happens if cancellation happens midway in
the process? Control flow will long jump to `undo` without actually calling
`LockAnimation`. And now an unpaired `UnlockAnimation` is called!

To circumvent this problem, the scripting engine has a guarantee: stack unwinding
to handle cancellation cannot be initiated between "do" and "atomic" keywords.
This is the fixed code:

    do {
        atomic {
            enity LockAnimation "sit"
        }
    } undo {
        entity UnlockAnimation
    }

Even if the track was cancelled long before it jumps into this `do` block,
the first atomic block will be guaranteed to run. And, by definition, control flow
cannot escape from an atomic block, be it cancellation or suspension. Don't
forget that atomic blocks cannot be preempted either! You should not place long
running loops inside atomic blocks, or they will block the entire native thread.

Another pecularity to consider is: what happens if cancellation is injected
when the track is inside an `undo` block:

    do {
        # Something.
    } undo {
        cancel
        trace "Will it be called?"
    }

The answer is yes, it will be called. Cancellation handling will be postponed
until the control flow enters another `do` block, and then the regular rules
follow.

Yet another pecularity is with the `sleep` opcode. Sleeping is aborted if we need
to start cancellation, however it's fully functional during cancellation itself.

Note that the whole cancellation process is preemptable just like the rest of
code and therefore can contain long-running operations, such as movement animations.