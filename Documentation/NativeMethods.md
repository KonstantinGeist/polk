If a calculation is too complex, it makes sense to expose it
as a native method (via reflection/MetaObject.h) to achieve
maximum performance.

A small benchmark was created. A function of the following form:

    local Lerp = {|a b delta|
        a + ((b - a) * delta)
    }

was created and compared to a native method (Math Lerp a b delta).

The benchmark showed that the native method was 2.35 times faster.
You'd expect it to be even faster, but there's always some overhead
when it comes to calling native methods: we have to prepare arguments
in a form understandable by native code, with additional type checks.
So native methods have no benefits if you call them for very small
calculations.

However, native methods have a disadvantage: they cannot be preempted
and can potentially block the entire thread.