// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/core.h>
#include <script/Executable.h>
#include <script/Reflection.h>
#include <script/ScriptList.h>
#include <script/ScriptMap.h>
#include <script/SharedRef.h>

#include <assert.h>

namespace polk { namespace script {

static bool g_isScriptInitialized = false;

void InitScript()
{
    assert(polk::core::IsCoreInitialized());
    assert(!g_isScriptInitialized);

    // ***************************
    __InitExecutable();
    __InitReflection();
    __InitScriptList();
	__InitScriptMap();
    __InitSharedRef();
    // ***************************

    g_isScriptInitialized = true;
}

void DeinitScript()
{
    assert(polk::core::IsCoreInitialized());
    assert(g_isScriptInitialized);

    g_isScriptInitialized = false;

    // ***************************
    __DeinitSharedRef();
	__DeinitScriptMap();
    __DeinitScriptList();
    __DeinitReflection();
    __DeinitExecutable();
    // ***************************
}

bool IsScriptInitialized()
{
    return g_isScriptInitialized;
}

} }
