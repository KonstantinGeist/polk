// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/ScriptValue.h>
#include <core/Contract.h>
#include <core/Log.h>
#include <core/String.h>
#include <reflection/IdTable.h>
#include <io/Stream.h>

namespace polk { namespace script {
using namespace polk::core;
using namespace polk::reflection;
using namespace polk::io;

SScriptValue::SScriptValue(void* v) //-V730
{
    // Default value.
    m_metaValue = MetaValueFromNull();
}

SScriptValue::SScriptValue() //-V730
{
    // Default value.
    m_metaValue = MetaValueFromNull();
}

SScriptValue::~SScriptValue()
{
    bool b;
    UnrefMetaValue(&m_metaValue, &b);
}

SScriptValue::SScriptValue(const SScriptValue& copy)
{
    this->m_metaValue = copy.m_metaValue;

    RefMetaValue(&this->m_metaValue);
}

SScriptValue& SScriptValue::operator=(const SScriptValue& copy)
{
    METAVALUE copyValue = copy.m_metaValue;

    // NOTE We should ref before unrefing for the case when it is called on the
    // same object twice.

    bool b;

    RefMetaValue(&copyValue);
    UnrefMetaValue(&this->m_metaValue, &b);

    this->m_metaValue = copy.m_metaValue;

    return *this;
}

bool SScriptValue::IsStatic() const
{
    // For some classes, 0 is a valid object reference, and so to be 100%
    // sure, we have to consult with the metadata of the class.
    return (m_metaValue.type == METATYPE_OBJECT)
        && (m_metaValue.u.asObject.idptr == 0)
        && (m_metaValue.u.asObject.klass->flags & METACLASSFLAGS_STATIC);
}

void SScriptValue::SetBool(bool value)
{
    bool b;
    UnrefMetaValue(&m_metaValue, &b);

    m_metaValue = MetaValueFromBoolean(value);
}

void SScriptValue::SetNumber(float value)
{
    bool b;
    UnrefMetaValue(&m_metaValue, &b);

    m_metaValue = MetaValueFromNumber(value);
}

void SScriptValue::SetObject(const char* klassName, intptr_t ptr)
{
    bool b;
    UnrefMetaValue(&m_metaValue, &b);

    // NOTE MetaValueFromVal(..) automatically ref's the value.
    m_metaValue = MetaValueFromVal(klassName, (void*)ptr);
}

void SScriptValue::SetNothing()
{
    bool b;
    UnrefMetaValue(&m_metaValue, &b);

    m_metaValue = MetaValueFromNull();
}

bool SScriptValue::BoolValue() const
{
    POLK_REQ(m_metaValue.type == METATYPE_BOOL, EC_TYPE_MISMATCH);

    return m_metaValue.u.asBool;
}

float SScriptValue::NumberValue() const
{
    POLK_REQ(m_metaValue.type == METATYPE_NUMBER, EC_TYPE_MISMATCH);

    return m_metaValue.u.asNumber;
}

intptr_t SScriptValue::ObjectValue(const char* klassName) const
{
    POLK_REQ_PTR(klassName);
    POLK_REQ(m_metaValue.type == METATYPE_OBJECT, EC_TYPE_MISMATCH);

    if(m_metaValue.u.asObject.klass->name // just in case
    && strcmp(m_metaValue.u.asObject.klass->name, klassName) == 0)
    {
        return m_metaValue.u.asObject.idptr;
    } else {
        return 0;
    }
}

bool SScriptValue::Equals(const SScriptValue& other) const
{
    METAVALUE thisValue = this->m_metaValue;
    METAVALUE otherValue = other.m_metaValue;

    return MetaValueEquals(&thisValue, &otherValue);
}

int SScriptValue::GetHashCode() const
{
    METAVALUE thisValue = this->m_metaValue;

    return GetMetaValueHashCode(&thisValue);
}

const CString* SScriptValue::ToString() const
{
    METAVALUE thisValue = this->m_metaValue;

    if(thisValue.type == METATYPE_OBJECT) {
        
        METAVALUE strValue = MetaValueToString(&thisValue);
        if(strValue.type == METATYPE_NULL) {
            return CString::CreateEmptyString();
        } else {
            return METAVALUE_TO_STRINGPTR(&strValue);
        }

    } else if(thisValue.type == METATYPE_BOOL) {
        return CoreUtils::BoolToString(thisValue.u.asBool);
    } else if(thisValue.type == METATYPE_NUMBER) {
        return CoreUtils::FloatToString(thisValue.u.asNumber, 0, true);
    } else if(thisValue.type == METATYPE_NULL) {
        return CString::FromASCII("nothing");
    } else {
        POLK_THROW(EC_NOT_IMPLEMENTED);
        return nullptr;
    }
}

static const CString* AddQuotes(const CString* source)
{
    const int sourceLength = source->Length();
    const auto sourceChars = source->Chars();
    polk_char16* outputChars;
    const CString* output = CString::CreateBuffer(sourceLength + 2, &outputChars);

    outputChars[0] = outputChars[output->Length() - 1] = POLK_CHAR('"');
    for(int i = 0; i < sourceLength; i++) {
        outputChars[i + 1] = sourceChars[i];
    }

    return output;
}

const CString* SScriptValue::ToItemString() const
{
    // Special-cased for strings and string builders.
    if(m_metaValue.type == METATYPE_OBJECT && strcmp(m_metaValue.u.asObject.klass->name, "String") == 0) {
        const CString* source = static_cast<const CString*>((CObject*)(void*)m_metaValue.u.asObject.idptr);
        return AddQuotes(source);
    } else if (m_metaValue.type == METATYPE_OBJECT && strcmp(m_metaValue.u.asObject.klass->name, "StringBuilder") == 0) {
        CObject* sb = (CObject*)(void*)m_metaValue.u.asObject.idptr;
        Auto<const CString> source (sb->ToString());
        return AddQuotes(source);
    } else {
        return this->ToString();
    }
}

const char* SScriptValue::ClassName() const
{
    if(m_metaValue.type == METATYPE_OBJECT)
        return m_metaValue.u.asObject.klass->name;
    else
        return nullptr;
}

    // *********************
    //     Serialization.
    // *********************

void SScriptValue::Serialize(CStream* stream, CIdTable* idTable, CLog* log) const
{
    METASTREAM metaStream;
    stream->ToMetaStream(&metaStream);

    METAIDTABLE metaIdTable;
    idTable->ToMetaIdTable(&metaIdTable);

    this->Serialize(&metaStream, &metaIdTable, log);
}

void SScriptValue::Serialize(METASTREAM* stream, METAIDTABLE* idTable, CLog* log) const
{
    METARESULT res = SerializeMetaValue(const_cast<METAVALUE*>(&m_metaValue), stream, idTable);

    if(res != METARESULT_SUCCESS) {
        if(res == METARESULT_MISSING_METHOD) {

            // Should happen for OBJECTS only...
            assert(m_metaValue.type == METATYPE_OBJECT);
            POLK_LOG_ERROR(log,
                        "Script",
                        "Failed to serialize script value of type '%s'.",
                         m_metaValue.u.asObject.klass->name);

        }

        POLK_THROW_WITH_MSG(EC_BAD_FORMAT, "Failed to serialize a script value.");
    }
}

SScriptValue SScriptValue::Deserialize(CStream* stream, CIdTable* idTable, CLog* log)
{
    METASTREAM metaStream;
    stream->ToMetaStream(&metaStream);

    METAIDTABLE metaIdTable;
    idTable->ToMetaIdTable(&metaIdTable);

    return SScriptValue::Deserialize(&metaStream, &metaIdTable, log);
}

SScriptValue SScriptValue::Deserialize(METASTREAM* metaStream, METAIDTABLE* idTable, CLog* log)
{
    SScriptValue r;
    METARESULT res = DeserializeMetaValue(metaStream, r.MetaValue(), idTable);

    if(res != METARESULT_SUCCESS) {
        POLK_LOG_ERROR(log,
                    "Script",
                    "Failed to deserialize script value, error: %s.",
                     MetaResultToCString(res));

        POLK_THROW_WITH_MSG(EC_BAD_FORMAT, "Failed to deserialize a script value.");
    }

    return r;
}

    // *********************
    //     Static ctors.
    // *********************

SScriptValue SScriptValue::FromNothing()
{
    SScriptValue v;
    v.SetNothing();
    return v;
}

SScriptValue SScriptValue::FromNumber(float f)
{
    SScriptValue v;
    v.SetNumber(f);
    return v;
}

SScriptValue SScriptValue::FromBool(bool b)
{
    SScriptValue v;
    v.SetBool(b);
    return v;
}

SScriptValue SScriptValue::FromString(const char* str)
{
    Auto<const CString> daStr (CString::FromUtf8(str));
    SScriptValue v;
    v.SetObject("String", (intptr_t)static_cast<const CObject*>(daStr.Ptr()));
    return v;
}

SScriptValue SScriptValue::FromIdPtr(const char* klassName, intptr_t ptr)
{
    SScriptValue v;
    v.SetObject(klassName, ptr);
    return v;
}

SScriptValue SScriptValue::FromMetaValue(METAVALUE* metaValue)
{
    SScriptValue v;
    RefMetaValue(metaValue);
    *v.MetaValue() = *metaValue;
    return v;
}

SScriptValue SScriptValue::FromMetaValueNoRef(METAVALUE* metaValue)
{
    SScriptValue v;
    *v.MetaValue() = *metaValue;
    return v;
}

} }
