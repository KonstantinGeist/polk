// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/ScriptMap.h>
#include <core/String.h>
#include <core/StringBuilder.h>
#include <reflection/MetaObject.h>

namespace polk { namespace script {
using namespace polk::core;
using namespace polk::collections;

#define GET_SELF auto const self = static_cast<CScriptMap*>((CObject*)(void*)_self->idptr);

const CString* CScriptMap::ToString() const
{
    Auto<CStringBuilder> sb (new CStringBuilder());

    sb->Append(POLK_CHAR('['));

    SHashMapEnumerator<SScriptValue, SScriptValue> mapEnum (this);
    SScriptValue key;
    SScriptValue value;
    int i = 0;
    while(mapEnum.MoveNext(&key, &value)) {
        Auto<const CString> keyStr (key.ToItemString());
        Auto<const CString> valueStr (value.ToItemString());

        sb->Append(POLK_CHAR('('));
        sb->Append(keyStr);
        sb->Append(POLK_CHAR(' '));
        sb->Append(valueStr);
        sb->Append(POLK_CHAR(')'));

        if(i < this->Size() - 1) {
            sb->Append(POLK_CHAR(' '));
        }
        i++;
    }

    sb->Append(POLK_CHAR(']'));

    return sb->ToString();
}

METACTORIMPL(Map_ctor)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN

        CScriptMap* const r = new CScriptMap();
        *out_ret = (intptr_t)(void*)static_cast<CObject*>(r);

    METAGUARD_END
}

METASERIALIMPL(Map_serialize)
{
    METAGUARD_BEGIN
        auto const self = static_cast<CScriptMap*>((CObject*)(void*)obj->idptr);
        const int size = self->Size();
        METARESULT res;

        res = stream->writeimpl(stream, &size, sizeof(size));
        if(res != METARESULT_SUCCESS)
            return res;
        
        SHashMapEnumerator<SScriptValue, SScriptValue> mapEnum (self);
        SScriptValue key;
        SScriptValue value;
        while(mapEnum.MoveNext(&key, &value)) {
            key.Serialize(stream, idTable);
            value.Serialize(stream, idTable);
        }
    METAGUARD_END
}

METADESERIALIMPL(Map_deserialize)
{
    METAGUARD_BEGIN
        int size;
        METARESULT res;

        res = stream->readimpl(stream, &size, sizeof(size));
        if(res != METARESULT_SUCCESS)
            return res;
        if(size < 0) {
            return METARESULT_INTERNAL_EXCEPTION; // TODO better error
        }

        Auto<CScriptMap> map (new CScriptMap());

        for(int i = 0; i < size; i++) {
            const SScriptValue key (SScriptValue::Deserialize(stream, idTable));
            const SScriptValue value (SScriptValue::Deserialize(stream, idTable));
            map->Set(key, value);
        }

        map->Ref();
        *out_ret = MetaValueFromPtr("Map", (void*)static_cast<CObject*>(map.Ptr()));
    METAGUARD_END
}

METAMETHODIMPL(Map_Set_impl)
{
    VERIFY_NARG(2)

    METAGUARD_BEGIN
        GET_SELF
        self->Set(SScriptValue::FromMetaValue(&args[0]), SScriptValue::FromMetaValue(&args[1]));
    METAGUARD_END
}

METAMETHODIMPL(Map_Get_impl)
{
    VERIFY_NARG(1)

    METAGUARD_BEGIN
        GET_SELF
        SScriptValue ret;
        self->TryGet(SScriptValue::FromMetaValue(&args[0]), &ret);
        *out_ret = *ret.MetaValue();
        RefMetaValue(out_ret);
    METAGUARD_END
}

METAMETHODIMPL(Map_Size_impl)
{
    GET_SELF
    VERIFY_NARG(0)
    *out_ret = MetaValueFromNumber(self->Size());
    return METARESULT_SUCCESS;
}

METAMETHODIMPL(Map_Clear_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        GET_SELF
        self->Clear();
    METAGUARD_END
}

void __InitScriptMap()
{
    BEGIN_VTABLE(4)
        DEFINE_VMETHOD("Get", Map_Get_impl, 0)
        DEFINE_VMETHOD("Set", Map_Set_impl, 0)
        DEFINE_VMETHOD("Size", Map_Size_impl, 0)
        DEFINE_VMETHOD("Clear", Map_Clear_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "Map";
    metaClass.ctor = Map_ctor;
    metaClass.serialimpl = Map_serialize;
    metaClass.deserialimpl = Map_deserialize;
    metaClass.parent = GetMetaClassByName("Object");
    POLK_REQ_PTR(metaClass.parent);

    RegisterMetaClass(&metaClass);
}

void __DeinitScriptMap()
{
    // Nothing.
}

} }
