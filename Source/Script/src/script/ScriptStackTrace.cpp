// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/ScriptStackTrace.h>
#include <core/String.h>
#include <core/StringBuilder.h>

namespace polk { namespace script {
using namespace polk::core;
using namespace polk::collections;

CScriptStackTrace::CScriptStackTrace(CArrayList<SSourceRef>* sourceRefs)
{
    m_sourceRefs.SetVal(sourceRefs);
}

const CString* CScriptStackTrace::ToString() const 
{
    Auto<CStringBuilder> sb (new CStringBuilder());

    for(int i = 0; i < m_sourceRefs->Count(); i++) {
        const SSourceRef sourceRef = m_sourceRefs->Array()[i];

        sb->AppendASCII("    ");
        Auto<const CString> exeNameStr (sourceRef.ExecutableName.ToString());
        sb->Append(exeNameStr);
        sb->Append(POLK_CHAR(' '));

        sb->Append(POLK_CHAR('('));
        // Displays starting from 1, as this is how most text editors do it.
        sb->Append(sourceRef.LineNumber + 1);
        sb->Append(POLK_CHAR(')'));

        sb->Append(POLK_CHAR(':'));
        sb->Append(POLK_CHAR(' '));

        Auto<const CString> lineStr (sourceRef.Line.ToString());
        sb->Append(lineStr);
        sb->AppendLine();
    }

    return sb->ToString();
}

} }
