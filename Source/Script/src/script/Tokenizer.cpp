// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/Token.h>
#include <core/Log.h>
#include <core/String.h>
#include <collections/HashMap.h>
#include <script/CharCategory.h>
#include <script/StringSlice.h>

namespace polk { namespace script {
using namespace polk::core;
using namespace polk::collections;

    // ****************
    //    SLineInfo
    // ****************

struct SLineInfo
{
    int Number;

    int Start;
    int Count;

    SLineInfo(int dummy = 0)
        : Number(0), Start(0), Count(0)
    {
    }
};
inline void POLK_REF(const SLineInfo& t) { /* No-op. */ }
inline void POLK_UNREF(const SLineInfo& t) { /* No-op. */ }

    // ****************
    //     Tokenize
    // ****************

namespace
{

struct STokenizer
{
private:
    const SStringSlice code;
    const CString* exeName;
    CLog* log;
    
    mutable int curIndex;

public:
    STokenizer(const SStringSlice& code, const CString* exeName, CLog* log)
        : code(code), exeName(exeName), log(log), curIndex(0)
    {
    }

    SStringSlice ReadIdentOrNumber() const
    {
        const int startIndex = curIndex;

        while(++curIndex < code.Length) {
            const polk_char16 c = code.CharAt(curIndex);
            const ECharCategory cat = GetCharCategory(c);

            if(cat != E_CHARCATEGORY_LETTER && cat != E_CHARCATEGORY_DIGIT) {
                break;
            }
        }

        const int endIndex = curIndex;
        return SStringSlice(code, startIndex, endIndex - startIndex);
    }

    SStringSlice ReadString() const
    {
        const int startIndex = curIndex;
        bool found = false;

        while(++curIndex < code.Length) {
            const polk_char16 c = code.CharAt(curIndex);
            if(c == POLK_CHAR('"')) {
                found = true;
                break;
            }
        }
        curIndex++;

        if (!found) {
            POLK_LOG_ERROR(log,
                         "Script",
                         "Unclosed double quote (in '%o')",
                         static_cast<const CObject*>(exeName));

            POLK_THROW_WITH_MSG(EC_EXECUTION_ERROR, "Unclosed double quote.");
        }

        const int endIndex = curIndex;
        return SStringSlice(code, startIndex, endIndex - startIndex);
    }

    SStringSlice ReadComment() const
    {
        const int startIndex = curIndex;

        while(++curIndex < code.Length) {
            const polk_char16 c = code.CharAt(curIndex);
            if(c == POLK_CHAR('\n') || c == POLK_CHAR(';')) {
                break;
            }
        }

        const int endIndex = curIndex;
        return SStringSlice(code, startIndex, endIndex - startIndex);
    }

    SStringSlice ReadBody() const
    {
        const int startIndex = curIndex;
        int curlyCount = 1;

        while(++curIndex < code.Length) {
            const polk_char16 c = code.CharAt(curIndex);
            if (c == POLK_CHAR('{')) {
                curlyCount++;
            }
            if (c == POLK_CHAR('}')) {
                curlyCount--;
                
                if(curlyCount == 0) {
                    break;
                }
            }
        }
        curIndex++;

        if(curlyCount != 0) {
            POLK_LOG_ERROR(log,
                         "Script",
                         "Unclosed curly bracket (in '%o')",
                         static_cast<const CObject*>(exeName));

            POLK_THROW_WITH_MSG(EC_EXECUTION_ERROR, "Unclosed curly bracket.");
        }

        const int endIndex = curIndex;
        return SStringSlice(code, startIndex, endIndex - startIndex);
    }

    SStringSlice ReadControl() const
    {
        const int startIndex = curIndex;
        curIndex++;
        return SStringSlice(code, startIndex, 1);
    }

    CArrayList<SStringSlice>* GetSlices() const
    {
        Auto<CArrayList<SStringSlice> > slices (new CArrayList<SStringSlice>());

        while(curIndex < code.Length) {
            const polk_char16 c = code.CharAt(curIndex);
            const ECharCategory cat = GetCharCategory(c);

            if(cat == E_CHARCATEGORY_LETTER || cat == E_CHARCATEGORY_DIGIT) {
                slices->Add(ReadIdentOrNumber());
            } else if(c == POLK_CHAR('"')) {
                slices->Add(ReadString());
            } else if(c == POLK_CHAR('#')) {
                // skips
                ReadComment();
            } else if(c == POLK_CHAR('{')) {
                slices->Add(ReadBody());
            } else if(cat == E_CHARCATEGORY_WHITESPACE) {
                // skips
                curIndex++;
            } else {
                slices->Add(ReadControl());
            }
        }

        slices->Ref();
        return slices;
    }

    CHashMap<int, SLineInfo>* GetPositionToLineInfoMap() const
    {
        Auto<CArrayList<const CString*>> lines (code.Source()->Split(POLK_CHAR('\n')));
        Auto<CHashMap<int, SLineInfo>> map (new CHashMap<int, SLineInfo>());
        int curPos = 0;

        for(int i = 0; i < lines->Count(); i++) {
            const CString* const line = lines->Array()[i];

            SLineInfo lineInfo;
            lineInfo.Number = i;
            lineInfo.Start = curPos;
            lineInfo.Count = line->Length();

            // +1 for '\n' which was ommited in split
            for(int j = 0; j < line->Length() + 1; j++) {
                map->Set(curPos, lineInfo);
                curPos++;
            }
        }

        map->Ref();
        return map;
    }

    CArrayList<SToken>* SlicesToTokens(const CArrayList<SStringSlice>* slices) const
    {
        Auto<CArrayList<SToken>> tokens (new CArrayList<SToken>());

        SSourceRef curSourceRef;
        curSourceRef.ExecutableName = SStringSlice(exeName);

        Auto<CHashMap<int, SLineInfo>> posToLineInfoMap (GetPositionToLineInfoMap());

        for(int i = 0; i < slices->Count(); i++) {
            const SStringSlice curSlice = slices->Array()[i];

            const SLineInfo lineInfo = posToLineInfoMap->Item(curSlice.Start);
            curSourceRef.LineNumber = lineInfo.Number;
            curSourceRef.Line = SStringSlice(code.Source(), lineInfo.Start, lineInfo.Count);
            curSourceRef.CharPosition = curSlice.Start - lineInfo.Start;

            if(curSlice.IsIdentifier()) {
                tokens->Add(SToken::From(curSlice.ToKeywordOrIdentifier(), curSlice, curSourceRef));
            } else if(curSlice.IsStringLiteral()) {
                tokens->Add(SToken::From(E_TOKENTYPE_STRINGLITERAL, curSlice.ToStringLiteral(), curSourceRef));
            } else if(curSlice.IsBody()) {
                tokens->Add(SToken::From(E_TOKENTYPE_BODYLITERAL, curSlice.ToBody(), curSourceRef));
            } else {
                const SNullable<float> number = curSlice.TryAsNumber();

                if(number.HasValue()) {
                    tokens->Add(SToken::From(E_TOKENTYPE_NUMBERLITERAL, number.Value(), curSourceRef));
                } else {
                    tokens->Add(SToken::From(curSlice.ToControlCharacter(curSourceRef, log), curSlice, curSourceRef));
                }
            }
        }

        tokens->Ref();
        return tokens;
    }

    CArrayList<SToken>* PostProcessTokens(const CArrayList<SToken>* source) const
    {
        Auto<const CString> eqLit (CString::FromASCII("=="));

        Auto<CArrayList<SToken>> output (new CArrayList<SToken>());

        int i = 0;
        while(i < source->Count()) {

            if(i < source->Count() - 1) {

                if(source->Array()[i].Type == E_TOKENTYPE_ASSIGNMENT
                   && source->Array()[i + 1].Type == E_TOKENTYPE_ASSIGNMENT)
                {
                    // '=='
                    output->Add(SToken::From(E_TOKENTYPE_IDENTIFIER, SStringSlice(eqLit), source->Array()[i].SourceRef));
                    i++;
                } else if(source->Array()[i].Type == E_TOKENTYPE_LINECONTINUATION
                       && source->Array()[i + 1].Type == E_TOKENTYPE_SEMICOLON)
                {
                    // ';' are inserted where '\n' is found; but '\' can cancel
                    // it out.
                    i++;
                } else {
                    output->Add(source->Array()[i]);
                }

            } else {
                output->Add(source->Array()[i]);
            }

            i++;
        }
        
        output->Ref();
        return output;
    }

    CArrayList<SToken>* Tokenize() const
    {
        if(code.Length == 0)
            return new CArrayList<SToken>();

        Auto<CArrayList<SStringSlice>> slices (GetSlices());
        Auto<CArrayList<SToken>> tokens (SlicesToTokens(slices));
        return PostProcessTokens(tokens);
    }
};

}

CArrayList<SToken>* Tokenize(const SStringSlice& code, const CString* exeName, CLog* log)
{
    STokenizer tokenizer(code, exeName, log);
    return tokenizer.Tokenize();
}

const CString* SToken::ToString() const
{
    const char* typeStr;

    switch(this->Type) {
        case E_TOKENTYPE_LEFTPARENTHESIS: typeStr = "LEFTPARENTHESIS"; break;
        case E_TOKENTYPE_RIGHTPARENTHESIS: typeStr = "RIGHTPARENTHESIS"; break;
        case E_TOKENTYPE_LINECONTINUATION: typeStr = "LINECONTINUATION"; break;
        case E_TOKENTYPE_SEMICOLON: typeStr = "SEMICOLON"; break;
        case E_TOKENTYPE_DELIMITER: typeStr = "DELIMITER"; break;
        case E_TOKENTYPE_IDENTIFIER: typeStr = "IDENTIFIER"; break;
        case E_TOKENTYPE_NUMBERLITERAL: typeStr = "NUMBERLITERAL"; break;
        case E_TOKENTYPE_TRUELITERAL: typeStr = "TRUELITERAL"; break;
        case E_TOKENTYPE_FALSELITERAL: typeStr = "FALSELITERAL"; break;
        case E_TOKENTYPE_NOTHINGLITERAL: typeStr = "NOTHINGLITERAL"; break;
        case E_TOKENTYPE_STRINGLITERAL: typeStr = "STRINGLITERAL"; break;
        case E_TOKENTYPE_BODYLITERAL: typeStr = "BODYLITERAL"; break;
        case E_TOKENTYPE_GLOBAL: typeStr = "GLOBAL"; break;
        case E_TOKENTYPE_LOCAL: typeStr = "LOCAL"; break;
        case E_TOKENTYPE_ASSIGNMENT: typeStr = "ASSIGNMENT"; break;
        case E_TOKENTYPE_IF: typeStr = "IF"; break;
        case E_TOKENTYPE_WHILE: typeStr = "WHILE"; break;
        case E_TOKENTYPE_RETURN: typeStr = "RETURN"; break;
        case E_TOKENTYPE_TRACE: typeStr = "TRACE"; break;
        case E_TOKENTYPE_ASSERT: typeStr = "ASSERT"; break;
        case E_TOKENTYPE_YIELD: typeStr = "YIELD"; break;
        case E_TOKENTYPE_INCLUDE: typeStr = "INCLUDE"; break;
        case E_TOKENTYPE_SLEEP: typeStr = "SLEEP"; break;
        case E_TOKENTYPE_ATOMIC: typeStr = "ATOMIC"; break;
        case E_TOKENTYPE_DO: typeStr = "DO"; break;
        case E_TOKENTYPE_CANCEL: typeStr = "CANCEL"; break;
        case E_TOKENTYPE_CANCELLING: typeStr = "CANCELLING"; break;
        case E_TOKENTYPE_PREEMPT: typeStr = "PREEMPT"; break;
        case E_TOKENTYPE_NOPREEMPT: typeStr = "NOPREEMPT"; break;
        default: typeStr = "UNKNOWN (TODO)"; break;
    }

    if(this->Type == E_TOKENTYPE_NUMBERLITERAL) {

        return CString::Format("{Token: Type=%s Value=%f}",
                                           typeStr,
                                           this->NumberValue);

    } else {

        Auto<const CString> str (this->StringValue.ToString());
        return CString::Format("{Token: Type=%s Value=%o}",
                                typeStr,
                                static_cast<const CObject*>(str.Ptr()));

    }
}

} }
