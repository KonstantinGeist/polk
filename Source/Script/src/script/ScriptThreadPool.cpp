// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/ScriptThreadPool.h>
#include <core/Contract.h>
#include <core/Log.h>
#include <core/Mutex.h>
#include <core/String.h>
#include <core/Thread.h>
#include <core/WaitObject.h>
#include <reflection/MetaObject.h>
#include <collections/ArrayList.h>
#include <collections/Queue.h>
#include <script/AsyncMethodCallResult.h>
#include <script/script.h> // for constants
#include <script/ScriptValue.h>

namespace polk { namespace script {
using namespace polk::core;
using namespace polk::collections;

    // See also the comments for ExecutionTrackPrivate::m_amcResult in
    // ExecutionTrack.cpp for the overview of the asynchronous method call
    // algorithm.

    // ******************************
    //     SAsyncMethodCallRequest
    // ******************************

struct SAsyncMethodCallRequest
{
    METAVALUE Self;
    METAMETHOD* Method;
    int ArgCount;
    METAVALUE Args[POLK_MAX_MARG_COUNT];
    CAsyncMethodCallResult* AMCResult;
};
inline void POLK_REF(const SAsyncMethodCallRequest& v) { }
inline void POLK_UNREF(const SAsyncMethodCallRequest& v) { }

    // *****************************
    //     CThreadPoolThread
    // *****************************

class CThreadPoolThread final: public CThread
{
public:
    Auto<CQueue<SAsyncMethodCallRequest> > Queue;
    Auto<CMutex> QueueMutex; // not good to lock, but easy to implement
    Auto<CWaitObject> WaitObject;

    ScriptThreadPoolPrivate* PThreadPool;
    int Id;
    Auto<CLog> Log;
    
    // For quicker access by IsCurrentMethodAbandoned()
    CAsyncMethodCallResult* m_curAMCResult;

    virtual void OnStart() override;

    CThreadPoolThread(ScriptThreadPoolPrivate* pThreadPool, int id, CLog* log)
        : Queue(new CQueue<SAsyncMethodCallRequest>()),
          QueueMutex(new CMutex()),
          WaitObject(new CWaitObject()),
          PThreadPool(pThreadPool),
          Id(id),
          m_curAMCResult(nullptr)
    {
        this->Log.SetVal(log);
    }
};

    // *****************************
    //      EThreadPoolState
    // *****************************

enum EThreadPoolState
{
    E_THREADPOOLSTATE_UNSTARTED,
    E_THREADPOOLSTATE_RUNNING,
    E_THREADPOOLSTATE_ABORTED
};

    // *****************************
    //    ScriptThreadPoolPrivate
    // *****************************

struct ScriptThreadPoolPrivate
{
    Auto<CArrayList<CThreadPoolThread*> > m_workerThreads;

    // To unref stuff on the base thread: even though CAsyncMethodCallResult's
    // are thread-safe, the values they hold aren't necessarily so; if a track
    // abandons a result object, its destructor should unref the values on the
    // base thread; rerouted through this list.
    Auto<CArrayList<CAsyncMethodCallResult*> > m_amcResultListToRemove;
    Auto<CMutex> m_amcResultListMutex;

    int m_curWorkerThreadIndex; // robin round
    EThreadPoolState m_state;

    Auto<CLog> m_log;

    ScriptThreadPoolPrivate(CLog* log)
        : m_workerThreads(new CArrayList<CThreadPoolThread*>()),
          m_amcResultListToRemove(new CArrayList<CAsyncMethodCallResult*>()),
          m_amcResultListMutex(new CMutex()),
          m_curWorkerThreadIndex(0),
          m_state(E_THREADPOOLSTATE_UNSTARTED)
    {
        m_log.SetVal(log);
    }

    void start();
};

    // *****************************
    //      CScriptThreadPool
    // *****************************

CScriptThreadPool::CScriptThreadPool(int threadCount, CLog* log)
    : p (new ScriptThreadPoolPrivate(log))
{
    POLK_REQ_POS(threadCount)

    for(int i = 0; i < threadCount; i++) {
        Auto<CThreadPoolThread> workerThread (new CThreadPoolThread(p, i + 1, log));
        workerThread->SetName("Script");
        p->m_workerThreads->Add(workerThread);
    }
}

CScriptThreadPool::~CScriptThreadPool()
{
    delete p;
}

void CScriptThreadPool::Start()
{
    p->start();
}

void ScriptThreadPoolPrivate::start()
{
    if(m_state != E_THREADPOOLSTATE_UNSTARTED) {
        POLK_THROW_WITH_MSG(EC_INVALID_STATE, "Trying to start a script thread pool which was already started.");
    }

    POLK_LOG_INFO(m_log, "Script", "Launching %d worker script threads...", m_workerThreads->Count());

    for(int i = 0; i < m_workerThreads->Count(); i++) {
        m_workerThreads->Array()[i]->Start();
    }

    m_state = E_THREADPOOLSTATE_RUNNING;
}

void CScriptThreadPool::EnqueueAsyncMethodCall(const SScriptValue& self,
                                               METAMETHOD* method,
                                               int argCount,
                                               SScriptValue* args,
                                               CAsyncMethodCallResult* amcResult)
{
    if(argCount < 0 || argCount > POLK_MAX_MARG_COUNT) {
        POLK_THROW(EC_ILLEGAL_ARGUMENT);
    }
    // Checks in advance to avoid segfaulting on other threads some time later.
    POLK_REQ_PTR(method)
    POLK_REQ_PTR(amcResult)
    POLK_REQ_EQUALS(amcResult->State, E_ASYNCMETHODSTATE_NONE)

    if(p->m_state != E_THREADPOOLSTATE_RUNNING) {
        POLK_THROW_WITH_MSG(EC_INVALID_STATE, "Trying to enqueue a method call to a thread pool which is not running.");
    }

    // NOTE All to be done on the base thread, so we're allowed to ref/unref,
    // copy and such.

    // We have to use metavalues instead of SScriptValues to make sure the compiler
    // does not insert random Ref's/Unref's wherever it wants which could wreck
    // havoc to our possibly non-thread-safe reference counting.

    SAsyncMethodCallRequest request;

    // Makes sure `self` is alive for the duration of the asynchronous method
    // call.
    request.Self = *self.MetaValue();
    RefMetaValue(&request.Self);

    request.Method = method;     // Static pointer, no memory management needed.
    request.ArgCount = argCount;

    for(int i = 0; i < argCount; i++) {
        intptr_t strIntPtr;
        const CString* clone;
        bool deleted;

        switch(args[i].Type()) {
            case METATYPE_NULL:
            case METATYPE_NUMBER:
            case METATYPE_BOOL:
                // These types can be supported by a simple copy.
                request.Args[i] = *args[i].MetaValue();
                break;

            case METATYPE_OBJECT:
                // For objects, we support only strings: strings are guaranteed
                // to have no pointers to other objects, and they're clonable.
                // Cloning strings allows to avoid sharing the same reference-
                // counted string among different threads (reference counting 
                // is not necessarily thread-safe).
                strIntPtr = args[i].ObjectValue("String");
                if(strIntPtr) {
                    clone = static_cast<const CString*>((const CObject*)strIntPtr)->Clone();
                    request.Args[i] = MetaValueFromPtr("String", (void*)(CObject*)clone); // NOTE clone already preref'd
                    break;
                }
                // Falls through on error.
            default:
                // Cleanup.
                UnrefMetaValue(&request.Self, &deleted);
                // "i - 1" because did not have a chance to ref the current argument
                for(int j = 0; j < i - 1; j++) {
                    UnrefMetaValue(&request.Args[j], &deleted);
                }

                POLK_THROW_WITH_MSG(EC_NOT_IMPLEMENTED,
                                  "Asynchronous method calls support only nothing, boolean, number and string.");
                break;
        }
    }

    // Shouldn't throw anymore, meaning we can update our state.
    // The state will be read by the track next time it tries to resume.
    // When it sees WAITING, it will just yield immediately. Safe to update here
    // no matter what because we're on the track's thread and the track cannot
    // run concurrently.
    amcResult->State = E_ASYNCMETHODSTATE_WAITING;

    request.AMCResult = amcResult;
    amcResult->Ref(); // atomic ref, will be unref'd in ::DoEvents()

    // All arguments prepared (copied/preref'd), now time to enqueue them to a
    // selected worker thread.

    const int workerCount = p->m_workerThreads->Count();
    int curWorkerIndex = p->m_curWorkerThreadIndex;

    // Clamps the index just in case someone decided to call this method from
    // different threads (m_curWorkerThreadIndex update is not thread-safe).
    if(curWorkerIndex < 0)
        curWorkerIndex = 0;
    else if(curWorkerIndex >= workerCount)
        curWorkerIndex = workerCount - 1;

    // Next request will happen on the next thread in the pool.
    if(++p->m_curWorkerThreadIndex >= workerCount)
        p->m_curWorkerThreadIndex = 0; // wraps around

    auto const workerThread = p->m_workerThreads->Item(curWorkerIndex);

    // Enqueues the request under a lock.
    POLK_LOCK(workerThread->QueueMutex)
    {
        workerThread->Queue->Enqueue(request);
    }
    POLK_END_LOCK(workerThread->QueueMutex);

    // Wakes the worker thread.
    workerThread->WaitObject->Pulse();
}

void CThreadPoolThread::OnStart()
{
    SAsyncMethodCallRequest request;
    bool hasSomething = false;

    POLK_LOG_INFO(this->Log, "Script", "Worker script thread #%d started.", this->Id);

    while(this->State() != E_THREADSTATE_ABORT_REQUESTED) {

        // Timeout is required to check for E_THREADSTATE_ABORT_REQUESTED
        // for graceful thread abort.
        CThread::Wait(this->WaitObject, 32);

        // TODO should dequeue all existing items at once, or some of the items won't be
		// dequeued until the next unrelated pulse

        POLK_LOCK(this->QueueMutex)
        {
            // Just in case something is wrong about our queues.
            hasSomething = this->Queue->Count() > 0;

            if(hasSomething)
                request = this->Queue->Dequeue();
        }
        POLK_END_LOCK(this->QueueMutex);

        if(!hasSomething)
            continue;

        // Finally, the method call. Metamethods should never throw exceptions,
        // and if they do, it's the problem of the implementation. We do not
        // catch exceptions here to avoid unnecessary overhead.

        POLK_REQ_EQUALS(request.Self.type, METATYPE_OBJECT)

        METAVALUE ret;
        METARESULT res;

        if(request.AMCResult->Abandoned) {

            // When the track abandons its method call (finishes or cancelled),
            // it marks the result object as abandoned. There is no proper
            // synchronization because it's merely a hint to skip abandoned calls.

            // Emulates as if a method call happened. The result object is abandoned,
            // we do not care about it anymore, however we still need to perform
            // the usual cleanup sequence below.

            res = METARESULT_SUCCESS;
            ret = MetaValueFromNull();

        } else {

            // See ::IsCurrentMethodAbandoned()
            this->m_curAMCResult = request.AMCResult;

            res = InvokeMetaMethodDirect(&request.Self.u.asObject,
                                         request.Method,
                                         request.ArgCount,
                                         &request.Args[0],
                                         &ret);

            // m_curAMCResult should be valid only for the duration of the metamethod.
            this->m_curAMCResult = nullptr;

        }

        request.AMCResult->Result = res;

        if(res == METARESULT_SUCCESS) {
            // The returned value should have been preref'd for us.
            // The requirement for asynchronous method call implementations is
            // that the value they return should be a fresh object so that it
            // wasn't potentially owned by several threads. This allows us to
            // simply copy the metavalue without cloning, avoiding some overhead.
            request.AMCResult->ReturnValue = ret;
        }

        // Needs to keep it inside AMCResult to unref later on the base (owner)
        // thread.
        request.AMCResult->Self = request.Self;

        // Remember that we had preref'd arguments to make sure they're alive.
        // Reference objects are copies to make sure they are not shared by
        // 2 threads, so we can say that this thread owns them, which means we
        // can safely unref them, except for `self`.
        for(int i = 0; i < request.ArgCount; i++) {
            bool deleted;
            UnrefMetaValue(&request.Args[i], &deleted);
        }

		// TODO is this correct?
        std::atomic_thread_fence(std::memory_order_release);

        // The state is updated to HAS_RESULT, to be read by the track to proceed.
        // NOTE The track is reading the value concurrently.
        request.AMCResult->State = E_ASYNCMETHODSTATE_HAS_RESULT;

        // The request object has been written to, we can abandon it.
        // If it's still held by a track, it will survive. If it was abandoned
        // by the track, too, then the result object is destroyed and the return
        // value it holds is unref'd too in CAsyncMethodCallResult's destructor.
        // To make sure unref's of held values happen on the base thread, we
        // need to add this result object to another list to be processed.

        POLK_LOCK(this->PThreadPool->m_amcResultListMutex)
        {
            // Will be cleared in CScriptThreadPool::DoEvents()
            this->PThreadPool->m_amcResultListToRemove->Add(request.AMCResult);

            // The request object was ref'd manually in EnqueueAsyncMethodCall(..).
            // After this call, there should be no references to the object on
            // the worker thread anymore, it should be completely owned by the
            // base thread from now on.
            request.AMCResult->Unref();
        }
        POLK_END_LOCK(this->PThreadPool->m_amcResultListMutex);
    }

    // EnqueueAsyncMethodCall(..) refs stuff, we need to unref data of enqueued
    // methods which never had a chance to be executed due to worker abort.

    POLK_LOCK(this->QueueMutex)
    {
        while(this->Queue->Count() > 0) {
            request = this->Queue->Dequeue();

            for(int i = 0; i < request.ArgCount; i++) {
                bool deleted;
                UnrefMetaValue(&request.Args[i], &deleted);
            }

            POLK_LOCK(this->PThreadPool->m_amcResultListMutex)
            {
                // See comments above for the similar code.
                this->PThreadPool->m_amcResultListToRemove->Add(request.AMCResult);
                request.AMCResult->Unref();
            }
            POLK_END_LOCK(this->PThreadPool->m_amcResultListMutex);
            
        }
    }
    POLK_END_LOCK(this->QueueMutex);
}

bool CScriptThreadPool::IsCurrentMethodAbandoned()
{
    auto const thread = dynamic_cast<CThreadPoolThread*>(CThread::Current());

    if(!thread || !thread->m_curAMCResult) {
        POLK_THROW_WITH_MSG(EC_WRONG_THREAD, "CScriptThreadPool::IsCurrentMethodAbandoned() should be called on a thread pool thread.");
    }

    return thread->m_curAMCResult->Abandoned;
}

void CScriptThreadPool::DoEvents()
{
    // A fast probe without locking.
    if(p->m_amcResultListToRemove->Count() > 0) {

        POLK_LOCK(p->m_amcResultListMutex)
        {
            // By simply clearing the list we unref all the objects inside.
            p->m_amcResultListToRemove->Clear();
        }
        POLK_END_LOCK(p->m_amcResultListMutex);

    }
}

void CScriptThreadPool::Abort()
{
    if(p->m_state != E_THREADPOOLSTATE_RUNNING)
        return;

    for(int i = 0; i < p->m_workerThreads->Count(); i++) {
        // State flag set to E_THREADSTATE_ABORT_REQUESTED.
        p->m_workerThreads->Array()[i]->Abort();
    }

    for(int i = 0; i < p->m_workerThreads->Count(); i++) {
        // Pulses the wait object to speed it up.
        p->m_workerThreads->Array()[i]->WaitObject->Pulse();
    }

    for(int i = 0; i < p->m_workerThreads->Count(); i++) {
        // Waits for the worker threads to finish.
        CThread::Join(p->m_workerThreads->Array()[i]);
    }

    p->m_state = E_THREADPOOLSTATE_ABORTED;
}

} }
