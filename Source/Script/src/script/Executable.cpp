// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/Executable.h>
#include <core/Contract.h>
#include <core/CoreUtils.h>
#include <core/Log.h>
#include <core/String.h>
#include <core/StringBuilder.h>
#include <reflection/IdTable.h>
#include <reflection/MetaObject.h>
#include <io/BinaryReader.h>
#include <io/BinaryWriter.h>
#include <io/Stream.h>
#include <script/Debugging.h>
#include <script/Expression.h>
#include <script/ExecutableMetadata.h>
#include <script/Opcode.h>
#include <script/Parser.h>
#include <script/StringSlice.h>
#include <script/Token.h>

namespace polk { namespace script {
using namespace polk::core;
using namespace polk::reflection;
using namespace polk::collections;
using namespace polk::io;

static polk_atomic_int g_uniqueLocalUID;

static int generateLocalUID()
{
    int r = -1;

    // WARNING? A rollover is highly unlikely, because it would require
    // 2^32 executables loaded from the source to be confusable, i.e.
    // inside one compilation, or inside one deserialization, one needs
    // 2^32 executables to be created at once to have the first POTENTIAL
    // id collision. Obviosly, such a number is highly unlikely.
    while(r == -1) { // does not set to -1 because -1 means "no id set"
        r = CoreUtils::AtomicIncrement(&g_uniqueLocalUID);
    }

    return r;
}

struct ExecutablePrivate
{
    // Uniquely identifies the executable instance no matter the actual pointer.
    // Used during ident resolution while scanning the stack (see ExecutableTrackPrivate::loadIdent(..))
    // WARNING? What if there's an id collision if a track is deserialized from
    // a file into a long-running system? I think id collisions only make sense
    // if two related linked executables are confused during ident resolution, but
    // that cannot happen because that would require two executables to be loaded
    // from different sources and be linked to each other somehow. In case of
    // deserialization, old ID's will be used, which are, as before the serialization,
    // internally unique among each other, and their ID's cannot collide with ID's
    // created in the running system because they are not related in any way.
    int m_localUID;

    // Child executables should inherit the context from their parent executables
    // (for global variables).
    Auto<CExecutableMetadata> m_metadata;
    Auto<CArrayList<int> > m_rootBytecode;
    Auto<CArrayList<SSourceRef> > m_sourceRefMap;

    // A link to an executable within which this executable is defined. Note that
    // it's different from stack frames: this value is used to access locals
    // and arguments defined in outer executables; it's NOT used to return to
    // the previous frame (for that, a track has its own frame stack).
    // This invariant MUST NOT change because serialization depends on this (we
    // deserialize dummy executables with only local/argument maps in them).
    CExecutable* m_parentExecutable; // weak reference to avoid cyclic references

    // Maps string ID's to argument positions. String ID's are used to encode
    // argument names inside the bytecode stream. Argument positions are required
    // to efficiently reference arguments by offsetting the data stack upwards.
    Auto<CHashMap<int, int> > m_argumentNames;

    // Maps string ID's to alias slot indices.
    Auto<CHashMap<int, int> > m_localMap;

    Auto<CArrayList<CExecutable*> > m_childExecutables;
    Auto<const CString> m_name;

    Auto<CLog> m_log;

    // Required to pin executables during serialization to prevent them from being
    // deallocated. Used only for deserialized executables.
    Auto<CArrayList<CExecutable*> > m_pinnedExecutables;
    bool m_localVarDummy; // for debug

    // Maps bytecode indices in this executable to breakpoint handlers.
    // Created on demand only when breakpoints are actually used.
    Auto<CArrayList<CBreakpointHandler*> > m_breakpointHandlers;

    // If `localVarDummy` is true, it means the executable exists only to provide
    // local/argument data. The flag allows to not allocate unneeded structures
    // for such dummy executables.
    ExecutablePrivate(bool localVarDummy)
        : m_localUID(-1),
          m_rootBytecode(localVarDummy? nullptr: new CArrayList<int>()),
          m_sourceRefMap(localVarDummy? nullptr: new CArrayList<SSourceRef>()),
          m_parentExecutable(nullptr),
          m_argumentNames(new CHashMap<int, int>()),
          m_localMap(new CHashMap<int, int>()),
          m_childExecutables(localVarDummy? nullptr: new CArrayList<CExecutable*>()),
          m_localVarDummy(localVarDummy)
    {
    }

    ~ExecutablePrivate()
    {
        if(m_childExecutables) { // can be null if localVarDummy=true
            // If an executable is destroyed before its children, we must make
            // sure that the parent values of the children are set to null. This
            // ensures that if a detached child executable tries to scan its parent
            // when resolving an identifier, it won't try to access a dead parent
            // (see also ExecutableTrackPrivate::loadValue(..)).
            for(int i = 0; i < m_childExecutables->Count(); i++) {
                m_childExecutables->Array()[i]->p->m_parentExecutable = nullptr;
            }
        }
    }

    // Returns true if breakpoint was set, otherwise keeps searching.
    bool setBreakpointRec(int lineNumber, CBreakpointHandler* handler);

    void serializeSourceRefMap(SBinaryWriter& bw, CStream* stream, CLog* log);
    void deserializeSourceRefMap(SBinaryReader& br, CStream* stream, CLog* log);
};

CExecutable::CExecutable(const CString* name, CExecutableMetadata* ctx, CLog* log, bool localVarDummy)
    : p (new ExecutablePrivate(localVarDummy))
{
    p->m_metadata.SetVal(ctx);
    p->m_name.SetVal(name);
    p->m_log.SetVal(log);
}

CExecutable::~CExecutable()
{
    delete p;
}

int CExecutable::LocalUID() const
{
    // Verifies that the ID was set.
    assert(p->m_localUID != -1);
    return p->m_localUID;
}

const CString* CExecutable::Name() const
{
    return p->m_name;
}

CExecutableMetadata* CExecutable::Metadata() const
{
    return p->m_metadata;
}

const CArrayList<SSourceRef>* CExecutable::SourceRefMap() const
{
    return p->m_sourceRefMap;
}

const CArrayList<int>* CExecutable::RootBytecode() const
{
    return p->m_rootBytecode;
}

CHashMap<int, int>* CExecutable::ArgumentNames() const
{
    return p->m_argumentNames;
}

CHashMap<int, int>* CExecutable::LocalMap() const
{
    return p->m_localMap;
}

bool CExecutable::IsLocalVarDummy() const
{
    return p->m_localVarDummy;
}

CExecutable* CExecutable::ParentExecutable() const
{
    return p->m_parentExecutable;
}

CLog* CExecutable::Log() const
{
    return p->m_log;
}

void CExecutable::EmitOpcode(int opcode, const SSourceRef& sourceRef)
{
    p->m_rootBytecode->Add(opcode);
    p->m_sourceRefMap->Add(sourceRef);
}

int CExecutable::ReserveLocal(const SStringSlice& localName)
{
    const int stringIndex = p->m_metadata->GetStringIndex(localName);

    int localIndex;
    if(!p->m_localMap->TryGet(stringIndex, &localIndex)) {
        localIndex = p->m_localMap->Size();
        p->m_localMap->Set(stringIndex, localIndex);
    }

    return stringIndex;
}

CExecutable* CExecutable::Compile(const SStringSlice& code,
                                  const CString* name,
                                  CExecutableMetadata* ctx,
                                  CLog* log)
{
    POLK_REQ_PTR(log);

    Auto<CArrayList<SToken> > tokens (Tokenize(code, name, log));
    Auto<CExpr> rootExpr (Parse(tokens, log));

    Auto<CExecutable> exe (new CExecutable(name, ctx, log));
    exe->p->m_localUID = generateLocalUID();

    rootExpr->Emit(exe);

    POLK_REQ_EQUALS(exe->p->m_rootBytecode->Count(), exe->p->m_sourceRefMap->Count())

    exe->Ref();
    return exe;
}

CExecutable* CExecutable::Compile(const CString* _code,
                                  const CString* name,
                                  CLog* log)
{
    const SStringSlice code (_code);
    Auto<CExecutableMetadata> ctx (new CExecutableMetadata());

    return CExecutable::Compile(code, name, ctx, log);
}

int CExecutable::AddChildExecutable(CExecutable* childExe)
{
    const int index = p->m_childExecutables->Count();
    p->m_childExecutables->Add(childExe);
    childExe->p->m_parentExecutable = this;
    return index;
}

CExecutable* CExecutable::ChildExecutableAt(int index) const
{
    return p->m_childExecutables->Item(index);
}

static void indent(CStringBuilder* sb, int level)
{
    for(int i = 0; i < level; i++) {
        sb->Append(POLK_CHAR(' '));
    }
}

const CString* CExecutable::Disassemble(int level) const
{
    Auto<CStringBuilder> sb (new CStringBuilder());

    if(p->m_argumentNames->Size() > 0) {
        sb->Append("| ");

        SHashMapEnumerator<int, int> mapEnum (p->m_argumentNames);
        int k;
        while(mapEnum.MoveNext(&k, nullptr)) {
            
            Auto<const CString> str (p->m_metadata->GetString(k).ToString());
            sb->Append(str);
            sb->Append(POLK_CHAR(' '));

        }

        sb->Append("|\n");
    }

    int i = 0;
    CArrayList<int>* _bc = p->m_rootBytecode;
    const int* bc = _bc->Array();
    CExecutableMetadata* ctx = p->m_metadata;
    Auto<const CString> str;

    while(i < _bc->Count()) {
        const int opcode = bc[i];

        indent(sb, level);

        switch(opcode) {
            case E_OPCODE_NOP:
                sb->Append("NOP\n");
                i++;
                break;

            case E_OPCODE_STOREG:
                str.SetPtr(ctx->GetString(bc[i + 1]).ToString());
                sb->AppendFormat("STOREG %o\n", static_cast<const CObject*>(str.Ptr()));
                i += 2;
                break;

            case E_OPCODE_STOREL:
                str.SetPtr(ctx->GetString(bc[i + 1]).ToString());
                sb->AppendFormat("STOREL %o\n", static_cast<const CObject*>(str.Ptr()));
                i += 2;
                break;

            case E_OPCODE_LOAD:
                str.SetPtr(ctx->GetString(bc[i + 1]).ToString());
                sb->AppendFormat("LOAD %o\n", static_cast<const CObject*>(str.Ptr()));
                i += 2;
                break;

            case E_OPCODE_POP:
                sb->Append("POP\n");
                i++;
                break;

            case E_OPCODE_NUMBER:
                sb->AppendFormat("NUMBER %f\n", ctx->GetNumber(bc[i + 1]));
                i += 2;
                break;

            case E_OPCODE_STRING:
                sb->AppendFormat("STRING \"%o\"\n", static_cast<const CObject*>(ctx->PolkStringForIndex(bc[i + 1])));
                i += 2;
                break;
            
            case E_OPCODE_TRUE:
                sb->Append("TRUE\n");
                i++;
                break;

            case E_OPCODE_FALSE:
                sb->Append("FALSE\n");
                i++;
                break;

            case E_OPCODE_NOTHING:
                sb->Append("NOTHING\n");
                i++;
                break;

            case E_OPCODE_BODY:
                
                sb->Append("{");
                str.SetPtr(this->ChildExecutableAt(bc[i + 1])->Disassemble(level + 4));
                sb->Append(str);
                sb->AppendLine();
                indent(sb, level);
                sb->Append("}\n");

                i += 2;
                break;

            case E_OPCODE_CALL:
                sb->AppendFormat("CALL %d %s\n", bc[i + 1], ctx->GetBasicString(bc[i + 2]));
                i += 3;
                break;
            
            case E_OPCODE_CALLB:
                sb->AppendFormat("CALLB %d\n", bc[i + 1]);
                i += 2;
                break;
            
            case E_OPCODE_NEW:
                sb->AppendFormat("NEW %d %s\n",
                                 ctx->GetNumber(bc[i + 1]),
                                 ctx->GetBasicString(bc[i + 2]));
                i += 3;
                break;

            case E_OPCODE_YIELD:
                sb->Append("YIELD\n");
                i++;
                break;
            
            case E_OPCODE_RETURN:
                sb->AppendFormat("RETURN %d\n", bc[i + 1]);
                i += 2;
                break;
        
            case E_OPCODE_COND:
                
                sb->Append("COND:\n");
                
                indent(sb, level);
                sb->Append(".{\n");
                str.SetPtr(this->ChildExecutableAt(bc[i + 1])->Disassemble(level + 4));
                sb->Append(str);
                sb->AppendLine();
                indent(sb, level);
                sb->Append("}\n");

                i += 2;
                break;

            case E_OPCODE_JUMP:
                                        
                sb->AppendFormat("JUMP[%d]:\n", bc[i + 2]);

                indent(sb, level);
                sb->Append(".{\n");
                str.SetPtr(this->ChildExecutableAt(bc[i + 1])->Disassemble(level + 4));
                sb->Append(str);
                sb->AppendLine();
                indent(sb, level);
                sb->Append("}\n");

                i += 3;
                break;

            case E_OPCODE_TRACE:
                sb->Append("TRACE\n");
                i++;
                break;

            case E_OPCODE_ASSERT:
                str.SetPtr(ctx->GetString(bc[i + 1]).ToString());
                sb->AppendFormat("ASSERT \"%o\"", static_cast<const CObject*>(str.Ptr()));
                i += 2;
                break;

            case E_OPCODE_INCLUDE:
                sb->Append("INCLUDE\n");
                i++;
                break;

            case E_OPCODE_SLEEP:
                sb->Append("SLEEP\n");
                i++;
                break;

            case E_OPCODE_DUPNEG:
                sb->Append("DUPNEG\n");
                i++;
                break;

            case E_OPCODE_ATOMIC:
                sb->Append("ATOMIC:\n");

                indent(sb, level);
                sb->Append(".{");
                sb->AppendLine();
                str.SetPtr(this->ChildExecutableAt(bc[i + 1])->Disassemble(level + 4));
                sb->Append(str);
                sb->AppendLine();
                indent(sb, level);
                sb->Append("}");
                sb->AppendLine();

                i += 2;
                break;

            case E_OPCODE_PROTECTED:
                sb->Append("PROTECTED:\n");

                indent(sb, level);
                sb->Append(".{");
                sb->AppendLine();
                str.SetPtr(this->ChildExecutableAt(bc[i + 1])->Disassemble(level + 4));
                sb->Append(str);
                sb->AppendLine();
                indent(sb, level);
                sb->Append("}");
                sb->AppendLine();

                indent(sb, level);
                sb->Append(".{");
                sb->AppendLine();
                str.SetPtr(this->ChildExecutableAt(bc[i + 2])->Disassemble(level + 4));
                sb->Append(str);
                sb->AppendLine();
                indent(sb, level);
                sb->Append("}");
                sb->AppendLine();

                i += 3;
                break;

            case E_OPCODE_CANCEL:
                sb->Append("CANCEL\n");
                i++;
                break;

            case E_OPCODE_CANCELLING:
                sb->Append("CANCELLING\n");
                i++;
                break;

            case E_OPCODE_PREEMPT:
                sb->Append("PREEMPT\n");
                i++;
                break;

            case E_OPCODE_NOPREEMPT:
                sb->Append("NOPREEMPT\n");
                i++;
                break;

            default:
                {
                    POLK_THROW(EC_NOT_IMPLEMENTED);
                }
        }
    }

    return sb->ToString();
}

    // ************************************************
    //         Serialization/deserialization.
    // ************************************************

void ExecutablePrivate::serializeSourceRefMap(SBinaryWriter& bw, CStream* stream, CLog* log)
{
    Auto<CHashMap<SStringSlice, int> > stringTable (new CHashMap<SStringSlice, int>());
    Auto<CArrayList<SStringSlice> > stringList (new CArrayList<SStringSlice>());

    for(int i = 0; i < m_sourceRefMap->Count(); i++) {
        const SSourceRef sourceRef (m_sourceRefMap->Array()[i]);

        if(!stringTable->Contains(sourceRef.ExecutableName)) {
            stringTable->Set(sourceRef.ExecutableName, stringList->Count());
            stringList->Add(sourceRef.ExecutableName);
        }

        if(!stringTable->Contains(sourceRef.Line)) {
            stringTable->Set(sourceRef.Line, stringList->Count());
            stringList->Add(sourceRef.Line);
        }
    }

    bw.WriteInt(stringList->Count());
    for(int i = 0; i < stringList->Count(); i++) {
        Auto<const CString> str (stringList->Array()[i].ToString());
        bw.WriteUTF16(str, true); // lengthHeader=true
    }

    bw.WriteInt(m_sourceRefMap->Count());
    for(int i = 0; i < m_sourceRefMap->Count(); i++) {
        const SSourceRef sourceRef (m_sourceRefMap->Array()[i]);

        bw.WriteInt(stringTable->Item(sourceRef.ExecutableName));
        bw.WriteInt(stringTable->Item(sourceRef.Line));

        bw.WriteInt(sourceRef.LineNumber);
        bw.WriteInt(sourceRef.CharPosition);
    }
}

void ExecutablePrivate::deserializeSourceRefMap(SBinaryReader& br, CStream* stream, CLog* log)
{
    Auto<CArrayList<const CString*> > stringList (new CArrayList<const CString*>());

    const int stringTableSize = br.ReadInt();
    for(int i = 0; i < stringTableSize; i++) {
        Auto<const CString> str (br.ReadUTF16());
        stringList->Add(str);
    }

    const int sourceRefCount = br.ReadInt();
    for(int i = 0; i < sourceRefCount; i++) {
        SSourceRef sourceRef;

        const int exeNameStrId = br.ReadInt();
        sourceRef.ExecutableName = SStringSlice(stringList->Item(exeNameStrId));

        const int lineStrId = br.ReadInt();
        sourceRef.Line = SStringSlice(stringList->Item(lineStrId));

        sourceRef.LineNumber = br.ReadInt();
        sourceRef.CharPosition = br.ReadInt();

        m_sourceRefMap->Add(sourceRef);
    }
}

void CExecutable::Serialize(CStream* stream, CIdTable* idTable, CLog* log) const
{
    // We will serialize data in the order it's defined.

    SBinaryWriter bw (stream);

    // Local UID.

    assert(p->m_localUID != -1);
    bw.WriteInt(p->m_localUID);

    // Metadata.

    POLK_REQ_PTR(p->m_metadata)
    p->m_metadata->Serialize(stream, idTable, log);

    // Bytecode.

    const CArrayList<int>* const rootBytecode = p->m_rootBytecode;
    const int rbCount = rootBytecode->Count();
    bw.WriteInt(rbCount);
    for(int i = 0; i < rbCount; i++) {
        int opcode = rootBytecode->Array()[i];
        bw.WriteInt(opcode);
    }

    // Source ref map.

    p->serializeSourceRefMap(bw, stream, log);

    // Argument names.

    {
        bw.WriteInt(p->m_argumentNames->Size());

        int k, v;
        SHashMapEnumerator<int, int> argNameEnum (p->m_argumentNames);
        while(argNameEnum.MoveNext(&k, &v)) {
            bw.WriteInt(k);
            bw.WriteInt(v);
        }
    }

    // Local map.

    {
        bw.WriteInt(p->m_localMap->Size());

        int k, v;
        SHashMapEnumerator<int, int> localMapEnum (p->m_localMap);
        while(localMapEnum.MoveNext(&k, &v)) {
            bw.WriteInt(k);
            bw.WriteInt(v);
        }
    }

    // Child executables.

    const CArrayList<CExecutable*>* const children = p->m_childExecutables;
    bw.WriteInt(children->Count());
    for(int i = 0; i < children->Count(); i++) {
        const CExecutable* const child = children->Array()[i];

        child->Serialize(stream, idTable, log);
    }

    // Name.

    bw.WriteUTF16(p->m_name, true); // lengthHeader=true

    // Parent local and argument data.
    //
    // m_parentExecutable is used only to reference locals and arguments defined
    // in parent executables (it's not used to pass control back; for that, we
    // have an explicit frame stack). Which means we don't have to serialize
    // parent executables here or invent ways to link to them via some special
    // ID. Instead, we can simply serialize local/argument maps themselves and
    // then, on deserialization, create dummy executables.
    //
    // Note that if we re-serialize an already deserialized executable, then
    // we actually access a localVarDummy itself (so we should only access here
    // m_argumentNames, m_localMap and nothing else).

    const CExecutable* parent = this;
    while((parent = parent->p->m_parentExecutable) != nullptr) {

        // Marks the beginning of another parent.

        bw.WriteBool(true);

        // Local UID.

        assert(parent->p->m_localUID != -1);
        bw.WriteInt(parent->p->m_localUID);

        // Parent argument names.

        {
            const CHashMap<int, int>* const argumentMap = parent->p->m_argumentNames;
            bw.WriteInt(argumentMap->Size());

            int k, v;
            SHashMapEnumerator<int, int> argNameEnum (argumentMap);
            while(argNameEnum.MoveNext(&k, &v)) {
                bw.WriteInt(k);
                bw.WriteInt(v);
            }
        }

        // Parent local map.

        {
            const CHashMap<int, int>* const localMap = parent->p->m_localMap;
            bw.WriteInt(localMap->Size());
            int k, v;
            SHashMapEnumerator<int, int> localMapEnum (localMap);
            while(localMapEnum.MoveNext(&k, &v)) {
                bw.WriteInt(k);
                bw.WriteInt(v);
            }
        }

    }

    // Marks the end of the parent chain.
    bw.WriteBool(false);
}

CExecutable* CExecutable::Deserialize(CStream* stream, CIdTable* idTable, CLog* log)
{
    Auto<CExecutable> r (new CExecutable(nullptr, nullptr, log));
    ExecutablePrivate* pExecutable = r->p;

    SBinaryReader br (stream);

    // The UID.

    pExecutable->m_localUID = br.ReadInt();

    // Metadata.

    pExecutable->m_metadata.SetPtr(CExecutableMetadata::Deserialize(stream, idTable, log));

    // Bytecode.

    const int rbCount = br.ReadInt();
    for(int i = 0; i < rbCount; i++) {
        const int opcode = br.ReadInt();
        pExecutable->m_rootBytecode->Add(opcode);
    }

    // Source ref map.

    pExecutable->deserializeSourceRefMap(br, stream, log);

    // Argument names.

    {
        const int argNameCount = br.ReadInt();

        for(int i = 0; i < argNameCount; i++) {
            const int k = br.ReadInt();
            const int v = br.ReadInt();

            pExecutable->m_argumentNames->Set(k, v);
        }
    }

    // Local map.

    {
        const int localMapSize = br.ReadInt();

        for(int i = 0; i < localMapSize; i++) {
            const int k = br.ReadInt();
            const int v = br.ReadInt();

            pExecutable->m_localMap->Set(k, v);
        }
    }

    // Child executables.

    const int childExeCount = br.ReadInt();
    for(int i = 0; i < childExeCount; i++) {
        Auto<CExecutable> childExe (CExecutable::Deserialize(stream, idTable, log));
        r->AddChildExecutable(childExe);
    }

    // Name.

    pExecutable->m_name.SetPtr(br.ReadUTF16());

    // Parent local and argument data.
    // See the comments in the respective place of the Serialize(..) method.

    CExecutable* exeToAssignParentTo = r;

    // "true" means there's another parent in the chain.
    while(br.ReadBool()) {

        // Creates a dummy and assigns it to the current parent.

        Auto<CExecutable> dummy (new CExecutable(nullptr, pExecutable->m_metadata, log, true)); // localVarDummy=true
        assert(!exeToAssignParentTo->p->m_parentExecutable);
        exeToAssignParentTo->p->m_parentExecutable = dummy;

        // Pinns the dummy (note that all the parents are pinned to the original executable).

        if(!pExecutable->m_pinnedExecutables)
            pExecutable->m_pinnedExecutables.SetPtr(new CArrayList<CExecutable*>());
        pExecutable->m_pinnedExecutables->Add(dummy);

        // Local UID.

        dummy->p->m_localUID = br.ReadInt();

        // Argument names.

        {
            const int argNameCount = br.ReadInt();

            CHashMap<int, int>* argumentNames = dummy->p->m_argumentNames;
            for(int i = 0; i < argNameCount; i++) {
                int k = br.ReadInt();
                int v = br.ReadInt();

                argumentNames->Set(k, v);
            }
        }

        // Local map.

        {
            const int localMapSize = br.ReadInt();

            CHashMap<int, int>* localMap = dummy->p->m_localMap;
            for(int i = 0; i < localMapSize; i++) {
                const int k = br.ReadInt();
                const int v = br.ReadInt();

                localMap->Set(k, v);
            }
        }

        exeToAssignParentTo = dummy;

    }

    // **********************************************************************

    r->Ref();
    return r;
}

    // ************************************************
    //                  Debugging.
    // ************************************************

bool ExecutablePrivate::setBreakpointRec(int lineNumber, CBreakpointHandler* handler)
{
    if(m_sourceRefMap) {
        const int sz = m_sourceRefMap->Count();

        if(!m_breakpointHandlers) {
            m_breakpointHandlers.SetPtr(new CArrayList<CBreakpointHandler*>(sz));
            m_breakpointHandlers->Expand(sz, nullptr);
        }

        for(int bytecodeIndex = 0; bytecodeIndex < sz; bytecodeIndex++) {
            const SSourceRef& sourceRef = m_sourceRefMap->Array()[bytecodeIndex];

            if(sourceRef.LineNumber == lineNumber) {
                m_breakpointHandlers->Set(bytecodeIndex, handler);
                return true;
            }
        }
    }

    if(m_childExecutables) {
        for(int i = 0; i < m_childExecutables->Count(); i++) {
            CExecutable* const child = m_childExecutables->Array()[i];

            if(child->p->setBreakpointRec(lineNumber, handler))
                return true;
        }
    }

    return false;
}

void CExecutable::SetBreakpoint(int lineNumber, CBreakpointHandler* handler)
{
    p->setBreakpointRec(lineNumber, handler);
}

void CExecutable::ClearAllBreakpoints()
{
    p->m_breakpointHandlers.SetPtr(nullptr);

    const CArrayList<CExecutable*>* const children = p->m_childExecutables;
    if(children) {
        for(int i = 0; i < children->Count(); i++) {
            CExecutable* child = children->Array()[i];
            child->ClearAllBreakpoints();
        }
    }
}

CArrayList<int>* CExecutable::GetSetBreakpoints() const
{
    return nullptr; // TODO
}

bool CExecutable::TryExecuteBreakpointHandler(int bytecodeIndex)
{
    const CArrayList<CBreakpointHandler*>* const handlers = p->m_breakpointHandlers;
    if(handlers && bytecodeIndex >= 0 && bytecodeIndex < handlers->Count()) {

        CBreakpointHandler* const handler = handlers->Array()[bytecodeIndex];
        if(handler) {
            auto const sourceRefMap = p->m_sourceRefMap.Ptr();
            if(sourceRefMap) {
                SSourceRef sourceRef (sourceRefMap->Item(bytecodeIndex));

                handler->Invoke(sourceRef.LineNumber);
                return true;
            }
        }

    }

    return false;
}

const CString* CExecutable::ToString() const
{
    Auto<CStringBuilder> sb (new CStringBuilder());
    sb->AppendASCII("<executable \"");
    sb->Append(this->Name());
    sb->AppendASCII("\">");
    return sb->ToString();
}

    // ************************************************
    //                 Reflection.
    // ************************************************

#define GET_SELF auto self = static_cast<CExecutable*>((CObject*)(void*)_self->idptr);

METAMETHODIMPL(Executable_Disassemble_impl)
{
    GET_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = STRINGPTR_TO_METAVALUE(self->Disassemble());
    METAGUARD_END
}

METASERIALIMPL(Ex_serialize)
{
    METAGUARD_BEGIN
        auto const ex = static_cast<CExecutable*>((CObject*)(void*)obj->idptr);

        // TODO ?
        Auto<CStream> daStream (CStream::CreateFromMetaStream(stream));
        Auto<CIdTable> daIdTable (CIdTable::CreateFromMetaIdTable(idTable));

        ex->Serialize(daStream, daIdTable);
    METAGUARD_END
}

METADESERIALIMPL(Ex_deserialize)
{
    METAGUARD_BEGIN
        // TODO ?
        Auto<CStream> daStream (CStream::CreateFromMetaStream(stream));
        Auto<CIdTable> daIdTable (CIdTable::CreateFromMetaIdTable(idTable));

        CExecutable* const ex = CExecutable::Deserialize(daStream, daIdTable);

        *out_ret = MetaValueFromPtr("_Ex", (void*)static_cast<CObject*>(ex));
    METAGUARD_END
}

void __InitExecutable()
{
    BEGIN_VTABLE(1)
        DEFINE_VMETHOD("Disassemble", Executable_Disassemble_impl, 0)
    END_VTABLE    

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    // NOTE The name is short for quicker comparisons, as the interpreter
    // is going to check the types a lot when doing calls.
    metaClass.name = "_Ex";
    metaClass.serialimpl = Ex_serialize;
    metaClass.deserialimpl = Ex_deserialize;
    metaClass.parent = GetMetaClassByName("Object");

    RegisterMetaClass(&metaClass);
}

void __DeinitExecutable()
{
    // Nothing.
}

} }

// ********************************************************************
