// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/SharedRef.h>
#include <core/Exception.h>
#include <reflection/MetaObject.h>

namespace polk { namespace script {
using namespace polk::core;

    // ********************
    //   Metaobject desc.
    // ********************

METACTORIMPL(SharedRef_ctor)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        auto const r = new CSharedRef();
        *out_ret = (intptr_t)(void*)(CObject*)r;
    METAGUARD_END
}

METAMETHODIMPL(SharedRef_Set_impl)
{
    VERIFY_NARG(1)

    METAGUARD_BEGIN
        auto const self = static_cast<CSharedRef*>((CObject*)(void*)_self->idptr);

        self->Set(SScriptValue::FromMetaValue(&args[0]));
    METAGUARD_END
}

METAMETHODIMPL(SharedRef_Get_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        auto const self = static_cast<CSharedRef*>((CObject*)(void*)_self->idptr);

        SScriptValue ret (self->Get());

        RefMetaValue(ret.MetaValue());
        *out_ret = *ret.MetaValue();

    METAGUARD_END
}

METASERIALIMPL(SharedRef_serialize)
{
    METAGUARD_BEGIN
        auto const self = static_cast<CSharedRef*>((CObject*)(void*)obj->idptr);

        const SScriptValue value (self->Get());
        value.Serialize(stream, idTable);
    METAGUARD_END
}

METADESERIALIMPL(SharedRef_deserialize)
{
    METAGUARD_BEGIN
        const SScriptValue value (SScriptValue::Deserialize(stream, idTable));

        auto const r = new CSharedRef();
        r->Set(value);
        *out_ret = MetaValueFromPtr("SharedRef", (void*)static_cast<CObject*>(r));
    METAGUARD_END
}

void __InitSharedRef()
{
    #define SHAREDREF_METHOD_COUNT 2
    static METAMETHOD g_sharedRef_vtable[SHAREDREF_METHOD_COUNT];

    g_sharedRef_vtable[0].name = "Set";
    g_sharedRef_vtable[0].impl = SharedRef_Set_impl;

    g_sharedRef_vtable[1].name = "Get";
    g_sharedRef_vtable[1].impl = SharedRef_Get_impl;

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    metaClass.name = "SharedRef";
    metaClass.ctor = SharedRef_ctor;
    metaClass.serialimpl = SharedRef_serialize;
    metaClass.deserialimpl = SharedRef_deserialize;
    metaClass.vtable = g_sharedRef_vtable;
    metaClass.methodCount = SHAREDREF_METHOD_COUNT;
    metaClass.parent = GetMetaClassByName("Object");

    RegisterMetaClass(&metaClass);
}

void __DeinitSharedRef()
{
    // Nothing.
}

} }
