// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/ExecutableMetadata.h>
#include <collections/HashMap.h>
#include <io/BinaryReader.h>
#include <io/BinaryWriter.h>
#include <io/Stream.h>

namespace polk { namespace script {
using namespace polk::core;
using namespace polk::reflection;
using namespace polk::collections;
using namespace polk::io;

struct ExecutableMetadataPrivate
{
    // Only grows and has duplicates, we don't want to compare floats.
    Auto<CArrayList<float> > m_numberRegistry;

    Auto<CHashMap<SStringSlice, int> > m_stringRegistry;
    Auto<CHashMap<int, SStringSlice> > m_stringRegistry2;

    Auto<CHashMap<SStringSlice, int> > m_basicStringRegistry;
    Auto<CHashMap<int, void*> > m_basicStringRegistry2;

    Auto<CHashMap<SStringSlice, int> > m_polkStringRegistry;
    Auto<CHashMap<int, const CString*> > m_polkStringRegistry2;

    ExecutableMetadataPrivate()
        : m_numberRegistry(new CArrayList<float>()),
          m_stringRegistry(new CHashMap<SStringSlice, int>()),
          m_stringRegistry2(new CHashMap<int, SStringSlice>()),
          m_basicStringRegistry(new CHashMap<SStringSlice, int>()),
          m_basicStringRegistry2(new CHashMap<int, void*>()),
          m_polkStringRegistry(new CHashMap<SStringSlice, int>()),
          m_polkStringRegistry2(new CHashMap<int, const CString*>())
    {
    }

    ~ExecutableMetadataPrivate();
};

CExecutableMetadata::CExecutableMetadata()
    : p (new ExecutableMetadataPrivate())
{
    
}

CExecutableMetadata::~CExecutableMetadata()
{
    delete p;
}

int CExecutableMetadata::GetStringIndex(const CString* str) const
{
    const SStringSlice slice (str);

    return this->GetStringIndex(slice);
}

int CExecutableMetadata::GetStringIndex(const SStringSlice& slice) const
{
    int index;

    if(!p->m_stringRegistry->TryGet(slice, &index)) {
        index = p->m_stringRegistry->Size();

        p->m_stringRegistry->Set(slice, index);
        p->m_stringRegistry2->Set(index, slice);
    }

    return index;
}

CArrayList<SStringSlice>* CExecutableMetadata::GetStrings() const
{
    auto const r = new CArrayList<SStringSlice>();

    {
        SHashMapEnumerator<SStringSlice, int> mapEnum (p->m_stringRegistry);
        SStringSlice slice;
        while(mapEnum.MoveNext(&slice, nullptr)) {
            r->Add(slice);
        }
    }

    {
        SHashMapEnumerator<SStringSlice, int> mapEnum (p->m_basicStringRegistry);
        SStringSlice slice;
        while(mapEnum.MoveNext(&slice, nullptr)) {
            r->Add(slice);
        }
    }

    {
        SHashMapEnumerator<SStringSlice, int> mapEnum (p->m_polkStringRegistry);
        SStringSlice slice;
        while(mapEnum.MoveNext(&slice, nullptr)) {
            r->Add(slice);
        }
    }

    return r;
}

SStringSlice CExecutableMetadata::GetString(int index) const
{
    return p->m_stringRegistry2->Get(index);
}

int CExecutableMetadata::GetNumberIndex(float number) const
{
    const int index = p->m_numberRegistry->Count();
    p->m_numberRegistry->Add(number);
    return index;
}

float CExecutableMetadata::GetNumber(int index) const
{
    return p->m_numberRegistry->Item(index);
}

int CExecutableMetadata::GetBasicStringIndex(const SStringSlice& slice) const
{
    int index;

    if(!p->m_basicStringRegistry->TryGet(slice, &index)) {
        index = p->m_basicStringRegistry->Size();

        p->m_basicStringRegistry->Set(slice, index);

        Auto<const CString> daStr (slice.ToString());
        char* const cStr = daStr->ToUtf8();

        p->m_basicStringRegistry2->Set(index, cStr);
    }

    return index;
}

ExecutableMetadataPrivate::~ExecutableMetadataPrivate()
{
    SHashMapEnumerator<int, void*> mapEnum (m_basicStringRegistry2);
    void* v;
    while(mapEnum.MoveNext(nullptr, &v)) {
        CString::FreeUtf8(static_cast<char*>(v));
    }
}

const char* CExecutableMetadata::GetBasicString(int index) const
{
    return static_cast<const char*>(p->m_basicStringRegistry2->Item(index));
}

int CExecutableMetadata::GetPolkStringIndex(const SStringSlice& slice) const
{
    int index;

    if(!p->m_polkStringRegistry->TryGet(slice, &index)) {
        index = p->m_polkStringRegistry->Size();

        p->m_polkStringRegistry->Set(slice, index);

        Auto<const CString> daStr (slice.ToString());
        p->m_polkStringRegistry2->Set(index, daStr);
    }

    return index;
}

const CString* CExecutableMetadata::PolkStringForIndex(int index) const
{
    return p->m_polkStringRegistry2->Item(index);
}

    // **********************************
    //   Serialization/deserialization.
    // **********************************

void CExecutableMetadata::Serialize(CStream* stream, CIdTable* idTable, CLog* log) const
{
    SBinaryWriter bw (stream);

    // Number registry.
    {
        CArrayList<float>* numberRegistry = p->m_numberRegistry;
        bw.WriteInt(numberRegistry->Count());
        for(int i = 0; i < numberRegistry->Count(); i++) {
            const float f = numberRegistry->Array()[i];
            bw.WriteFloat(f);
        }
    }

    // String registry.
    {
        const CHashMap<SStringSlice, int>* const stringRegistry = p->m_stringRegistry;
        bw.WriteInt(stringRegistry->Size());
        SHashMapEnumerator<SStringSlice, int> mapEnum (stringRegistry);
        SStringSlice k;
        int v;
        while(mapEnum.MoveNext(&k, &v)) {
            // TODO
            Auto<const CString> kStr (k.ToString());

            bw.WriteUTF16(kStr, true); // lengthHeader=true
            bw.WriteInt(v);
        }
    }

    // String registry 2.
    {
        const CHashMap<int, SStringSlice>* const stringRegistry2 = p->m_stringRegistry2;
        bw.WriteInt(stringRegistry2->Size());
        SHashMapEnumerator<int, SStringSlice> mapEnum (stringRegistry2);
        int k;
        SStringSlice v;
        while(mapEnum.MoveNext(&k, &v)) {
            bw.WriteInt(k);

            // TODO
            Auto<const CString> vStr (v.ToString());
            bw.WriteUTF16(vStr, true); // lengthHeader=true
        }
    }

    // cStringRegistry
    {
        const CHashMap<SStringSlice, int>* const cStringRegistry = p->m_basicStringRegistry;
        bw.WriteInt(cStringRegistry->Size());
        SHashMapEnumerator<SStringSlice, int> mapEnum (cStringRegistry);
        SStringSlice k;
        int v;
        while(mapEnum.MoveNext(&k, &v)) {
            // TODO
            Auto<const CString> kStr (k.ToString());
            bw.WriteUTF16(kStr, true); // lengthHeader=true

            bw.WriteInt(v);
        }
    }

    // cStringRegistry2
    {
        const CHashMap<int, void*>* const cStringRegistry2 = p->m_basicStringRegistry2;
        bw.WriteInt(cStringRegistry2->Size());
        SHashMapEnumerator<int, void*> mapEnum (cStringRegistry2);
        int k;
        void* v;
        while(mapEnum.MoveNext(&k, &v)) {
            bw.WriteInt(k);

            auto cs = static_cast<char*>(v);
            bw.WriteUTF8(cs, strlen(cs), true); // lengthHeader=true
        }
    }

    // tStringRegistry
    {
        const CHashMap<SStringSlice, int>* const tStringRegistry = p->m_polkStringRegistry;
        bw.WriteInt(tStringRegistry->Size());
        SHashMapEnumerator<SStringSlice, int> mapEnum (tStringRegistry);
        SStringSlice k;
        int v;
        while(mapEnum.MoveNext(&k, &v)) {
            // TODO
            Auto<const CString> kStr (k.ToString());
            bw.WriteUTF16(kStr, true); // lengthHeader=true

            bw.WriteInt(v);
        }
    }

    // tStringRegistry2
    {
        const CHashMap<int, const CString*>* const tStringRegistry2 = p->m_polkStringRegistry2;
        bw.WriteInt(tStringRegistry2->Size());
        SHashMapEnumerator<int, const CString*> mapEnum (tStringRegistry2);
        int k;
        const CString* v;
        while(mapEnum.MoveNext(&k, &v)) {
            bw.WriteInt(k);
            bw.WriteUTF16(v, true); // lengthHeader=true
        }
    }
}

CExecutableMetadata* CExecutableMetadata::Deserialize(CStream* stream, CIdTable* idTable, CLog* log)
{
    Auto<CExecutableMetadata> r (new CExecutableMetadata());
    ExecutableMetadataPrivate* pMetadata = r->p;

    SBinaryReader br (stream);

    // Number registry.
    {
        const int count = br.ReadInt();
        POLK_REQ_NOT_NEG(count)

        CArrayList<float>* numberRegistry = pMetadata->m_numberRegistry;
        for(int i = 0; i < count; i++) {
            const float f = br.ReadFloat();
            numberRegistry->Add(f);
        }
    }

    // String registry.
    {
        const int count = br.ReadInt();
        POLK_REQ_NOT_NEG(count)

        CHashMap<SStringSlice, int>* const stringRegistry = pMetadata->m_stringRegistry;
        for(int i = 0; i < count; i++) {
            Auto<const CString> str (br.ReadUTF16());
            const SStringSlice strSlice (str);

            const int v = br.ReadInt();

            stringRegistry->Set(strSlice, v);
        }
    }

    // String registry 2.
    {
        const int count = br.ReadInt();
        POLK_REQ_NOT_NEG(count)

        CHashMap<int, SStringSlice>* const stringRegistry2 = pMetadata->m_stringRegistry2;
        for(int i = 0; i < count; i++) {
            const int k = br.ReadInt();

            Auto<const CString> str (br.ReadUTF16());
            const SStringSlice strSlice (str);

            stringRegistry2->Set(k, strSlice);
        }
    }

    // cStringRegistry
    {
        const int count = br.ReadInt();
        POLK_REQ_NOT_NEG(count)

        CHashMap<SStringSlice, int>* const cStringRegistry = pMetadata->m_basicStringRegistry;
        for(int i = 0; i < count; i++) {
            Auto<const CString> str (br.ReadUTF16());
            const SStringSlice strSlice (str);

            const int v = br.ReadInt();

            cStringRegistry->Set(strSlice, v);
        }
    }

    // cStringRegistry2
    {
        const int count = br.ReadInt();
        POLK_REQ_NOT_NEG(count)

        CHashMap<int, void*>* const cStringRegistry2 = pMetadata->m_basicStringRegistry2;
        for(int i = 0; i < count; i++) {
            const int k = br.ReadInt();
            auto const cs = br.ReadUTF8(); // will be deleted in the ctor

            cStringRegistry2->Set(k, cs);
        }
    }

    // tStringRegistry
    {
        const int count = br.ReadInt();
        POLK_REQ_NOT_NEG(count)

        CHashMap<SStringSlice, int>* const tStringRegistry = pMetadata->m_polkStringRegistry;
        for(int i = 0; i < count; i++) {
            Auto<const CString> str (br.ReadUTF16());
            const SStringSlice strSlice (str);

            const int v = br.ReadInt();

            tStringRegistry->Set(strSlice, v);
        }
    }

    // tStringRegistry2
    {
        const int count = br.ReadInt();
        POLK_REQ_NOT_NEG(count)

        CHashMap<int, const CString*>* const tStringRegistry2 = pMetadata->m_polkStringRegistry2;
        for(int i = 0; i < count; i++) {
            const int k = br.ReadInt();
            Auto<const CString> str (br.ReadUTF16());

            tStringRegistry2->Set(k, str);
        }
    }

    r->Ref();
    return r;
}

} }
