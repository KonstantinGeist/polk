// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/StringSlice.h>
#include <core/Contract.h>
#include <core/Log.h>
#include <script/CharCategory.h>
#include <script/SourceRef.h>

namespace polk { namespace script {
using namespace polk::core;

SStringSlice::SStringSlice(int dummy)
    : Start(0), Length(0)
{
}

SStringSlice::SStringSlice(const CString* source)
    : Start(0), Length(source->Length())
{
    this->VSource.SetObject(source);
}

SStringSlice::SStringSlice(const CString* source, int start, int length)
    : Start(start), Length(length)
{
    this->VSource.SetObject(source);
}

SStringSlice::SStringSlice(const char* cSource)
{
    Auto<const CString> daSource (CString::FromUtf8(cSource));

    this->Start = 0;
    this->Length = daSource->Length();
    this->VSource.SetObject(daSource);
}

SStringSlice::SStringSlice(const SStringSlice& inputSS, int start, int length)
    : VSource (inputSS.VSource), Start (inputSS.Start + start), Length(length)
{
}

bool SStringSlice::Equals(const SStringSlice& other) const
{
    if(this->Length != other.Length)
        return false;

    for(int i = 0; i < this->Length; i++) {
        if(this->Source()->Chars()[this->Start + i] != other.Source()->Chars()[other.Start + i])
            return false;
    }

    return true;
}

bool SStringSlice::Equals(const polk::core::CString* str) const
{
    if(str->Length() != this->Length)
        return false;

    for(int i = 0; i < str->Length(); i++) {
        if(str->Chars()[i] != this->Source()->Chars()[this->Start + i])
            return false;
    }

    return true;
}

bool SStringSlice::EqualsASCII(const char* str) const
{
    if(strlen(str) != (size_t)this->Length)
        return false;

    for(int i = 0; i < this->Length; i++) {
        if((polk_char16)str[i] != this->Source()->Chars()[this->Start + i])
            return false;
    }

    return true;
}

int SStringSlice::GetHashCode() const
{
    int hashCode = 0;

    for(int i = 0; i < this->Length; i++) {
        hashCode += (int)this->Source()->Chars()[this->Start + i] * 13;
    }

    return hashCode;
}

polk_char16 SStringSlice::CharAt(int index) const
{
    POLK_REQ_RANGE_D(index, 0, this->Length)

    return this->Source()->Chars()[this->Start + index];
}

bool SStringSlice::IsBody() const
{
    return this->Length >= 2
        && this->Source()->Chars()[this->Start] == POLK_CHAR('{')
        && this->Source()->Chars()[this->Start + this->Length - 1] == POLK_CHAR('}');
}

SStringSlice SStringSlice::ToBody() const
{
    // Just gets rid of the curly brackets.

    SStringSlice copy = *this;
    copy.Start++;
    copy.Length -= 2;

    return copy;
}

bool SStringSlice::IsStringLiteral() const
{
    return this->Length >= 2
        && this->Source()->Chars()[this->Start] == POLK_CHAR('"')
        && this->Source()->Chars()[this->Start + this->Length - 1] == POLK_CHAR('"');
}

// Should be checked with IsStringLiteralFirst.
SStringSlice SStringSlice::ToStringLiteral() const //-V524
{
    // Just gets rid of the quotes.

    SStringSlice copy = *this;
    copy.Start++;
    copy.Length -= 2;

    return copy;
}

bool SStringSlice::IsIdentifier() const
{
    if(this->Length == 0) {
        return false;
    }

    const polk_char16 firstChar = this->CharAt(0);

    if(this->Length == 1) {
        // Math operators are treated as identifiers so that it was possible to generalize
        // them as method calls on number objects.
        switch(firstChar) {
            case POLK_CHAR('+'):
            case POLK_CHAR('-'):
            case POLK_CHAR('*'):
            case POLK_CHAR('/'):
            case POLK_CHAR('<'):
            case POLK_CHAR('>'):
            case POLK_CHAR('!'): // also the executable invoker
                return true;
            default:
                break;
        }
    }

    if(GetCharCategory(firstChar) != E_CHARCATEGORY_LETTER) {
        return false;
    }

    for(int i = 1; i < this->Length; i++) {
        const polk_char16 c = this->CharAt(i);
        if(c == POLK_CHAR('.')) {
            return false;
        }

        const ECharCategory cat = GetCharCategory(c);
        if(cat != E_CHARCATEGORY_LETTER && cat != E_CHARCATEGORY_DIGIT) {
            return false;
        }
    }

    return true;
}

ETokenType SStringSlice::ToKeywordOrIdentifier() const
{
    if(this->EqualsASCII("global")) {
        return E_TOKENTYPE_GLOBAL;
    } else if(this->EqualsASCII("local")) {
        return E_TOKENTYPE_LOCAL;
    } else if(this->EqualsASCII("if")) {
        return E_TOKENTYPE_IF;
    } else if(this->EqualsASCII("while")) {
        return E_TOKENTYPE_WHILE;
    } else if(this->EqualsASCII("yield")) {
        return E_TOKENTYPE_YIELD;
    } else if(this->EqualsASCII("return")) {
        return E_TOKENTYPE_RETURN;
    } else if(this->EqualsASCII("true")) {
        return E_TOKENTYPE_TRUELITERAL;
    } else if(this->EqualsASCII("false")) {
        return E_TOKENTYPE_FALSELITERAL;
    } else if(this->EqualsASCII("nothing")) {
        return E_TOKENTYPE_NOTHINGLITERAL;
    } else if(this->EqualsASCII("do")) {
        return E_TOKENTYPE_DO;
    } else if(this->EqualsASCII("cancel")) {
        return E_TOKENTYPE_CANCEL;
    } else if(this->EqualsASCII("cancelling")) {
        return E_TOKENTYPE_CANCELLING;
    } else if(this->EqualsASCII("trace")) {
        return E_TOKENTYPE_TRACE;
    } else if(this->EqualsASCII("assert")) {
        return E_TOKENTYPE_ASSERT;
    } else if(this->EqualsASCII("include")) {
        return E_TOKENTYPE_INCLUDE;
    } else if(this->EqualsASCII("sleep")) {
        return E_TOKENTYPE_SLEEP;
    } else if(this->EqualsASCII("atomic")) {
        return E_TOKENTYPE_ATOMIC;
    } else if(this->EqualsASCII("preempt")) {
        return E_TOKENTYPE_PREEMPT;
    } else if(this->EqualsASCII("nopreempt")) {
        return E_TOKENTYPE_NOPREEMPT;
    } else {
        return E_TOKENTYPE_IDENTIFIER;
    }
}

ETokenType SStringSlice::ToControlCharacter(const SSourceRef& sourceRef,
                                            CLog* log) const
{
    const polk_char16* const cs = this->Source()->Chars();

    if(this->Length == 1 && cs[this->Start] == POLK_CHAR('='))
        return E_TOKENTYPE_ASSIGNMENT;
    else if(this->Length == 1 && cs[this->Start] == POLK_CHAR('('))
        return E_TOKENTYPE_LEFTPARENTHESIS;
    else if(this->Length == 1 && cs[this->Start] == POLK_CHAR(')'))
        return E_TOKENTYPE_RIGHTPARENTHESIS;
    else if((this->Length == 1 && cs[this->Start] == POLK_CHAR(';'))
         || (this->Length == 1 && cs[this->Start] == POLK_CHAR('\n')))
        return E_TOKENTYPE_SEMICOLON;
    else if(this->Length == 1 && cs[this->Start] == POLK_CHAR('\\'))
        return E_TOKENTYPE_LINECONTINUATION;
    else if(this->Length == 1 && cs[this->Start] == POLK_CHAR('|'))
        return E_TOKENTYPE_DELIMITER;
    else {
        Auto<const CString> thisAsStr (this->ToString());
        Auto<const CString> sourceRefAsStr (sourceRef.ToString());
        Auto<const CString> msg (CString::Format("Unexpected character sequence: '%o'\n%o",
                                                 static_cast<const CObject*>(thisAsStr.Ptr()),
                                                 static_cast<const CObject*>(sourceRefAsStr.Ptr())));

        log->Write(E_LOGPRIORITY_ERROR, "Script", msg);

        POLK_THROW_WITH_MSG(EC_EXECUTION_ERROR, "Unexpected character sequence.");
        return E_TOKENTYPE_LEFTPARENTHESIS; // just to shut up
    }
}

SNullable<float> SStringSlice::TryAsNumber() const
{
    if(this->Length == 0) {
        return SNullable<float>();
    }

    const polk_char16 firstChar = this->CharAt(0);
    if(GetCharCategory(firstChar) != E_CHARCATEGORY_DIGIT) {
        return SNullable<float>();
    }

    Auto<const CString> str (this->ToString());
    float r;
    if(str->TryParseFloat(&r)) {
        return r;
    } else {
        return SNullable<float>();
    }
}

const CString* SStringSlice::ToString() const
{
    if(this->Length == 0) {
        return CString::CreateEmptyString();
    } else {
        return this->Source()->Substring(this->Start, this->Length);
    }
}

} }
