// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/Reflection.h>
#include <core/String.h>
#include <script/ScriptList.h>

using namespace polk::core;
using namespace polk::collections;

namespace polk { namespace script {
    
    // CClass

CClass::CClass(METACLASS* pClass)
    : m_pClass(pClass)
{
    assert(m_pClass);
}

METACLASS* CClass::MetaClass() const
{
    return m_pClass;
}

const CString* CClass::GetName() const
{
    return CString::FromUtf8(m_pClass->name);
}

bool CClass::IsStatic() const
{
    return m_pClass->flags & METACLASSFLAGS_STATIC;
}

CMethod* CClass::GetMethod(const CString* name) const
{
    Utf8Auto cName (name->ToUtf8());
    METAMETHOD* pMethod = GetMetaMethod(m_pClass, cName);
    if(!pMethod) {
        return nullptr;
    }
    return new CMethod(pMethod, m_pClass);
}

CArrayList<CMethod*>* CClass::GetMethods() const
{
    Auto<CArrayList<CMethod*>> r (new CArrayList<CMethod*>());

    for(int i = 0; i < m_pClass->methodCount; i++) {
        METAMETHOD* pMethod = &m_pClass->vtable[i];

        Auto<CMethod> method (new CMethod(pMethod, m_pClass));
        r->Add(method);
    }

    r->Ref();
    return r;
}

const CString* CClass::ToString() const
{
    Auto<const CString> name (this->GetName());
    return CString::Format("<class \"%o\">", static_cast<const CObject*>(name.Ptr()));
}

    // CMethod

CMethod::CMethod(METAMETHOD* pMethod, METACLASS* pClass)
    : m_pMethod(pMethod), m_pClass(pClass)
{
}

const CString* CMethod::GetName() const
{
    return CString::FromUtf8(m_pMethod->name);
}

bool CMethod::IsAsync() const
{
    return m_pMethod->flags & METAMETHODFLAGS_ASYNC;
}

METARESULT CMethod::Invoke(int nArg, METAVALUE* args, METAVALUE* out_ret)
{
    if(m_pClass->flags & METACLASSFLAGS_STATIC) {
        return InvokeMetaMethodDirect(nullptr, m_pMethod, nArg, args, out_ret);
    } else {
        // The first argument is the target of invocation.
        if(nArg == 0) {
            return METARESULT_PARAMETER_MISMATCH;
        }
        if(args[0].type != METATYPE_OBJECT) {
            return METARESULT_TYPE_ERROR;
        }
        METAOBJECT* self = &args[0].u.asObject;
        if(!IsInstanceOf(self, m_pClass->name)) {
            return METARESULT_TYPE_ERROR;
        }

        return InvokeMetaMethodDirect(self, m_pMethod, nArg - 1, args + 1, out_ret);
    }
}

const CString* CMethod::ToString() const
{
    Auto<const CString> methodName (this->GetName());
    Auto<const CString> className (CString::FromUtf8(m_pClass->name));

    return CString::Format("<method \"%o %o\">",
        static_cast<const CObject*>(className.Ptr()),
        static_cast<const CObject*>(methodName.Ptr()));
}

namespace Reflection {

CArrayList<CClass*>* GetClasses()
{
    const int size = GetMetaClasses(nullptr);
    const char** names = new const char*[size];
    GetMetaClasses(names);
    Auto<CArrayList<CClass*>> r (new CArrayList<CClass*>());
    for(int i = 0; i < size; i++) {
        METACLASS* metaClass = GetMetaClassByName(names[i]);
        Auto<CClass> klass (new CClass(metaClass));
        r->Add(klass);
    }
    delete[] names;
    r->Ref();
    return r;
}

}

// *************************
    // ***************
    //   Reflection.
    // ***************
// *************************

METAMETHODIMPL(Reflection_GetClasses)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        Auto<CArrayList<CClass*>> classes (Reflection::GetClasses());
        Auto<CScriptList> output (new CScriptList());

        for(int i = 0; i < classes->Count(); i++) {
            CClass* klass = classes->Array()[i];
            const SScriptValue value (SScriptValue::FromIdPtr("Class", (intptr_t)(void*)static_cast<CObject*>(klass)));
            output->Add(value);
        }

        *out_ret = MetaValueFromVal("List", (void*)static_cast<CObject*>(output.Ptr()));
    METAGUARD_END
}

METAMETHODIMPL(Reflection_GetClass)
{
    VERIFY_NARG(1)

    METAVALUE* const nameArg = &args[0];
    VERIFY_ARGOBJ(nameArg, "String")

    METAGUARD_BEGIN
        Utf8Auto cName (METAVALUE_TO_STRINGPTR(nameArg)->ToUtf8());
        METACLASS* metaClass = GetMetaClassByName(cName);
        if(metaClass) {
            auto r = new CClass(metaClass);
            *out_ret = MetaValueFromPtr("Class", (void*)static_cast<CObject*>(r));
        }
    METAGUARD_END
}

#define GET_CLASS_SELF auto self = static_cast<CClass*>((CObject*)(void*)_self->idptr);

METAMETHODIMPL(Class_GetName_impl)
{
    GET_CLASS_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = STRINGPTR_TO_METAVALUE(self->GetName());
    METAGUARD_END
}

METAMETHODIMPL(Class_IsStatic_impl)
{
    GET_CLASS_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = MetaValueFromBoolean(self->IsStatic());
    METAGUARD_END
}

METAMETHODIMPL(Class_CreateInstance_impl)
{
    GET_CLASS_SELF
    if(self->IsStatic()) {
        return METARESULT_MISSING_CTOR;
    }

    METAVALUE ret;
    const auto res = NewMetaObject(self->MetaClass()->name, nArg, args, &ret);
    *out_ret = ret;
    return res;
}

METAMETHODIMPL(Class_GetMethod_impl)
{
    GET_CLASS_SELF

    VERIFY_NARG(1)

    METAVALUE* const nameArg = &args[0];
    VERIFY_ARGOBJ(nameArg, "String")

    METAGUARD_BEGIN
        Auto<CMethod> method (self->GetMethod(METAVALUE_TO_STRINGPTR(nameArg)));
        if(method) {
            *out_ret = MetaValueFromVal("Method", (void*)static_cast<CObject*>(method.Ptr()));
        }
    METAGUARD_END
}

METAMETHODIMPL(Class_GetMethods_impl)
{
    GET_CLASS_SELF

    VERIFY_NARG(0)

    METAGUARD_BEGIN
        Auto<CArrayList<CMethod*>> methods (self->GetMethods());
        Auto<CScriptList> output (new CScriptList());

        for(int i = 0; i < methods->Count(); i++) {
            CMethod* method = methods->Array()[i];
            const SScriptValue value (SScriptValue::FromIdPtr("Method", (intptr_t)(void*)static_cast<CObject*>(method)));
            output->Add(value);
        }

        *out_ret = MetaValueFromVal("List", (void*)static_cast<CObject*>(output.Ptr()));
    METAGUARD_END
}

METASERIALIMPL(Class_serialize)
{
    METAGUARD_BEGIN
        const auto self = static_cast<CClass*>((CObject*)(void*)(obj->idptr));

        const char* name = self->MetaClass()->name;
        const int len = strlen(name);
        METARESULT res;

        res = stream->writeimpl(stream, &len, sizeof(len));
        if(res != METARESULT_SUCCESS)
            return res;

        res = stream->writeimpl(stream, &name[0], sizeof(char) * len);
        if(res != METARESULT_SUCCESS)
            return res;
    METAGUARD_END
}

METADESERIALIMPL(Class_deserialize)
{
    METAGUARD_BEGIN
        METARESULT res;
        int len;

        res = stream->readimpl(stream, &len, sizeof(int));
        if(res != METARESULT_SUCCESS)
            return res;
        if(len <= 0)
            return METARESULT_INTERNAL_EXCEPTION; // TODO better error

        char* cs = new char[len];
        res = stream->readimpl(stream, &cs[0], sizeof(char) * len);
        if(res != METARESULT_SUCCESS)
            return res;

        METACLASS* metaClass = GetMetaClassByName(cs);
        delete[] cs;
        if(!metaClass) {
            return METARESULT_UNKNOWN_CLASS;
        }

        auto r = new CClass(metaClass);
        *out_ret = MetaValueFromPtr("Class", (void*)static_cast<CObject*>(r));
    METAGUARD_END
}

#define GET_METHOD_SELF auto self = static_cast<CMethod*>((CObject*)(void*)_self->idptr);

METAMETHODIMPL(Method_GetName_impl)
{
    GET_METHOD_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = STRINGPTR_TO_METAVALUE(self->GetName());
    METAGUARD_END
}

METAMETHODIMPL(Method_IsAsync_impl)
{
    GET_METHOD_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = MetaValueFromBoolean(self->IsAsync());
    METAGUARD_END
}

METAMETHODIMPL(Method_Invoke_impl)
{
    GET_METHOD_SELF
    return self->Invoke(nArg, args, out_ret);
}

METASERIALIMPL(Method_serialize)
{
    return METARESULT_NOT_SUPPORTED; // TODO
}

METADESERIALIMPL(Method_deserialize)
{
    return METARESULT_NOT_SUPPORTED; // TODO
}

static void __InitClass()
{
    BEGIN_VTABLE(5)
        DEFINE_VMETHOD("GetName", Class_GetName_impl, 0)
        DEFINE_VMETHOD("IsStatic", Class_IsStatic_impl, 0)
        DEFINE_VMETHOD("CreateInstance", Class_CreateInstance_impl, 0)
        DEFINE_VMETHOD("GetMethod", Class_GetMethod_impl, 0)
        DEFINE_VMETHOD("GetMethods", Class_GetMethods_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "Class";
    metaClass.parent = GetMetaClassByName("Object");
    metaClass.serialimpl = Class_serialize;
    metaClass.deserialimpl = Class_deserialize;
    POLK_REQ_PTR(metaClass.parent);

    RegisterMetaClass(&metaClass);
}

static void __InitMethod()
{
    BEGIN_VTABLE(3)
        DEFINE_VMETHOD("GetName", Method_GetName_impl, 0)
        DEFINE_VMETHOD("Invoke", Method_Invoke_impl, 0)
        DEFINE_VMETHOD("IsAsync", Method_IsAsync_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "Method";
    metaClass.parent = GetMetaClassByName("Object");
    metaClass.serialimpl = Method_serialize;
    metaClass.deserialimpl = Method_deserialize;
    POLK_REQ_PTR(metaClass.parent);

    RegisterMetaClass(&metaClass);
}

void __InitReflection()
{
    BEGIN_VTABLE(2)
        DEFINE_VMETHOD("GetClasses", Reflection_GetClasses, 0)
        DEFINE_VMETHOD("GetClass", Reflection_GetClass, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "Reflection";
    metaClass.flags = METACLASSFLAGS_STATIC;

    RegisterMetaClass(&metaClass);
    
    __InitClass();
    __InitMethod();
}

void __DeinitReflection()
{
    // Nothing.
}

} }
