// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/ExecutionTrack.h>
#include <core/Application.h>
#include <core/Console.h>
#include <core/Contract.h>
#include <core/Delegate.h>
#include <core/Log.h>
#include <core/Nullable.h>
#include <core/String.h>
#include <core/StringBuilder.h>
#include <reflection/IdTable.h>
#include <io/BinaryReader.h>
#include <io/BinaryWriter.h>
#include <io/Stream.h>
#include <script/AsyncMethodCallResult.h>
#include <script/Executable.h>
#include <script/ExecutableMetadata.h>
#include <script/ExecutionTrackMethods.h>
#include <script/Opcode.h>
#include <script/script.h> // for constants
#include <script/ScriptStackTrace.h>
#include <script/ScriptThreadPool.h>
#include <script/SourceRef.h>
#include <script/StringSlice.h>

namespace polk { namespace script {
using namespace polk::core;
using namespace polk::reflection;
using namespace polk::collections;
using namespace polk::io;

class CExecutableEventHandler;

    // *********************
    //   CVariableContext
    // *********************

CVariableContext* CVariableContext::Clone() const
{
    auto r = new CVariableContext();
    r->Import(this);
    return r;
}

struct SExecutableFrame
{
    CExecutable* Executable;
    int BcPtr; // pointers into the bytecode of the exe's bytecode

    // The original data ptr is recorded before a frame starts. This allows us
    // to take care of situations when stack code leaks stack slots. On stack
    // frame teardown, we can unwind all superfluous data frames and return to
    // the original state.
    // Also useful to remember the start of the stack frame to use as a reference
    // to access local variables up the stack with offsets.
    int DataStackPtrOnEnter;

    int ArgCount;

    // Used to signal that control just returned from a child executable. Child
    // executables should return to the parent executable at the same bc pointer
    // as before, i.e. landing on the same opcode (such as `cond`, `jump` or `callb`)
    // To differentiate between the cases "need to call into child exe" and
    // "returned from child exe and must process the result", we use this flag.
    // This is similar to the longjump idiom in C or the fork idiom in Linux.
    bool JustReturned;

    SExecutableFrame()
        : Executable(nullptr),
          BcPtr(0), DataStackPtrOnEnter(0),
          ArgCount(0), JustReturned(false)
    {
    }

    SExecutableFrame(CExecutable* executable, int bcPtr)
        : Executable(executable),
          BcPtr(bcPtr), DataStackPtrOnEnter(0),
          ArgCount(0), JustReturned(false)
    {
    }
};

    // *************************
    //   ExecutionTrackPrivate
    // *************************

struct ExecutionTrackPrivate
{
    CExecutionTrack* m_public;

    // Execution stack.
    #define FRAMESTACK_SIZE 25
    SExecutableFrame m_frameStack[FRAMESTACK_SIZE];
    int m_frameStackPtr;

    // Data stack.
    #define DATASTACK_SIZE 128
    SScriptValue m_dataStack[DATASTACK_SIZE];
    int m_dataStackPtr;

    Auto<CVariableContext> m_vars; // NOTE Shared with child executables.
    Auto<CHashMap<const CString*, const CString*> > m_includedCache;

    Auto<CSourceRegistry> m_sourceRegistry;

    // Used for backward communication from child to parent.
    SScriptValue m_retValue;

    int m_quantum;
    bool m_preempt; // to support preempt/nopreempt bytecodes
                    // NOTE m_preempt==true doesn't mean it's actually preempted
                    // unless m_quantum is also > 0

    // To expand variables in included scripts, we create temporary tracks and
    // interpret them. It is possible that two scripts reference each other,
    // which is prone to stack overflows. To solve this, we mark temporary
    // subtracks with the include level. If it's larger than a threshold, we fail.
    int m_includeLevel;

    // Used to implement E_OPCODE_SLEEP.
    SNullable<polk_long> m_timeToWakeUp;

    Auto<CArrayList<void*> > m_registeredEventHandlers;

    SVariant m_customData;
    Auto<const CString> m_name;

    // ATOMIC opcode increases the atomic counter every time the current track
    // transitions into a child executable marked as atomic. When it exits, the
    // atomic counter is decreased. The track can yield automatically or via
    // `yield` only if atomicCounter == 0.
    int m_atomicCounter;

    // `cancelled` is set to true by a cancel statement or programmatically.
    // However, cancellation is postponed till the track reaches one of the `do`
    // blocks. This is required because some operations should be guaranteed to
    // run to the end. Also it's important not to cancel inside an atomic block.
    // After all is set, the track acknowledges cancellation by setting
    // `cancelling` to true.
    bool m_cancelled, m_cancelling;

    // doBlockCounter is updated because we need to postpone cancellation till
    // the first `do` block.
    //
    // undoBlockCounter is updated to compare it to doBlockCounter: if doBlockCounter
    // is bigger than undoBlockCounter, then we're currently inside a `do` block,
    // otherwise in an `undo` block. Used to postpone `cancel` operations in
    // immediate `undo` blocks. Note that the counter is not updated after transition
    // to cancelling, as it doesn't matter anymore.
    int m_doBlockCounter, m_undoBlockCounter;

    // IMPLEMENTATION INFO on asynchronous method calls in one place.
    // Only native methods can be asynchronous. They are used to avoid blocking
    // on long-running operations. All a native metamethod needs is to be marked
    // as METAMETHODFLAGS_ASYNC. The engine has a special code path for it.
    // The engine first clones the arguments to a special buffer. This is required
    // because the engine is most likely compiled with single-threaded reference
    // counting and we do not know if a value is shared by other concurrently running
    // code on the main thread. For booleans and numbers, it's a simple copy. As
    // for reference objects, only strings are supported. Only for strings we can
    // guarantee that the cloned copy is not shared by other code. So, basically,
    // asynchronous methods so far only support 4 types: float, boolean, string,
    // and nothing. The `self` object pointer is sent, too (see below) and it's
    // the responsibility of the native method implementation to keep it thread-safe.
    // The `self` object should be preref'd to keep it allocated for the duration
    // of the asynchronous method. We can unref it again on the main thread once
    // the asynchronous method finishes (see CScriptThreadPool::Update(). This way
    // we guarantee that the object is alive and we ref/unref it safely, unless
    // the object is shared among tracks and there's another asynchronous method
    // being called at the same time which manipulates the same reference count.
    // After the arguments are copied, we mark the result object as E_ASYNCMETHODSTATE_WAITING.
    // The value should be atomic. Then we send the argument buffer, together with
    // the object pointer and the direct method pointer (because the indirect
    // InvokeMetaMethod(..) is not thread-safe) to a thread pool. The thread pool
    // should be preregistered on track creation, so that it was possible for several
    // tracks to reuse the same thread pool. Note that to avoid cyclic references,
    // tracks refer only to CAsyncMethodCallResult's instead of the whole tracks.
    // As far as the original main thread is concerned, after sending the arguments
    // to the pooled thread, it should simply preempt the track (i.e. ::Resume()
    // exits). Each time ::Resume() is called, it will land inside the asynchronous
    // call instruction again because we did not update the instruction pointer,
    // and it should check if the state is still E_ASYNCMETHODSTATE_WAITING. If
    // it is, then ::Resume() should immediately quit: the asynchronous method is
    // still performing, nothing to do. Note that this makes sense only for
    // automatically preempted tracks which are preempted to perform other things
    // such as rendering. Otherwise,  while(track->Resume()) {} will just block
    // defeating the whole point of asynchronous method calls.
    // All the thread pool does is invoke the passed metamethod with the passed
    // arguments. After the metamethod exits, we know the returned value. We copy
    // this returned value to the result object's ReturnValue. It is thread safe
    // because we do not allow the track to resume because the state is still
    // E_ASYNCMETHODSTATE_WAITING. After updating the return value, it should set
    // the state to E_ASYNCMETHODSTATE_HAS_RESULT. Adds a memory fence in between
    // just in case. The thread pool has finished its job and can work with
    // other asychronous requests. We also should have a requirement that a native
    // asynchronous method implementation should return objects not possibly shared
    // by other threads -- to avoid unneeded copies.
    // Back on the main thread, when the track attempts to resume again, it will see
    // that the state is not E_ASYNCMETHODSTATE_WAITING, therefore it will resume.
    // Bytecode pointer was not updated by the call, so the interpreter will land
    // on the same instruction which started it all. However, this time, the state
    // is E_ASYNCMETHODSTATE_HAS_RESULT rather than E_ASYNCMETHODSTATE_NONE. The
    // interpreter will simply push m_amcResult->ReturnValue to the data stack,
    // set the state back to E_ASYNCMETHODSTATE_NONE and return.
    // Note that if the original track was abandoned before the thread pool had
    // a chance to return, then the thread pool will simply update the field of
    // of an abandoned CAsyncMethodCallResult object.
    // The benefit of all this is that asynchronous method calls are absolutely
    // transparent to the scripting language: from the view point of it, they're
    // just ordinary method calls, which although have a "strange" quirk of forcing
    // the track to yield to other tracks for some time.
    Auto<CAsyncMethodCallResult> m_amcResult;

    Auto<CScriptThreadPool> m_threadPool;

    // Pins executables here after track deserialization.
    Auto<CArrayList<CExecutable*> > m_pinnedExecutables;

    Auto<CExecutionTrackMethods> m_methods;

    bool m_debuggingEnabled;

    ExecutionTrackPrivate(CExecutionTrack* public_)
        : m_public(public_),
          m_frameStackPtr(0), m_dataStackPtr(0),
          m_includedCache(new CHashMap<const CString*, const CString*>()),
          m_quantum(0), m_preempt(true),
          m_includeLevel(0),
          m_atomicCounter(0),
          m_cancelled(false), m_cancelling(false),
          m_doBlockCounter(0), m_undoBlockCounter(0),
          m_amcResult(new CAsyncMethodCallResult()),
          m_debuggingEnabled(false)
    {
    }

    void initStaticClasses();

    // Returns the current (top) frame.
    SExecutableFrame* frame();
    CExecutable* executable();
    CExecutableMetadata* metadata();

    SSourceRef getSourceRef();

    bool tryFixExToEventHandler(METAVALUE* metaValue);
    void transitionToChildExe(CExecutable* childExe);

    void pushFrame(const SExecutableFrame& value);
    void popFrame();
    void pushValue(const SScriptValue& value);
    SScriptValue popValue();
    void performDupNeg();
    SScriptValue offsetStack(int dataStackPtrOnEnter, int offset);
    CScriptStackTrace* getStackTrace() const;

    void performTrace(const SScriptValue& value);
    void performAssert(const SScriptValue& value, const SStringSlice& msg);
    void performInclude(const SScriptValue& source);
    polk_long tickCount() const;
    bool performSleep(const SScriptValue& value);

    // returns true if async call
    bool performBoolCall(bool self, const char* methodName, int argCount, SScriptValue* args);
    bool performNumberCall(float self, const char* methodName, int argCount, SScriptValue* args);
    bool performNothingCall(const char* methodName, int argCount, SScriptValue* args);
    bool performObjectCall(const SScriptValue& self, const char* methodName, int argCount, SScriptValue* args);
    bool performCall(const SScriptValue& self, const char* methodName, int argCount, SScriptValue* args);

    void performNew(const char* className, int argCount, SScriptValue* args);

    void loadIdent(int identIndex);
    void performStoreLocal(int identIndex, const SScriptValue& value);

    void performCond(int condIndex);
    void performReturn(int retCount);
    void performJump(int childExeIndex, int jumpTargetOffset);
    void performProtected(int doExeIndex, int undoExeIndex);
    void performAtomic(int childExeIndex);
    void performCallB(int argCount);

    bool beforeAtomic() const;
    bool resume();

    void throwNewSourceException(const char* msg, const SSourceRef& src, ...);

    // CVariableContext of spawned tracks inside callbacks are not ref'd to
    // avoid circular references, but it can lead to access violation if 
    // parent track is destroyed (together with the variable context) while
    // a child still makes use of this. To warn against this, parents remember
    // their spawned children (weak references, CExecutableEventHandler themselves
    // write to the list). When the parent track is destroyed, child tracks'
    // parents are cleared, so that when on the next call, the callback will
    // gracefully warn against this. Hosting code should invent better ways
    // to deal with it, I guess the parent track should never be deleted
    // in certain cases (such as when a script is run to assign callbacks to UI),
    // and has to be kept around.
    void registerEventHandler(CExecutableEventHandler* handler);
    void deregisterEventHandler(CExecutableEventHandler* handler);
    void clearEventHandlerParents();

    void serialize(CStream* stream, CLog* log) const;
};

    // **************
    //   Ctor/dtor.
    // **************

CExecutionTrack::CExecutionTrack(CExecutable* exe)
    : p (new ExecutionTrackPrivate(this))
{
    POLK_REQ_PTR(exe)

    p->m_vars.SetPtr(new CVariableContext());

    p->transitionToChildExe(exe);

    // If it's a new, fresh variable context (which is the case here), creates
    // pseudo-variables to access static classes.
    // WARNING Should follow after transitionToChildExe(..) because depends
    // on metadata of the current executable (on top of the stack) due to the
    // dependency on SetVariable(..)
    p->initStaticClasses();
}

CExecutionTrack::CExecutionTrack(CExecutable* exe, CVariableContext* varContext)
    : p (new ExecutionTrackPrivate(this))
{
    p->m_vars.SetVal(varContext);

    if(exe) // can be null of constructed during deserialization
        p->transitionToChildExe(exe);
}

CExecutionTrack::~CExecutionTrack()
{
    p->clearEventHandlerParents();

    // Abandons the result object.
    // See CAsyncMethodCallResult::Abandoned for more info.
    p->m_amcResult->Abandoned = 1;

    delete p;
}

void ExecutionTrackPrivate::initStaticClasses()
{
    const int count = GetMetaClasses(nullptr);
    const char** const classes = new const char*[count];
    GetMetaClasses(classes);

    for(int i = 0; i < count; i++) {
        const METACLASS* const klass = GetMetaClassByName(classes[i]);

        if(klass->flags & METACLASSFLAGS_STATIC) {
            Auto<const CString> daClass (CString::FromUtf8(klass->name));
            const SStringSlice slice (daClass);

            SScriptValue value;
            value.SetObject(klass->name, 0); // null object because static class
            // NOTE value.IsStatic() now should return true

            // Do not override existing variables (probably set by the user).
            if(!m_public->HasVariable(slice))
                m_public->SetVariable(slice, value);
        }
    }

    delete [] classes;
}

void CExecutionTrack::SetMethods(CExecutionTrackMethods* methods)
{
    p->m_methods.SetVal(methods);
}

void ExecutionTrackPrivate::throwNewSourceException(const char* msg, const SSourceRef& src, ...)
{
    va_list vl;
    va_start(vl, src);

    Auto<const CString> daMsg (CString::FormatImpl(msg, vl));

    va_end(vl);

    Auto<CStringBuilder> sb (new CStringBuilder());
    sb->Append(daMsg);

    Auto<const CString> srcAsStr (src.ToString());
    sb->Append(POLK_CHAR('\n'));
    sb->Append(srcAsStr);

    sb->Append("Stack trace:\n");
    Auto<CScriptStackTrace> stackTrace (getStackTrace());
    Auto<const CString> stackTraceStr (stackTrace->ToString());
    sb->Append(stackTraceStr);

    Auto<const CString> finalMsg (sb->ToString());

    // FIX executable() tries to use frame() which will stack overflow if this
    // function (throwNewSourceException(..)) was called exactly because frame()
    // complained.
    if(m_frameStackPtr == 0) {
        Console::WriteLine(finalMsg); // last resort
    } else {
        executable()->Log()->Write(E_LOGPRIORITY_ERROR, "Script", finalMsg);
    }

    POLK_THROW_WITH_MSG(EC_EXECUTION_ERROR, "Runtime error");
}

CExecutable* CExecutionTrack::TopExecutable() const
{
    if(p->m_frameStackPtr == 0)
        return nullptr;

    SExecutableFrame* const frame = &p->m_frameStack[p->m_frameStackPtr - 1];
    CExecutable* r = frame->Executable;

    while(r->ParentExecutable())
        r = r->ParentExecutable();

    return r;
}

CSourceRegistry* CExecutionTrack::SourceRegistry() const
{
    return p->m_sourceRegistry;
}

void CExecutionTrack::SetSourceRegistry(CSourceRegistry* registry)
{
    p->m_sourceRegistry.SetVal(registry);
}

void CExecutionTrack::SetThreadPool(CScriptThreadPool* threadPool)
{
    p->m_threadPool.SetVal(threadPool);
}

CScriptThreadPool* CExecutionTrack::ThreadPool() const
{
    return p->m_threadPool;
}

CVariableContext* CExecutionTrack::VariableContext() const
{
    return p->m_vars;
}

SScriptValue CExecutionTrack::ReturnValue() const
{
    return p->m_retValue;
}

void CExecutionTrack::SetVariable(const SStringSlice& name, const SScriptValue& value)
{
    // See the same for GetVariable(..)
    if(p->m_frameStackPtr == 0)
        return;

    auto const md = p->metadata();
    const int varIndex = md->GetStringIndex(name);

    p->m_vars->Set(varIndex, value);
}

SScriptValue CExecutionTrack::GetVariable(const SStringSlice& name) const
{
    // Variables are extracted based on frames (mappings "variable name" => "variable index"
    // are stored in metadata which is bound to executable frames). If the track
    // finished, there are no more frames: no variables can be identified. Sooner
    // or later, it would crash with no frame error. Instead, we preventively
    // check if it's the case and simply return a null value (logically, the value
    // was erased because the track finished execution.)
    if(p->m_frameStackPtr == 0)
        return SScriptValue::FromNothing();

    auto const md = p->metadata();
    const int varIndex = md->GetStringIndex(name);

    SScriptValue r;

    if(!p->m_vars->TryGet(varIndex, &r))
        r = SScriptValue::FromNothing();

    return r;
}

bool CExecutionTrack::HasVariable(const SStringSlice& name) const
{
    const SScriptValue value (this->GetVariable(name));

    return value.Type() != METATYPE_NULL;
}

CHashMap<SStringSlice, SScriptValue>* CExecutionTrack::GetVariables() const
{
    // See CExecutionTrack::GetVariable(..)
    if(p->m_frameStackPtr == 0)
        return nullptr;

    auto const md = p->metadata();
    Auto<CArrayList<SStringSlice> > strings (md->GetStrings());

    auto const r = new CHashMap<SStringSlice, SScriptValue>();

    for(int i = 0; i < strings->Count(); i++) {
        const int varIndex = md->GetStringIndex(strings->Array()[i]);
        SScriptValue value;

        if(p->m_vars->TryGet(varIndex, &value)) {
            r->Set(strings->Array()[i], value);
        }
    }

    return r;
}

void CExecutionTrack::SetData(const SVariant& v)
{
     p->m_customData = v;
}

SVariant CExecutionTrack::Data() const
{
    return p->m_customData;
}

void CExecutionTrack::SetName(const CString* name) const
{
    p->m_name.SetVal(name);
}

const CString* CExecutionTrack::Name() const
{
    return p->m_name;
}

void CExecutionTrack::SetQuantum(int quantum)
{
    POLK_REQ_NOT_NEG(quantum);

    p->m_quantum = quantum;
}

int CExecutionTrack::Quantum() const
{
    return p->m_quantum;
}

bool CExecutionTrack::IsPreemptable() const
{
    return p->m_quantum > 0;
}

SExecutableFrame* ExecutionTrackPrivate::frame()
{
    if(m_frameStackPtr == 0) {
        // FIX Can't use getSourceRef() because it uses frame() itself, which
        // would stack overflow. We have to forge an empty source ref ourselves.
        SSourceRef sourceRef;
        sourceRef.Line = SStringSlice("unknown line");

        throwNewSourceException("Attempt to access empty executable stack.", sourceRef);
    }

    return &m_frameStack[m_frameStackPtr - 1];
}

CExecutable* ExecutionTrackPrivate::executable()
{
    return frame()->Executable;
}

CExecutableMetadata* ExecutionTrackPrivate::metadata()
{
    return frame()->Executable->Metadata();
}

SSourceRef ExecutionTrackPrivate::getSourceRef()
{
    const SExecutableFrame* const curFrame = frame();

    return curFrame->Executable->SourceRefMap()->Item(curFrame->BcPtr);
}

    // ********************************************
    //     Hosting code => polk callback.
    // ********************************************

    // CExecutableEventHandler

class CExecutableEventHandler final: public CDelegate<SEventArgs>
{
private:
    Auto<CExecutable> m_exe;
    CExecutionTrack* m_parent;
    Auto<CExecutionTrackMethods> m_methods;
    Auto<CVariableContext> m_clonedVarCtx;
    Auto<CScriptThreadPool> m_threadPool;

public:
    CExecutableEventHandler(CExecutable* exe, CExecutionTrack* parent)
    {
        m_exe.SetVal(exe);
        m_parent = parent;

        m_methods.SetVal(parent->p->m_methods);
        m_threadPool.SetVal(parent->p->m_threadPool);

        // Adds ourselves to the list of detachables.
        m_parent->p->registerEventHandler(this);
    }

    virtual ~CExecutableEventHandler()
    {
        // Deregister ourselves from the list of event handlers of the parent.
        // Note that the parent value may have been nulled out by the parent
        // itself on destruction.
        if(m_parent) {
            m_parent->p->deregisterEventHandler(this);
        }
    }

    void ClearParent()
    {
        POLK_REQ_PTR(m_parent);

        // Clones the var context if the parent is removed, this solves cyclic
        // references.
        m_clonedVarCtx.SetPtr(m_parent->VariableContext()->Clone());

        m_parent = nullptr;
    }

    virtual void Invoke(const CObject* sender, SEventArgs& e) override
    {          
        // NOTE Doesn't allow scripting exceptions to propagate outside of the
        // wrapper, as we don't know how hosting code works.
        try {
            // We've considered it potentially error-prone to reuse the same
            // track, so we now recreate a new one instead. Better invest into
            // creating very lightweight tracks.
            Auto<CExecutionTrack> subTrack (new CExecutionTrack(m_exe,
                                                                m_parent? m_parent->VariableContext(): m_clonedVarCtx));

            // Inherits methods and the thread pool.
            subTrack->SetMethods(m_methods);
            subTrack->SetThreadPool(m_threadPool);

            // As stated above, we should not allow the code to yield, because
            // we're running inside arbitrary hosting code which most likely has
            // no idea about our yields.
            //
            // WARNING Interestingly, this makes the code to fully block the
            // current OS thread on a sleep E_OPCODE_
            while(subTrack->Resume()) { }

        } catch(const SException& e) {

            POLK_LOG_ERROR_S(m_exe->Log(),
                          "Script",
                          "Swallowed exception in a callback implemented in polk.");

        }
    }

    virtual const CString* ToString() const override
    {
        return m_exe->ToString();
    }
};

void ExecutionTrackPrivate::registerEventHandler(CExecutableEventHandler* track)
{
    if(!m_registeredEventHandlers)
        m_registeredEventHandlers.SetPtr(new CArrayList<void*>());

    m_registeredEventHandlers->Add(track);
}

void ExecutionTrackPrivate::deregisterEventHandler(CExecutableEventHandler* handler)
{
    if(m_registeredEventHandlers) {
        m_registeredEventHandlers->Remove(handler);
    }
}

// Called on CExecutionTrack destruction to remove event handlers' references to
// this parent. That will tell the event handler objects to keep track of
// CVariableContext on their own now.
void ExecutionTrackPrivate::clearEventHandlerParents()
{
    if(m_registeredEventHandlers) {
        for(int i = 0; i < m_registeredEventHandlers->Count(); i++) {
            static_cast<CExecutableEventHandler*>(m_registeredEventHandlers->Array()[i])->ClearParent();
        }
    }
}

// We don't want to expose CExecutables to the metaobject system, it should rather
// be oblivious and see them as usual EventHandlers. Modifies the value in place
// if it's an executable.
//
// This method creates an event handler which wraps a child executable,
// which is required to allow us to use callback mechanisms defined in hosting
// code. Due to the fact that interpretation is automatically yieldable at any
// point, we cannot reuse the same track, because this code is to be called from
// hosting C/C++ code, and we must ensure that the callback is completed without
// yielding, because C/C++ has no concept of preemption.
// Moreover, we have no GC, relying on reference counting, and the original
// (current) executable may finish before the callback is called. Separating
// callbacks to be called from C/C++ into separate tracks (albeit with shared
// variables) allows us to decouple code fragments from each other, allowing them
// to get released gradually, depending on the current use.
//
// NOTE Returns true if the argument was fixed.
// NOTE The replaced value is ref'd, the calling code should make sure the value
// is unref'd somewhere eventually.
bool ExecutionTrackPrivate::tryFixExToEventHandler(METAVALUE* metaValue)
{
    if(metaValue->type == METATYPE_OBJECT
    && strcmp(metaValue->u.asObject.klass->name, "_Ex") == 0)
    {
        auto const exe = static_cast<CExecutable*>((CObject*)(void*)metaValue->u.asObject.idptr);
        CObject* const del = new CExecutableEventHandler(exe, m_public);

        *metaValue = MetaValueFromPtr("EventHandler", del);
        return true;
    } else {
        return false;
    }
}

    // *************************************

void ExecutionTrackPrivate::transitionToChildExe(CExecutable* childExe)
{
    SExecutableFrame childFrame (childExe, 0);
    childFrame.DataStackPtrOnEnter = m_dataStackPtr;

    // Locals are stored on the stack, so we need to reserve space for them.
    // Note that the space is reserved even if parent executables contain
    // similarly named locals. This is used as a safe guard if a child executable
    // detaches from the parent executable which gets collected -- code which
    // uses locals then would still work.
    SScriptValue nothing;
    const int localCount = childExe->LocalMap()->Size();
    for(int i = 0; i < localCount; i++) {
        pushValue(nothing);
    }

    pushFrame(childFrame);
}

void ExecutionTrackPrivate::pushFrame(const SExecutableFrame& value)
{
    if(m_frameStackPtr == FRAMESTACK_SIZE - 1) {
        throwNewSourceException("Executable stack overflow.", getSourceRef());
    }

    m_frameStack[m_frameStackPtr++] = value;
}

void ExecutionTrackPrivate::popFrame()
{
    if(m_frameStackPtr == 0) {
        throwNewSourceException("Attempt to access empty executable stack.", getSourceRef());
    }

    m_frameStackPtr--;
}

void ExecutionTrackPrivate::pushValue(const SScriptValue& value)
{
    if(m_dataStackPtr == DATASTACK_SIZE - 1) {
        throwNewSourceException("Data stack overflow.", getSourceRef());
    }

    m_dataStack[m_dataStackPtr++] = value;
}

SScriptValue ExecutionTrackPrivate::popValue()
{
    if(m_dataStackPtr == 0) {
        throwNewSourceException("Data stack underflow.", getSourceRef());
    }

    m_dataStackPtr--;

    const SScriptValue r = m_dataStack[m_dataStackPtr];

    // Nullifies the value on the stack to avoid having dangling garbage.
    m_dataStack[m_dataStackPtr].SetNothing();

    return r;
}

void ExecutionTrackPrivate::performDupNeg()
{
    if(m_dataStackPtr == 0) {
        throwNewSourceException("Data stack underflow.", getSourceRef());
    }
    if(m_dataStackPtr == DATASTACK_SIZE - 1) {
        throwNewSourceException("Data stack overflow.", getSourceRef());
    }

    SScriptValue value = m_dataStack[m_dataStackPtr - 1];
    if(value.Type() != METATYPE_BOOL) {
        throwNewSourceException("Expected a boolean.", getSourceRef());
    }

    // Negation in question.
    value.SetBool(!value.BoolValue());

    m_dataStack[m_dataStackPtr++] = value;
}

SScriptValue ExecutionTrackPrivate::offsetStack(int dataStackPtrOnEnter, int offset)
{
    const int dataStackPtrOffset = dataStackPtrOnEnter + offset;

    if(dataStackPtrOffset < 0) {
        throwNewSourceException("Stack underflow (in offsetStack(..)).", getSourceRef());
    }
    if(dataStackPtrOffset >= DATASTACK_SIZE) {
        throwNewSourceException("Stack overflow (in offsetStack(..)).", getSourceRef());
    }

    return m_dataStack[dataStackPtrOffset];
}

CScriptStackTrace* CExecutionTrack::GetStackTrace() const
{
    return p->getStackTrace();
}

CScriptStackTrace* ExecutionTrackPrivate::getStackTrace() const
{
    Auto<CArrayList<SSourceRef> > sourceRefs (new CArrayList<SSourceRef>());

    if(m_frameStackPtr > 0) {

        for(int i = m_frameStackPtr - 1; i >= 0; i--) {
            const SExecutableFrame* const frame = &m_frameStack[i];
            const auto sourceRefMap = frame->Executable->SourceRefMap();

            // Can be equal or higher to signalize stop, need to check for that.
            if(frame->BcPtr < sourceRefMap->Count()) {
                sourceRefs->Add(sourceRefMap->Item(frame->BcPtr));
            }
        }

    }

    return new CScriptStackTrace(sourceRefs);
}

void ExecutionTrackPrivate::performTrace(const SScriptValue& value)
{
    Auto<const CString> vStr (value.ToString());
    Auto<const CString> sourceRefStr (getSourceRef().ToString());

    Auto<const CString> finalStr (CString::Format("TRACE %o\n%o",
                                                    static_cast<const CObject*>(vStr.Ptr()),
                                                    static_cast<const CObject*>(sourceRefStr.Ptr())));

    executable()->Log()->Write(E_LOGPRIORITY_DEBUG, "Script", finalStr);

    m_retValue = SScriptValue(); // Empty.
}

void ExecutionTrackPrivate::performAssert(const SScriptValue& value, const SStringSlice& msg)
{
    if(value.Type() != METATYPE_BOOL) {
        throwNewSourceException("`assert` expects a boolean argument.", getSourceRef());
    }

    if(!value.BoolValue()) {
        Auto<const CString> msgAsStr (msg.ToString());
        Auto<const CString> finalStr (CString::Format("ASSERT:\n%o", static_cast<const CObject*>(msgAsStr.Ptr())));

        executable()->Log()->Write(E_LOGPRIORITY_ERROR, "Script", finalStr);
    }

    m_retValue = SScriptValue(); // Empty.
}

void ExecutionTrackPrivate::performInclude(const SScriptValue& source)
{
    if(source.Type() != METATYPE_OBJECT) {
        throwNewSourceException("`include` expects an object argument.", getSourceRef());
    }

    Auto<const CString> stringValue (source.ToString());

    // Variables already imported.
    if(m_includedCache->Contains(stringValue))
        return;

    Auto<const CString> includedSource (m_sourceRegistry? m_sourceRegistry->LoadSource(stringValue): nullptr);
    if(!includedSource) {
        throwNewSourceException("Could not include source '%o' (not found).",
                                getSourceRef(),
                                static_cast<const CObject*>(stringValue.Ptr()));
    }

    if(m_includeLevel == 10) {
        throwNewSourceException("Include level too deep (%d).",
                                getSourceRef(),
                                m_includeLevel);
    }

    // The included source needs to be interpreted to expand the variables, before
    // we can import. Note also that inclusion happens separately per executable,
    // to better isolate executables from one another.

    try
    {
        Auto<CExecutable> includedExe (CExecutable::Compile(SStringSlice(includedSource),
                                                            stringValue,
                                                            metadata(),
                                                            executable()->Log()));
        if(!includedExe) {
            throwNewSourceException("Error while interpreting included source.", getSourceRef());
        }

        Auto<CExecutionTrack> includedTrack (new CExecutionTrack(includedExe, m_vars));
        // Inherits methods.
        includedTrack->SetMethods(m_methods);
        // Inherits the source registry, so all changes to the global variables
        // while running the included source will be reflected in the parent
        // (current) track.
        includedTrack->SetSourceRegistry(m_sourceRegistry);
        includedTrack->p->m_includeLevel = m_includeLevel + 1;

        while(includedTrack->Resume()) { }

    } catch (const SException& e) {

        throwNewSourceException("Error while interpreting included source.", getSourceRef());
    }

    // Marks that the next time we try to include this source, it should be ignored,
    // as we've already imported its variables.
    m_includedCache->Set(stringValue, stringValue);
}

polk_long ExecutionTrackPrivate::tickCount() const
{
    if(m_methods)
        return m_methods->TickCount();
    else
        return Application::TickCount();
}

// Returns true to signal `sleeping` so that we could prematurely leave the
// parent Resume() without having to wait for the whole quantum duration.
bool ExecutionTrackPrivate::performSleep(const SScriptValue& value)
{
    if(m_cancelled && !m_cancelling) {

        // If cancelled, we need to prematurely stop sleeping to start the
        // unwinding process BUT if we're already unwinding, then sleep should
        // work again because cancellation logic may want to make use of sleep.

        // Skips the opcode.
        frame()->BcPtr++;

        // Unmarks the condition variable whatever it was.
        m_timeToWakeUp = SNullable<polk_long>();

        return false; // == "not sleeping"

    } else if(m_timeToWakeUp.HasValue()) {

        // If already sleeping, it means Resume() was called again, we landed on
        // the sleep bytecode again (as we do not update it for the purpose of
        // relanding), so we must check if we can proceed or not yet.
        const polk_long curTime = tickCount();

        if(curTime >= m_timeToWakeUp.Value()) {
            // Skips the opcode finally (we can proceed).
            frame()->BcPtr++;

            // Do not forget to unmark the condition variable.
            m_timeToWakeUp = SNullable<polk_long>();

            return false; // == "not sleeping"
        } else {
            // Otherwise, we'll land on this opcode again when Resume() is called.
            return true; // == "sleeping"
        }

    } else {

        // It's the first time we hit this opcode, so we must only initialize
        // the wakeup time.

        if(value.Type() != METATYPE_NUMBER) {
            throwNewSourceException("`sleep` expects a number argument.", getSourceRef());
        }

        m_timeToWakeUp = tickCount() + (int)value.NumberValue();

        // NOTE Instruction pointer not updated to reland on this opcode against
        // in a next Resume().

        // NOTE Returns `non sleeping` on initialization just in case the time
        // to wait is so small we can finish on the next run (otherwise, we'd
        // have to wait until one calls Resume())
        return false; // == "not sleeping"

    }
}

// Intrinsic methods for booleans.
bool ExecutionTrackPrivate::performBoolCall(bool self,
                                            const char* methodName,
                                            int argCount, SScriptValue* args)
{
    if(strcmp(methodName, "not") == 0) {

        // ************
        //    `not`
        // ************

        if(argCount != 0) {
            throwNewSourceException("`not` requires 0 arguments.", getSourceRef());
        }

        SScriptValue v;
        v.SetBool(!self);
        pushValue(v);

    } else if(argCount == 1) {
        
        // Binary operators.

        if(args[0].Type() != METATYPE_BOOL) {
            throwNewSourceException("Call on bool method %s requires 1 number argument.",
                                    getSourceRef(),
                                    methodName);
        }

        const bool arg = args[0].BoolValue();

        SScriptValue v;

        if(strcmp(methodName, "or") == 0) {
            v.SetBool(self | arg);
        } else if(strcmp(methodName, "and") == 0) {
            v.SetBool(self & arg);
        } else {

            throwNewSourceException("Unknown bool method '%s'.",
                                    getSourceRef(),
                                    methodName);

        }

        pushValue(v);

    } else if(strcmp(methodName, "ToString") == 0) {

        if(argCount != 0) {
            throwNewSourceException("`bool::ToString` requires 0 arguments.", getSourceRef());
        }

        pushValue(SScriptValue::FromString(self? "true": "false"));

    } else {
        throwNewSourceException("Unknown bool method '%s'.",
                                getSourceRef(),
                                methodName);
    }

    return false; // never an async call
}

// Intrinsic methods for numbers.
bool ExecutionTrackPrivate::performNumberCall(float self,
                                              const char* methodName,
                                              int argCount, SScriptValue* args)
{
    SScriptValue r;
    int len = strlen(methodName);
    bool knownMethod = true;

    if(len == 1) {

        // ***********************
        //   1-letter operators.
        // ***********************

        if(argCount != 1 || args[0].Type() != METATYPE_NUMBER) {
            throwNewSourceException("Call on number method '%s' requires 1 number argument.",
                                    getSourceRef(),
                                    methodName);
        }

        const float arg = args[0].NumberValue();

        switch(methodName[0]) {
            case '+': r.SetNumber(self + arg); break;
            case '-': r.SetNumber(self - arg); break;
            case '*': r.SetNumber(self * arg); break;
            case '/':
                // Should be checked in advance to avoid faulting in CPU.
                if(arg == 0.0f) { //-V550
                    throwNewSourceException("Division by zero.", getSourceRef());
                }
                r.SetNumber(self / arg);
                break;
            case '<': r.SetBool(self < arg); break;
            case '>': r.SetBool(self > arg); break;
            default: knownMethod = false; break;
        }

    } else if(len == 2) {

        // ***********************
        //   2-letter operators.
        // ***********************
        
        if(argCount != 1 || args[0].Type() != METATYPE_NUMBER) {
            throwNewSourceException("Call on number method '%s' requires 1 number argument.",
                                    getSourceRef(),
                                    methodName);
        }

        const float arg = args[0].NumberValue();

        if(strcmp(methodName, "==") == 0) {
            r.SetBool(self == arg); //-V550
        } else {
            knownMethod = false;
        }

    } else {

        // *******************
        //   Other methods.
        // *******************

        if(strcmp(methodName, "ToString") == 0) {

            if(argCount != 0) {
                throwNewSourceException("Call on number method '%s' requires no arguments.",
                                        getSourceRef(),
                                        methodName);
            }

            Auto<const CString> selfAsStr (CoreUtils::FloatToString(self, 0, true));

            r.SetObject("String", (intptr_t)(static_cast<const CObject*>(selfAsStr.Ptr())));

        } else {
            knownMethod = false;
        }

    }

    if(!knownMethod) {
        throwNewSourceException("Unknown number method '%s'.",
                                getSourceRef(),
                                methodName);
    }

    pushValue(r);

    return false; // never an async call
}

// Intrinsic methods for nothing.
bool ExecutionTrackPrivate::performNothingCall(const char* methodName, int argCount, SScriptValue* args)
{
    if(strcmp(methodName, "==") == 0) {
        if(argCount != 1) {
            throwNewSourceException("Nothing::== requires 1 argument.", getSourceRef());
        }

        pushValue(SScriptValue::FromBool(args[0].Type() == METATYPE_NULL));
    } else if(strcmp(methodName, "ToString") == 0) {
        if(argCount != 0) {
            throwNewSourceException("Nothing::ToString requires 1 argument.", getSourceRef());
        }

        pushValue(SScriptValue::FromString("nothing"));
    } else {
        throwNewSourceException("Unknown method 'Nothing::%s'.",
                                getSourceRef(),
                                methodName);
    }

    return false; // never an async call
}

bool ExecutionTrackPrivate::performObjectCall(const SScriptValue& _self,
                                              const char* methodName,
                                              int argCount,
                                              SScriptValue* args)
{
    // Changes the shorter `==` to the standard `Equals` of the metaobject
    // system. The `==` syntax is useful as it unites it with bool/number
    // operators.
    if(strcmp(methodName, "==") == 0)
        methodName = "Equals";

    auto const self = const_cast<METAVALUE*>(_self.MetaValue());

    if(argCount > POLK_MAX_MARG_COUNT) {
        throwNewSourceException("Method '%s::%s' has more than %d arguments (limit).",
                                getSourceRef(),
                                self->u.asObject.klass->name,
                                methodName,
                                POLK_MAX_MARG_COUNT);
    }

    METAMETHOD* const method = GetMetaMethod(self->u.asObject.klass, methodName);
    if(!method) {
        throwNewSourceException("Method '%s::%s' not found.",
                                getSourceRef(),
                                self->u.asObject.klass->name,
                                methodName);
    }

    // ************************************************

    if(method->flags & METAMETHODFLAGS_ASYNC) {

        // ******************************
        //   Asynchronous method call.
        // ******************************

        if(!m_threadPool) {
            throwNewSourceException("Asynchronous methods require tracks to have a thread pool assigned.",
                                    getSourceRef());
        }

        // See also the comments for ExecutionTrackPrivate::m_amcResult in for
        // the overview of the asynchronous method call algorithm.

        // If we landed here, that means that the state should be E_ASYNCMETHODSTATE_NONE.
        // The other cases are processed directly in the interpreter loop, to avoid
        // incorrectly popping arguments (which happens in the interpreter loop)
        // again in case we reland on the same instruction (for rechecking the
        // async method call state).
        POLK_REQ_EQUALS(m_amcResult->State, E_ASYNCMETHODSTATE_NONE);

        // The method deals with all the corner cases.
        m_threadPool->EnqueueAsyncMethodCall(_self,
                                             method,
                                             argCount,
                                             args,
                                             m_amcResult);

        // Afterwards, we only need to yield and make sure that the next ::Resume()
        // will land us on the same instruction for a state recheck.

        // true = "it was an async call"
        return true;

    } else {

        // ******************************
        //    Synchronous method call.
        // ******************************

        METAVALUE mArgs[POLK_MAX_MARG_COUNT];

        // Keeps track of replaced values; if an argument value was replaced inside
        // tryFixExToEventHandler, it's ref'd to keep it alive, and we need to unref
        // it after making the call.
        bool replaced[POLK_MAX_MARG_COUNT];

        for(int i = 0; i < argCount; i++) {
            mArgs[i] = *args[i].MetaValue();
            replaced[i] = tryFixExToEventHandler(&mArgs[i]);
        }

        METAVALUE ret;

        const METARESULT r = InvokeMetaMethodDirect(&self->u.asObject,
                                                     method,
                                                     argCount,
                                                     &mArgs[0],
                                                     &ret);

        // See replaced.
        for(int i = 0; i < argCount; i++) {
            if(replaced[i]) {
                POLK_REQ_EQUALS(mArgs[i].type, METATYPE_OBJECT);

                bool deleted;
                UnrefMetaValue(&mArgs[i], &deleted);
            }
        }

        if(r == METARESULT_SUCCESS) {

            SScriptValue r2;
            *r2.MetaValue() = ret; // direct copy, no need to ref, because already ref'd.
            pushValue(r2);

        } else {

            throwNewSourceException("Method '%s::%s' failed with %s.",
                                    getSourceRef(),
                                    self->u.asObject.klass->name,
                                    methodName,
                                    MetaResultToCString(r));

        }

        // false = "it was a sync call"
        return false;

    }
}

bool ExecutionTrackPrivate::performCall(const SScriptValue& self,
                                        const char* methodName,
                                        int argCount,
                                        SScriptValue* args)
{
    const METATYPE type = self.Type();

    switch(type) {
        case METATYPE_NUMBER:
            return performNumberCall(self.NumberValue(), methodName, argCount, args);

        case METATYPE_OBJECT:
            return performObjectCall(self, methodName, argCount, args);

         case METATYPE_BOOL:
            return performBoolCall(self.BoolValue(), methodName, argCount, args);

         case METATYPE_NULL:
            return performNothingCall(methodName, argCount, args);

         default:
            throwNewSourceException("Method calls for typecode = %d not implemented.",
                                    getSourceRef(),
                                    (int)type);
            return false; // to shut up the compiler
    }
}

void ExecutionTrackPrivate::performNew(const char* className,
                                       int argCount,
                                       SScriptValue* args)
{
    if(argCount > POLK_MAX_MARG_COUNT) {
        throwNewSourceException("Constructor of '%s' has more than %d arguments (limit).",
                                getSourceRef(),
                                className,
                                POLK_MAX_MARG_COUNT);
    }

    METAVALUE mArgs[POLK_MAX_MARG_COUNT];

    for(int i = 0; i < argCount; i++) {
        mArgs[i] = *args[i].MetaValue();
    }

    METAVALUE ret;

    const METARESULT r = NewMetaObject(className, argCount, &mArgs[0], &ret);

    if(r == METARESULT_SUCCESS) {

        SScriptValue r2;
        *r2.MetaValue() = ret; // direct copy, no need to ref, because already ref'd.
        pushValue(r2);

    } else {

        throwNewSourceException("Constructor of '%s' failed with %s.",
                                getSourceRef(),
                                className,
                                MetaResultToCString(r));

    }
}

void ExecutionTrackPrivate::loadIdent(int identIndex)
{
    SScriptValue value;

    // First checks locals and arguments: because semantically, locals and
    // arguments overwrite global variables.
    // Arguments can exist in the current stack frame or in the parent executables
    // (an inner function refers to an outer function's argument).

    // First, we check arguments in the current stack frame.

    int frameIndex = m_frameStackPtr - 1;
    const SExecutableFrame* wframe = &m_frameStack[frameIndex];

    int argIndex = 0;
    if(wframe->Executable->ArgumentNames()->TryGet(identIndex, &argIndex)) {
        value = offsetStack(wframe->DataStackPtrOnEnter, -wframe->ArgCount + argIndex);
        pushValue(value);

        // Mission accomplished.
        return;
    }

    // If we haven't still gotten the value, we need to traverse the executable
    // hierarchy as defined in the source code.
    CExecutable* curExe = wframe->Executable->ParentExecutable();
    while(curExe) {
        if(curExe->ArgumentNames()->TryGet(identIndex, &argIndex)) {
            // OK, we've found a parent executable which declares this argument.
            // Now we need to locate it in the frame stack. It should be somewhere
            // up the stack if this function is called through one of the
            // declaring executables, otherwise we're out of luck (variables are
            // not captured).

            while(--frameIndex >= 0) {
                wframe = &m_frameStack[frameIndex];

                if(wframe->Executable->LocalUID() == curExe->LocalUID()) {
                    value = offsetStack(wframe->DataStackPtrOnEnter, -wframe->ArgCount + argIndex);
                    pushValue(value);

                    // Mission accomplished.
                    return;
                }
            }
        }

        curExe = curExe->ParentExecutable();
    }

    // Same as above, but for locals and, interestingly, we need to traverse the
    // other way around, i.e. from the first frame to the last frame:
    //
    // outer frame:
    //    local a = 0
    //
    // inner frame:
    //    local a = a + 1 # <-- this should not override but use 'a' from the outer frame
    //
    // So instead of using the first chance find like above, we use the last find.

    int localIndex = 0;
    curExe = wframe->Executable;
    CExecutable* lastExe = nullptr;
    int lastLocalIndex = -1;
    while(curExe) {
        if(curExe->LocalMap()->TryGet(identIndex, &localIndex)) {
            lastExe = curExe;
            lastLocalIndex = localIndex;
        }

        curExe = curExe->ParentExecutable();
    }

    if(lastExe) {
        frameIndex = m_frameStackPtr - 1;

        // Now we need to locate the frame which corresponds to this executable.
        while(frameIndex >= 0) {
            wframe = &m_frameStack[frameIndex];

            if(wframe->Executable->LocalUID() == lastExe->LocalUID()) {
                value = offsetStack(wframe->DataStackPtrOnEnter, lastLocalIndex);
                pushValue(value);

                // Mission accomplished.
                return;
            }

            frameIndex--;
        }
    }

    // Tries with global variables.

    if(m_vars->TryGet(identIndex, &value)) {
        pushValue(value);
        return;
    }

    Auto<const CString> argAsStr (metadata()->GetString(identIndex).ToString());
    throwNewSourceException("Unknown variable `%o`",
                            getSourceRef(),
                            static_cast<const CObject*>(argAsStr.Ptr()));
}

void ExecutionTrackPrivate::performStoreLocal(int identIndex, const SScriptValue& value)
{
    auto curFrame = frame();

    int localIndex = 0;
    auto curExe = curFrame->Executable;
    CExecutable* lastExe = nullptr;
    int lastLocalIndex = -1;
    while(curExe) {
        if(curExe->LocalMap()->TryGet(identIndex, &localIndex)) {
            lastExe = curExe;
            lastLocalIndex = localIndex;
        }

        curExe = curExe->ParentExecutable();
    }

    if(lastExe) {
        int frameIndex = m_frameStackPtr - 1;

        // Now we need to locate the frame which corresponds to this executable.
        while(frameIndex >= 0) {
            const SExecutableFrame* const frame = &m_frameStack[frameIndex];

            if(frame->Executable->LocalUID() == lastExe->LocalUID()) {
                const int fn = frame->DataStackPtrOnEnter + lastLocalIndex;
                POLK_REQ(fn < DATASTACK_SIZE, EC_CONTRACT_UNSATISFIED);

                m_dataStack[fn] = value;

                // Mission accomplished.
                return;
            }

            frameIndex--;
        }
    }

    Auto<const CString> identAsStr (curFrame->Executable->Metadata()->GetString(identIndex).ToString());

    throwNewSourceException("Local `%o` not found.",
                            getSourceRef(),
                            static_cast<const CObject*>(identAsStr.Ptr()));
}

void ExecutionTrackPrivate::performCond(int condIndex)
{
    // Flow control is managed by splitting 'bodies' into their own child
    // executable objects (which share their data with their parent executables).
    // It transitions into a child executable, and interpretation proceeds as
    // usual, just with a different current executable.

    SExecutableFrame* const curFrame = frame();
    CExecutable* const curExe = curFrame->Executable;
    CExecutable* const condExe = curExe->ChildExecutableAt(condIndex);

    if(curFrame->JustReturned) {
        curFrame->BcPtr += 2; // skip the current opcode + childExe index

        pushValue(m_retValue);
        m_retValue = SScriptValue(); // TODO check?

        curFrame->JustReturned = false; // marks as processed
    } else {
        transitionToChildExe(condExe);
    }
}

void ExecutionTrackPrivate::performReturn(int retCount)
{
    SExecutableFrame* const curFrame = frame();

    // This will gracefully terminate the current executable in the while loop's
    // preamble inside Resume().
    curFrame->BcPtr = curFrame->Executable->RootBytecode()->Count();

    if(retCount == 0) {
        m_retValue = SScriptValue();
    } else if(retCount == 1) {
        m_retValue = popValue();
    } else {
        throwNewSourceException("`return` opcode only supports 0 or 1 arguments.", getSourceRef());
    }
}

void ExecutionTrackPrivate::performJump(int childExeIndex, int jumpTargetOffset)
{
    SExecutableFrame* const curFrame = frame();

    if(curFrame->JustReturned) {

        const SScriptValue condVal (popValue());

        if(condVal.BoolValue())
            curFrame->BcPtr += jumpTargetOffset; // TODO verify range
        else
            curFrame->BcPtr += 3; // skips the opcode + childExeIndex + jumpTargetOffset

        curFrame->JustReturned = false; // marks as processed

    } else {

        const SScriptValue condVal (popValue());
        if(condVal.Type() != METATYPE_BOOL) {
            throwNewSourceException("Expected a boolean value.", getSourceRef());
        }

        if(condVal.BoolValue()) {
            // Repushes the value so that when returned from the jump, (see above "if(justReturned)"),
            // we could know the relevant value.
            pushValue(condVal);

            CExecutable* const childExe = curFrame->Executable->ChildExecutableAt(childExeIndex);
            transitionToChildExe(childExe);
        } else {
            // skips the opcode + childExeIndex + jumpTargetOffset
            curFrame->BcPtr += 3;
        }

    }
}

#define RETURNED_FROM_DO true
#define RETURNED_FROM_UNDO false
void ExecutionTrackPrivate::performProtected(int doExeIndex, int undoExeIndex)
{
    SExecutableFrame* curFrame = frame();

    if(curFrame->JustReturned) {

        // Pops our mark we pushed earlier. We need to tell whether we returned
        // from the `do` or `undo` blocks, as they're both "curFrame->JustReturned"
        const SScriptValue mark (popValue());

        if(mark.BoolValue() == RETURNED_FROM_DO) {

            // Returned from the `do` block.
            // Well, transition to the `undo` block, then.

            curFrame->JustReturned = false;

            m_doBlockCounter--;
            POLK_REQ_NOT_NEG(m_doBlockCounter);

            // Again, pushes the mark.
            pushValue(SScriptValue::FromBool(RETURNED_FROM_UNDO));

            CExecutable* undoExe = curFrame->Executable->ChildExecutableAt(undoExeIndex);
            m_undoBlockCounter++;
            transitionToChildExe(undoExe);

        } else {

            if(m_cancelling) {

                // If cancelling, then we should find a parent `undo` and jump
                // to it instead of falling through.

                auto bc = curFrame->Executable->RootBytecode();
                do {
                    // Unwinds stack data further.
                    while(m_dataStackPtr != curFrame->DataStackPtrOnEnter)
                        popValue();

                    if(m_frameStackPtr == 1) {
                        // No more blocks to execute => all undo blocks executed
                        // => finished.
                        // Sets the bytecode pointer to the last bytecode to make
                        // it gracefully finish in Resume()
                        curFrame->BcPtr = curFrame->Executable->RootBytecode()->Count();
                        return;
                    }

                    // Pops the previous frame which was found to be not the one
                    // which spawned the `do` block (can be a callback, a
                    // condition etc.)
                    popFrame();

                    curFrame = frame();
                    bc = curFrame->Executable->RootBytecode();
                }
                while(bc->Item(curFrame->BcPtr) != E_OPCODE_PROTECTED);

                undoExeIndex = bc->Item(curFrame->BcPtr + 2);
                CExecutable* const undoExe = curFrame->Executable->ChildExecutableAt(undoExeIndex);

                // Now we need to emulate as if we just interpreted the `PROTECTED`
                // instruction's `do` block succesfully.

                // The required mark.
                pushValue(SScriptValue::FromBool(RETURNED_FROM_UNDO));

                // Just in case.
                curFrame->JustReturned = false;

                transitionToChildExe(undoExe);

            } else {

                // Returned from the `undo` block.
                // In normal mode, we just skip the instruction.

                curFrame->JustReturned = false;

                // skips the opcode + doExeIndex + undoExeIndex
                curFrame->BcPtr += 3;

                m_undoBlockCounter--;
                POLK_REQ_NOT_NEG(m_undoBlockCounter);

            }
        }
        
    } else {

        // TODO Can implement this special case in the future.
        if(m_cancelling) {
            throwNewSourceException("`do` blocks inside `undo` blocks not supported when cancelling.", getSourceRef());
        }

        // Transitions to the `do` block.

        // Pushes the mark (see above).
        pushValue(SScriptValue::FromBool(RETURNED_FROM_DO));

        CExecutable* const doExe = curFrame->Executable->ChildExecutableAt(doExeIndex);

        // We need to mark the current track as "inside a `do` block" as
        // cancellation is possible only inside a `do` block. Code outside of
        // `do` blocks should be guaranteed to run in presence of cancellation.
        m_doBlockCounter++;

        transitionToChildExe(doExe);

    }

}

void ExecutionTrackPrivate::performAtomic(int childExeIndex)
{
    SExecutableFrame* const curFrame = frame();

    if(curFrame->JustReturned) {
        curFrame->BcPtr += 2; // skips the opcode + childExeIndex
        curFrame->JustReturned = false; // marks as processed

        // Atomic count down. If it's the last recursive atomic block, we can
        // yield.
        m_atomicCounter--;
        POLK_REQ_NOT_NEG(m_atomicCounter);
    } else {
        CExecutable* const childExe = curFrame->Executable->ChildExecutableAt(childExeIndex);

        // Transitions the track to the atomic state. We cannot yield from now,
        // until we leave all atomic blocks.
        m_atomicCounter++;

        transitionToChildExe(childExe);
    }
}

void ExecutionTrackPrivate::performCallB(int argCount)
{
    SExecutableFrame* const curFrame = frame();

    if(curFrame->JustReturned) {

        // Call performed + returned from the child executable.

        // "-1" includes self.
        const int argStart = m_dataStackPtr - argCount - 1;
        if(argStart < 0) {
            throwNewSourceException("Data stack underflow (E_OPCODE_CALLB).",
                                    getSourceRef());
        }

        // Unwinds the stack. "+1" includes self.
        for(int j = 0; j < argCount + 1; j++) {
            m_dataStack[argStart + j].SetNothing();
        }
        m_dataStackPtr -= argCount + 1;

        // Push what the child executable wrote into retValue.
        pushValue(m_retValue);

        curFrame->BcPtr += 2; // skips the opcode + argument count
        curFrame->JustReturned = false; // marks as processed

    } else {
        // NOTE Arguments are not popped, they're remembered to access them later
        // from inside the child executable. They will be popped when control
        // returns in the branch above.

        // "-1" includes self.
        const int argStart = m_dataStackPtr - argCount - 1;
        if(argStart < 0) {
            throwNewSourceException("Data stack underflow (E_OPCODE_CALLB).",
                                    getSourceRef());
        }

        const SScriptValue self (m_dataStack[argStart]);

        if(self.Type() != METATYPE_OBJECT) {
            throwNewSourceException("Expected an executable object.", getSourceRef());
        }

        auto const childExe = dynamic_cast<CExecutable*>(reinterpret_cast<CObject*>(self.ObjectValue("_Ex")));
        if(childExe == nullptr) {
            // ****************************************************
            //   Native callbacks.
            // ****************************************************

            auto const asEventHandler = dynamic_cast<CDelegate<SEventArgs>*>(reinterpret_cast<CObject*>(self.ObjectValue("EventHandler")));
            if(!asEventHandler) {
                throwNewSourceException("Expected an executable object.", getSourceRef());
            }
            if(argCount != 0) {
                throwNewSourceException("Native functions support only zero arguments.", getSourceRef());
            }

            SEventArgs dummyEventArgs;
            asEventHandler->Invoke(nullptr, dummyEventArgs); // TODO wrap with try/catch?

            curFrame->BcPtr += 2; // skips the opcode + argument count

            pushValue(SScriptValue());

            // ****************************************************
        } else {

            if(childExe->ArgumentNames()->Size() != argCount) {
                throwNewSourceException("Function expected %d arguments; given: %d",
                                        getSourceRef(),
                                        childExe->ArgumentNames()->Size(),
                                        argCount);
            }

            transitionToChildExe(childExe);

            // Need to tell how many arguments passed so that loadIdent(..)
            // could correctly calculate argument positions.
            frame()->ArgCount = argCount;

            // NOTE bcPtr not updated so that after exiting from the child executable,
            // we landed on the same instruction and could later proceed into the branch above
            // (conditioned a curFrame->JustReturned)

        }
    }
}

void CExecutionTrack::Cancel()
{
    p->m_cancelled = true;
}

// The specification guarantees that the transition cancelled=>cancelling should
// never happen between `do` and `atomic` if there are no other instructions
// between them:
//
//  do {
//      # Should never transition here.
//      atomic {
//      }
//  } undo {
//  }
//
// i.e. atomic is guaranteed to run in presence of cancellation if it's the first
// instruction in a `do` block.
//
// The solution is to check if the next instruction is going to be ATOMIC.
bool ExecutionTrackPrivate::beforeAtomic() const
{
    if(m_frameStackPtr == 0)
        return false;

    const SExecutableFrame* const curFrame = &m_frameStack[m_frameStackPtr - 1];
    const auto bc = curFrame->Executable->RootBytecode();

    return (curFrame->BcPtr == 0)
        && (bc->Count() > 0)
        && (bc->Array()[curFrame->BcPtr] == E_OPCODE_ATOMIC);
}

bool CExecutionTrack::Resume()
{
    return p->resume();
}

bool ExecutionTrackPrivate::resume()
{
    int instrCount = 0;

    while(true) {

        if(m_frameStackPtr == 0)
            return false; // no frames => script completed
        SExecutableFrame* curFrame = &m_frameStack[m_frameStackPtr - 1];

        auto _bc = curFrame->Executable->RootBytecode();
        auto bc = _bc->Array();

        if(curFrame->BcPtr >= _bc->Count()) {

            // Unwinds the data stack to the stable value before frame enter.
            int dif = m_dataStackPtr - curFrame->DataStackPtrOnEnter;
            while(--dif >= 0)
                popValue();

            // After a child executable ends, we want to pop it off the stack
            // and return to the previous parent executable.
            popFrame();

            // If there are no stack frames anymore, finishes execution.
            if(m_frameStackPtr == 0) {
                // Verifies that the stack is balanced at the end.
                POLK_REQ_EQUALS(m_dataStackPtr, 0);
                break;
            }

            curFrame = frame();

            // Tells the parent stackframe (now the current one) that we just
            // returned from a child executable. This is used by COND/JMP opcodes.
            curFrame->JustReturned = true;

            // Otherwise, proceed executing the previous frame.
            continue;

        }

        // *********************************************************
        // Debugging support -- right before interpretation starts.
        // NOTE We ignore breakpoints inside atomic blocks.
        if(m_debuggingEnabled && m_atomicCounter == 0) {
            if(curFrame->Executable->TryExecuteBreakpointHandler(curFrame->BcPtr)) {
                return true; // Suspends if a debug handler was executed.
            }
        }
        // *********************************************************

        const int opcode = bc[curFrame->BcPtr];
        auto const md = curFrame->Executable->Metadata();
        SScriptValue v;

        switch(opcode) {
            case E_OPCODE_NOP:
                curFrame->BcPtr++;
                break;

            case E_OPCODE_STOREG:
                m_vars->Set(bc[curFrame->BcPtr + 1], popValue());
                curFrame->BcPtr += 2;
                break;

            case E_OPCODE_STOREL:
                performStoreLocal(bc[curFrame->BcPtr + 1], popValue());
                curFrame->BcPtr += 2;
                break;

            case E_OPCODE_LOAD:
                loadIdent(bc[curFrame->BcPtr + 1]);
                curFrame->BcPtr += 2;
                break;

            case E_OPCODE_POP:
                popValue();
                curFrame->BcPtr++;
                break;

            case E_OPCODE_NUMBER:
                v.SetNumber(md->GetNumber(bc[curFrame->BcPtr + 1]));
                pushValue(v);
                curFrame->BcPtr += 2;
                break;

            case E_OPCODE_TRUE:
                v.SetBool(true);
                pushValue(v);
                curFrame->BcPtr++;
                break;

            case E_OPCODE_FALSE:
                v.SetBool(false);
                pushValue(v);
                curFrame->BcPtr++;
                break;

            case E_OPCODE_NOTHING:
                v.SetNothing();
                pushValue(v);
                curFrame->BcPtr++;
                break;

            case E_OPCODE_STRING:
                {
                    const CString* const str = md->PolkStringForIndex(bc[curFrame->BcPtr + 1]);
                    v.SetObject("String", (intptr_t)(static_cast<const CObject*>(str)));
                    pushValue(v);
                    curFrame->BcPtr += 2;
                }
                break;

            case E_OPCODE_BODY:
                v.SetObject("_Ex", (intptr_t)(static_cast<const CObject*>(executable()->ChildExecutableAt(bc[curFrame->BcPtr + 1]))));
                pushValue(v);
                curFrame->BcPtr += 2;
                break;

            case E_OPCODE_CALL:
                {
                    // Asynchronous method's state check.

                    // TODO overhead to fetch an atomic value on each call?
                    const int asyncState = m_amcResult->State;

                    if(asyncState == E_ASYNCMETHODSTATE_WAITING) {

                        if(m_cancelled && !m_cancelling
                        && (m_doBlockCounter > 0) && (m_doBlockCounter > m_undoBlockCounter)
                        && (m_atomicCounter == 0))
                        {
                            // If the track is cancelled but not cancelling, we
                            // need to abandon the asynchronous method to initiate
                            // cancellation (otherwise a long-running operation
                            // would block cancellation). However, this cancelling
                            // thing makes sense only if we're inside a `do` block
                            // and outside of any `atomic` blocks.
                            // NOTE Condition duplicated with a condition at the
                            // end of the method in the cancellation injection
                            // segment.

                            // We need a new result object because the old one
                            // may be still held by the thread pool and we don't
                            // want the thread pool to overwrite the new values 
                            // with stale data from a previous method call if
                            // unwinding will issue new asynchronous method calls
                            // after cancellation. CAsyncMethodCallResult's destructor
                            // can unref things by itself, so we don't do any
                            // cleanup there.

                            m_amcResult->Abandoned = true;
                            m_amcResult.SetPtr(new CAsyncMethodCallResult());

                            // Goes to the next bytecode, which is probably not
                            // needed, because the cancellation segment after the
                            // switch loop will longjump to an undo block anyway.
                            curFrame->BcPtr += 3;
                            break;

                        } else {
                            // Still waiting for the result, nothing to do on this
                            // track => yields immediately.

                            return true; // "returns true if yielded"
                        }

                    } else if(asyncState == E_ASYNCMETHODSTATE_HAS_RESULT) {
                        // Got a result.

                        // See http://preshing.com/20130922/acquire-and-release-fences/
						// TODO is this correct?
                        std::atomic_thread_fence(std::memory_order_acquire);

                        const METARESULT res = m_amcResult->Result;

                        // To report on failure. Have to assign nullptr because
                        // otherwise the compiler complains in pedantic mode.
                        const char* className = nullptr;

                        // Pushes it to the data stack in case of success.
                        if(res == METARESULT_SUCCESS) {
                            pushValue(SScriptValue::FromMetaValue(&m_amcResult->ReturnValue));
                        } else { // failure
                            // will be used below on failure
                            className = (m_amcResult->Self.type == METATYPE_OBJECT)? // just in case
                                            m_amcResult->Self.u.asObject.klass->name
                                          : "<unknown class>";
                        }

                        // Important to unref and null the values stored inside
                        // m_amcResult.
                        m_amcResult->Result = METARESULT_SUCCESS;
                        bool deleted;
                        UnrefMetaValue(&m_amcResult->ReturnValue, &deleted);
                        UnrefMetaValue(&m_amcResult->Self, &deleted);
                        m_amcResult->ReturnValue = m_amcResult->Self = MetaValueFromNull();

                        // Returns the state back to normal.
                        m_amcResult->State = E_ASYNCMETHODSTATE_NONE;

                        // Throw an exception on failure.
                        if(res != METARESULT_SUCCESS) {
                            POLK_REQ_PTR(className);

                            throwNewSourceException("Asynchronous method '%s::%s' failed with %s.",
                                                    getSourceRef(),
                                                    className,
                                                    md->GetBasicString(bc[curFrame->BcPtr + 2]),
                                                    MetaResultToCString(res));
                        }

                        // Finally skips the instruction. No relandings anymore.
                        curFrame->BcPtr += 3;
                        break;
                    }

                    // Fetches the method and the argumens and tries to perform
                    // the call.

                    const int argCount = bc[curFrame->BcPtr + 1]; // arg #1
                    const char* const methodName = md->GetBasicString(bc[curFrame->BcPtr + 2]); // arg #2

                    // "-1" includes self.
                    const int argStart = m_dataStackPtr - argCount - 1;
                    if(argStart < 0) {
                        throwNewSourceException("Data stack underflow (E_OPCODE_CALL).",
                                                getSourceRef());
                    }

                    bool asyncCall = performCall(m_dataStack[argStart],
                                                 methodName,
                                                 argCount,
                                                 &m_dataStack[argStart + 1]);

                    // Post-call processing.

                    if(asyncCall) {

                        // For the async call, we only want to unwind the arguments.
                        // The instruction pointer should stay intact to make
                        // the track reland on the same instruction for a state
                        // recheck (see above). Also there's nothing to push yet.

                        // Unwinds the stack. "+1" includes self.
                        for(int j = 0; j < argCount + 1; j++) {
                            m_dataStack[argStart + j].SetNothing();
                        }
                        m_dataStackPtr -= argCount + 1;

                    } else {

                        const SScriptValue ret (popValue());

                        // Unwinds the stack. "+1" includes self.
                        for(int j = 0; j < argCount + 1; j++) {
                            m_dataStack[argStart + j].SetNothing();
                        }
                        m_dataStackPtr -= argCount + 1;

                        pushValue(ret);

                        curFrame->BcPtr += 3;

                    }
                }
                break;

            case E_OPCODE_CALLB:
                {
                    const int argCount = bc[curFrame->BcPtr + 1];
                    performCallB(argCount);
                    // NOTE performCallB takes care of updating the current
                    // instruction pointer.
                }
                break;

            case E_OPCODE_NEW:
                {
                    const int argCount = bc[curFrame->BcPtr + 1];
                    const char* const className = md->GetBasicString(bc[curFrame->BcPtr + 2]);

                    const int argStart = m_dataStackPtr - argCount;
                    if(argStart < 0) {
                        throwNewSourceException("Data stack underflow (E_OPCODE_NEW).",
                                                getSourceRef());
                    }

                    performNew(className, argCount, &m_dataStack[argStart]);

                    const SScriptValue ret (popValue());

                    // Unwinds the stack.
                    for(int j = 0; j < argCount; j++) {
                        m_dataStack[argStart + j].SetNothing();
                    }
                    m_dataStackPtr -= argCount;

                    pushValue(ret);

                    curFrame->BcPtr += 3;
                }
                break;

            case E_OPCODE_YIELD:
                curFrame->BcPtr += 1;

                if(m_atomicCounter == 0) {
                    // Simply exits. The next call to Resume() will resume from
                    // where we left it.
                    return true;
                } else {
                    // We're in an atomic block, so we have to just skip the yield
                    // statement.
                    break;
                }

            case E_OPCODE_RETURN:
                performReturn(bc[curFrame->BcPtr + 1]);
                // NOTE performReturn takes care of updating the current
                // instruction pointer.
                break;

            case E_OPCODE_COND:
                performCond(bc[curFrame->BcPtr + 1]);
                // NOTE performCond takes care of updating the current
                // instruction pointer.
                break;

            case E_OPCODE_JUMP:
                performJump(bc[curFrame->BcPtr + 1], bc[curFrame->BcPtr + 2]);
                // NOTE performJump takes care of updating the current
                // instruction pointer.
                break;

            case E_OPCODE_TRACE:
                performTrace(popValue());
                curFrame->BcPtr++;
                break;

            case E_OPCODE_ASSERT:
                performAssert(popValue(), md->GetString(bc[curFrame->BcPtr + 1]));
                curFrame->BcPtr += 2;
                break;

            case E_OPCODE_INCLUDE:
                performInclude(popValue());
                curFrame->BcPtr++;
                break;

            case E_OPCODE_SLEEP:
                // NOTE The function returns `true` to signal `still sleeping`,
                // if that's the case, we immediately quit the interpreting
                // function without waiting for the quantum to expire: the track
                // is sleeping and we'd rather spend CPU time on other tracks.
                // NOTE Doesn't pop a value if we're already sleeping (relanded).
                // NOTE The function also takes care of updating the current
                // instruction pointer.
                if(performSleep(m_timeToWakeUp.HasValue()? SScriptValue(): popValue()))
                    return true;
                break;

            case E_OPCODE_DUPNEG:
                performDupNeg();
                curFrame->BcPtr++;
                break;

            case E_OPCODE_ATOMIC:
                performAtomic(bc[curFrame->BcPtr + 1]);
                // NOTE performAtomic takes care of updating the current instruction pointer.
                break;

            case E_OPCODE_PROTECTED:
                performProtected(bc[curFrame->BcPtr + 1], bc[curFrame->BcPtr + 2]);
                // NOTE performProtected takes care of updating the current instruction pointer.
                break;

            case E_OPCODE_CANCEL:
                // NOTE Further calls will be essentially ignored, as the flag is already set.
                m_cancelled = true;
                curFrame->BcPtr++;
                break;

            case E_OPCODE_CANCELLING:
                pushValue(SScriptValue::FromBool(m_cancelling));
                curFrame->BcPtr++;
                break;

            case E_OPCODE_PREEMPT:
                m_preempt = true;
                curFrame->BcPtr++;
                break;

            case E_OPCODE_NOPREEMPT:
                m_preempt = false;
                curFrame->BcPtr++;
                break;

            default:
                throwNewSourceException("Unknown bytecode %d.",
                                        executable()->SourceRefMap()->Item(curFrame->BcPtr),
                                        (int)opcode);
                break;
        }

        // *********************************************************************
        // Cancellation injection.
        // *********************************************************************

        // If the track is marked as cancelled and we're inside a `do` block AND
        // we're outside of any atomic blocks, we need to change our state to
        // `cancelling`. From now on, the track should only interpret undo blocks
        // bottom-up.
        //
        // NOTE !cancelling -- important to transition cancelled=>cancelling only once.
        // NOTE m_doBlockCounter > m_undoBlockCounter -- do not cancel in immediate `undo` block
        // NOTE Condition duplicated with a condition above.
        if(m_cancelled && !m_cancelling
        && (m_doBlockCounter > 0) && (m_doBlockCounter > m_undoBlockCounter) && (m_atomicCounter == 0)
        && !beforeAtomic()) // see the comments for beforeAtomic() 
        {
            m_cancelling = true;

            // FIX (current frame might have been updated in the last instruction)
            curFrame = frame();

            // We're inside a `do` block right now (as checked by "doBlockCounter > 0")
            // and we need to finish it prematurely.

            // Unwinds the data stack back to the beginning.
            while(m_dataStackPtr != curFrame->DataStackPtrOnEnter)
                popValue();

            // Pops the current frame (which is a `do` block.)
            popFrame();

            // "m_frameStackPtr == 0" should never happen because a `do` block
            // is always inside at least a root frame.
            POLK_REQ_POS(m_frameStackPtr);

            // Now we're back at the frame which contains the `do/undo` blocks.
            curFrame = frame();
            _bc = curFrame->Executable->RootBytecode();
            bc = _bc->Array();

            // Now we need to locate the instruction which spawned this `do`
            // block to find out the corresponding `undo` block. BcPtr in
            // parent executables (which we are currently at) always refer to
            // the last instruction before transition to the child.
            //
            // Note that current code's immediate parent may be something
            // else, for example an `if` block. It means we need to skip
            // recursively till we reach the actual block that spawned it.
            while(_bc->Item(curFrame->BcPtr) != E_OPCODE_PROTECTED) {

                // Unwinds stack data further.
                while(m_dataStackPtr != curFrame->DataStackPtrOnEnter)
                    popValue();

                // Pops the previous frame which was found to be not the one
                // which spawned the `do` block (can be a callback, a
                // condition etc.)
                popFrame();

                // "m_frameStackPtr == 0" should never happen because a `do`
                // block is always inside at least a root frame.
                POLK_REQ_POS(m_frameStackPtr);

                curFrame = frame();
                _bc = curFrame->Executable->RootBytecode();
                bc = _bc->Array();
            }

            // Finally we've found the correct parent executable and we now
            // can extract the index and then the target undo executable.
            // Arg #2 in the instruction contains the said index
            // (see E_OPCODE_PROTECT)

            const int undoExeIndex = _bc->Item(curFrame->BcPtr + 2);
            CExecutable* const undoExe = curFrame->Executable->ChildExecutableAt(undoExeIndex);

            // Now we need to emulate as if we just interpreted the `PROTECTED`
            // instruction's `undo` block successfully. Control flow will later
            // be managed inside performProtect(..)

            // The required mark.
            pushValue(SScriptValue::FromBool(RETURNED_FROM_UNDO));

            m_doBlockCounter--;
            POLK_REQ_NOT_NEG(m_doBlockCounter);

            // Just in case.
            curFrame->JustReturned = false;

            transitionToChildExe(undoExe);
        }

        // *********************************************************************
        // Automatic yield injection if quantum > 0 but only if
        // not inside atomic blocks.
        // *********************************************************************

        instrCount++;

        // *********************************************************************
        // Note ">=". Due to atomic blocks, the eventual instruction count can be
        // greater than the quantum.
        if(m_quantum > 0 && m_preempt && m_atomicCounter == 0 && instrCount >= m_quantum) {
            return true;
        }
        // *********************************************************************
    }

    return false;
}

    // **********************************
    //   Serialization/deserialization.
    // **********************************

void CExecutionTrack::Serialize(CStream* stream, CLog* log) const
{
    p->serialize(stream, log);
}

void ExecutionTrackPrivate::serialize(CStream* stream, CLog* log) const
{
    SBinaryWriter bw (stream);
    Auto<CIdTable> idTable (CIdTable::CreateInstance());

    const polk_long idTableOffsetPos = stream->GetPosition();
    bw.WriteInt(0); // allocates space, will be written here later

    // Other data.

    bw.WriteInt(m_quantum);
    bw.WriteBool(m_preempt);
    bw.WriteInt(m_includeLevel);
    bw.WriteUTF16(m_name, true); // lengthHeader=true
    bw.WriteInt(m_atomicCounter);
    bw.WriteBool(m_cancelled);
    bw.WriteBool(m_cancelling);
    bw.WriteInt(m_doBlockCounter);
    bw.WriteInt(m_undoBlockCounter);

    // Return value.

    m_retValue.Serialize(stream, idTable, log);

    // m_timeToWakeUp

    if(m_timeToWakeUp.HasValue()) {
        bw.WriteBool(true);
        bw.WriteLong(m_timeToWakeUp.Value());
    } else {
        bw.WriteBool(false);
    }

    // Variable context.

    try {

        SHashMapEnumerator<int, SScriptValue> varEnum (m_vars);

        int index;
        SScriptValue scriptVal;

        int serializableCount = 0;
        while(varEnum.MoveNext(&index, &scriptVal)) {
            if(!scriptVal.IsStatic()) {
                serializableCount++;
            }
        }

        bw.WriteInt(serializableCount);

        varEnum.Reset();
        while(varEnum.MoveNext(&index, &scriptVal)) {
            if(!scriptVal.IsStatic()) {
                bw.WriteInt(index);
                scriptVal.Serialize(stream, idTable, log);
            }
        }

    } catch (const SException& e) {

        // Common error.
        POLK_LOG_ERROR(log,
                    "Script",
                    "Global variable serialization error for track '%o' ('%s').",
                     static_cast<const CObject*>(m_name.Ptr()),
                     e.Message());
        throw;

    }

    // Included cache.

    {
        SHashMapEnumerator<const CString*, const CString*> includedCacheEnum (m_includedCache);
        const CString* included;
        bw.WriteInt(m_includedCache->Size());
        while(includedCacheEnum.MoveNext(&included, nullptr)) {
            bw.WriteUTF16(included, true); // lengthHeader=true
        }
    }

    // Data stack.

    try {

        bw.WriteInt(m_dataStackPtr);

        for(int i = 0; i < m_dataStackPtr; i++) {
            const SScriptValue* const value = &m_dataStack[i];
            value->Serialize(stream, idTable, log);
        }

    } catch (const SException& e) {

        // Common error.
        POLK_LOG_ERROR(log,
                    "Script",
                    "Data stack variable serialization error for track '%o' ('%s').",
                     static_cast<const CObject*>(m_name.Ptr()),
                     e.Message());
        throw;

    }

    // Frame stack.

    bw.WriteInt(m_frameStackPtr);

    for(int i = 0; i < m_frameStackPtr; i++) {
        const SExecutableFrame* const frame = &m_frameStack[i];

        const CExecutable* const executable = frame->Executable;
        executable->Serialize(stream, idTable, log);

        bw.WriteInt(frame->BcPtr);
        bw.WriteInt(frame->DataStackPtrOnEnter);
        bw.WriteInt(frame->ArgCount);
        bw.WriteBool(frame->JustReturned);
    }

    // Writes the ID table.
    const int idTableOffset = (int)stream->GetPosition();
    idTable->Serialize(bw);

    // Returns and writes the offset to the table.
    const polk_long oldPos = stream->GetPosition();
    stream->SetPosition(idTableOffsetPos);
    bw.WriteInt(idTableOffset);
    stream->SetPosition(oldPos);
}

CExecutionTrack* CExecutionTrack::Deserialize(CStream* stream, CLog* log)
{
    Auto<CVariableContext> vars (new CVariableContext());
    Auto<CExecutionTrack> r (new CExecutionTrack(nullptr, vars));
    ExecutionTrackPrivate* pTrack = r->p;

    SBinaryReader br (stream);

    const int idTableOffset = br.ReadInt();
    const polk_long oldPos = stream->GetPosition();
    stream->SetPosition(idTableOffset);
    Auto<CIdTable> idTable (CIdTable::Deserialize(br));
    const polk_long afterIdTableOffset = stream->GetPosition();
    stream->SetPosition(oldPos);

    // Other data.

    pTrack->m_quantum = br.ReadInt();
    pTrack->m_preempt = br.ReadBool();
    pTrack->m_includeLevel = br.ReadInt();
    pTrack->m_name.SetPtr(br.ReadUTF16());
    pTrack->m_atomicCounter = br.ReadInt();
    pTrack->m_cancelled = br.ReadBool();
    pTrack->m_cancelling = br.ReadBool();
    pTrack->m_doBlockCounter = br.ReadInt();
    pTrack->m_undoBlockCounter = br.ReadInt();

    // Return value.

    pTrack->m_retValue = SScriptValue::Deserialize(stream, idTable, log);

    // m_timeToWakeUp (what if we're on the sleep opcode right now?)

    const bool timeToWakeUpHasValue = br.ReadBool();
    if(timeToWakeUpHasValue) {
        pTrack->m_timeToWakeUp = SNullable<polk_long>(br.ReadLong());
    }

    // Variable context.

    try {

        const int varCount = br.ReadInt();

        for(int i = 0; i < varCount; i++) {
            int index = br.ReadInt();
            SScriptValue scriptVal (SScriptValue::Deserialize(stream, idTable, log));

            vars->Set(index, scriptVal);
        }

    } catch (const SException& e) {

        // Common error.
        POLK_LOG_ERROR(log,
                    "Script",
                    "Global variable deserialization error for track '%o' ('%s').",
                     static_cast<const CObject*>(pTrack->m_name.Ptr()),
                     e.Message());
        throw;

    }

    // Included cache.

    const int includedCacheSize = br.ReadInt();
    for(int i = 0; i < includedCacheSize; i++) {
        Auto<const CString> included (br.ReadUTF16());
        pTrack->m_includedCache->Set(included, included);
    }

    // Data stack.

    try {

        pTrack->m_dataStackPtr = br.ReadInt();

        for(int i = 0; i < pTrack->m_dataStackPtr; i++) {
            pTrack->m_dataStack[i] = SScriptValue::Deserialize(stream, idTable, log);
        }

    } catch (const SException& e) {

        // Common error.
        POLK_LOG_ERROR(log,
                    "Script",
                    "Data stack variable deserialization error for track '%o' ('%s').",
                     static_cast<const CObject*>(pTrack->m_name.Ptr()),
                     e.Message());
        throw;

    }

    // Frame stack.

    pTrack->m_frameStackPtr = br.ReadInt();

    pTrack->m_pinnedExecutables.SetPtr(new CArrayList<CExecutable*>());

    for(int i = 0; i < pTrack->m_frameStackPtr; i++) {
        SExecutableFrame* const frame = &pTrack->m_frameStack[i];

        Auto<CExecutable> executable (CExecutable::Deserialize(stream, idTable, log));
        frame->Executable = executable;
        pTrack->m_pinnedExecutables->Add(executable); // Pins the executable instance.

        frame->BcPtr = br.ReadInt();
        frame->DataStackPtrOnEnter = br.ReadInt();
        frame->ArgCount = br.ReadInt();
        frame->JustReturned = br.ReadBool();
    }

    // initStaticClasses(..) tries to set variables, and variables are referred
    // via indices, and variable names are mapped to indices via metadata stored
    // inside executables which are referred via the current frame, which may not
    // exist anymore if the track was serialized right after it terminated. Hence
    // the check for m_frameStackPtr.
    if(pTrack->m_frameStackPtr > 0) {
        pTrack->initStaticClasses(); // Should be called here (see initStaticClasses).
    }

    // Returns the stream back to the position after the ID table.
    stream->SetPosition(afterIdTableOffset);

    r->Ref();
    return r;
}

void CExecutionTrack::EnableDebugging(bool b)
{
    p->m_debuggingEnabled = b;
}

SScriptValue CExecutionTrack::GetIdent(const CString* identName) const
{
    try {

        const CExecutableMetadata* const metadata = p->metadata();
        const int identIndex = metadata->GetStringIndex(identName);

        p->loadIdent(identIndex);
        return p->popValue();

    } catch (const SException& e) {
        return SScriptValue();
    }
}

} }
