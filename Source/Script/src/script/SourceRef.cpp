// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/SourceRef.h>
#include <core/CoreUtils.h>
#include <core/Log.h>
#include <core/String.h>
#include <core/StringBuilder.h>
#include <io/BinaryReader.h>
#include <io/BinaryWriter.h>
#include <io/Stream.h>

namespace polk { namespace script {
using namespace polk::core;
using namespace polk::io;

bool SSourceRef::Equals(const SSourceRef& other) const
{
    return this->ExecutableName == other.ExecutableName
        && this->Line == other.Line
        && this->LineNumber == other.LineNumber
        && this->CharPosition == other.CharPosition;
}

int SSourceRef::GetHashCode() const
{
    int hashCode = 0;

    hashCode += 1000000007 * this->ExecutableName.GetHashCode();
    hashCode += 1000000009 * this->Line.GetHashCode();
    hashCode += 1000000021 * this->LineNumber;
    hashCode += 1000000033 * this->CharPosition;

    return hashCode;
}

const CString* SSourceRef::ToString() const
{
    Auto<CStringBuilder> sb (new CStringBuilder());

    if(this->ExecutableName.Length > 0) {
        Auto<const CString> exeNameStr (this->ExecutableName.ToString());
        sb->Append(exeNameStr);
    } else {
        sb->Append("unknown executable");
    }

    sb->Append(POLK_CHAR(' '));
    sb->Append(POLK_CHAR('('));
    // Displays starting from 1, as this is how most text editors do it.
    sb->Append(this->LineNumber + 1);
    sb->Append(POLK_CHAR(')'));
    sb->Append(POLK_CHAR(':'));
    sb->Append(POLK_CHAR(' '));

    sb->Append(POLK_CHAR('\n'));

    Auto<const CString> lineAsStr (this->Line.ToString());
    sb->Append(lineAsStr);
    sb->Append(POLK_CHAR('\n'));

    for(int i = 0; i < this->CharPosition; i++)
        sb->Append(POLK_CHAR(' '));

    sb->Append(POLK_CHAR('^'));
    sb->Append(POLK_CHAR('\n'));

    return sb->ToString();
}

} }
