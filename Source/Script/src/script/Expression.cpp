// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/Expression.h>
#include <core/Log.h>
#include <core/StringBuilder.h>
#include <script/Executable.h>
#include <script/ExecutableMetadata.h>
#include <script/Opcode.h>
#include <script/StringSlice.h>

namespace polk { namespace script {
using namespace polk::core;
using namespace polk::collections;

    // **************
    //      CExpr
    // **************

void CExpr::EmitOpcode(CExecutable* exe, int opcode) const
{
    exe->EmitOpcode(opcode, m_sourceRef);
}

void CExpr::throwNewSourceException(CLog* log, const char* msg, ...) const
{
    va_list vl;
    va_start(vl, msg);

    Auto<const CString> daMsg (CString::FormatImpl(msg, vl));

    va_end(vl);

    Auto<CStringBuilder> sb (new CStringBuilder());
    sb->Append(daMsg);

    Auto<const CString> srcAsStr (m_sourceRef.ToString());
    sb->Append(POLK_CHAR('\n'));
    sb->Append(srcAsStr);

    Auto<const CString> finalMsg (sb->ToString());
    log->Write(E_LOGPRIORITY_ERROR, "Script", finalMsg);

    POLK_THROW_WITH_MSG(EC_EXECUTION_ERROR, "Bytecode emission error");
}

    // **************
    //   CRootExpr
    // **************

CRootExpr::CRootExpr(const SSourceRef& sourceRef)
    : CExpr(sourceRef),
      Arguments(new CArrayList<SStringSlice>()),
      Exprs(new CArrayList<CExpr*>())
{
}

void CRootExpr::Emit(CExecutable* exe) const
{
    for(int i = 0; i < this->Arguments->Count(); i++) {
        const int argNameIndex = exe->Metadata()->GetStringIndex(this->Arguments->Array()[i]);
        exe->ArgumentNames()->Set(argNameIndex, i);
    }

    for(int i = 0; i < this->Exprs->Count(); i++) {
        this->Exprs->Array()[i]->Emit(exe);
    }
}

    // **********************
    //      CStoreExpr
    // **********************

void CStoreExpr::Emit(CExecutable* exe) const
{
    this->ValueExpr->Emit(exe);

    if(this->IsGlobal) {
        this->EmitOpcode(exe, E_OPCODE_STOREG);
        this->EmitOpcode(exe, exe->Metadata()->GetStringIndex(this->VarName));
    } else {
        // NOTE Emits a string index instead of local index because it may refer to a local
        // in outer executables which will have different local indices from the local indices
        // of the current executable anyway.
        this->EmitOpcode(exe, E_OPCODE_STOREL);
        this->EmitOpcode(exe, exe->ReserveLocal(this->VarName));
    }
}

    // *******************
    //   CIdentifierExpr
    // *******************

void CIdentifierExpr::Emit(CExecutable* exe) const
{
    this->EmitOpcode(exe, E_OPCODE_LOAD);
    this->EmitOpcode(exe, exe->Metadata()->GetStringIndex(this->IdentName));
}

    // **************
    //   CTraceExpr
    // **************

void CTraceExpr::Emit(CExecutable* exe) const
{
    this->ValueExpr->Emit(exe);

    this->EmitOpcode(exe, E_OPCODE_TRACE);
}

    // **************
    //   CSleepExpr
    // **************

void CSleepExpr::Emit(CExecutable* exe) const
{
    this->ValueExpr->Emit(exe);
    
    this->EmitOpcode(exe, E_OPCODE_SLEEP);
}

    // ***************
    //   CAssertExpr
    // ***************

void CAssertExpr::Emit(CExecutable* exe) const
{
    this->ValueExpr->Emit(exe);

    this->EmitOpcode(exe, E_OPCODE_ASSERT);
    this->EmitOpcode(exe, exe->Metadata()->GetStringIndex(this->Message));
}

    // ****************
    //   CIncludeExpr
    // ****************

void CIncludeExpr::Emit(CExecutable* exe) const
{
    this->ValueExpr->Emit(exe);

    this->EmitOpcode(exe, E_OPCODE_INCLUDE);
}

    // ****************
    //   CPreemptExpr
    // ****************

void CPreemptExpr::Emit(CExecutable* exe) const
{
    this->EmitOpcode(exe, E_OPCODE_PREEMPT);
}

    // ****************
    //   CNoPreemptExpr
    // ****************

void CNoPreemptExpr::Emit(CExecutable* exe) const
{
    this->EmitOpcode(exe, E_OPCODE_NOPREEMPT);
}

    // ****************
    //    CYieldExpr
    // ****************

void CYieldExpr::Emit(CExecutable* exe) const
{
    this->EmitOpcode(exe, E_OPCODE_YIELD);
}

    // ****************
    //    CTrueExpr
    // ****************

void CTrueExpr::Emit(CExecutable* exe) const
{
    this->EmitOpcode(exe, E_OPCODE_TRUE);
}

    // ****************
    //    CFalseExpr
    // ****************

void CFalseExpr::Emit(CExecutable* exe) const
{
    this->EmitOpcode(exe, E_OPCODE_FALSE);
}

    // *****************
    //    CNothingExpr
    // *****************

void CNothingExpr::Emit(CExecutable* exe) const
{
    this->EmitOpcode(exe, E_OPCODE_NOTHING);
}

    // **********************
    //   CNumberLiteralExpr
    // **********************

void CNumberLiteralExpr::Emit(CExecutable* exe) const
{
    this->EmitOpcode(exe, E_OPCODE_NUMBER);
    this->EmitOpcode(exe, exe->Metadata()->GetNumberIndex(this->NumberValue));
}

    // **********************
    //   CStringLiteralExpr
    // **********************

void CStringLiteralExpr::Emit(CExecutable* exe) const
{
    this->EmitOpcode(exe, E_OPCODE_STRING);
    this->EmitOpcode(exe, exe->Metadata()->GetPolkStringIndex(this->StringValue));
}

    // **********************
    //   CBodyLiteralExpr
    // **********************

void CBodyLiteralExpr::Emit(CExecutable* exe) const
{
    Auto<CExecutable> bodyExe (CExecutable::Compile(this->BodyValue,
                                                    exe->Name(),
                                                    exe->Metadata(),
                                                    exe->Log()));
    int bodyIndex = exe->AddChildExecutable(bodyExe);

    this->EmitOpcode(exe, E_OPCODE_BODY);
    this->EmitOpcode(exe, bodyIndex);
}

    // **********************
    //    CMethodCallExpr
    // **********************

CMethodCallExpr::CMethodCallExpr(const SSourceRef& sourceRef)
    : CExpr(sourceRef),
      ArgExprs(new CArrayList<CExpr*>()),
      IsReturnValueIgnored(false)
{
}

void CMethodCallExpr::Emit(CExecutable* exe) const
{
    // ******************************************
    auto const maybeNewIdent = dynamic_cast<CIdentifierExpr*>(this->ThisExpr.Ptr());
    const bool isObjectInstantiation = (maybeNewIdent != nullptr) && (maybeNewIdent->IdentName.EqualsASCII("new"));
    // ******************************************

    if(!isObjectInstantiation) // `new` opcode has no `this`
        this->ThisExpr->Emit(exe);

    for(int i = 0; i < this->ArgExprs->Count(); i++) {
        this->ArgExprs->Array()[i]->Emit(exe);
    }

    if(this->Method.EqualsASCII("!")) {
        // Calling an inner function.

        if(isObjectInstantiation) {
            throwNewSourceException(exe->Log(), "`new!` not supported.");
        }

        this->EmitOpcode(exe, E_OPCODE_CALLB);
        this->EmitOpcode(exe, this->ArgExprs->Count());
    } else {
        // Usual method call or an object instantiation (new ClassName).

        if(isObjectInstantiation)
            this->EmitOpcode(exe, E_OPCODE_NEW);
        else
            this->EmitOpcode(exe, E_OPCODE_CALL);

        this->EmitOpcode(exe, this->ArgExprs->Count()); // to check
        this->EmitOpcode(exe, exe->Metadata()->GetBasicStringIndex(this->Method)); // ClassName for `new`
    }

    if(this->IsReturnValueIgnored)
        this->EmitOpcode(exe, E_OPCODE_POP);
}

    // ******************
    //    CReturnExpr
    // *****************

void CReturnExpr::Emit(CExecutable* exe) const
{
    if(this->m_valueExpr) {

        this->m_valueExpr->Emit(exe);

        this->EmitOpcode(exe, E_OPCODE_RETURN);
        this->EmitOpcode(exe, 1);

    } else {

        this->EmitOpcode(exe, E_OPCODE_RETURN);
        this->EmitOpcode(exe, 0);

    }
}

    // *************
    //    CIfExpr
    // *************

void CIfExpr::Emit(CExecutable* exe) const
{
    // NOTE Shares data with the parent executable.
    Auto<CExecutable> conditionExe (CExecutable::Compile(this->ConditionExpr->BodyValue,
                                                         exe->Name(),
                                                         exe->Metadata(),
                                                         exe->Log()));
    Auto<CExecutable> bodyExe (CExecutable::Compile(this->BodyLiteralExpr->BodyValue,
                                                    exe->Name(),
                                                    exe->Metadata(),
                                                    exe->Log()));

    Auto<CExecutable> elseBodyExe;
    if(this->ElseBodyLiteralExpr) {
        elseBodyExe.SetPtr(CExecutable::Compile(this->ElseBodyLiteralExpr->BodyValue,
                                                exe->Name(),
                                                exe->Metadata(),
                                                exe->Log()));
    }

    if(conditionExe->ArgumentNames()->Size() > 0) {
        throwNewSourceException(exe->Log(), "Condition body should have no arguments.");
    }
    if(bodyExe->ArgumentNames()->Size() > 0) {
        throwNewSourceException(exe->Log(), "`if` body should have no arguments.");
    }
    if(elseBodyExe && elseBodyExe->ArgumentNames()->Size() > 0) {
        throwNewSourceException(exe->Log(), "`else` body should have no arguments.");
    }

    const int condIndex = exe->AddChildExecutable(conditionExe);
    const int bodyIndex = exe->AddChildExecutable(bodyExe);

    int elseBodyIndex = 0;
    if(elseBodyExe)
        elseBodyIndex = exe->AddChildExecutable(elseBodyExe);    

    if(elseBodyExe) {

        // `if ... else ...`

        // Places true or false on the stack.
        this->EmitOpcode(exe, E_OPCODE_COND);
        this->EmitOpcode(exe, condIndex);

        // The returned value is duplicated and negated.
        // There is now a condition for `else` on top of the stack.
        this->EmitOpcode(exe, E_OPCODE_DUPNEG);

        // Pops our negated condition for `else` and jumps to the else body if
        // the original condition is false. Otherwise, falls through to the next
        // check.
        this->EmitOpcode(exe, E_OPCODE_JUMP);
        this->EmitOpcode(exe, elseBodyIndex);
        this->EmitOpcode(exe, 3);

        // Our negated duplicated value was popped, so now we're back at the
        // original non-negated value. Pops the remainder and jumps to it if it's
        // true.
        this->EmitOpcode(exe, E_OPCODE_JUMP);
        this->EmitOpcode(exe, bodyIndex);
        this->EmitOpcode(exe, 3);

    } else {

        // `if` only

        this->EmitOpcode(exe, E_OPCODE_COND);
        this->EmitOpcode(exe, condIndex);
        this->EmitOpcode(exe, E_OPCODE_JUMP);
        this->EmitOpcode(exe, bodyIndex);
        this->EmitOpcode(exe, 3);

    }
}

    // **************
    //   CWhileExpr
    // **************

void CWhileExpr::Emit(CExecutable* exe) const
{
    // NOTE Shares data with the parent executable.
    Auto<CExecutable> conditionExe (CExecutable::Compile(this->ConditionExpr->BodyValue,
                                                         exe->Name(),
                                                         exe->Metadata(),
                                                         exe->Log()));
    Auto<CExecutable> bodyExe (CExecutable::Compile(this->BodyLiteralExpr->BodyValue,
                                                    exe->Name(),
                                                    exe->Metadata(),
                                                    exe->Log()));

    if(conditionExe->ArgumentNames()->Size() > 0) {
        throwNewSourceException(exe->Log(), "Condition body should have no arguments.");
    }
    if(bodyExe->ArgumentNames()->Size() > 0) {
        throwNewSourceException(exe->Log(), "`while` body should have no arguments.");
    }

    const int condIndex = exe->AddChildExecutable(conditionExe);
    const int bodyIndex = exe->AddChildExecutable(bodyExe);

    this->EmitOpcode(exe, E_OPCODE_COND);
    this->EmitOpcode(exe, condIndex);
    this->EmitOpcode(exe, E_OPCODE_JUMP);
    this->EmitOpcode(exe, bodyIndex);
    this->EmitOpcode(exe, -2); // on
}

    // ***************
    //   CAtomicExpr
    // ***************

void CAtomicExpr::Emit(CExecutable* exe) const
{
    Auto<CExecutable> bodyExe (CExecutable::Compile(this->BodyLiteralExpr->BodyValue,
                                                    exe->Name(),
                                                    exe->Metadata(),
                                                    exe->Log()));
    if(bodyExe->ArgumentNames()->Size() > 0) {
        throwNewSourceException(exe->Log(), "`atomic` body should have no arguments.");
    }

    const int bodyIndex = exe->AddChildExecutable(bodyExe);

    this->EmitOpcode(exe, E_OPCODE_ATOMIC);
    this->EmitOpcode(exe, bodyIndex);
}

    // **************
    //    CDoExpr
    // **************

CDoExpr::CDoExpr(const SSourceRef& sourceRef, CBodyLiteralExpr* doBody, CBodyLiteralExpr* undoBody)
    : CExpr(sourceRef)
{
    this->DoBodyLiteralExpr.SetVal(doBody);
    this->UndoBodyLiteralExpr.SetVal(undoBody);
}

void CDoExpr::Emit(CExecutable* exe) const
{
    Auto<CExecutable> doBodyExe (CExecutable::Compile(this->DoBodyLiteralExpr->BodyValue,
                                                      exe->Name(),
                                                      exe->Metadata(),
                                                      exe->Log()));
    Auto<CExecutable> undoBodyExe (CExecutable::Compile(this->UndoBodyLiteralExpr->BodyValue,
                                                        exe->Name(),
                                                        exe->Metadata(),
                                                        exe->Log()));

    if(doBodyExe->ArgumentNames()->Size() > 0 || undoBodyExe->ArgumentNames()->Size() > 0) {
        throwNewSourceException(exe->Log(), "`do` bodies should have no arguments.");
    }

    const int doBodyIndex = exe->AddChildExecutable(doBodyExe);
    const int undoBodyIndex = exe->AddChildExecutable(undoBodyExe);

    this->EmitOpcode(exe, E_OPCODE_PROTECTED);
    this->EmitOpcode(exe, doBodyIndex);
    this->EmitOpcode(exe, undoBodyIndex);
}

    // ***************
    //   CCancelExpr
    // ***************

void CCancelExpr::Emit(CExecutable* exe) const
{
    this->EmitOpcode(exe, E_OPCODE_CANCEL);
}

    // *******************
    //   CCancellingExpr
    // *******************

void CCancellingExpr::Emit(CExecutable* exe) const
{
    this->EmitOpcode(exe, E_OPCODE_CANCELLING);
}

} }
