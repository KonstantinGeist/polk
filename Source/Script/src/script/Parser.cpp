// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/Parser.h>
#include <core/Log.h>
#include <core/StringBuilder.h>
#include <script/Expression.h>

#include <stdarg.h>

namespace polk { namespace script {
using namespace polk::core;    
using namespace polk::collections;

    // ************************
    //      SParserContext
    // ************************

struct SParserContext
{
    const CArrayList<SToken>* Tokens;
    mutable CLog* Log;

    void throwNewSourceException(const char* msg, const SSourceRef& src, ...) const
    {
        va_list vl;
        va_start(vl, src);

        Auto<const CString> daMsg (CString::FormatImpl(msg, vl));

        va_end(vl);

        Auto<CStringBuilder> sb (new CStringBuilder());
        sb->Append(daMsg);

        Auto<const CString> srcAsStr (src.ToString());
        sb->Append(POLK_CHAR('\n'));
        sb->Append(srcAsStr);

        Auto<const CString> finalMsg (sb->ToString());
        this->Log->Write(E_LOGPRIORITY_ERROR, "Script", finalMsg);

        POLK_THROW_WITH_MSG(EC_EXECUTION_ERROR, "Parse error");
    }
};

    // ************************

static CExpr* parseValueExpr(const SParserContext& parseCtx, int start, int end);

static CArrayList<CExpr*>* parseSpecialCallArgs(const SParserContext& parseCtx,
                                                int start, int end,
                                                const char* name)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    Auto<CArrayList<CExpr*> > exprs (new CArrayList<CExpr*>());

    int prevIndex = start;
    int parenthCount = 0;

    for(int curIndex = start; curIndex <= end; curIndex++) {
        const SToken& curToken = tokens[curIndex];

        if(curToken.Type == E_TOKENTYPE_LEFTPARENTHESIS) {

            if(++parenthCount == 1)
                prevIndex = curIndex;

        } else if(curToken.Type == E_TOKENTYPE_RIGHTPARENTHESIS) {
            
            if(--parenthCount < 0) {
                parseCtx.throwNewSourceException("Unexpected `)` while parsing `%s`.", sourceRef, name);
            }

            if(parenthCount == 0) {
                Auto<CExpr> expr (parseValueExpr(parseCtx, prevIndex, curIndex));
                exprs->Add(expr);
            }

        } else if(parenthCount == 0) {
            Auto<CExpr> expr (parseValueExpr(parseCtx, curIndex, curIndex));
            exprs->Add(expr);
        }
    }
    
    if(parenthCount != 0) {
        parseCtx.throwNewSourceException("Unmatched `(` while parsing `%s`.", sourceRef, name);
    }

    exprs->Ref();
    return exprs;
}

static CExpr* parseReturnExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;
    
    Auto<CArrayList<CExpr*> > argExprs (parseSpecialCallArgs(parseCtx, start + 1, end, "return"));

    if(argExprs->Count() != 0 && argExprs->Count() != 1) {
        parseCtx.throwNewSourceException("`return` expects 0 or 1 arguments.", sourceRef);
    }

    auto const returnExpr = new CReturnExpr(sourceRef);
    if(argExprs->Count() == 1) {
        POLK_REQ_PTR(argExprs->Array()[0]);
        returnExpr->SetValueExpr(argExprs->Array()[0]);
    }

    return returnExpr;
}

static CExpr* parseWhileExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    Auto<CArrayList<CExpr*> > argExprs (parseSpecialCallArgs(parseCtx, start + 1, end, "while"));

    if(argExprs->Count() != 2) {
        parseCtx.throwNewSourceException("`while` expects 2 arguments.", sourceRef);
    }

    auto const conditionExpr = dynamic_cast<CBodyLiteralExpr*>(argExprs->Array()[0]);
    auto const bodyExpr = dynamic_cast<CBodyLiteralExpr*>(argExprs->Array()[1]);

    if(!conditionExpr || !bodyExpr) {
        parseCtx.throwNewSourceException("`while` expects 2 body literals.", sourceRef);
    }

    auto const whileExpr = new CWhileExpr(sourceRef);
    whileExpr->ConditionExpr.SetVal(conditionExpr);
    whileExpr->BodyLiteralExpr.SetVal(bodyExpr);

    return whileExpr;
}

static CExpr* parseIfExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    Auto<CArrayList<CExpr*> > argExprs (parseSpecialCallArgs(parseCtx, start + 1, end, "if"));
    const bool isIfElse = argExprs->Count() == 4;

    if(argExprs->Count() != 2 && !isIfElse) {
        parseCtx.throwNewSourceException("`if` expects 2 or 4 arguments.", sourceRef);
    }

    auto const conditionExpr = dynamic_cast<CBodyLiteralExpr*>(argExprs->Array()[0]);
    auto const bodyExpr = dynamic_cast<CBodyLiteralExpr*>(argExprs->Array()[1]);

    if(!conditionExpr || !bodyExpr) {
        parseCtx.throwNewSourceException("`if` expects 2 body literals.", sourceRef);
    }

    // ***************************************
    CBodyLiteralExpr* elseBodyExpr = nullptr;
    if(isIfElse) {
        auto const elseIdentExpr = dynamic_cast<CIdentifierExpr*>(argExprs->Array()[2]);
        // NOTE `else` has a special meaning only in this context, not a keyword. 
        if(!elseIdentExpr || !elseIdentExpr->IdentName.EqualsASCII("else")) {
            parseCtx.throwNewSourceException("`else` expected.", sourceRef);
        }

        elseBodyExpr = dynamic_cast<CBodyLiteralExpr*>(argExprs->Array()[3]);
        if(!elseBodyExpr) {
            parseCtx.throwNewSourceException("`else` expects a body literal.", sourceRef);
        }
    }
    // ***************************************

    auto ifExpr = new CIfExpr(sourceRef);
    ifExpr->ConditionExpr.SetVal(conditionExpr);
    ifExpr->BodyLiteralExpr.SetVal(bodyExpr);
    ifExpr->ElseBodyLiteralExpr.SetVal(elseBodyExpr);

    return ifExpr;
}

static CExpr* parseTraceExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    Auto<CArrayList<CExpr*> > argExprs (parseSpecialCallArgs(parseCtx, start + 1, end, "trace"));

    if(argExprs->Count() != 1) {
        parseCtx.throwNewSourceException("Trace expression expects 1 argument; given: %d.",
                                         sourceRef,
                                         argExprs->Count());
    }

    auto const traceExpr = new CTraceExpr(sourceRef);
    traceExpr->ValueExpr.SetVal(argExprs->Array()[0]);

    return traceExpr;
}

static CExpr* parseAssertExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;
    
    Auto<CArrayList<CExpr*> > argExprs (parseSpecialCallArgs(parseCtx, start + 1, end, "assert"));

    if(argExprs->Count() != 1) {
        parseCtx.throwNewSourceException("Assert expression expects 1 argument; given: %d.",
                                         sourceRef,
                                         argExprs->Count());
    }

    Auto<const CString> sourceRefAsStr (tokens[start].SourceRef.ToString());
    const SStringSlice slice (sourceRefAsStr);
    
    return new CAssertExpr(sourceRef, argExprs->Array()[0], slice);
}

static CExpr* parseIncludeExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    Auto<CArrayList<CExpr*> > argExprs (parseSpecialCallArgs(parseCtx, start + 1, end, "include"));

    if(argExprs->Count() != 1) {
        parseCtx.throwNewSourceException("Include expression expects 1 argument; given: %d.",
                                         sourceRef,
                                         argExprs->Count());
    }

    return new CIncludeExpr(sourceRef, argExprs->Array()[0]);
}

static CExpr* parseSleepExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;
    
    Auto<CArrayList<CExpr*> > argExprs (parseSpecialCallArgs(parseCtx, start + 1, end, "sleep"));
    
    if(argExprs->Count() != 1) {
        parseCtx.throwNewSourceException("`sleep` expects 1 argument.", sourceRef);
    }

    return new CSleepExpr(sourceRef, argExprs->Array()[0]);
}

static CExpr* parseDoExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    Auto<CArrayList<CExpr*> > argExprs (parseSpecialCallArgs(parseCtx, start + 1, end, "do"));

    if(argExprs->Count() != 3) {
        parseCtx.throwNewSourceException("`do` expects 3 arguments.", sourceRef);
    }
    
    auto const doBodyExpr = dynamic_cast<CBodyLiteralExpr*>(argExprs->Array()[0]);
    auto const undoIdentExpr = dynamic_cast<CIdentifierExpr*>(argExprs->Array()[1]);
    auto const undoBodyExpr = dynamic_cast<CBodyLiteralExpr*>(argExprs->Array()[2]);

    if(!doBodyExpr || !undoBodyExpr) {
        parseCtx.throwNewSourceException("`do` expects 2 body literals.", sourceRef);
    }
    if(!undoIdentExpr || !undoIdentExpr->IdentName.EqualsASCII("undo")) {
        parseCtx.throwNewSourceException("`undo` expected.", sourceRef);
    }

    return new CDoExpr(sourceRef, doBodyExpr, undoBodyExpr);
}

static CExpr* parseCancelExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    if(start != end) {
        parseCtx.throwNewSourceException("`cancel` does not accept arguments.", sourceRef);
    }

    return new CCancelExpr(sourceRef);
}

static CExpr* parsePreemptExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    if(start != end) {
        parseCtx.throwNewSourceException("`preempt` does not accept arguments.", sourceRef);
    }

    return new CPreemptExpr(sourceRef);
}

static CExpr* parseNoPreemptExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    if(start != end) {
        parseCtx.throwNewSourceException("`nopreempt` does not accept arguments.", sourceRef);
    }

    return new CNoPreemptExpr(sourceRef);
}

static CExpr* parseYieldExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    if(start != end) {
        parseCtx.throwNewSourceException("`yield` does not accept arguments.", sourceRef);
    }

    return new CYieldExpr(sourceRef);
}

static CExpr* parseAtomicExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    Auto<CArrayList<CExpr*> > argExprs (parseSpecialCallArgs(parseCtx, start + 1, end, "atomic"));    

    if(argExprs->Count() != 1) {
        parseCtx.throwNewSourceException("`atomic` expects 1 argument.", sourceRef);
    }

    auto const bodyExpr = dynamic_cast<CBodyLiteralExpr*>(argExprs->Array()[0]);
    if(!bodyExpr) {
        parseCtx.throwNewSourceException("`atomic` expects a body literal.", sourceRef);
    }

    return new CAtomicExpr(sourceRef, bodyExpr);
}

static CExpr* parseMethodCallExpr(const SParserContext& parseCtx,
                                  int start,
                                  int end,
                                  bool isReturnValueIgnored)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    if(end - start == 0) {
        parseCtx.throwNewSourceException("Empty method call.", sourceRef);
    }

    Auto<CMethodCallExpr> methodCallExpr (new CMethodCallExpr(tokens[start].SourceRef));
    methodCallExpr->IsReturnValueIgnored = isReturnValueIgnored;

    Auto<CArrayList<CExpr*> > exprs (new CArrayList<CExpr*>());
    int prevIndex = start;
    int parenthCount = 0;

    for(int curIndex = start; curIndex <= end; curIndex++) {
        const SToken& curToken = tokens[curIndex];

        if(curToken.Type == E_TOKENTYPE_LEFTPARENTHESIS) {

            if(++parenthCount == 1)
                prevIndex = curIndex + 1;

        } else if(curToken.Type == E_TOKENTYPE_RIGHTPARENTHESIS) {

            if(--parenthCount < 0) {
                parseCtx.throwNewSourceException("Superfluous `)`", sourceRef);
            }

            if(parenthCount == 0) {
                Auto<CExpr> expr (parseValueExpr(parseCtx, prevIndex, curIndex - 1));
                exprs->Add(expr);
            }

        } else if(parenthCount == 0) {

            Auto<CExpr> expr (parseValueExpr(parseCtx, curIndex, curIndex));
            exprs->Add(expr);

        }
    }

    if(parenthCount != 0) {
        parseCtx.throwNewSourceException("Parenthesis imbalance.", sourceRef);
    }

    // Converts `(a + 1)` to `a + 1` (without parentheses).
    if(exprs->Count() == 1) {
        auto const nestedMethodCall = dynamic_cast<CMethodCallExpr*>(exprs->Array()[0]);

        if(nestedMethodCall) {
            nestedMethodCall->Ref();
            return nestedMethodCall;
        }
    }

    if(exprs->Count() < 2) {
        parseCtx.throwNewSourceException("Too few arguments in a method call; at least `this` and a method name are required.",
                                         sourceRef);
    }

    auto const identifierExpr = dynamic_cast<CIdentifierExpr*>(exprs->Array()[1]);
    if(!identifierExpr) {
        parseCtx.throwNewSourceException("Invalid method name.", sourceRef);
    }

    methodCallExpr->ThisExpr.SetVal(exprs->Array()[0]);
    methodCallExpr->Method = identifierExpr->IdentName;

    for(int i = 2; i < exprs->Count(); i++) {
        methodCallExpr->ArgExprs->Add(exprs->Array()[i]);
    }

    methodCallExpr->Ref();
    return methodCallExpr;
}

static CExpr* parseValueExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    if(end - start == 0) {
        // Literal or variable name.

        if(tokens[start].Type == E_TOKENTYPE_NUMBERLITERAL) {

            auto const numberLiteralExpr = new CNumberLiteralExpr(sourceRef);
            numberLiteralExpr->NumberValue = tokens[start].NumberValue;
            return numberLiteralExpr;

        } else if(tokens[start].Type == E_TOKENTYPE_IDENTIFIER) {

            auto const varReferenceExpr = new CIdentifierExpr(sourceRef);
            varReferenceExpr->IdentName = tokens[start].StringValue;
            return varReferenceExpr;

        } else if(tokens[start].Type == E_TOKENTYPE_STRINGLITERAL) {

            auto const stringLiteralExpr = new CStringLiteralExpr(sourceRef);
            stringLiteralExpr->StringValue = tokens[start].StringValue;
            return stringLiteralExpr;

        } else if(tokens[start].Type == E_TOKENTYPE_BODYLITERAL) {

            auto const bodyLiteralExpr = new CBodyLiteralExpr(sourceRef);
            bodyLiteralExpr->BodyValue = tokens[start].StringValue;
            return bodyLiteralExpr;

        } else if(tokens[start].Type == E_TOKENTYPE_TRUELITERAL) {

            return new CTrueExpr(sourceRef);

        } else if(tokens[start].Type == E_TOKENTYPE_FALSELITERAL) {

            return new CFalseExpr(sourceRef);

        } else if(tokens[start].Type == E_TOKENTYPE_NOTHINGLITERAL) {

            return new CNothingExpr(sourceRef);

        } else if(tokens[start].Type == E_TOKENTYPE_CANCELLING) {

            return new CCancellingExpr(sourceRef);

        } else {

            parseCtx.throwNewSourceException("Unexpected expression.", sourceRef);
            return nullptr;

        }

    } else {
        // Composite value.
        return parseMethodCallExpr(parseCtx, start, end, false); // isReturnedValueIgnored=false
    }
}

static CExpr* parseGlobalStoreExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    if(end - start + 1 < 4) {
        parseCtx.throwNewSourceException("`global` expression too small.", sourceRef);
    }
    if(tokens[start + 1].Type != E_TOKENTYPE_IDENTIFIER) {
        Auto<const CString> tokenAsStr (tokens[start + 1].ToString());
        parseCtx.throwNewSourceException("Cannot use `%o` as a global variable name.",
                                         sourceRef,
                                         static_cast<const CObject*>(tokenAsStr.Ptr()));
    }
    if(tokens[start + 2].Type != E_TOKENTYPE_ASSIGNMENT) {
        Auto<const CString> tokenAsStr (tokens[start + 2].ToString());
        parseCtx.throwNewSourceException("`=` expected in a `global` expression; found: `%o`.",
                                         sourceRef,
                                         static_cast<const CObject*>(tokenAsStr.Ptr()));
    }

    Auto<CStoreExpr> storeExpr (new CStoreExpr(tokens[start].SourceRef));
    storeExpr->VarName = tokens[start + 1].StringValue;
    storeExpr->ValueExpr.SetPtr(parseValueExpr(parseCtx, start + 3, end));
    storeExpr->IsGlobal = true;

    storeExpr->Ref();
    return storeExpr;
}

static CExpr* parseLocalStoreExpr(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();
    auto sourceRef = tokens[start].SourceRef;

    if(end - start + 1 < 4) {
        parseCtx.throwNewSourceException("`local` expression too small.", sourceRef);
    }
    if(tokens[start + 1].Type != E_TOKENTYPE_IDENTIFIER) {
        Auto<const CString> tokenAsStr (tokens[start + 1].ToString());
        parseCtx.throwNewSourceException("Cannot use `%o` as a local variable name.",
                                         sourceRef,
                                         static_cast<const CObject*>(tokenAsStr.Ptr()));
    }
    if(tokens[start + 2].Type != E_TOKENTYPE_ASSIGNMENT) {
        Auto<const CString> tokenAsStr (tokens[start + 2].ToString());
        parseCtx.throwNewSourceException("`=` expected in a `local` expression; found: `%o`.",
                                         sourceRef,
                                         static_cast<const CObject*>(tokenAsStr.Ptr()));
    }

    Auto<CStoreExpr> storeExpr (new CStoreExpr(tokens[start].SourceRef));
    storeExpr->VarName = tokens[start + 1].StringValue;
    storeExpr->ValueExpr.SetPtr(parseValueExpr(parseCtx, start + 3, end));
    storeExpr->IsGlobal = false;

    storeExpr->Ref();
    return storeExpr;
}

static CExpr* parseStatement(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();

    if(tokens[start].Type == E_TOKENTYPE_GLOBAL)
        return parseGlobalStoreExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_LOCAL)
        return parseLocalStoreExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_RETURN)
        return parseReturnExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_IF)
        return parseIfExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_WHILE)
        return parseWhileExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_YIELD)
        return parseYieldExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_TRACE)
        return parseTraceExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_ASSERT)
        return parseAssertExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_INCLUDE)
        return parseIncludeExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_SLEEP)
        return parseSleepExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_ATOMIC)
        return parseAtomicExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_DO)
        return parseDoExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_CANCEL)
        return parseCancelExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_PREEMPT)
        return parsePreemptExpr(parseCtx, start, end);
    else if(tokens[start].Type == E_TOKENTYPE_NOPREEMPT)
        return parseNoPreemptExpr(parseCtx, start, end);
    else {
        if(end - start == 0) {
            return parseValueExpr(parseCtx, start, end);
        } else {
            // Nothing special matches, probably a method call.
            return parseMethodCallExpr(parseCtx, start, end, true); // isReturnedValueIgnored=true
        }
    }
}

static CExpr* parseBody(const SParserContext& parseCtx, int start, int end)
{
    const SToken* const tokens = parseCtx.Tokens->Array();

    Auto<CRootExpr> bodyExpr (new CRootExpr(tokens[start].SourceRef));

    // Ignores starting ';' which can be inserted automatically.
    while(tokens[start].Type == E_TOKENTYPE_SEMICOLON && (start + 1 < end)) {
        start++;
    }

    // ************************************************
    //  A body may have arguments.
    //
    if(tokens[start].Type == E_TOKENTYPE_DELIMITER) { // '|' found
        // Now we need to scan till we find another '|'

        int secondDelIndex = -1;
        for(int i = start + 1; i <= end; i++) {
            if(tokens[i].Type == E_TOKENTYPE_DELIMITER) {
                secondDelIndex = i;
                break;
            }
        }

        if(secondDelIndex == -1) {
            parseCtx.throwNewSourceException("Matching `|` not found.",
                                             tokens[start].SourceRef);
        }

        // Make it ignore for the rest of the function.
        if(secondDelIndex == end) {
            bodyExpr->Ref();
            return bodyExpr;
        }

        for(int i = start + 1; i <= secondDelIndex - 1; i++) {
            const SToken& argToken = tokens[i];

            if(argToken.Type != E_TOKENTYPE_IDENTIFIER) {
                parseCtx.throwNewSourceException("Arguments should be identifiers.",
                                                 argToken.SourceRef);
            }

            if(bodyExpr->Arguments->Contains(argToken.StringValue)) {
                Auto<const CString> str (argToken.StringValue.ToString());
                parseCtx.throwNewSourceException("Function already has an argument named `%o`.",
                                         argToken.SourceRef,
                                         static_cast<const CObject*>(str.Ptr()));
            }

            bodyExpr->Arguments->Add(argToken.StringValue);
        }

        start = secondDelIndex + 1;
    }
    // ************************************************

    int prevIndex = start;
    if(tokens[end].Type == E_TOKENTYPE_SEMICOLON) {
        // ';' provided.
        
        for(int curIndex = start; curIndex <= end; curIndex++) {
            if(tokens[curIndex].Type == E_TOKENTYPE_SEMICOLON) {
                if(curIndex != prevIndex) { // fixes ';'
                    Auto<CExpr> statementExpr (parseStatement(parseCtx, prevIndex, curIndex - 1));
                    bodyExpr->Exprs->Add(statementExpr);
                }

                prevIndex = curIndex + 1;
            }
        }

    } else {

        // ';' not provided.

        for(int curIndex = start; curIndex <= end + 1; curIndex++) {
            if((curIndex == end + 1) || (tokens[curIndex].Type == E_TOKENTYPE_SEMICOLON)) {

                Auto<CExpr> statementExpr (parseStatement(parseCtx, prevIndex, curIndex - 1));
                bodyExpr->Exprs->Add(statementExpr);

                prevIndex = curIndex + 1;

            }
        }

    }

    // The last method call in a body is automatically a return expr.
    // This allows to have shorter syntax, such as "while { a < 10 }" instead of
    // "while { return a < 10 }"
    if(bodyExpr->Exprs->Count() > 0) {

        CExpr* const lastExpr = bodyExpr->Exprs->Item(bodyExpr->Exprs->Count() - 1);

        // *********************************************
        // HACK? Generally, methods calls are marked as
        // ignoring their return values if they are used
        // as a statement (as opposed to a value); however,
        // we make the last statement automatically a
        // return statement if possible; and that makes
        // the method call expr to be actually using its
        // value.
        auto const methodCallExpr = dynamic_cast<CMethodCallExpr*>(lastExpr);
        if(methodCallExpr) {
            methodCallExpr->IsReturnValueIgnored = false;
        }
        // *********************************************

        if(lastExpr->IsValue()) { // NOTE that .IsValue already excludes ReturnExpr
            Auto<CReturnExpr> retExpr (new CReturnExpr(lastExpr->SourceRef()));
            retExpr->SetValueExpr(lastExpr);

            bodyExpr->Exprs->Set(bodyExpr->Exprs->Count() - 1, retExpr);
        }
    }

    bodyExpr->Ref();
    return bodyExpr;
}

CExpr* Parse(const CArrayList<SToken>* tokens, CLog* log)
{
    if(tokens->Count() == 0)
        return new CRootExpr(SSourceRef());

    SParserContext parseCtx;
    parseCtx.Tokens = tokens;
    parseCtx.Log = log;

    return parseBody(parseCtx, 0, tokens->Count() - 1);
}

} }
