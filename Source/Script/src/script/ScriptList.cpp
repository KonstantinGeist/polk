// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/ScriptList.h>
#include <core/String.h>
#include <core/StringBuilder.h>
#include <reflection/MetaObject.h>

namespace polk { namespace script {
using namespace polk::core;

const CString* CScriptList::ToString() const
{
    Auto<CStringBuilder> sb (new CStringBuilder());

    sb->Append(POLK_CHAR('['));

    for(int i = 0; i < this->Count(); i++) {
        Auto<const CString> str (this->Array()[i].ToItemString());
        sb->Append(str);

        if(i < this->Count() - 1)
            sb->Append(POLK_CHAR(' '));
    }

    sb->Append(POLK_CHAR(']'));

    return sb->ToString();
}

METACTORIMPL(List_ctor)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN

        CScriptList* const r = new CScriptList();
        *out_ret = (intptr_t)(void*)static_cast<CObject*>(r);

    METAGUARD_END
}

METASERIALIMPL(List_serialize)
{
    METAGUARD_BEGIN
        auto const self = static_cast<CScriptList*>((CObject*)(void*)obj->idptr);
        const int count = self->Count();
        METARESULT res;

        res = stream->writeimpl(stream, &count, sizeof(count));
        if(res != METARESULT_SUCCESS)
            return res;

        for(int i = 0; i < count; i++) {
            const SScriptValue v (self->Array()[i]);

            v.Serialize(stream, idTable);
        }
    METAGUARD_END
}

METADESERIALIMPL(List_deserialize)
{
    METAGUARD_BEGIN
        int count;
        METARESULT res;

        res = stream->readimpl(stream, &count, sizeof(count));
        if(res != METARESULT_SUCCESS)
            return res;
        if(count < 0) {
            return METARESULT_INTERNAL_EXCEPTION; // TODO better error
        }

        Auto<CScriptList> list (new CScriptList());

        for(int i = 0; i < count; i++) {
            const SScriptValue v (SScriptValue::Deserialize(stream, idTable));
            list->Add(v);
        }

        list->Ref();
        *out_ret = MetaValueFromPtr("List", (void*)static_cast<CObject*>(list.Ptr()));
    METAGUARD_END
}

METAMETHODIMPL(List_Add_impl)
{
    VERIFY_NARG(1)

    METAGUARD_BEGIN
        auto const self = static_cast<CScriptList*>((CObject*)(void*)_self->idptr);
        
        self->Add(SScriptValue::FromMetaValue(&args[0]));
    METAGUARD_END
}

METAMETHODIMPL(List_Set_impl)
{
    VERIFY_NARG(2)

    METAVALUE* const indexArg = &args[0];
    VERIFY_ARGTYPE(indexArg, METATYPE_NUMBER)

    METAGUARD_BEGIN
        auto const self = static_cast<CScriptList*>((CObject*)(void*)_self->idptr);

        self->Set((int)indexArg->u.asNumber, SScriptValue::FromMetaValue(&args[1]));
    METAGUARD_END
}

METAMETHODIMPL(List_Get_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const indexArg = &args[0];
    VERIFY_ARGTYPE(indexArg, METATYPE_NUMBER)

    METAGUARD_BEGIN
        auto self = static_cast<CScriptList*>((CObject*)(void*)_self->idptr);
    
        const SScriptValue ret (self->Item((int)indexArg->u.asNumber));

        *out_ret = *ret.MetaValue();
        RefMetaValue(out_ret);
    METAGUARD_END
}

METAMETHODIMPL(List_Count_impl)
{
    auto const self = static_cast<CScriptList*>((CObject*)(void*)_self->idptr);
    VERIFY_NARG(0)
    *out_ret = MetaValueFromNumber(self->Count());
    return METARESULT_SUCCESS;
}

METAMETHODIMPL(List_RemoveAt_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const indexArg = &args[0];
    VERIFY_ARGTYPE(indexArg, METATYPE_NUMBER)

    METAGUARD_BEGIN
        auto const self = static_cast<CScriptList*>((CObject*)(void*)_self->idptr);
        
        self->RemoveAt((int)indexArg->u.asNumber);
    METAGUARD_END
}

METAMETHODIMPL(List_Expand_impl)
{
    VERIFY_NARG(2)

    METAVALUE* const countArg = &args[0];
    VERIFY_ARGTYPE(countArg, METATYPE_NUMBER)
    if((int)countArg->u.asNumber < 0)
        return METARESULT_INVALID_ARGUMENT;

    METAVALUE* const valueArg = &args[1];

    METAGUARD_BEGIN
        auto const self = static_cast<CScriptList*>((CObject*)(void*)_self->idptr);

        const int count = (int)countArg->u.asNumber;
        for(int i = 0; i < count; i++) {
            self->Add(SScriptValue::FromMetaValue(valueArg));
        }
    METAGUARD_END
}

METAMETHODIMPL(List_Clear_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        auto const self = static_cast<CScriptList*>((CObject*)(void*)_self->idptr);

        self->Clear();
    METAGUARD_END
}

METAMETHODIMPL(List_Reverse_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        auto const self = static_cast<CScriptList*>((CObject*)(void*)_self->idptr);

        auto const r = new CScriptList();
        for(int i = self->Count() - 1; i >= 0; i--) {
            r->Add(self->Array()[i]);
        }

        *out_ret = MetaValueFromPtr("List", r);
    METAGUARD_END
}

void __InitScriptList()
{
    #define METHOD_COUNT 8
    static METAMETHOD g_vtable[METHOD_COUNT];
    memset(g_vtable, 0, sizeof(g_vtable));

    g_vtable[0].name = "Add";
    g_vtable[0].impl = List_Add_impl;

    g_vtable[1].name = "Set";
    g_vtable[1].impl = List_Set_impl;

    g_vtable[2].name = "Get";
    g_vtable[2].impl = List_Get_impl;

    g_vtable[3].name = "Count";
    g_vtable[3].impl = List_Count_impl;

    g_vtable[4].name = "RemoveAt";
    g_vtable[4].impl = List_RemoveAt_impl;

    g_vtable[5].name = "Expand";
    g_vtable[5].impl = List_Expand_impl;

    g_vtable[6].name = "Clear";
    g_vtable[6].impl = List_Clear_impl;

    g_vtable[7].name = "Reverse";
    g_vtable[7].impl = List_Reverse_impl;

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    metaClass.vtable = g_vtable;
    metaClass.methodCount = METHOD_COUNT;
    metaClass.name = "List";
    metaClass.ctor = List_ctor;
    metaClass.serialimpl = List_serialize;
    metaClass.deserialimpl = List_deserialize;
    metaClass.parent = GetMetaClassByName("Object");
    POLK_REQ_PTR(metaClass.parent);

    RegisterMetaClass(&metaClass);
}

void __DeinitScriptList()
{
    // Nothing.
}

} }
