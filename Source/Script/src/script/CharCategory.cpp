// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <script/CharCategory.h>
#include <core/CoreUtils.h>

namespace polk { namespace script {
using namespace polk::core;

ECharCategory GetCharCategory(polk_char16 c)
{
    if(CoreUtils::IsDigit(c) || c == POLK_CHAR('.'))
        return E_CHARCATEGORY_DIGIT;
    else if(CoreUtils::IsLetter(c) || c == POLK_CHAR('_'))
        return E_CHARCATEGORY_LETTER;
    else if(CoreUtils::IsWhiteSpace(c) && c != POLK_CHAR('\n'))
        return E_CHARCATEGORY_WHITESPACE;
    else
        return E_CHARCATEGORY_CONTROL;
}

} }
