// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef SCRIPTTHREADPOOL_H_INCLUDED
#define SCRIPTTHREADPOOL_H_INCLUDED

#include <core/Object.h>
#include <reflection/MetaObject.h>

namespace polk { namespace core {
    class CLog;
} }

namespace polk { namespace script {
struct SScriptValue;
class CAsyncMethodCallResult;

/**
 * Several tracks can share the same thread pool.
 */
class CScriptThreadPool final: public polk::core::CObject
{
    friend class CExecutionTrack;
    friend struct ExecutionTrackPrivate;

private:
    struct ScriptThreadPoolPrivate* p;

protected: // internal

    /**
     * Enqueues an asynchronous method call request. Preferable to call this method
     * from one thread only.
     *
     * @note Called by CExecutionTrack.
     * @note argCount cannot be larger than POLK_MAX_MARG_COUNT.
     */
    void EnqueueAsyncMethodCall(const SScriptValue& self,
                                METAMETHOD* method,
                                int argCount,
                                SScriptValue* args,
                                CAsyncMethodCallResult* amcResult);

public:
    /**
     * Constructor.
     *
     * @param threadCount the number or worker threads.
     */
    CScriptThreadPool(int threadCount, polk::core::CLog* log);

    virtual ~CScriptThreadPool();

    /**
     * Starts the thread pool (worker threads activated, etc.). Can be started
     * only once. Should be called from the base thread.
     */
    void Start();

    /**
     * Potentially abandoned CAsyncMethodCallResult's are unref'd on the base
     * thread for thread safety; this method should be called repeatedly on the
     * base thread to get them unref'd.
     *
     * @warning Important to call this method repeatedly or memory will grow.
     */
    void DoEvents();

    /**
     * Gracefully aborts worker threads. After this call, threads are guaranteed
     * to be aborted.
     *
     * @warning Can block for some time.
     * @warning No effect on a thread pool which was already aborted or was
     * never started.
     */
    void Abort();

    /**
     * Returns true if the current asynchronous method was abandoned, to cancel
     * a long running native operation as soon as possible. Should be polled inside
     * native asynchronous method implementations.
     *
     * @note Not a very lightweight method, do not call too often.
     * @throws EC_WRONG_THREAD if it's not a thread pool thread
     */
    static bool IsCurrentMethodAbandoned();
};

} }

#endif // SCRIPTTHREADPOOL_H_INCLUDED
