// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef SOURCEREF_H_INCLUDED
#define SOURCEREF_H_INCLUDED

#include <script/StringSlice.h>

namespace polk { namespace core {
    class CLog;
} }
namespace polk { namespace io {
    class CStream;
} }

namespace polk { namespace script {

/**
 * Represents a source reference: assigned to all tokens, expressions, etc.
 * to produce accurate error messages.
 */
struct SSourceRef
{
    // *****************
    //     Fields.
    // *****************

    SStringSlice ExecutableName;

    SStringSlice Line;
    int LineNumber;
    int CharPosition;

    // *****************
    //   Constructors.
    // *****************

    SSourceRef()
        : LineNumber(0), CharPosition(0)
    {
    }

    // *****************
    //     Identity.
    // *****************

    bool Equals(const SSourceRef& other) const;

    int GetHashCode() const;

    bool operator == (const SSourceRef& other) const
    {
        return this->Equals(other);
    }

    bool operator != (const SSourceRef& other) const
    {
        return !this->Equals(other);
    }

    // *****************
    //     Methods.
    // *****************

    const polk::core::CString* ToString() const;
};
inline void POLK_REF(const SSourceRef& s) { /* No-op. */ }
inline void POLK_UNREF(const SSourceRef& s) { /* No-op. */ }

} }

#endif // SOURCEREF_H_INCLUDED
