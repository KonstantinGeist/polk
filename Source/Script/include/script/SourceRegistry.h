// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef SOURCEREGISTRY_H_INCLUDE
#define SOURCEREGISTRY_H_INCLUDE

#include <core/Object.h>

namespace polk { namespace script {

/**
 * Maps source paths to source code. Users of Polk must provide
 * implementations themselves.
 */
class CSourceRegistry: public polk::core::CObject
{
public:
    /**
     * @return null if source not found.
     */ 
    virtual const polk::core::CString* LoadSource(const polk::core::CString* path) = 0;
};

} }

#endif // SOURCEREGISTRY_H_INCLUDE
