// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef EXECUTABLEMETADATA_H_INCLUDED
#define EXECUTABLEMETADATA_H_INCLUDED

#include <core/Object.h>
#include <collections/ArrayList.h>
#include <script/StringSlice.h>

namespace polk { namespace core {
    class CLog;
    class CString;
} }

namespace polk { namespace reflection {
    class CIdTable;
} }

namespace polk { namespace io {
    class CStream;
} }

namespace polk { namespace script {

/**
 * Identifiers and literals are converted to unique indices for quicker retrieval.
 *
 * @warning We rely on metadata being a unique executable ID generator, so it's
 * necessary that the metadata is shared by all related executables so that
 * their ID's were unique among each other.
 */
class CExecutableMetadata final: public polk::core::CObject
{
    friend class CExecutable;
    friend class CRootExpr;
    friend class CStoreExpr;
    friend class CIdentifierExpr;
    friend class CAssertExpr;
    friend class CNumberLiteralExpr;
    friend class CStringLiteralExpr;
    friend class CMethodCallExpr;
    friend struct ExecutionTrackPrivate;
    friend class CExecutionTrack;

private:
    struct ExecutableMetadataPrivate* p;

protected: // internal
    int GetStringIndex(const polk::core::CString* str) const;
    int GetStringIndex(const SStringSlice& slice) const;
    int GetNumberIndex(float number) const;
    int GetBasicStringIndex(const SStringSlice& slice) const;
    int GetPolkStringIndex(const SStringSlice& slice) const;

public:
    CExecutableMetadata();
    virtual ~CExecutableMetadata();

    float GetNumber(int index) const;
    SStringSlice GetString(int index) const;
    const char* GetBasicString(int index) const;
    const polk::core::CString* PolkStringForIndex(int index) const;

    polk::collections::CArrayList<SStringSlice>* GetStrings() const;

    void Serialize(polk::io::CStream* stream, polk::reflection::CIdTable* idTable, polk::core::CLog* log = nullptr) const;
    static CExecutableMetadata* Deserialize(polk::io::CStream* stream, polk::reflection::CIdTable* idTable, polk::core::CLog* log = nullptr);
};

} }

#endif // EXECUTABLEMETADATA_H_INCLUDED
