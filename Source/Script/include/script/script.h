// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef _SCRIPT_H_INCLUDED
#define _SCRIPT_H_INCLUDED

namespace polk { namespace script {

/**
 * Maximum supported number of arguments per method.
 */
#define POLK_MAX_MARG_COUNT 8

/**
 * Initializes the script module.
 */
void InitScript();

/**
 * Deinitializes the script module.
 */
void DeinitScript();

/**
 * Tells whether InitScript() was called. Useful to check for initialization
 * problems.
 */
bool IsScriptInitialized();

} }

#endif // _SCRIPT_H_INCLUDED
