// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef OPCODE_H_INCLUDED
#define OPCODE_H_INCLUDED

namespace polk { namespace script {

/**
 * Polk interprets opcodes because it's the easiest way to implement
 * the desired semantics of yield at any code point.
 */
enum EOpcode
{
    /**
     * Does nothing.
     */
    E_OPCODE_NOP = 0,

    /**
     * Pops the value on top of the stack and assigns it to the global variable
     * referenced by the index at arg #1.
     */
    E_OPCODE_STOREG = 1,

    /**
     * Pops the value on top of the stack and assigns it to the local variable
     * referenced by the index at arg #1.
     */
    E_OPCODE_STOREL = 2,

    /**
     * Loads a string value as referenced by the index at arg #1 and tries to
     * resolve the variable name (is it a global variable? an argument?)
     * If resolution is successful, the value of the variable is pushed on the
     * data stack.
     */
    E_OPCODE_LOAD = 3,

    /**
     * Pops the value off the top of the stack; useful when a returned value is
     * ignored (such as in method call statements).
     */
    E_OPCODE_POP = 4,

    /**
     * Pushes a number constant to the stack, as referenced by the index at arg #1.
     */
    E_OPCODE_NUMBER = 5,

    /**
     * Pushes a string constant to the stack, as referenced by the index at arg #1.
     */
    E_OPCODE_STRING = 6,

    /**
     * Pushes a boolean true value on the data stack.
     */
    E_OPCODE_TRUE = 7,

    /**
     * Pushes a boolean false value on the data stack.
     */    
    E_OPCODE_FALSE = 8,

    /**
     * Pushes nothing onto the stack.
     */
    E_OPCODE_NOTHING = 9,

    /**
     * Pushes a reference to a child executable, given the index specified at
     * arg #1.
     */
    E_OPCODE_BODY = 10,

    /**
     * Polymorphic method call: pops 'this' and the number of arguments specified
     * in arg #1. Arg #2 specifies the method name. Syntactically, it is calls
     * such as "1 + 1" or "obj doMethod arg1 arg2"
     */
    E_OPCODE_CALL = 11,

    /**
     * Pops an executable on top of the stack and performs a call on it, with
     * the number of arguments specified in arg #1. Unlike CALL, there's no
     * method name to specify, because the executable is a functor itself.
     * Syntactically, it is calls such as "obj! arg1 arg2"
     */
    E_OPCODE_CALLB = 12,

    /**
     * Similar to CALL, however, arg #2 specifies the class name instead of
     * of a method name. Returns a newly created object.
     */
    E_OPCODE_NEW = 13,

    /**
     * Returns from the whole execution track. It's possible to resume the track
     * with CExecutableTrack::Resume(). Ignored if inside an atomic block.
     */
    E_OPCODE_YIELD = 14,

    /**
     * Returns from the current executable and returns control to the parent
     * executable (if any). Arg #1 specifies the number of values on the stack
     * to consider as "returned values". Typically should be 0 or 1 for user
     * functions; can be higher for internal stuff.
     *
     * @note Bodies of `if`, `while` etc. expressions are executables too, so
     * having `return` inside one of such blocks does not actually return from
     * the whole function.
     */
    E_OPCODE_RETURN = 15,

    /**
     * Runs the child executable at the given index (arg #1).
     * The child executable should return true or false and push it on the stack.		
     */
    E_OPCODE_COND = 16,

    /**
     * Should be paired with COND. If there's true on top of the stack, the
     * function calls the child executable at the given index (arg #1), and after
     * control leaves the child executable, it jumps to an opcode in the current
     * executable at the offset specified by arg #2. If there's false on the
     * stack, the opcode is skipped altogether.
     */
    E_OPCODE_JUMP = 17,

    /**
     * Pops the value on top of the stack and prints it for debugging purposes.
     * Arg #1 specifies a string to be printed to identify the source line.
     */
    E_OPCODE_TRACE = 18,

    /**
     * Pops the value on top of the stack and, in case an assertion failure
     * happens (the popped value is false), prints the message referenced by the
     * string indexed at arg #1.
     */
    E_OPCODE_ASSERT = 19,

    /**
     * Pops a string value off the stack, locates the referenced executable by
     * name (through CSourceRegistry), interprets the executable and imports its
     * global variables into the space of the current executable. The result is cached
     * (further calls to include the same source are ignored).
     */
    E_OPCODE_INCLUDE = 20,

    /**
     * Scripts often need a non-blocking wait, so it's made a built-in opcode.
     * A script in the sleeping mode will return from Resume() without doing any
     * work until the specified amount of time (which is popped from the stack)
     * has passed.
     *
     * @note The opcode expects Resume() to be called reasonably often, otherwise
     * the opcode is not guaranteed to finish sleeping as soon as the specified
     * time runs out.
     */
    E_OPCODE_SLEEP = 21,

    /**
     * Duplicates the value on top of the stack and negates it. Used by `else`. 
     */
    E_OPCODE_DUPNEG = 22,

    /**
     * Transitions the current track to the atomic state and jumps to the child
     * executable referenced at arg #1. The track cannot yield until it exits the
     * atomic state.  Useful to make sure operations can be atomic, i.e. not
     * suspended in the middle of execution. The opcode supports recursive atomics.
     */
    E_OPCODE_ATOMIC = 23,

    /**
     * Transitions to the child executable at arg #1 (`do` block). If cancellation
     * was not injected, proceeds to the executable at arg #2 (`undo` block),
     * after which execution is continued. If, however, cancellation was injected
     * inside a `do` block, control flow immediately long jumps to the `undo`
     * block. After `undo` block is finished, it long jumps to a parent undo block
     * until there are no blocks, after which the track is finished.
     */
    E_OPCODE_PROTECTED = 24,

    /**
     * Cancels the current track (see E_OPCODE_PROTECTED).
     */
    E_OPCODE_CANCEL = 25,

    /**
     * Pushes true if the current track is in the process of being cancelled. 
     * Note that the track is not immediately considered cancelled after
     * a `cancel` opcode is issued, or CExecutableTrack::Cancel() called directly;
     * the track will transition to the cancelling state only once it's inside a
     * non-atomic `do` block.
     */
    E_OPCODE_CANCELLING = 26,

    /**
     * Enables automatic preemption. Ignored if no quantum is set. By default,
     * a fresh new track can be preempted (as if "preempt" was called right
     * before the start).
     */
    E_OPCODE_PREEMPT = 27,

    /**
     * Disables automatic preemption.
     */
    E_OPCODE_NOPREEMPT = 28
};

} }

#endif // OPCODE_H_INCLUDED
