// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef TOKEN_H_INCLUDED
#define TOKEN_H_INCLUDED

#include <core/String.h>
#include <collections/ArrayList.h>
#include <script/SourceRef.h>
#include <script/StringSlice.h>
#include <script/TokenType.h>

namespace polk { namespace core {
    class CLog;
} }

namespace polk { namespace script {

/**
 * Produced by the tokenizer.
 */
struct SToken
{
    ETokenType Type;

    SSourceRef SourceRef;

    // Only 1 of these fields is used depending on the type.
    float NumberValue;
    SStringSlice StringValue;

    static SToken From(ETokenType type, const SStringSlice& stringValue, const SSourceRef& sourceRef)
    {
        SToken r;
        r.Type = type;
        r.StringValue = stringValue;
        r.SourceRef = sourceRef;
        return r;
    }

    static SToken From(ETokenType type, float numberValue, const SSourceRef& sourceRef)
    {
        SToken r;
        r.Type = type;
        r.NumberValue = numberValue;
        r.SourceRef = sourceRef;
        return r;
    }

    const polk::core::CString* ToString() const;
};
inline void POLK_REF(const SToken& t) { /* No-op. */ }
inline void POLK_UNREF(const SToken& t) { /* No-op. */ }

    // **************
    //   Tokenize
    // **************

/**
 * Tokenizes input source code into tokens. The tokens should later be consumed
 * by Parse(..)
 */
polk::collections::CArrayList<SToken>* Tokenize(const SStringSlice& code,
                                                const polk::core::CString* exeName,
                                                polk::core::CLog* log);

} }

#endif // TOKEN_H_INCLUDED
