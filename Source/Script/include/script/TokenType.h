// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef TOKENTYPE_H_INCLUDED
#define TOKENTYPE_H_INCLUDED

namespace polk { namespace script {

enum ETokenType
{
    E_TOKENTYPE_LEFTPARENTHESIS,
    E_TOKENTYPE_RIGHTPARENTHESIS,
    E_TOKENTYPE_SEMICOLON,
    E_TOKENTYPE_LINECONTINUATION,
    E_TOKENTYPE_DELIMITER,

    E_TOKENTYPE_IDENTIFIER,

    E_TOKENTYPE_NUMBERLITERAL,
    E_TOKENTYPE_TRUELITERAL,
    E_TOKENTYPE_FALSELITERAL,
    E_TOKENTYPE_NOTHINGLITERAL,
    E_TOKENTYPE_STRINGLITERAL,
    E_TOKENTYPE_BODYLITERAL,

    E_TOKENTYPE_GLOBAL,
    E_TOKENTYPE_LOCAL,
    E_TOKENTYPE_ASSIGNMENT,

    E_TOKENTYPE_IF,
    E_TOKENTYPE_WHILE,
    E_TOKENTYPE_RETURN,

    E_TOKENTYPE_TRACE,
    E_TOKENTYPE_ASSERT,
    E_TOKENTYPE_YIELD,
    E_TOKENTYPE_INCLUDE,

    E_TOKENTYPE_SLEEP,

    E_TOKENTYPE_ATOMIC,

    E_TOKENTYPE_DO,
    E_TOKENTYPE_CANCEL,
    E_TOKENTYPE_CANCELLING,

    E_TOKENTYPE_PREEMPT,
    E_TOKENTYPE_NOPREEMPT
};

} }

#endif // TOKENTYPE_H_INCLUDED
