// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef SCRIPTLIST_H_INCLUDED
#define SCRIPTLIST_H_INCLUDED

#include <collections/ArrayList.h>
#include <script/ScriptValue.h>

namespace polk { namespace script {

/**
 * A list which can contain an arbitrary number of any script values.
 * This class is used to implement lists inside the scripting language
 * (mapped as the "List" type).
 */
class CScriptList final: public polk::collections::CArrayList<SScriptValue>
{
public:
    virtual const polk::core::CString* ToString() const override;
};

// WARNING Do not call directly.
void __InitScriptList();
void __DeinitScriptList();

} }

#endif // SCRIPTLIST_H_INCLUDED
