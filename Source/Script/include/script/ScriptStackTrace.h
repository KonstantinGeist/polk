// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef SCRIPTSTACKTRACE_H_INCLUDED
#define SCRIPTSTACKTRACE_H_INCLUDED

#include <core/Object.h>
#include <collections/ArrayList.h>
#include <script/SourceRef.h>

namespace polk { namespace script {

/**
 * Polk stack trace. Note that bodies of `if`, `while` etc. statements
 * are internally no different from closures, which means that they are reported
 * in the stack trace too, unlike in other languages.
 */
class CScriptStackTrace final: public polk::core::CObject
{
private:
    polk::core::Auto<polk::collections::CArrayList<SSourceRef>> m_sourceRefs;

public:
    CScriptStackTrace(polk::collections::CArrayList<SSourceRef>* sourceRefs);

    virtual const polk::core::CString* ToString() const override;
};

} }

#endif // SCRIPTSTACKTRACE_H_INCLUDED
