// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef STRINGSLICE_H_INCLUDED
#define STRINGSLICE_H_INCLUDED

#include <core/Object.h>
#include <core/Nullable.h>
#include <core/String.h>
#include <core/Variant.h>
#include <script/TokenType.h>

namespace polk { namespace core {
    class CLog;
    class CString;
} }

namespace polk { namespace script {
struct SSourceRef;

/**
 * A reference to a portion of a string. Used to avoid allocating thousands of
 * of CString substrings during tokenization.
 */
struct SStringSlice
{
    // *****************
    //     Fields.
    // *****************

private:
    polk::core::SVariant VSource; // reused for copy semantics

public:
    int Start, Length;

    // *****************
    //   Constructors.
    // *****************

    SStringSlice(int dummy = 0);
    explicit SStringSlice(const polk::core::CString* source);
    explicit SStringSlice(const char* source);
    SStringSlice(const polk::core::CString* source, int start, int length);
    SStringSlice(const SStringSlice& inputSS, int start, int length);

    // *****************
    //     Identity.
    // *****************

    bool Equals(const SStringSlice& other) const;

    bool operator == (const SStringSlice& other) const
    {
        return this->Equals(other);
    }

    bool operator != (const SStringSlice& other) const
    {
        return !this->Equals(other);
    }

    bool Equals(const polk::core::CString* str) const;
    bool EqualsASCII(const char* str) const;

    int GetHashCode() const;

    // *****************
    //     Methods.
    // *****************

    const polk::core::CString* Source() const
    {
        return static_cast<const polk::core::CString*>(VSource.ObjectValue());
    }

    polk_char16 CharAt(int index) const;

    bool IsBody() const;
    SStringSlice ToBody() const;

    bool IsStringLiteral() const;
    SStringSlice ToStringLiteral() const; // Should be checked with IsStringLiteral first.
    
    bool IsIdentifier() const;
    ETokenType ToKeywordOrIdentifier() const;
    
    ETokenType ToControlCharacter(const SSourceRef& sourceRef, polk::core::CLog* log) const;

    polk::core::SNullable<float> TryAsNumber() const;

    const polk::core::CString* ToString() const;
};
inline void POLK_REF(const SStringSlice& s) { /* No-op. */ }
inline void POLK_UNREF(const SStringSlice& s) { /* No-op. */ }
inline bool POLK_IS_NULL(const SStringSlice& s)
{
    return false;
}
inline bool POLK_EQUALS(const SStringSlice& a, const SStringSlice& b)
{
    return a.Equals(b);
}
inline int POLK_HASHCODE(const SStringSlice& s)
{
    return s.GetHashCode();
}

} }

#endif // STRINGSLICE_H_INCLUDED
