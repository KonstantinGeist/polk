// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef EXECUTIONTRACKMETHODS_H_INCLUDED
#define EXECUTIONTRACKMETHODS_H_INCLUDED

#include <core/Object.h>

namespace polk { namespace script {

/**
 * Contains overridable methods.
 *
 * See CExecutionTrack::SetMethods(..)
 */
class CExecutionTrackMethods: public polk::core::CObject
{
public:
    /**
     * By default, the `sleep` opcode uses Application::TickCount() to figure out
     * for how much to sleep. This may not be desirable if an application uses
     * virtual time (that can be stopped, slowed down etc.)
     */
    virtual polk_long TickCount() const = 0;
};

} }

#endif // EXECUTIONTRACKMETHODS_H_INCLUDED
