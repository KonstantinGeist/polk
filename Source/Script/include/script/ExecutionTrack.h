// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef EXECUTIONTRACK_H_INCLUDED
#define EXECUTIONTRACK_H_INCLUDED

#include <core/Variant.h>
#include <collections/HashMap.h>
#include <script/ScriptValue.h>
#include <script/SourceRegistry.h>

#ifdef Resume
    #undef Resume
#endif

namespace polk { namespace core {
    class CLog;
    class CString;
} }

namespace polk { namespace io {
    class CStream;
} }

namespace polk { namespace script {
class CExecutable;
struct SStringSlice;
class CScriptStackTrace;
class CScriptThreadPool;

    // ********************
    //   CVariableContext
    // ********************

/**
 * Connects a variable ID to its actual value.
 */
class CVariableContext final: public polk::collections::CHashMap<int, SScriptValue>
{
public:
    CVariableContext* Clone() const;
};

    // *******************
    //   CExecutionTrack
    // *******************

/**
 * An independent thread of execution. There can be hundreds of execution
 * tracks on an OS thread, provided they have appropriate quanta set (see
 * SetQuantum(..)).
 */
class CExecutionTrack final: public polk::core::CObject
{
    friend struct ExecutionTrackPrivate;
    friend class CExecutableEventHandler;

private:
    struct ExecutionTrackPrivate* p;

public:
    /**
     * Creates a new execution track which will execute the provided executable.
     * To actually start executing the executable, call Resume() (see).
     */
    CExecutionTrack(CExecutable* exe);

    /**
     * Creates a new execution track with a known variable context. Allows tracks
     * to share global variables.
     *
     * @note Since variable contexts depend on metadata for matching string indices
     * to variable names and vice versa, the executable must also have the same
     * metadata as the metadata of the track the variable context belongs to
     * (if it does).
     */
    CExecutionTrack(CExecutable* exe, CVariableContext* varContext);

    virtual ~CExecutionTrack();

    /**
     * Built-in behavior of an execution track can be modified using this interface.
     * See also CExecutionTrackMethods.
     */
    void SetMethods(class CExecutionTrackMethods* methods);

    /**
     * Returns the current top executable, or null if the track finished execution.
     */
    CExecutable* TopExecutable() const;

    /**
     * To allow an execution track get access to other scripts, one should
     * first set the source registry. See also CSourceRegistry.
     */
    CSourceRegistry* SourceRegistry() const;

    /**
     * See SourceRegistry()
     */
    void SetSourceRegistry(CSourceRegistry* registry);

    /**
     * A track which makes use of asynchronous method calls should have a reference
     * to a thread pool. Several tracks can share the same thread pool.
     */
    void SetThreadPool(CScriptThreadPool* threadPool);

    /**
     * See SetThreadPool(..)
     */
    CScriptThreadPool* ThreadPool() const;

    /**
     * All global variables are saved to this context object. Can be shared
     * among several tracks (i.e. tracks would share global variables).
     */
    CVariableContext* VariableContext() const;

    /**
     * Extracts all variables.
     *
     * @note For finished tracks, returns null.
     */
    polk::collections::CHashMap<SStringSlice, SScriptValue>* GetVariables() const;

    /**
     * If the last expression of the executable's script is a return expression
     * or a value expression, the expression's value will be saved to this property.
     */
    SScriptValue ReturnValue() const;

    /**
     * Sets the value of a global variable given the name of the variable.
     *
     * @note If the variable context (see VariableContext()) is shared by other
     * tracks, the change will be visible to other tracks as well.
     */
    void SetVariable(const SStringSlice& name, const SScriptValue& value);

    /**
     * Retrieves the value of a global variable given the name of the variable.
     *
     * @note Returns a nothing value if the track finished execution.
     */
    SScriptValue GetVariable(const SStringSlice& name) const;

    /**
     * Returns true if a variable with this name exists. A variable does not exist
     * if there's no such variable or its value is nothing.
     */
    bool HasVariable(const SStringSlice& name) const;

    /**
     * Associates the track with custom user data. Useful for associating
     * a track with custom data without exposing it to the script code.
     */
    void SetData(const polk::core::SVariant& v);

     /**
      * See SetData(..)
      */
    polk::core::SVariant Data() const;

    /**
     * Associates this track with a host-specific name. Useful for debugging
     * to tell tracks from each other by their names.
     */
    void SetName(const polk::core::CString* name) const;

    /**
     * See SetName(..)
     */
    const polk::core::CString* Name() const;

    /**
     * Specifies how many instructions to interpret before automatically yielding.
	 * Ignored for child tracks, which inherit it from parent tracks.
     *
     * See also Resume().
     */
    void SetQuantum(int quantum);

    /**
     * See SetQuantum(..)
     */
    int Quantum() const;

    /**
     * Returns true if the track is preempted automatically (quantum > 0).
     */
    bool IsPreemptable() const;

    /**
     * Starts execution or, if execution yielded, resumes execution.
     * Automatic yielding is activated only if the quantum (see SetQuantum(..))
     * is > 0. Returns true if yielded (there's still code to execute); false if
     * script completed. Typical usage is while(track->Resume()) { }
     */
    bool Resume();

    /**
     * Cancels the track: waits till it enters the first possible `do` block,
     * jumps to the corresponding `undo` block and unwinds execution until
     * all `undo` blocks are executed.
     */
    void Cancel();

    /**
     * Extracts the current stack trace.
     */
    CScriptStackTrace* GetStackTrace() const;

    /**
     * Serializes the entire track state to a stream. It will be possible to
     * deserialize the track later with Deserialize(..)
     *
     * @note Serialization should take place in a parked position, i.e. when
     * the track is not executing any code and it's outside of any atomic blocks,
     * etc. Basically, this method should not be called from inside the track itself.
     * This requirement makes serialization safer because atomic operations are
     * not torn apart, etc.
     *
     * @note All the script values the track references as global, local variables
     * should be serializable. Otherwise, may throw. See also SScriptValue::Serialize(..)
     * and SScriptValue::Deserialize(..)
     */
    void Serialize(polk::io::CStream* stream, polk::core::CLog* log = nullptr) const;

    /**
     * See Serialize(..)
     */
    static CExecutionTrack* Deserialize(polk::io::CStream* stream, polk::core::CLog* log = nullptr);

	/**
	 * Enables debugging for this track. With debugging enabled, breakpoints
	 * can be hit.
	 */
	void EnableDebugging(bool b);

    /**
     * Used during debugging to inspect track locals, arguments and globals after
     * a breakpoint was hit.
     */
    SScriptValue GetIdent(const polk::core::CString* identName) const;
};

} }

#endif // EXECUTIONTRACK_H_INCLUDED
