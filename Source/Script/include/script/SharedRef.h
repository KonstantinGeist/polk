// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef SHAREDREF_H_INCLUDED
#define SHAREDREF_H_INCLUDED

#include <core/Object.h>
#include <script/ScriptValue.h>

namespace polk { namespace script {

/**
 * A shared reference. Allows to share data between detached callbacks, mapped as
 * "SharedRef" in Polk.
 * After the parent executable which described callbacks finishes and is destroyed,
 * the surviving callbacks get "detached" from it, i.e. they receive their own
 * copy of the variable context and stop being able to share variable updates
 * between each other because they have separate variable contexts. SharedRef
 * solves this problem by creating a level of indirection: if callbacks had a
 * reference to a shared reference before detachment/separation, they still can
 * communicate via the shared reference indirection by using methods Set/Get.
 */
class CSharedRef final: public polk::core::CObject
{
private:
    SScriptValue m_value;

public:
    void Set(const SScriptValue& value)
    {
        m_value = value;
    }

    SScriptValue Get() const
    {
        return m_value;
    }

    virtual const polk::core::CString* ToString() const override
    {
        return m_value.ToString();
    }
};

// WARNING Do not call directly.
void __InitSharedRef();
void __DeinitSharedRef();

} }

#endif // SHAREDREF_H_INCLUDED
