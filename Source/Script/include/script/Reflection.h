// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef REFLECTION_H_INCLUDED
#define REFLECTION_H_INCLUDED

#include <core/Object.h>
#include <collections/ArrayList.h>
#include <reflection/MetaObject.h>

namespace polk { namespace script {

class CMethod;

/**
 * Wraps a metaclass to expose the metaobject system to itself.
 */
class CClass: public polk::core::CObject
{
private:
    METACLASS* m_pClass;

public:
    explicit CClass(METACLASS* pClass);

    /**
     * The wrapped metaclass.
     */
    METACLASS* MetaClass() const;

    /**
     * The name of the class as registered in the metaobject system.
     */
    const polk::core::CString* GetName() const;

    /**
     * A static class contains only static methods; that is, its target of invocation
     * is always null.
     */
    bool IsStatic() const;

    /**
     * Returns an object wrapping a metamethod belonging to this class. Returns null if no method under such name was found.
     */
    CMethod* GetMethod(const polk::core::CString* name) const;

    /**
     * Retrieves all registered methods of this metaclass.
     */
    polk::collections::CArrayList<CMethod*>* GetMethods() const;

    virtual const polk::core::CString* ToString() const override;
};

class CMethod: public polk::core::CObject
{
private:
    METAMETHOD* m_pMethod;
    METACLASS* m_pClass;

public:
    CMethod(METAMETHOD* pMethod, METACLASS* pClass);

    /**
     * The name of the method as registered in the metaobject system.
     */
    const polk::core::CString* GetName() const;

    /**
     * An asynchronous method is always scheduled on a thread pool.
     */
    bool IsAsync() const;

    /**
     * Invokes the wrapped metamethod as-is.
     */
    METARESULT Invoke(int nArg, METAVALUE* args, METAVALUE* out_ret);

    virtual const polk::core::CString* ToString() const override;
};

namespace Reflection {

polk::collections::CArrayList<CClass*>* GetClasses();

}

// Do not call directly.
void __InitReflection();
void __DeinitReflection();

} }

#endif // REFLECTION_H_INCLUDED
