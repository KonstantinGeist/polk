// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef SCRIPTMAP_H_INCLUDED
#define SCRIPTMAP_H_INCLUDED

#include <collections/HashMap.h>
#include <script/ScriptValue.h>

namespace polk { namespace script {

/**
 * A hash map which can contain an arbitrary number of key-value pairs.
 * This class is used to implement maps inside the scripting language
 * (mapped as the "Map" type).
 */
class CScriptMap final: public polk::collections::CHashMap<SScriptValue, SScriptValue>
{
public:
    virtual const polk::core::CString* ToString() const override;
};

// WARNING Do not call directly.
void __InitScriptMap();
void __DeinitScriptMap();

} }

#endif // SCRIPTMAP_H_INCLUDED
