// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef SCRIPTVALUE_H_INCLUDED
#define SCRIPTVALUE_H_INCLUDED

#include <core/Object.h>
#include <reflection/MetaObject.h>

namespace polk { namespace core {
    class CLog;
    class CString;
} }

namespace polk { namespace reflection {
    class CIdTable;
} }

namespace polk { namespace io {
    class CStream;
} }

namespace polk { namespace script {

/**
 * Represents a Polk value.
 * Internally, a wrapper around METAVALUE with automatic ref/unref, which allows
 * to natively connect Polk values to the metaobject system.
 * The semantics and the API are similar to those of SVariant.
 */
struct SScriptValue
{
private:
    METAVALUE m_metaValue;

public:
    // A hack TODO.
    SScriptValue(void* v);

    SScriptValue();
    ~SScriptValue();
    SScriptValue(const SScriptValue& copy);
    SScriptValue& operator=(const SScriptValue& rhs);

    /**
     * Returns true if the value is a dummy value to represent a static class
     * (i.e. the type is METATYPE_OBJECT, but the idptr is null).
     */
    bool IsStatic() const;

    void SetBool(bool value);
    void SetNumber(float value);
    void SetObject(const char* klassName, intptr_t ptr);
    void SetNothing();

    bool BoolValue() const;
    float NumberValue() const;

    /**
     * Checks if the current class name is klassName; if not,
     * returns null.
     */
    intptr_t ObjectValue(const char* klassName) const;

    METATYPE Type() const { return m_metaValue.type; }

    bool Equals(const SScriptValue& other) const;
    int GetHashCode() const;

    const polk::core::CString* ToString() const;

    /**
     * Returns the string as it should be viewed as part of containers'
     * ToString implementations.
     */
    const polk::core::CString* ToItemString() const;

    METAVALUE* MetaValue()
    {
        return &m_metaValue;
    }

    const METAVALUE* MetaValue() const
    {
        return &m_metaValue;
    }

    /**
     * Tries to cast the value to a CObject-derived object.
     *
     * @warning If the internal idptr is not a pointer to a CObject-derived
     * object (cast to CObject* before saving), the behavior is undefined.
     *
     * @note The returned value is NOT pre-ref'd.
     */
    template <class T>
    T* TryCast(const char* klassName) const
    {
        if(m_metaValue.type == METATYPE_OBJECT
        && IsInstanceOf(const_cast<METAOBJECT*>(&m_metaValue.u.asObject), klassName))
        {
            return dynamic_cast<T*>(reinterpret_cast<polk::core::CObject*>(m_metaValue.u.asObject.idptr));
        } else {
            return nullptr;
        }
    }

    /**
     * If the script value wraps an object, returns its class. Otherwise (if it's
     * a number, a boolean, a nothing etc.), returns null. Used for probing built-in
     * types.
     */
    const char* ClassName() const;

    /** 
     * Makes use of SerializeMetaValue(..)
     */
    void Serialize(polk::io::CStream* stream, polk::reflection::CIdTable* idTable, polk::core::CLog* log = nullptr) const;
    void Serialize(METASTREAM* stream, METAIDTABLE* idTable, polk::core::CLog* log = nullptr) const;

    /** 
     * Makes use of DeserializeMetaValue(..)
     */
    static SScriptValue Deserialize(polk::io::CStream* stream, polk::reflection::CIdTable* idTable, polk::core::CLog* log = nullptr);
    static SScriptValue Deserialize(METASTREAM* stream, METAIDTABLE* idTable, polk::core::CLog* log = nullptr);

    static SScriptValue FromNothing();
    static SScriptValue FromNumber(float f);
    static SScriptValue FromBool(bool b);
    static SScriptValue FromString(const char* str);
    static SScriptValue FromIdPtr(const char* klassName, intptr_t ptr);
    static SScriptValue FromMetaValue(METAVALUE* metaValue);
    static SScriptValue FromMetaValueNoRef(METAVALUE* metaValue);
};
inline void POLK_REF(const SScriptValue& v) { /* No-op. */ }
inline void POLK_UNREF(const SScriptValue& v) { /* No-op. */ }
inline bool POLK_EQUALS(const SScriptValue& v1, const SScriptValue& v2)
{
    return v1.Equals(v2);
}
inline int POLK_HASHCODE(const SScriptValue& v1)
{
    return v1.GetHashCode();
}
inline bool POLK_IS_NULL(const SScriptValue& v)
{
    return false;
}

} }

#endif // SCRIPTVALUE_H_INCLUDED
