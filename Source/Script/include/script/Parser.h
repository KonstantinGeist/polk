// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef PARSER_H_INCLUDED
#define PARSER_H_INCLUDED

#include <collections/ArrayList.h>
#include <script/Token.h>

namespace polk { namespace core {
    class CLog;
} }

namespace polk { namespace script {
class CExpr;

/**
 * Takes a list of tokens and outputs an hierarchy of expressions.
 */
CExpr* Parse(const polk::collections::CArrayList<SToken>* tokens, polk::core::CLog* log);

} }

#endif // PARSER_H_INCLUDED
