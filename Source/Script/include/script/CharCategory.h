// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef CHARCATEGORY_H_INCLUDED
#define CHARCATEGORY_H_INCLUDED

#include <core/basedefs.h>

namespace polk { namespace script {

/**
 * Character category - used by the tokenizer.
 */
enum ECharCategory
{
    E_CHARCATEGORY_NONE,
    E_CHARCATEGORY_LETTER,
    E_CHARCATEGORY_DIGIT,
    E_CHARCATEGORY_WHITESPACE,
    E_CHARCATEGORY_CONTROL
};

ECharCategory GetCharCategory(polk_char16 c);

} }

#endif // CHARCATEGORY_H_INCLUDED
