// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef EXECUTABLE_H_INCLUDED
#define EXECUTABLE_H_INCLUDED

#include <core/Object.h>
#include <collections/ArrayList.h>
#include <collections/HashMap.h>

namespace polk { namespace core {
    class CLog;
    class CString; 
} }

namespace polk { namespace reflection {
    class CIdTable;
} }

namespace polk { namespace io {
    class CStream;
} }

namespace polk { namespace script {
class CExecutableMetadata;
struct SStringSlice;
struct SSourceRef;

/**
 * Script implementation. Compiled from source code and executed on execution
 * tracks.
 */
class CExecutable final: public polk::core::CObject
{
    friend struct ExecutionTrackPrivate;
    friend struct ExecutablePrivate;
    friend class CBodyLiteralExpr;
    friend class CIfExpr;
    friend class CWhileExpr;
    friend class CExpr;
    friend class CStoreExpr;
    friend class CAtomicExpr;
    friend class CDoExpr;

private:
    struct ExecutablePrivate* p;

protected: // internal
    CExecutable(const polk::core::CString* name,
                CExecutableMetadata* ctx,
                polk::core::CLog* log,
                bool localVarDummy = false);

    void EmitOpcode(int opcode, const SSourceRef& sourceRef);

    /**
     * Reserves a local in the current executable. Returns a string ID (see also
     * CStoreExpr).
     */
    int ReserveLocal(const SStringSlice& localName);

    int AddChildExecutable(CExecutable* childExe);

    bool TryExecuteBreakpointHandler(int bytecodeIndex);

public:
    virtual ~CExecutable();

    /**
     * The ID that uniquely identifies the executable inside a source or a deserialized
     * executable tree (it's not guaranteed to be unique across several
     * sources/deserializations).
     *
     * @note Implementation detail: a UID rollover can happen if 2^32 executables
     * are created inside an executable tree at once (which is very unlikely to
     * happen in practice).
     */
    int LocalUID() const;

    /**
     * Executable names are used to identify executables and tell them one
     * from another.
     */
    const polk::core::CString* Name() const;

    /**
     * See CExecutableMetadata
     */
    CExecutableMetadata* Metadata() const;

    /**
     * Maps bytecode index => SSourceRef of the bytecode (source information).
     */
    const polk::collections::CArrayList<SSourceRef>* SourceRefMap() const;

    /**
     * Emitted bytecode of the executable. Directly interpretable by CExecutionTrack.
     */
    const polk::collections::CArrayList<int>* RootBytecode() const;

    /**
     * Maps identifier ID in metadata to the actual argument slot ID
     * of the function.
     */
    polk::collections::CHashMap<int, int>* ArgumentNames() const;

    /**
     * Maps identifier ID in metadata to the actual local variable slot ID
     * of the frame.
     */
    polk::collections::CHashMap<int, int>* LocalMap() const;

    /**
     * Returns true if the executable is a dummy which exists to only provide
     * local/argument data.
     */
    bool IsLocalVarDummy() const;

    /**
     * Parent executable.
     */
    CExecutable* ParentExecutable() const;

    polk::core::CLog* Log() const;

    /**
     * Tries to compile an executable. Returns null on error, which is written
     * to the log.
     */
    static CExecutable* Compile(const polk::core::CString* code,
                                const polk::core::CString* name,
                                polk::core::CLog* log);

    /**
     * Compiles with a given metadata object.
     */
    static CExecutable* Compile(const SStringSlice& code,
                                const polk::core::CString* name,
                                CExecutableMetadata* ctx,
                                polk::core::CLog* log);

    /**
     * Maps a child executable index to the actual executable. Used by the interpreter.
     */
    CExecutable* ChildExecutableAt(int index) const;

    /**
     * Converts this executable and all its children recursively into a disassembled
     * string. For debugging.
     */
    const polk::core::CString* Disassemble(int level = 0) const;

    /**
     * Serializes the executable to a stream. Used to implement track serialization.
     */
    void Serialize(polk::io::CStream* stream, polk::reflection::CIdTable* idTable, polk::core::CLog* log = nullptr) const;

    /**
     * Deserializes an executable from a stream. Used to implement track
     * serialization.
     */
    static CExecutable* Deserialize(polk::io::CStream* stream, polk::reflection::CIdTable* idTable, polk::core::CLog* log = nullptr);

    /**
     * Sets a breakpoint for this executable. The line number corresponds to
     * the original source code. Breakpoints are set for executables instead of
     * individual tracks because we want to be able to catch events in all tracks
     * at once. Pass null as handler to unset the breakpoint.
     *
     * @note Setting a breakpoint is not enough, debugging should also be
     * enabled for tracks (see CExecutionTrack::EnableDebugging(..)) This allows
     * to select only required tracks for debugging.
     * @note When a breakpoint is hit, the track executing the executable is suspended.
     * User code is reponsible for suspending other tracks in the system as required.
     * @note Breakpoints inside atomic blocks are ignored.
     * @note Setting breakpoints from under breakpoint handlers isn't guaranteed
     * to work.
     */
    void SetBreakpoint(int lineNumber, class CBreakpointHandler* handler);

    /**
     * Unsets all set breakpoints and deletes all associated data.
     */
    void ClearAllBreakpoints();

    /**
     * Returns a list of line numbers for which breakpoints are set. Useful
     * for editors to synchronize UI with set breakpoints. Returns it for all
     * child executables.
     */
    polk::collections::CArrayList<int>* GetSetBreakpoints() const;

    virtual const polk::core::CString* ToString() const override;
};

// WARNING Do not call directly.
void __InitExecutable();
void __DeinitExecutable();

} }

#endif // EXECUTABLE_H_INCLUDED
