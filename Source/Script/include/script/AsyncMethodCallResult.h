// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef ASYNCMETHODCALLRESULT_H_INCLUDED
#define ASYNCMETHODCALLRESULT_H_INCLUDED

#include <core/AtomicObject.h>
#include <reflection/MetaObject.h>
#include <script/script.h> // for constants

namespace polk { namespace script {

/**
 * Execution track is not waiting for an asynchronous method call.
 */
#define E_ASYNCMETHODSTATE_NONE 0

/**
 * Execution track is waiting for an asynchronous method call.
 */
#define E_ASYNCMETHODSTATE_WAITING 1

/**
 * Execution track has received a result of a method call, should stop yielding
 * on resume and start executing code again.
 */
#define E_ASYNCMETHODSTATE_HAS_RESULT 2

/**
 * Results of asynchronous method calls are written to CAsyncMethodCallResult's
 * by thread pools and read by tracks. This class allows to remove cyclic
 * dependencies between thread pools and tracks + ensures thread-safety.
 */
class CAsyncMethodCallResult final: public polk::core::CAtomicObject // needs to be atomic
{
public:
    // See E_ASYNCMETHODSTATE_XXX above.
    polk_atomic_int State;

    // If a track abandons its result object (because the track is deleted),
    // it marks the result object as abandoned. This will help the thread pool
    // to ignore abandoned calls (otherwise it could lock up the entire system
    // trying to perform thousands of long-running abandoned calls before it gets
    // to perform any non-abandoned calls).
    polk_atomic_int Abandoned;

    METARESULT Result;
    METAVALUE ReturnValue;

    // We need to give the track a chance to unref `self` on the original thread,
    // because it's owned by the base thread and we do not know if its reference-
    // counting is safe. In case the track abandons the result, this class'
    // destructor will take care of unref'ing it, then.
    METAVALUE Self;

    CAsyncMethodCallResult()
        : State(E_ASYNCMETHODSTATE_NONE), Abandoned(0),
          Result(METARESULT_SUCCESS), ReturnValue(MetaValueFromNull()),
          Self(MetaValueFromNull())
    {
    }

    virtual ~CAsyncMethodCallResult()
    {
        // Required to unref the held values in case the result object is
        // abandoned by its track after a thread pool already assigned values.
        // Normally, the value should be unref'd and set to null by the execution
        // track once it resumes and fetches the result of the asynchronous method
        // call, hence making this call a no-op if its type is METATYPE_NULL.

        bool deleted;

        UnrefMetaValue(&this->ReturnValue, &deleted);
        UnrefMetaValue(&this->Self, &deleted);
    }
};

} }

#endif // ASYNCMETHODCALLRESULT_H_INCLUDED
