// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef POLKSCRIPT_DEBUGGING_H_INCLUDED
#define POLKSCRIPT_DEBUGGING_H_INCLUDED

#include <core/Object.h>

namespace polk { namespace script {

class CBreakpointHandler: public polk::core::CObject
{
public:
	virtual void Invoke(int lineNumber) = 0;
};

} }

#endif // POLKSCRIPT_DEBUGGING_H_INCLUDED
