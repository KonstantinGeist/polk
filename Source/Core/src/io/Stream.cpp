// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <io/Stream.h>
#include <core/Contract.h>
#include <core/Exception.h>
#include <reflection/MetaObject.h>

namespace polk { namespace io {
using namespace polk::core;

bool CStream::CanSeek() const
{
    return false;
}

polk_long CStream::GetPosition() const
{
    POLK_THROW(EC_NOT_IMPLEMENTED);
    return 0;
}

void CStream::SetPosition(polk_long pos)
{
    POLK_THROW(EC_NOT_IMPLEMENTED);
}

polk_long CStream::Size() const
{
    POLK_THROW(EC_NOT_IMPLEMENTED);
    return 0;
}

polk_long CStream::Read(char* buf, polk_long count, bool allowPartial)
{
    return this->Read(buf, count);
}

polk_long CStream::Write(const char* buf, polk_long count, bool allowPartial)
{
    return this->Write(buf, count);
}

#define READTO_BUFSZ 1024
void CStream::ReadTo(CStream* stream, polk_long sz)
{
    POLK_REQ_NOT_NEG(sz);

    char buf[READTO_BUFSZ];
    polk_long totalSize = 0;

    while(totalSize < sz) {
        int bReq = sz - totalSize;
        if(bReq > READTO_BUFSZ)
            bReq = READTO_BUFSZ;

        const polk_long bRead = this->Read(buf, bReq, true);
        if(bRead < READTO_BUFSZ) {
            // The remnant.
            stream->Write(buf, bRead);
            break;
        } else {
            stream->Write(buf, READTO_BUFSZ);
        }

        totalSize += bRead;
    }
}

void CStream::Flush()
{
    // No-op.
}

    // **********************
    //      ToMetaStream
    // **********************

static METARESULT CStream_Write_wrapper(METASTREAM* metaStream, const void* p, size_t sz)
{
    auto const stream = static_cast<CStream*>(metaStream->ctx);

    const polk_long r = stream->Write(static_cast<const char*>(p), (polk_long)sz);
    if(r != (polk_long)sz)
        return METARESULT_INTERNAL_EXCEPTION; // TODO

    return METARESULT_SUCCESS;
}

static METARESULT CStream_Read_wrapper(METASTREAM* metaStream, void* p, size_t sz)
{
    auto const stream = static_cast<CStream*>(metaStream->ctx);

    const polk_long r = stream->Read(static_cast<char*>(p), (polk_long)sz);
    if(r != (polk_long)sz)
        return METARESULT_INTERNAL_EXCEPTION; // TODO

    return METARESULT_SUCCESS;
}

void CStream::ToMetaStream(METASTREAM* out_r)
{
    out_r->writeimpl = CStream_Write_wrapper;
    out_r->readimpl = CStream_Read_wrapper;
    out_r->ctx = static_cast<void*>(this);
}

    // **************************
    //    CreateFromMetaStream
    // **************************

class METASTREAMStream final: public CStream
{
private:
    METASTREAM m_metaStream;

public:
    METASTREAMStream(METASTREAM* metaStream)
        : m_metaStream(*metaStream)
    {
    }

    virtual bool CanRead() const override
    {
        return m_metaStream.readimpl != nullptr;
    }

    virtual bool CanWrite() const override
    {
        return m_metaStream.writeimpl != nullptr;
    }

    virtual polk_long Read(char* buf, polk_long count) override
    {
        m_metaStream.readimpl(&m_metaStream, buf, (size_t)count);
        return count; // TODO
    }

    virtual polk_long Write(const char* buf, polk_long count) override
    {
        m_metaStream.writeimpl(&m_metaStream, buf, (size_t)count);
        return count; // TODO
    }
};

CStream* CStream::CreateFromMetaStream(METASTREAM* metaStream)
{
    return new METASTREAMStream(metaStream);
}

} }
