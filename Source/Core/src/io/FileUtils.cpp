// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <io/FileUtils.h>
#include <io/FileStream.h>
#include <io/TextReader.h>
#include <io/TextWriter.h>
#include <core/StringBuilder.h>

#include <reflection/MetaObject.h>

namespace polk { namespace io {
using namespace polk::core;
using namespace polk::collections;

namespace FileUtils {

CArrayList<const CString*>* ReadAllLines(CStream* stream)
{
    Auto<CTextReader> reader (new CTextReader(stream));
    Auto<CArrayList<const CString*> > r (new CArrayList<const CString*>());

    while(const CString* const str = reader->ReadLine()) {
        r->Add(str);
        str->Unref();
    }

    r->Ref();
    return r;
}

CArrayList<const CString*>* ReadAllLines(const CString* path)
{
    Auto<CFileStream> stream (CFileStream::Open(path, E_FILEACCESS_READ));
    return ReadAllLines(stream);
}

const CString* ReadAllText(CStream* stream)
{
    Auto<CTextReader> reader (new CTextReader(stream));
    CStringBuilder sb;

    while(const CString* str = reader->ReadLine()) {
        sb.Append(str);
        sb.AppendLine();
        str->Unref();
    }

    return sb.ToString();
}

const CString* ReadAllText(const CString* path)
{
    Auto<CFileStream> stream (CFileStream::Open(path, E_FILEACCESS_READ));
    return ReadAllText(stream);
}

void WriteAllLines(const CString* path, CArrayList<const CString*>* lines)
{
    Auto<CFileStream> stream (CFileStream::Open(path, E_FILEACCESS_WRITE));
    Auto<CTextWriter> writer (new CTextWriter(stream));

    for(int i = 0; i < lines->Count(); i++) {
        const CString* const line = lines->Array()[i];

        writer->Write(line);
        writer->WriteLine();
    }
}

polk_byte* ReadAllBytes(CStream* stream, polk_long* out_size)
{
    const polk_long streamSize = stream->Size();
    if(out_size)
        *out_size = streamSize;
    POLK_REQ_POS(streamSize);

    polk_byte* const tmpBuf = new polk_byte[streamSize];
    const polk_long readSize = stream->Read((char*)tmpBuf, streamSize);
    if(readSize != streamSize) {
        POLK_THROW(EC_BAD_FORMAT);
    }

    return tmpBuf;
}

}

// *************************
    // ***************
    //   Reflection.
    // ***************
// *************************

METAMETHODIMPL(FileUtils_ReadAllText_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        const CString* const str = FileUtils::ReadAllText(METAVALUE_TO_STRINGPTR(pathArg));
        *out_ret = STRINGPTR_TO_METAVALUE(str);
    METAGUARD_END
}

void __InitMetaFileUtils()
{
    BEGIN_VTABLE(1)
        DEFINE_VMETHOD("ReadAllText", FileUtils_ReadAllText_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "FileUtils";
    metaClass.flags = METACLASSFLAGS_STATIC;

    RegisterMetaClass(&metaClass);
}

} }
