// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <io/ByteBuffer.h>
#include <core/CoreUtils.h>
#include <core/Exception.h>
#include <core/Contract.h>

namespace polk { namespace io {
using namespace polk::core;

void CByteBuffer::appendBytesGeneric(const polk_byte* bytes, polk_long count)
{
#ifdef POLK_CONTRACT
    const polk_long cctSize = this->Size();
#endif

    if(((float)(m_size + count) / (float)m_cap) >= POLK_BYTEBUFFER_GROW_FACTOR) {
        m_cap = m_cap * 2 + count;
        m_bytes = CoreUtils::ReallocArray<polk_byte>(m_bytes, m_size, m_cap *= 2);
    }

    if(bytes)
        memcpy(m_bytes + m_size, bytes, count);

    m_size += count;

#ifdef POLK_CONTRACT
    // Post-condition.
    POLK_REQ_EQUALS(cctSize + count, this->Size());
#endif
}

void CByteBuffer::AppendBytes(const polk_byte* bytes, polk_long count)
{
    POLK_REQ_NOT(m_isFixed, EC_INVALID_STATE);
    POLK_REQ_NOT_NEG(count);

    appendBytesGeneric(bytes, count);
}

void CByteBuffer::AppendByte(polk_byte b)
{
    const polk_byte bs[1] = { b };
    this->AppendBytes(bs, 1);
}

void CByteBuffer::Clear()
{
    POLK_REQ_NOT(m_isFixed, EC_INVALID_STATE);

    m_size = 0;
}

CByteBuffer::CByteBuffer(polk_long cap)
{
    POLK_REQ_NOT_NEG(cap);

    m_size = 0;
    m_cap = cap? cap: POLK_DEF_BYTEBUFFER_CAP;
    m_initCap = m_cap;
    m_bytes = new polk_byte[m_cap];
    m_isFixed = false;
}

CByteBuffer::~CByteBuffer()
{
    POLK_REQ_PTR(m_bytes);
    delete [] m_bytes;
}

void CByteBuffer::SetFixed(bool b)
{
    m_isFixed = b;
}

} }
