// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <io/FileSystem.h>
#include <io/FileStream.h> // for CreateFile(..)

#include <core/String.h>
#include <reflection/MetaObject.h>

using namespace polk::core;
using namespace polk::collections;

namespace polk { namespace io {

// **********
// CFileSystemInfo
// *********

CFileSystemInfo::CFileSystemInfo(const SDateTime& lastWriteTimeUtc, polk_long size)
    : m_lastWriteTimeUtc(lastWriteTimeUtc), m_size(size)
{
}

SDateTime CFileSystemInfo::LastWriteTime()
{
    return m_lastWriteTimeUtc.ToLocalTime();
}

SDateTime CFileSystemInfo::LastWriteTimeUtc()
{
    return m_lastWriteTimeUtc;
}

polk_long CFileSystemInfo::Size()
{
    return m_size;
}

namespace FileSystem {

void CreateFile(const CString* path)
{
    // Forces creation of a file.
    auto fs = CFileStream::Open(path, E_FILEACCESS_WRITE);
    fs->Unref();
}

}

// *************************
    // ***************
    //   Reflection.
    // ***************
// *************************

METAMETHODIMPL(FileSystem_FileExists_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        const bool b = FileSystem::FileExists(METAVALUE_TO_STRINGPTR(pathArg));
        *out_ret = MetaValueFromBoolean(b);
    METAGUARD_END
}

METAMETHODIMPL(FileSystem_DirectoryExists_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        const bool b = FileSystem::DirectoryExists(METAVALUE_TO_STRINGPTR(pathArg));
        *out_ret = MetaValueFromBoolean(b);
    METAGUARD_END
}

METAMETHODIMPL(FileSystem_GetCurrentDirectory_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        const CString* const path = FileSystem::GetCurrentDirectory();
        *out_ret = STRINGPTR_TO_METAVALUE(path);
    METAGUARD_END
}

METAMETHODIMPL(FileSystem_SetCurrentDirectory_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        FileSystem::SetCurrentDirectory(METAVALUE_TO_STRINGPTR(pathArg));
    METAGUARD_END
}

METAMETHODIMPL(FileSystem_CreateDirectory_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        FileSystem::CreateDirectory(METAVALUE_TO_STRINGPTR(pathArg));
    METAGUARD_END
}

METAMETHODIMPL(FileSystem_DeleteDirectory_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        FileSystem::DeleteDirectory(METAVALUE_TO_STRINGPTR(pathArg));
    METAGUARD_END
}

METAMETHODIMPL(FileSystem_CopyFile_impl)
{
    VERIFY_NARG(2)

    METAVALUE* const path1Arg = &args[0];
    METAVALUE* const path2Arg = &args[1];
    VERIFY_ARGOBJ(path1Arg, "String")
    VERIFY_ARGOBJ(path2Arg, "String")

    METAGUARD_BEGIN
        FileSystem::CopyFile(METAVALUE_TO_STRINGPTR(path1Arg),
                             METAVALUE_TO_STRINGPTR(path2Arg));
    METAGUARD_END
}

METAMETHODIMPL(FileSystem_CreateFile_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        FileSystem::CreateFile(METAVALUE_TO_STRINGPTR(pathArg));
    METAGUARD_END
}

void __InitMetaFileSystem()
{
    BEGIN_VTABLE(8)
        DEFINE_VMETHOD("FileExists", FileSystem_FileExists_impl, 0)
        DEFINE_VMETHOD("DirectoryExists", FileSystem_DirectoryExists_impl, 0)
        DEFINE_VMETHOD("GetCurrentDirectory", FileSystem_GetCurrentDirectory_impl, 0)
        DEFINE_VMETHOD("SetCurrentDirectory", FileSystem_SetCurrentDirectory_impl, 0)
        DEFINE_VMETHOD("CreateDirectory", FileSystem_CreateDirectory_impl, 0)
        DEFINE_VMETHOD("DeleteDirectory", FileSystem_DeleteDirectory_impl, 0)
        DEFINE_VMETHOD("CopyFile", FileSystem_CopyFile_impl, 0)
        DEFINE_VMETHOD("CreateFile", FileSystem_CreateFile_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "FileSystem";
    metaClass.flags = METACLASSFLAGS_STATIC;

    RegisterMetaClass(&metaClass);
}

} }
