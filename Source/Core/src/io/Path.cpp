// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <io/Path.h>
#include <core/Exception.h>
#include <core/StringBuilder.h>
#include <core/String.h>
#include <core/StringBuilder.h>
#include <core/Application.h>

#include <reflection/MetaObject.h>

namespace polk { namespace io {
using namespace polk::core;

namespace Path {

static int findExtDot(const CString* path)
{
    CoreUtils::ValidatePath(path);

    const polk_char16* const cs = path->Chars();
    const int length = path->Length();
    if(length == 0) // To be sure (I'm paranoid).
        return -1;

    for(int i = length - 1; i >= 0; i--) {
        const polk_char16 c = cs[i];
        // The dot must be after any '/'s.
        if(c == POLK_CHAR('/')) {
            return -1;
        } else if(c == POLK_CHAR('.')) {
            // Dot can't be the last (length - 1) or the first (0). Also, the
            // extension is returned without a dot (i + 1).
            return (i == (length - 1) || i == 0)? (-1): i + 1;
        }
    }

    return -1;
}

const CString* GetExtension(const CString* path)
{
    const int i = findExtDot(path);
    if(i == -1)
        return nullptr;

    return path->Substring(i);
}

const CString* ChangeExtension(const CString* path, const CString* newExt)
{
    // Path is checked in findExtDot.

    const CString* fstPart = nullptr;
    const int i = findExtDot(path);

    // No extension -- use the whole path...
    if(i == -1) {
        fstPart = path;
    } else {
        // ... otherwise, substring the part before the actual extension.
        fstPart = path->Substring(0, i - 1);
    }

    if(newExt) {

        CoreUtils::ValidatePath(newExt);

        const CString* r;
        const CString* const constDot = CString::FromASCII("."); // NEVER THROWS.
        // newExt may start from "."
        if(newExt->StartsWith(constDot))
            r = fstPart->Concat(newExt);
        else
            r = fstPart->Concat(constDot, newExt);

        if(i != -1)
            fstPart->Unref();

        constDot->Unref();
        return r;

    } else {

        if(i == -1) // path was used, which isn't properly ref'd for return.
            fstPart->Ref();

        return fstPart;

    }
}

const CString* ChangeExtension(const CString* path, const char* cNewExt)
{
    if(cNewExt) {
        Auto<const CString> daNewExt (CString::FromUtf8(cNewExt));
        return ChangeExtension(path, daNewExt);
    } else {
        return ChangeExtension(path, (const CString*)nullptr);
    }
}

bool HasExtension(const CString* path, const CString* ext)
{
    CoreUtils::ValidatePath(path);
    CoreUtils::ValidatePath(ext);

    const int dot = findExtDot(path);
    if(dot == -1)
        return false;
    if((path->Length() - dot) != ext->Length())
        return false;

    for(int i = 0; i < ext->Length(); i++) {
        if((path->Chars()[dot + i]) != (ext->Chars()[i]))
            return false;
    }

    return true;
}

const CString* Combine(const CString* path1, const CString* path2)
{
    CoreUtils::ValidatePath(path1);
    CoreUtils::ValidatePath(path2);

    CStringBuilder sb;
    const polk_char16 sep = POLK_CHAR('/');

    sb.Append(path1);

    if(!((path1->Length() > 0) && (path1->Chars()[path1->Length() - 1] == sep))
    && !((path2->Length() > 0) && (path2->Chars()[0] == sep)))
    {
        sb.Append(sep);
    }
    sb.Append(path2);

    return sb.ToString();
}

const CString* Combine(const CString* path1, const char* path2)
{
    Auto<const CString> ppath2 (CString::FromUtf8(path2));
    return Combine(path1, ppath2);
}

#ifdef POLK_WIN
const CString* Normalize(const CString* path)
{
    if(path->FindChar(POLK_CHAR('\\')) != -1) { // a quick test

       Auto<CStringBuilder> sb (new CStringBuilder());

       for(int i = 0; i < path->Length(); i++) {
           const polk_char16 curChar = path->Chars()[i];

           if(curChar == POLK_CHAR('\\'))
                sb->Append(POLK_CHAR('/'));
            else
                sb->Append(curChar);
       }

       return sb->ToString();

    } else {

        path->Ref();
        return path;
    }
}
#else
const CString* Normalize(const CString* path)
{
    path->Ref();
    return path;
}
#endif

const CString* GetFileName(const CString* _path)
{
    CoreUtils::ValidatePath(_path);
    Auto<const CString> path (Normalize(_path));

    const int length = path->Length();
    const polk_char16* const chars = path->Chars();
    int num = length;
    while (--num >= 0) {
        const polk_char16 c = chars[num];
        if (c == POLK_CHAR('/')
        #ifdef POLK_WIN
         || c == POLK_CHAR(':') // Linux doesn't have volume separators
        #endif
        )
        {
            return path->Substring(num + 1, length - num - 1);
        }
    }

    path->Ref();
    return path;
}

const CString* GetDirectoryName(const CString* path)
{
    CoreUtils::ValidatePath(path);

    const int lastSeparator = path->FindLastChar('/');
    if(lastSeparator == -1) {
        path->Ref();
        return path;
    } else {
        return path->Substring(lastSeparator + 1);
    }
}

// NOTE inverse of GetDirectoryName
const CString* GetParent(const CString* path)
{
    CoreUtils::ValidatePath(path);

    const int lastSeparator = path->FindLastChar('/');

    // *************************************************************
    // Special case for root:
    //      the parent of "/dir1/dir2" is "/dir1"
    //
    // but
    //      the parent of "/dir1" should be "/"
    //
    // However, since lastSeparator == 0, then Substring(0, 0)
    // returns the whole string. Instead, we want to return just "/".
    if(lastSeparator == 0 || lastSeparator == -1) // + if no '/' at all, return '/' as the root!
        return CString::FromASCII("/");
    // *************************************************************

    return path->Substring(0, lastSeparator);
}

}

// *************************
    // ***************
    //   Reflection.
    // ***************
// *************************

METAMETHODIMPL(Path_ChangeExtension_impl)
{
    VERIFY_NARG(2)

    METAVALUE* const pathArg = &args[0];
    METAVALUE* const extArg = &args[1];

    VERIFY_ARGOBJ(pathArg, "String")
    VERIFY_ARGOBJ(extArg, "String")

    METAGUARD_BEGIN
        const CString* const newPath = Path::ChangeExtension(METAVALUE_TO_STRINGPTR(pathArg),
                                                             METAVALUE_TO_STRINGPTR(extArg));
        *out_ret = STRINGPTR_TO_METAVALUE(newPath);
    METAGUARD_END
}

METAMETHODIMPL(Path_GetExtension_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        const CString* const ext = Path::GetExtension(METAVALUE_TO_STRINGPTR(pathArg));
        *out_ret = STRINGPTR_TO_METAVALUE(ext);
    METAGUARD_END
}

METAMETHODIMPL(Path_HasExtension_impl)
{
    VERIFY_NARG(2)

    METAVALUE* const pathArg = &args[0];
    METAVALUE* const extArg = &args[1];

    VERIFY_ARGOBJ(pathArg, "String")
    VERIFY_ARGOBJ(extArg, "String")

    METAGUARD_BEGIN
        const bool b = Path::HasExtension(METAVALUE_TO_STRINGPTR(pathArg),
                                          METAVALUE_TO_STRINGPTR(extArg));
        *out_ret = MetaValueFromBoolean(b);
    METAGUARD_END
}

METAMETHODIMPL(Path_Combine_impl)
{
    VERIFY_NARG(2)

    METAVALUE* path1Arg = &args[0];
    METAVALUE* path2Arg = &args[1];

    VERIFY_ARGOBJ(path1Arg, "String")
    VERIFY_ARGOBJ(path2Arg, "String")

    METAGUARD_BEGIN
        const CString* const str = Path::Combine(METAVALUE_TO_STRINGPTR(path1Arg),
                                                 METAVALUE_TO_STRINGPTR(path2Arg));
        *out_ret = STRINGPTR_TO_METAVALUE(str);
    METAGUARD_END
}

METAMETHODIMPL(Path_GetDirectoryName_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        const CString* const ext = Path::GetDirectoryName(METAVALUE_TO_STRINGPTR(pathArg));
        *out_ret = STRINGPTR_TO_METAVALUE(ext);
    METAGUARD_END
}

METAMETHODIMPL(Path_GetFileName_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        const CString* const ext = Path::GetFileName(METAVALUE_TO_STRINGPTR(pathArg));
        *out_ret = STRINGPTR_TO_METAVALUE(ext);
    METAGUARD_END
}

METAMETHODIMPL(Path_GetParent_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        const CString* const ext = Path::GetParent(METAVALUE_TO_STRINGPTR(pathArg));
        *out_ret = STRINGPTR_TO_METAVALUE(ext);
    METAGUARD_END
}

METAMETHODIMPL(Path_GetFullPath_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const pathArg = &args[0];
    VERIFY_ARGOBJ(pathArg, "String")

    METAGUARD_BEGIN
        const CString* const ext = Path::GetFullPath(METAVALUE_TO_STRINGPTR(pathArg));
        *out_ret = STRINGPTR_TO_METAVALUE(ext);
    METAGUARD_END
}

void __InitMetaPath()
{
    BEGIN_VTABLE(8)
        DEFINE_VMETHOD("ChangeExtension", Path_ChangeExtension_impl, 0)
        DEFINE_VMETHOD("GetExtension", Path_GetExtension_impl, 0)
        DEFINE_VMETHOD("HasExtension", Path_HasExtension_impl, 0)
        DEFINE_VMETHOD("Combine", Path_Combine_impl, 0)
        DEFINE_VMETHOD("GetDirectoryName", Path_GetDirectoryName_impl, 0)
        DEFINE_VMETHOD("GetFileName", Path_GetFileName_impl, 0)
        DEFINE_VMETHOD("GetParent", Path_GetParent_impl, 0)
        DEFINE_VMETHOD("GetFullPath", Path_GetFullPath_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "Path";
    metaClass.flags = METACLASSFLAGS_STATIC;

    RegisterMetaClass(&metaClass);
}

} }
