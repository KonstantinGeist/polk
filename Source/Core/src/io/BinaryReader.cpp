// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <io/BinaryReader.h>
#include <core/Exception.h>
#include <core/String.h>
#include <io/Stream.h>
#include <core/Contract.h>

namespace polk { namespace io {
using namespace polk::core;

SBinaryReader::SBinaryReader(CStream* stream, EByteOrder byteOrder)
    : m_stream(stream), m_byteOrder(byteOrder)
{
}

polk_byte SBinaryReader::ReadByte()
{
    polk_byte b;
    const polk_long sz = m_stream->Read((char*)&b, sizeof(polk_byte));
    if(sz != sizeof(polk_byte)) {
        POLK_THROW(EC_BAD_FORMAT);
    }

    return b;
}

polk_char16 SBinaryReader::ReadChar()
{
    polk_char16 c;
    const polk_long sz = m_stream->Read((char*)&c, sizeof(polk_char16));
    if(sz != sizeof(polk_char16)) {
        POLK_THROW(EC_BAD_FORMAT);
    }

    return c;
}

polk_long SBinaryReader::ReadLong()
{
    // TODO ?
    POLK_REQ_EQUALS(m_byteOrder, E_BYTEORDER_HOST);

    polk_long l;
    const polk_long sz = m_stream->Read((char*)&l, sizeof(polk_long));
    if(sz != sizeof(polk_long)) {
        POLK_THROW(EC_BAD_FORMAT);
    }

    return l;
}

int SBinaryReader::ReadInt()
{
    int i;
    const polk_long sz = m_stream->Read((char*)&i, sizeof(int));
    if(sz != sizeof(int)) {
        POLK_THROW(EC_BAD_FORMAT);
    }

    return (m_byteOrder == E_BYTEORDER_NETWORK)? CoreUtils::ByteOrderNetworkToHost(i): i;
}

polk_uint16 SBinaryReader::ReadUInt16()
{
    polk_char16 r = ReadChar();
    return *((polk_uint16*)&r);
}

polk_uint32 SBinaryReader::ReadUInt32()
{
    int r = ReadInt();
    return *((polk_uint32*)&r);
}

float SBinaryReader::ReadFloat()
{
    float f;
    const polk_long sz = m_stream->Read((char*)&f, sizeof(float));
    if(sz != sizeof(float)) {
        POLK_THROW(EC_BAD_FORMAT);
    }

    // TODO network
    return f;
}

bool SBinaryReader::ReadBool()
{
    return ReadInt(); // TODO?
}

void SBinaryReader::ReadUtf8(char out_buf[256])
{
    polk_byte header8;
    polk_long sz = m_stream->Read((char*)&header8, sizeof(polk_byte));
    if(sz != sizeof(polk_byte)) {
        POLK_THROW(EC_BAD_FORMAT);
    }

    const int header = header8;
    // Can't use 256 because we need space for the null terminator.
    if(header == 0 || header == 256) {
        POLK_THROW(EC_BAD_FORMAT);
    }

    sz = m_stream->Read(out_buf, header);
    if(sz != header) {
        POLK_THROW(EC_BAD_FORMAT);
    }

    // The space for the null terminator was secured by not allowing 256 character
    // long strings.
    out_buf[header] = 0;
}

const CString* SBinaryReader::ReadUTF16()
{
    #define POLK_UTF16BUF_SIZE 256
    const int length = this->ReadInt();
    if(length >= (POLK_UTF16BUF_SIZE - 1)) { // "-1" for null termination
        POLK_THROW(EC_NOT_IMPLEMENTED);
    } else if(length < 0) {
        POLK_THROW(EC_BAD_FORMAT);
    } else if(length == 0) {
        return CString::CreateEmptyString();
    }
    polk_char16 buf[POLK_UTF16BUF_SIZE];
    const polk_long sz = m_stream->Read(reinterpret_cast<char*>(&buf[0]), (polk_long)(sizeof(polk_char16) * length));
    if(sz != polk_long(sizeof(polk_char16) * length)) {
        POLK_THROW(EC_BAD_FORMAT);
    }
    buf[length] = 0; // null termination
    return CString::FromUtf16(&buf[0]);
}

char* SBinaryReader::ReadUTF8()
{
    const int length = this->ReadInt();
    if(length <= 0) {
        POLK_THROW(EC_BAD_FORMAT);
    }
    char* const r = new char[length + 1];
    const polk_long sz = m_stream->Read(reinterpret_cast<char*>(&r[0]), length);
    if(sz != length) {
        POLK_THROW(EC_BAD_FORMAT);
    }
    r[length] = 0; // null termination
    return r;
}

SVariant SBinaryReader::ReadVariant()
{
    const polk_uint32 typeEx = this->ReadUInt32();
    const EVariantType type = (EVariantType)typeEx;
    SVariant value;

    // Reads the next data based on the type.
    switch(type) {
        case E_VARIANTTYPE_NOTHING:
            // Read nothing.
            break;

        case E_VARIANTTYPE_INT:
            value.SetInt(this->ReadInt());
            break;

        case E_VARIANTTYPE_BOOL:
            value.SetBool(this->ReadBool());
            break;

        case E_VARIANTTYPE_FLOAT:
            value.SetFloat(this->ReadFloat());
            break;

        default:
            if(typeEx == E_VARIANTTYPEEX_STRING) {
                const polk_uint32 length = this->ReadUInt32();
                char* const utf8Str = new char[length + 1]; // null termination wasn't recorded
                m_stream->Read(utf8Str, length);
                utf8Str[length] = 0;
                Auto<const CString> str (CString::FromUtf8(utf8Str));
                value.SetObject(static_cast<const CObject*>(str.Ptr()));
                delete [] utf8Str;
            } else {
                POLK_THROW(EC_NOT_IMPLEMENTED);
            }
            break;
    }

    return value;
}

} }
