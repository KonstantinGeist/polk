// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/DateTime.h>
#include <core/String.h>
#include <core/Contract.h>

#include <reflection/MetaObject.h>

namespace polk { namespace core {

SDateTime::SDateTime(
              EDateTimeKind kind,
              unsigned short year,
              unsigned short month,
              unsigned short day,
              unsigned short hour,
              unsigned short minute,
              unsigned short second,
              unsigned short ms)
        : m_kind(kind),
          m_year(year), m_month(month), m_day(day),
          m_hour(hour), m_minute(minute), m_second(second), m_ms(ms)
{
    POLK_REQ(!(year < 1601 || year > 30827 ||
           month == 0 || month > 12 ||
           day == 0 || day > 31 ||
           hour > 23 || minute > 59 || second > 59 ||
           ms > 999),
           EC_ILLEGAL_ARGUMENT);
}

SDateTime::SDateTime()
    : m_kind(E_DATETIMEKIND_UTC), m_year(1970), m_month(1), m_day(1), m_hour(1),
      m_minute(1), m_second(1), m_ms(1)
{
}

bool SDateTime::Equals(const SDateTime& other) const
{
    if(this->m_kind != other.m_kind)
        return false;
    if(this->m_year != other.m_year)
        return false;
    if(this->m_month != other.m_month)
        return false;
    if(this->m_day != other.m_day)
        return false;
    if(this->m_hour != other.m_hour)
        return false;
    if(this->m_minute != other.m_minute)
        return false;
    if(this->m_second != other.m_second)
        return false;
    if(this->m_ms != other.m_ms)
        return false;

    return true;
}

int SDateTime::GetHashCode() const
{
    int hash = 17;
    hash = hash * 23 + m_year;
    hash = hash * 23 + m_month;
    hash = hash * 23 + m_day;
    hash = hash * 23 + m_hour;
    hash = hash * 23 + m_minute;
    hash = hash * 23 + m_second;
    hash = hash * 23 + m_ms;
    return hash;
}

bool SDateTime::IsAfter(const SDateTime& other) const
{
    if(this->m_kind != other.m_kind) {
        POLK_THROW_WITH_MSG(EC_ILLEGAL_ARGUMENT, "Cannot compare DateTime objects of different kinds.");
    }

    if(this->m_year < other.m_year)
        return false;
    if(this->m_month < other.m_month)
        return false;
    if(this->m_day < other.m_day)
        return false;
    if(this->m_hour < other.m_hour)
        return false;
    if(this->m_minute < other.m_minute)
        return false;
    if(this->m_second < other.m_second)
        return false;
    if(this->m_ms < other.m_ms)
        return false;

    return !this->Equals(other);
}

#define GET_SELF auto self = static_cast<CBoxedStruct<SDateTime>*>((CObject*)(void*)_self->idptr);

METACTORIMPL(DateTime_ctor)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
    
        SDateTime now (SDateTime::Now());
        *out_ret = OBJECT_TO_IDPTR(new CBoxedStruct<SDateTime>(now)); //-V572

    METAGUARD_END
}

METAMETHODIMPL(DateTime_Year_impl)
{
    GET_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = MetaValueFromNumber(self->Value.Year());
    METAGUARD_END
}

METAMETHODIMPL(DateTime_Month_impl)
{
    GET_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = MetaValueFromNumber(self->Value.Month());
    METAGUARD_END
}

METAMETHODIMPL(DateTime_Hour_impl)
{
    GET_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = MetaValueFromNumber(self->Value.Hour());
    METAGUARD_END
}

METAMETHODIMPL(DateTime_Minute_impl)
{
    GET_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = MetaValueFromNumber(self->Value.Minute());
    METAGUARD_END
}

METAMETHODIMPL(DateTime_Day_impl)
{
    GET_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = MetaValueFromNumber(self->Value.Day());
    METAGUARD_END
}

METASERIALIMPL(DateTime_serialize)
{
    METAGUARD_BEGIN
        const SDateTime self = static_cast<CBoxedStruct<SDateTime>*>((CObject*)(void*)(obj->idptr))->Value;

        METARESULT res = stream->writeimpl(stream, &self, sizeof(SDateTime));
        if(res != METARESULT_SUCCESS)
            return res;
    METAGUARD_END
}

METADESERIALIMPL(DateTime_deserialize)
{
    METAGUARD_BEGIN
        SDateTime self;
        METARESULT res = stream->readimpl(stream, &self, sizeof(SDateTime));
        if(res != METARESULT_SUCCESS)
            return res;

        auto const r = new CBoxedStruct<SDateTime>(self);
        *out_ret = MetaValueFromPtr("DateTime", (void*)static_cast<CObject*>(r));
    METAGUARD_END
}

void __InitMetaDateTime()
{
    BEGIN_VTABLE(5)
        DEFINE_VMETHOD("Year", DateTime_Year_impl, 0)
        DEFINE_VMETHOD("Month", DateTime_Month_impl, 0)
        DEFINE_VMETHOD("Hour", DateTime_Hour_impl, 0)
        DEFINE_VMETHOD("Minute", DateTime_Minute_impl, 0)
        DEFINE_VMETHOD("Day", DateTime_Day_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "DateTime";
    metaClass.ctor = DateTime_ctor;
    metaClass.serialimpl = DateTime_serialize;
    metaClass.deserialimpl = DateTime_deserialize;
    metaClass.parent = GetMetaClassByName("Object");
    POLK_REQ_PTR(metaClass.parent);

    RegisterMetaClass(&metaClass);
}

} }
