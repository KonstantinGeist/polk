// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/Console.h>
#include <core/Exception.h>
#include <core/String.h>

#include <reflection/MetaObject.h>

namespace polk { namespace core {

namespace Console {

void Write(const char* format, ...)
{
    Auto<const CString> r;

    va_list vl;
    va_start(vl, format);

    r.SetPtr(CString::FormatImpl(format, vl));

    // A no-op on most systems, no problem if the wrapped Format(..)
    // throws an exception and this macro isn't called.
    va_end(vl);

    Write(r);    
}

void WriteLine(const char* format, ...)
{
    Auto<const CString> r;

    va_list vl;
    va_start(vl, format);

    r.SetPtr(CString::FormatImpl(format, vl));

    // A no-op on most systems, no problem if the wrapped Format(..)
    // throws an exception and this macro isn't called.
    va_end(vl);

    WriteLine(r);
}

}

// *************************
    // ***************
    //   Reflection.
    // ***************
// *************************

METAMETHODIMPL(Console_Write_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const strArg = &args[0];
    VERIFY_ARGOBJ(strArg, "String")

    METAGUARD_BEGIN
        Console::Write(METAVALUE_TO_STRINGPTR(strArg));
    METAGUARD_END
}

METAMETHODIMPL(Console_WriteLine_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const strArg = &args[0];
    VERIFY_ARGOBJ(strArg, "String")

    METAGUARD_BEGIN
        Console::WriteLine(METAVALUE_TO_STRINGPTR(strArg));
    METAGUARD_END
}

METAMETHODIMPL(Console_ReadLine_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        const CString* const str = Console::ReadLine();
        *out_ret = STRINGPTR_TO_METAVALUE(str);
    METAGUARD_END
}

METAMETHODIMPL(Console_SetForeColor_impl)
{
    VERIFY_NARG(1)
	
    METAVALUE* const strArg = &args[0];
    VERIFY_ARGOBJ(strArg, "String")
	const CString* const str = METAVALUE_TO_STRINGPTR(strArg);

    METAGUARD_BEGIN
		EConsoleColor color;
		if(str->EqualsASCII("RED")) {
			color = E_CONSOLECOLOR_RED;
		} else if(str->EqualsASCII("GREEN")) {
			color = E_CONSOLECOLOR_GREEN;
		} else if(str->EqualsASCII("BLUE")) {
			color = E_CONSOLECOLOR_BLUE;
		} else if(str->EqualsASCII("YELLOW")) {
			color = E_CONSOLECOLOR_YELLOW;
		} else {
			color = E_CONSOLECOLOR_WHITE;
		}
		
		Console::SetForeColor(color);
    METAGUARD_END
}

void __InitMetaConsole()
{
    BEGIN_VTABLE(4)
        DEFINE_VMETHOD("Write", Console_Write_impl, 0)
        DEFINE_VMETHOD("WriteLine", Console_WriteLine_impl, 0)
        DEFINE_VMETHOD("ReadLine", Console_ReadLine_impl, 0)
		DEFINE_VMETHOD("SetForeColor", Console_SetForeColor_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "Console";
    metaClass.flags = METACLASSFLAGS_STATIC;

    RegisterMetaClass(&metaClass);
}

} }
