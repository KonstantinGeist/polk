// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/StringBuilder.h>
#include <core/Exception.h>
#include <core/Marshal.h>
#include <core/CoreUtils.h>
#include <core/String.h>
#include <core/Application.h>
#include <core/Contract.h>

#include <reflection/MetaObject.h>

namespace polk { namespace core {
using namespace polk::core::Marshal;

CStringBuilder::CStringBuilder(int cap)
{
    POLK_REQ_POS(cap);

    m_count = 0;
    m_cap = cap;
    m_chars = new polk_char16[m_cap];
}

CStringBuilder::~CStringBuilder()
{
    POLK_REQ_PTR(m_chars);
    delete [] m_chars;
}

void CStringBuilder::Append(const CString* str, int start, int count)
{
    // Pre-condition.
    if(!str || !CoreUtils::ValidateRange(start, &count, str->Length())) {
        POLK_THROW(EC_ILLEGAL_ARGUMENT);
    }

#ifdef POLK_CONTRACT
    int cctLength = this->Length();
#endif

    /* Expands the underlying char array when needed. */
    expandIfNeeded(count);

    /* Copies chars from str to self basing on current count
       (pointer arithmetic involved). */
    polk_wmemcpy_16bit(m_chars + m_count, &str->m_chars + start, count);

    /* Adjusts count. */
    m_count += count;

    // Post-condition.
#ifdef POLK_CONTRACT
    POLK_REQ_EQUALS(cctLength + count, this->Length());
#endif
}

void CStringBuilder::expandIfNeeded(int toExpand)
{
    if((float)(m_count + toExpand) / (float)m_cap >= 0.75f) {
        polk_char16* oldChars = m_chars;
        m_chars = new polk_char16[m_cap = m_cap * 2 + toExpand];
        polk_wmemcpy_16bit(m_chars, oldChars, m_count);
        delete [] oldChars;
    }
}

const CString* CStringBuilder::ToString() const
{
    const CString* buffer = CString::createBuffer(m_count);

    /* Since string builder doesn't have a null termination, we can't
       use wcscpy. */
    polk_wmemcpy_16bit(&buffer->m_chars, m_chars, m_count);

    return buffer;
}

int CStringBuilder::Length() const
{
    return m_count;
}

int CStringBuilder::Capacity() const
{
    return m_cap;
}

polk_char16* CStringBuilder::Chars() const
{
    return m_chars;
}

void CStringBuilder::Append(int i)
{
    polk_char16 buf[POLK_TOWBUFFER_BUFSZ];
    int out_cnt;
    polk_char16* ptr = CoreUtils::__int_ToWBuffer(i, buf, &out_cnt);

    expandIfNeeded(out_cnt);

    polk_wmemcpy_16bit(m_chars + m_count, ptr, out_cnt); // wcscpy appends null termination what we don't want.

    m_count += out_cnt;
}

void CStringBuilder::Append(polk_long l)
{
    Auto<const CString> lAsStr (CoreUtils::LongToString(l));
    this->Append(lAsStr);
}

void CStringBuilder::Append(float f)
{
    Auto<const CString> str (CoreUtils::FloatToString(f, 0, true));
    this->Append(str);
}

void CStringBuilder::Append(polk_char16* c)
{
    const int len = polk_wcslen_16bit(c);

    expandIfNeeded(len);
    polk_wmemcpy_16bit(m_chars + m_count, c, len); // see Append(int)

    m_count += len;
}

void CStringBuilder::Append(const char* c)
{
    Auto<const CString> tmp (CString::FromUtf8(c));
    this->Append(tmp);
}

void CStringBuilder::AppendASCII(const char* c)
{
    POLK_REQ_PTR(c);

    const int len = strlen(c);
    for(int i = 0; i < len; i++) {
        this->Append((polk_char16)c[i]);
    }
}

void CStringBuilder::Append(polk_char16 c)
{
    expandIfNeeded(1);

    m_chars[m_count] = c;
    m_count++;
}

void CStringBuilder::Append(const CObject* obj)
{
    if(obj) {
        Auto<const CString> str (obj->ToString());
        this->Append(str);
    }
}

void CStringBuilder::Append(const SVariant& v)
{
    Auto<const CString> str (v.ToString());
    this->Append(str);
}

void CStringBuilder::SetLength(int cnt)
{
    // Pre-condition.
    POLK_REQ_NOT_NEG(cnt);
    POLK_REQ(cnt <= m_count, EC_NOT_IMPLEMENTED);

    m_count = cnt;
}

void CStringBuilder::AppendLine()
{
    this->Append(Application::PlatformString(E_PLATFORMSTRING_NEWLINE));
}

void CStringBuilder::AppendFormat(const char* format, ...)
{
    va_list vl;
    va_start(vl, format);
    Auto<const CString> r (CString::FormatImpl(format, vl));
    va_end(vl);

    Append(r);
}

void CStringBuilder::AppendFormat(const CString* format, ...)
{
    va_list vl;
    va_start(vl, format);
    Auto<const CString> r (CString::FormatImpl(format, vl));
    va_end(vl);

    Append(r);
}

void CStringBuilder::Clear()
{
    this->SetLength(0);

    // Post-condition.
    POLK_REQ_EQUALS(this->Length(), 0);
}

void CStringBuilder::Remove(int startOffset, int count)
{
    // Pre-condition.
    POLK_REQ(startOffset >= 0
        && count > 0
        && startOffset < m_count
        && startOffset + count <= m_count,
        EC_ILLEGAL_ARGUMENT);

#ifdef POLK_CONTRACT
    const int cctLength = this->Length();
#endif

    memcpy(m_chars + startOffset,
           m_chars + startOffset + count,
          (m_count - (startOffset + count)) * sizeof(polk_char16));

    m_count -= count;

#ifdef POLK_CONTRACT
    // Post-condition.
    POLK_REQ_EQUALS(this->Length(), cctLength - count);
#endif
}

void CStringBuilder::Insert(int startOffset, polk_char16 c)
{
    // Pre-condition.
    POLK_REQ(startOffset >= 0 && startOffset <= m_count, EC_ILLEGAL_ARGUMENT);

    if(startOffset == m_count) {
        this->Append(c);
    } else {
        expandIfNeeded(1);

        // Move anything after startOffset forward by 1
        memmove(m_chars + startOffset + 1,
                m_chars + startOffset,
               (m_count - startOffset) * sizeof(polk_char16));

        m_chars[startOffset] = c;
        m_count++;
    }
}

void CStringBuilder::Insert(int startOffset, const CString* str)
{
    // Pre-condition.
    POLK_REQ(startOffset >= 0 && startOffset <= m_count, EC_ILLEGAL_ARGUMENT);

    if(CString::IsNullOrEmpty(str))
        return;

    if(startOffset == m_count) {
        this->Append(str);
    } else {
        expandIfNeeded(str->Length());

        // Moves anything after startOffset forward by the number of chars in str.
        memmove(m_chars + startOffset + str->Length(),
                m_chars + startOffset,
               (m_count - startOffset) * sizeof(polk_char16));

        for(int i = 0; i < str->Length(); i++) {
            m_chars[startOffset + i] = str->Chars()[i];
        }

        m_count += str->Length();
    }
}

bool CStringBuilder::Equals(const CString* str) const
{
    if(m_count != str->Length())
        return false;

    for(int i = 0; i < m_count; i++) {
        if(m_chars[i] != str->Chars()[i])
            return false;
    }

    return true;
}

// *************************
    // ***************
    //   Reflection.
    // ***************
// *************************

#define GET_SELF auto self = static_cast<CStringBuilder*>((CObject*)(void*)_self->idptr);

METACTORIMPL(StringBuilder_ctor)
{
    int cap;

    if(nArg == 0) {
        cap = 32;
    } else {
        VERIFY_NARG(1)

        METAVALUE* capArg = &args[0];
        VERIFY_ARGTYPE(capArg, METATYPE_NUMBER)

        cap = (int)capArg->u.asNumber;
    }

    METAGUARD_BEGIN

        auto r = new CStringBuilder(cap);
        *out_ret = OBJECT_TO_IDPTR(r);

    METAGUARD_END
}

METAMETHODIMPL(StringBuilder_Append_impl)
{
    GET_SELF
    VERIFY_NARG(1)

    METAVALUE* strArg = &args[0];
    VERIFY_ARGOBJ(strArg, "String")

    METAGUARD_BEGIN
        self->Append(METAVALUE_TO_STRINGPTR(strArg));
    METAGUARD_END
}

METAMETHODIMPL(StringBuilder_Length_impl)
{
    GET_SELF
    VERIFY_NARG(0)
    *out_ret = MetaValueFromNumber(self->Length());
    return METARESULT_SUCCESS;
}

METAMETHODIMPL(StringBuilder_AppendLine_impl)
{
    GET_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        self->AppendLine();
    METAGUARD_END
}

METAMETHODIMPL(StringBuilder_Clear_impl)
{
    GET_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        self->Clear();
    METAGUARD_END
}

METAMETHODIMPL(StringBuilder_Remove_impl)
{
    GET_SELF
    VERIFY_NARG(2)

    METAVALUE* startArg = &args[0];
    VERIFY_ARGTYPE(startArg, METATYPE_NUMBER)
    
    METAVALUE* countArg = &args[1];
    VERIFY_ARGTYPE(countArg, METATYPE_NUMBER)

    METAGUARD_BEGIN
        self->Remove((int)startArg->u.asNumber, (int)countArg->u.asNumber);
    METAGUARD_END
}

METAMETHODIMPL(StringBuilder_Insert_impl)
{
    GET_SELF
    VERIFY_NARG(2)

    METAVALUE* startArg = &args[0];
    VERIFY_ARGTYPE(startArg, METATYPE_NUMBER)
    
    METAVALUE* strArg = &args[1];
    VERIFY_ARGOBJ(strArg, "String")

    METAGUARD_BEGIN
        self->Insert((int)startArg->u.asNumber, METAVALUE_TO_STRINGPTR(strArg));
    METAGUARD_END
}

METASERIALIMPL(StringBuilder_serialize)
{
    METAGUARD_BEGIN
        auto const self = static_cast<CStringBuilder*>((CObject*)(void*)(obj->idptr));

        // Serializes like a string.
        Auto<const CString> asStr (self->ToString());

        const int len = asStr->Length();
        const polk_char16* const chars = asStr->Chars();
        METARESULT res;

        res = stream->writeimpl(stream, &len, sizeof(len));
        if(res != METARESULT_SUCCESS)
            return res;

        res = stream->writeimpl(stream, &chars[0], sizeof(polk_char16) * len);
        if(res != METARESULT_SUCCESS)
            return res;
    METAGUARD_END
}

METADESERIALIMPL(StringBuilder_deserialize)
{
    METAGUARD_BEGIN
        // Deserializes like a string.

        METARESULT res;
        int len;

        res = stream->readimpl(stream, &len, sizeof(int));
        if(res != METARESULT_SUCCESS)
            return res;
        if(len <= 0)
            return METARESULT_INTERNAL_EXCEPTION; // TODO better error

        polk_char16* chars;
        Auto<const CString> str (CString::CreateBuffer(len, &chars));

        res = stream->readimpl(stream, &chars[0], sizeof(polk_char16) * len);
        if(res != METARESULT_SUCCESS)
            return res;

        auto const r = new CStringBuilder();
        r->Append(str);

        *out_ret = MetaValueFromPtr("StringBuilder", (void*)static_cast<CObject*>(r));
    METAGUARD_END
}

void __InitMetaStringBuilder()
{
    BEGIN_VTABLE(6)
        DEFINE_VMETHOD("Append", StringBuilder_Append_impl, 0)
        DEFINE_VMETHOD("Length", StringBuilder_Length_impl, 0)
        DEFINE_VMETHOD("AppendLine", StringBuilder_AppendLine_impl, 0)
        DEFINE_VMETHOD("Clear", StringBuilder_Clear_impl, 0)
        DEFINE_VMETHOD("Remove", StringBuilder_Remove_impl, 0)
        DEFINE_VMETHOD("Insert", StringBuilder_Insert_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "StringBuilder";
    metaClass.ctor = StringBuilder_ctor;
    metaClass.serialimpl = StringBuilder_serialize;
    metaClass.deserialimpl = StringBuilder_deserialize;
    metaClass.parent = GetMetaClassByName("Object");
    POLK_REQ_PTR(metaClass.parent);

    RegisterMetaClass(&metaClass);
}

} }
