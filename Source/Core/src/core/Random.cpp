// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/Random.h>
#include <core/Application.h>
#include <core/Exception.h>
#include <core/Contract.h>

#include <limits.h>

#include <reflection/MetaObject.h>

namespace polk { namespace core {

#define MSEED 161803398
#define MBIG INT_MAX

CRandom::CRandom(int seed)
{
    int mj, mk;

    /* Set seedArray to nulls? */

    if(seed == 0)
        seed = Application::TickCount();

    if (seed == INT_MIN)
        mj = MSEED - abs(INT_MIN + 1);
    else
        mj = MSEED - abs(seed);

    m_seedArray[55] = mj;
    mk = 1;

    int i;
    for (i = 1; i < 55; i++) { /*  [1, 55] is special (Knuth) */
        const int ii = (21 * i) % 55;
        m_seedArray[ii] = mk;
        mk = mj - mk;
        if (mk < 0)
            mk += MBIG;
        mj = m_seedArray[ii];
    }

    int k;
    for (k = 1; k < 5; k++) {
        for (i = 1; i < 56; i++) {
            m_seedArray[i] -= m_seedArray[1 + (i + 30) % 55];
            if (m_seedArray[i] < 0)
                m_seedArray[i] += MBIG;
        }
    }

    m_inext = 0;
    m_inextp = 31;
}

int CRandom::NextInt(int min, int max)
{
    POLK_REQ(max > min, EC_ILLEGAL_ARGUMENT);

    /* special case: a difference of one (or less) will always return the minimum
       e.g. -1,-1 or -1,0 will always return -1 */
    unsigned int diff = (unsigned int) (max - min);
    if (diff <= 1)
        return min;

    return (int)((unsigned int)(NextDouble() * diff) + min);
}

int CRandom::NextInt()
{
    return (int)(NextDouble () * INT_MAX);
}

double CRandom::NextDouble()
{
    int retVal;

    if (++m_inext  >= 56) m_inext  = 1;
    if (++m_inextp >= 56) m_inextp = 1;

    retVal = m_seedArray[m_inext] - m_seedArray[m_inextp];

    if(retVal < 0)
        retVal += MBIG;

    m_seedArray[m_inext] = retVal;

    return retVal * (1.0 / MBIG);
}

void CRandom::GetState(int* outputBuffer, int bufferSize) const
{
    if(bufferSize != 58) {
        POLK_THROW(EC_CONTRACT_UNSATISFIED);
    }

    memcpy(&outputBuffer[0], &m_seedArray[0], sizeof(int) * 56);
    outputBuffer[56] = m_inext;
    outputBuffer[57] = m_inextp;
}

void CRandom::SetState(int* inputBuffer, int bufferSize)
{
    if(bufferSize != 58) {
        POLK_THROW(EC_CONTRACT_UNSATISFIED);
    }

    memcpy(&m_seedArray[0], &inputBuffer[0], sizeof(int) * 56);
    this->m_inext = inputBuffer[56];
    this->m_inextp = inputBuffer[57];
}

const CString* CRandom::ToString() const
{
    return CString::FromASCII("<random>");
}

// *************************
    // ***************
    //   Reflection.
    // ***************
// *************************

#define GET_SELF auto self = static_cast<CRandom*>((CObject*)(void*)_self->idptr);

METACTORIMPL(Random_ctor)
{
    VERIFY_NARG(1)

    METAVALUE* seedArg = &args[0];
    VERIFY_ARGTYPE(seedArg, METATYPE_NUMBER)

    METAGUARD_BEGIN

        CRandom* r = new CRandom((int)seedArg->u.asNumber);
        *out_ret = OBJECT_TO_IDPTR(r);

    METAGUARD_END
}

METAMETHODIMPL(Random_NextNumber_impl)
{
    GET_SELF
    VERIFY_NARG(0)
    *out_ret = MetaValueFromNumber((float)self->NextDouble());
    return METARESULT_SUCCESS;
}

METAMETHODIMPL(Random_NextInt_impl)
{
    GET_SELF
    VERIFY_NARG(2)

    METAVALUE* aArg = &args[0];
    METAVALUE* bArg = &args[1];
    VERIFY_ARGTYPE(aArg, METATYPE_NUMBER)
    VERIFY_ARGTYPE(bArg, METATYPE_NUMBER)

    METAGUARD_BEGIN
        *out_ret = MetaValueFromNumber(self->NextInt((int)aArg->u.asNumber, (int)bArg->u.asNumber));
    METAGUARD_END
}

#define STATEBUFSIZE 58

METASERIALIMPL(Random_serialize)
{
    METAGUARD_BEGIN
        auto const self = static_cast<CRandom*>((CObject*)(void*)(obj->idptr));

        int buffer[STATEBUFSIZE];
        self->GetState(&buffer[0], STATEBUFSIZE);

        METARESULT res = stream->writeimpl(stream, &buffer[0], sizeof(int) * STATEBUFSIZE);
        if(res != METARESULT_SUCCESS)
            return res;
    METAGUARD_END
}

METADESERIALIMPL(Random_deserialize)
{
    METAGUARD_BEGIN
        int buffer[STATEBUFSIZE];
        METARESULT res = stream->readimpl(stream, &buffer[0], sizeof(int) * STATEBUFSIZE);
        if(res != METARESULT_SUCCESS)
            return res;

        auto const r = new CRandom(); // TODO does unnecessary stuff here which will be overriden in SetState(..)
        r->SetState(&buffer[0], STATEBUFSIZE);

        *out_ret = MetaValueFromPtr("Random", (void*)static_cast<CObject*>(r));
    METAGUARD_END
}

void __InitMetaRandom()
{
    BEGIN_VTABLE(2)
        DEFINE_VMETHOD("NextNumber", Random_NextNumber_impl, 0)
        DEFINE_VMETHOD("NextInt", Random_NextInt_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "Random";
    metaClass.ctor = Random_ctor;
    metaClass.serialimpl = Random_serialize;
    metaClass.deserialimpl = Random_deserialize;
    metaClass.parent = GetMetaClassByName("Object");
    POLK_REQ_PTR(metaClass.parent);

    RegisterMetaClass(&metaClass);
}

} }
