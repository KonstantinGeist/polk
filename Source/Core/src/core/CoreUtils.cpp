// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/CoreUtils.h>
#include <core/Object.h>
#include <core/Delegate.h>
#include <core/String.h>
#include <core/Marshal.h>
#include <core/Exception.h>
#include <core/String.h>
#include <core/Contract.h>

#include <reflection/MetaObject.h>
#include <core/Application.h>   // for the test function
#include <core/StringBuilder.h> // for the test function

#include <assert.h>
#include <limits.h>

#ifndef POLK_ANDROID
#ifndef POLK_JS
    #include <xmmintrin.h>
#endif
#endif

// htonl & al.
#ifdef POLK_WIN
    #include <winsock2.h>
#endif
#ifdef POLK_X
    #include <arpa/inet.h>
#endif

namespace polk { namespace core { namespace CoreUtils {
using namespace polk::core::Marshal;

bool ValidateRange(int startIndex, int* rangeCount, int totalCount)
{
    int rc = *rangeCount;

    /* --- */
    if(rc == 0)
        rc = totalCount - startIndex;
    if(rc < 0
    || rc > totalCount
    || (rc + startIndex) > totalCount)
        return false;
    /* --- */

    *rangeCount = rc;
    return true;
}

bool AreObjectsEqual(const CObject* obj1, const CObject* obj2)
{
    if(!obj1 || !obj2) // someone is null
        return obj1 == obj2; // then simply check pointers.
    else
        return obj1->Equals(obj2);
}

polk_char16* __int_ToWBuffer(int n, polk_char16* buf, int* out_count)
{
    if(n == INT_MIN) {
        const char* const mincs = "-2147483648";
        const size_t minlen = strlen(mincs) + 1;

        for(size_t i = 0; i < minlen; i++)
            buf[i] = mincs[i];

        return buf;
    }

    polk_char16* ptr = buf + (POLK_TOWBUFFER_BUFSZ - 1);

    const bool neg = (n < 0);
    if(neg)
        n = -n;

    *ptr-- = 0;

    do {
        *ptr-- = (n % 10) + '0';
        n /= 10;
    }
    while(n);

    if(neg)
        *ptr-- = '-';

    ++ptr;

    if(out_count)
        *out_count = (POLK_TOWBUFFER_BUFSZ - 1) - (int)(ptr - buf);

   return ptr;
}

const CString* BoolToString(bool b)
{
    return CString::FromUtf8(b? "true": "false");
}

const CString* IntToString(int i)
{
    polk_char16 tmp[POLK_TOWBUFFER_BUFSZ];
    const polk_char16* const r = __int_ToWBuffer(i, tmp, 0);
    return CString::FromUtf16(r);
}

const CString* LongToString(polk_long l)
{
    char tmp[64];
#ifdef POLK_WIN
    // NOTE MinGW uses Microsoft's C runtime, which uses a bit different formatting.

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
    sprintf(tmp, "%I64d", l);
#pragma GCC diagnostic pop
#else
    assert(sizeof(polk_long) == sizeof(long long));
    sprintf(tmp, "%ld", l);
#endif

    return CString::FromUtf8(tmp);
}

const CString* PtrToString(void* ptr)
{
    char tmp[32];
    sprintf(tmp, "%p", ptr);
    return CString::FromUtf8(tmp);
}

#define POLK_PRECISION_LIMIT 32
const CString* FloatToString(float f, int precision, bool noTrailingZeros)
{
    if(precision >= POLK_PRECISION_LIMIT)
        precision = POLK_PRECISION_LIMIT;

    if(precision < 0) {
        POLK_THROW(EC_ILLEGAL_ARGUMENT);
    }

    char formatStr[128];
    if(precision == 0)
        strcpy(formatStr, "%f");
    else {
        sprintf(formatStr, "A.%df", precision);
        formatStr[0] = '%';
    }

    char str[POLK_PRECISION_LIMIT]  = { 0 };
    sprintf(str, formatStr, f);

    if(noTrailingZeros && precision == 0) {
        // Now remove unnecessary zeros.
        const int length = strlen(str);
        for(int i = length - 1; i >= 0; i--) {
            char c = str[i];
            if(c == '0')
                str[i] = 0;
            else {
                // It's a point or something else.
                // We don't directly compare to "." here as different OS's might
                // have different formatting.
                if(c < '0' || c > '9')
                    str[i] = 0;
                break;
            }
        }
    }

    return CString::FromUtf8(str);
}

polk_char16 CharToUpperCase(polk_char16 c)
{
    Auto<const CString> tmp (CString::FromChar(c));
    tmp.SetPtr(tmp->ToUpperCase());
    POLK_REQ_POS(tmp->Length());
    return tmp->Chars()[0];
}

polk_char16 CharToLowerCase(polk_char16 c)
{
    Auto<const CString> tmp (CString::FromChar(c));
    tmp.SetPtr(tmp->ToLowerCase());
    POLK_REQ_POS(tmp->Length());
    return tmp->Chars()[0];
}

bool IsCharUpperCase(polk_char16 c)
{
    return CharToUpperCase(c) == c;
}

bool IsCharLowerCase(polk_char16 c)
{
    return CharToLowerCase(c) == c;
}

// TODO currently only works with spaces and tabs and '\r'
bool IsWhiteSpace(polk_char16 c)
{
    return c == POLK_CHAR(' ') || c == POLK_CHAR('\t') || c == POLK_CHAR('\r');
}

bool IsDigit(polk_char16 c)
{
    return c >= POLK_CHAR('0') && c <= POLK_CHAR('9');
}

bool IsLetter(polk_char16 c)
{
    // TODO currently only supports the Latin alphabet
    return (c >= POLK_CHAR('a') && c <= POLK_CHAR('z'))
        || (c >= POLK_CHAR('A') && c <= POLK_CHAR('Z'));
}

bool IsControl(polk_char16 c)
{
    // http://www.cplusplus.com/reference/cctype/iscntrl/
    // For the standard ASCII character set (used by the "C" locale), control characters are those between ASCII codes 0x00 (NUL) and 0x1f (US), plus 0x7f (DEL).
    return (c <= 0x1f) || (c == 0x7f);
}

void ThrowHelper(int exCode, const char* msg, const char* file, int line)
{
#ifdef POLK_DEBUG_MODE
    // FIX cppcheck reports "possible null dereference" for msg argument
    fprintf(stderr, "EXCEPTION: '%s' (code=%d at %s:%d)\n",
                     msg? msg: "no specific message, see the error code",
                     exCode,
                     file,
                     line);
#endif

    throw SException((EExceptionCode)exCode, msg);
}

void ShowMessage(const char* msg, bool isFatal)
{
    Auto<const CString> daMsg (CString::FromUtf8(msg));
    ShowMessage(daMsg, isFatal);
}

// ***************

// TODO for linux?
void ValidatePath(const CString* path)
{
    if(!path) {
        POLK_THROW_WITH_MSG(EC_ILLEGAL_ARGUMENT, "Invalid path.");
    }

    const int length = path->Length();
    for (int i = 0; i < length; i++) {
        const polk_char16 c = path->Chars()[i];

        switch(c) {
            case POLK_CHAR('"'):
            case POLK_CHAR('<'):
            case POLK_CHAR('>'):
            case POLK_CHAR('|'):
                {
                    POLK_THROW_WITH_MSG(EC_BAD_FORMAT, "Invalid path.");
                }
                break;

            case POLK_CHAR('\\'):
                {
                    POLK_THROW_WITH_MSG(EC_BAD_FORMAT, "Only normalized paths accepted (backward slash found).");
                }
                break;

            default:
                if(c < 32) {
                    POLK_THROW_WITH_MSG(EC_BAD_FORMAT, "Invalid path.");
                }
                break;
        }
    }
}

#ifndef POLK_ANDROID
#ifndef POLK_JS
polk_byte* AllocAligned(size_t size, size_t alignment, bool clear)
{
    polk_byte* const r = (polk_byte*)_mm_malloc(size, alignment);
    if(!r) {
        POLK_THROW(EC_OUT_OF_RESOURCES);
    }

    if(clear) {
        memset(r, 0, size);
    }

    return r;
}

void FreeAligned(polk_byte* ptr)
{
    _mm_free(ptr);
}
#endif
#endif

polk_uint32 ByteOrderHostToNetwork(polk_uint32 c)
{
    return htonl(c);
}

polk_uint32 ByteOrderNetworkToHost(polk_uint32 c)
{
    return ntohl(c);
}

void DumpMemory(const void* mem, size_t sz)
{
    auto p = (const unsigned char* const)mem;

    size_t i;
    for (i = 0; i < sz; ++i)
        printf("%02x ", p[i]);

    printf("\n");
}

void SegFault()
{
    *(volatile int*)0 = 0;
}

}

// *************************
    // ***************
    //   Reflection.
    // ***************
// *************************

METAMETHODIMPL(CoreUtils_IntToString_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const iArg = &args[0];
    VERIFY_ARGTYPE(iArg, METATYPE_NUMBER)

    METAGUARD_BEGIN
        const CString* const str = CoreUtils::IntToString((int)iArg->u.asNumber);
        *out_ret = STRINGPTR_TO_METAVALUE(str);
    METAGUARD_END
}

METAMETHODIMPL(CoreUtils_NumberToString_impl)
{
    VERIFY_NARG(2)

    METAVALUE* const iArg = &args[0];
    VERIFY_ARGTYPE(iArg, METATYPE_NUMBER)

    METAVALUE* const presArg = &args[1];
    VERIFY_ARGTYPE(presArg, METATYPE_NUMBER)

    METAGUARD_BEGIN
        const CString* const str = CoreUtils::FloatToString(iArg->u.asNumber,
                                                           (int)presArg->u.asNumber,
														    true);
        *out_ret = STRINGPTR_TO_METAVALUE(str);
    METAGUARD_END
}

METAMETHODIMPL(CoreUtils_ShowMessage_impl)
{
    VERIFY_NARG(2)

    METAVALUE* const msgArg = &args[0];
    VERIFY_ARGOBJ(msgArg, "String")

    METAVALUE* const isFatalArg = &args[1];
    VERIFY_ARGTYPE(isFatalArg, METATYPE_BOOL)

    METAGUARD_BEGIN
        CoreUtils::ShowMessage(METAVALUE_TO_STRINGPTR(msgArg),
                               isFatalArg->u.asBool);
    METAGUARD_END
}

METAMETHODIMPL(CoreUtils_MemorySizeToString_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const iArg = &args[0];
    VERIFY_ARGTYPE(iArg, METATYPE_NUMBER)

    METAGUARD_BEGIN
        const CString* const str = CoreUtils::MemorySizeToString(iArg->u.asNumber);
        *out_ret = STRINGPTR_TO_METAVALUE(str);
    METAGUARD_END
}

void __InitMetaCoreUtils()
{
    BEGIN_VTABLE(4)
        DEFINE_VMETHOD("IntToString", CoreUtils_IntToString_impl, 0)
        DEFINE_VMETHOD("NumberToString", CoreUtils_NumberToString_impl, 0)
        DEFINE_VMETHOD("ShowMessage", CoreUtils_ShowMessage_impl, 0)
        DEFINE_VMETHOD("MemorySizeToString", CoreUtils_MemorySizeToString_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "CoreUtils";
    metaClass.flags = METACLASSFLAGS_STATIC;

    RegisterMetaClass(&metaClass);
}

} }
