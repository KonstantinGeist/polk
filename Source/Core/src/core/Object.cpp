// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/Object.h>
#include <core/AtomicObject.h>
#include <core/Exception.h>
#include <core/CoreUtils.h>
#include <core/String.h>
#include <core/Contract.h>
#include <reflection/MetaObject.h>

#include <typeinfo>
#include <stdint.h>
#include <core/core.h>
#include <assert.h>

namespace polk { namespace core {
    
    // *************************
    //   Basic leak detector.
    // *************************

#ifdef POLK_BASIC_LEAK_DETECTOR
// Basic leak detector.
// Basically just counts the number of polk objects created/deleted. Doesn't
// specify which objects were created/deleted, doesn't do double-free detection
// etc.
//
// It is a basic step -- if you find that object count is different/huge after
// the program ends, it means you have a leak -- and then you can use advanced
// memory profiling tools such as Valgrind or DrMemory.
//
// Note that some static objects may still be alive, so a small fixed number of
// live objects doesn't mean you have a leak.
struct BasicLeakDetector
{
    polk_atomic_int ObjectCount;
    bool Suppress;

    BasicLeakDetector()
        : ObjectCount(0), Suppress(false)
    {
    }

    void printLeakInfo()
    {
        if(this->ObjectCount && !this->Suppress) {
            printf("WARNING: basic leak detector found %d objects unreleased."
                   " Note that some static objects may be still alive, so a small"
                   " fixed number of live objects doesn't mean you have a leak. Also, certain systems"
                   " don't free objects when an unhandled exception is thrown.", (int)ObjectCount);
        }
    }

    ~BasicLeakDetector()
    {
        printLeakInfo();
    }
};
// FIX init_priority tells GCC that it should be called the first in the construtor chain
// and the last in the destructor chain.
static BasicLeakDetector basicLeakDetector __attribute__ ((init_priority (101)));

void PrintLeakInfo()
{
    basicLeakDetector.printLeakInfo();
}

void SuppressBasicLeakDetector(bool b)
{
    basicLeakDetector.Suppress = b;
}
#endif

    // *************************
    //        CObject
    // *************************

CObject::CObject()
{
    assert(IsCoreInitialized());

    m_refCount = 1;

#ifdef POLK_BASIC_LEAK_DETECTOR
    CoreUtils::AtomicIncrement(&basicLeakDetector.ObjectCount);
#endif
}

CObject::~CObject()
{
#ifdef POLK_BASIC_LEAK_DETECTOR
    CoreUtils::AtomicDecrement(&basicLeakDetector.ObjectCount);
#endif
}

void CObject::Ref() const
{
#ifdef POLK_SINGLE_THREADED
    POLK_REQ_NOT_EQUALS(m_refCount, 0);

    m_refCount++;
#else
    // TODO check for being < INT_MAX (NOTE: _not_ "<= INT_MAX"
    CoreUtils::AtomicIncrement(&m_refCount);
#endif
}

bool CObject::Unref() const
{
    POLK_REQ_NOT_EQUALS(m_refCount, 0);

    // TODO check for INT_MIN probably
#ifdef POLK_SINGLE_THREADED
    if(--m_refCount == 0) {
        delete this;
        return true;
    } else {
        return false;
    }
#else
    if(CoreUtils::AtomicDecrement(&m_refCount) == 0) {
        delete this;
        return true;
    } else {
        return false;
    }
#endif
}

int CObject::GetHashCode() const
{
    return (int)(((reinterpret_cast<uintptr_t>(this)) >> 1) * 1000000007);
}

const CString* CObject::ToString() const
{
    return GetDebugStringInfo(this);
}

bool CObject::Equals(const CObject* obj) const
{
    // Just a reference check.
    return this == obj;
}

    // *************************
    //      CAtomicObject
    // *************************

    // (here to have access to the basic leak detector's fields)

CAtomicObject::CAtomicObject()
{
    m_refCount = 1;

#ifdef POLK_BASIC_LEAK_DETECTOR
    CoreUtils::AtomicIncrement(&basicLeakDetector.ObjectCount);
#endif
}

CAtomicObject::~CAtomicObject()
{
    // In a few places, mutexes are created and deleted manually,
    // without using reference counting...
    // assert(m_refCount == 0);

#ifdef POLK_BASIC_LEAK_DETECTOR
    CoreUtils::AtomicDecrement(&basicLeakDetector.ObjectCount);
#endif
}

    // *************************
    //    GetDebugStringInfo
    // *************************

const CString* GetDebugStringInfo(const CObject* obj)
{
    return CString::Format("<object of type '%s' at %p refCount = %d>",
                           const_cast<char*>(typeid(*obj).name()),
                           static_cast<const void*>(obj),
                           (int)obj->ReferenceCount());
}

// *************************
    // ***************
    //   Reflection.
    // ***************
// *************************

#define GET_SELF auto self = (CObject*)(void*)_self->idptr;

METAMETHODIMPL(Object_ToString_impl)
{
    GET_SELF
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = STRINGPTR_TO_METAVALUE(self->ToString());
    METAGUARD_END
}

static void initEventHandler()
{
    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    metaClass.name = "EventHandler";
    metaClass.parent = GetMetaClassByName("Object");

    RegisterMetaClass(&metaClass);
}

void __InitMetaObjectBase()
{
    BEGIN_VTABLE(1)
        DEFINE_VMETHOD("ToString", Object_ToString_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "Object";
    metaClass.refimpl = MetaObjectRefImpl;
    metaClass.unrefimpl = MetaObjectUnrefImpl;

    RegisterMetaClass(&metaClass);

    initEventHandler();
}

} }
