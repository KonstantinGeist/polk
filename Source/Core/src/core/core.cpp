// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/core.h>
#include <core/Application.h>
#include <core/Thread.h>
#include <reflection/MetaObject.h>

#include <core/Console.h>
#include <core/CoreUtils.h>
#include <core/DateTime.h>
#include <core/Random.h>
#include <core/StringBuilder.h>
#include <io/FileUtils.h>
#include <io/FileSystem.h>
#include <io/Path.h>

namespace polk { namespace core {

using namespace polk::io;

static bool g_isCoreInitialized = false;

void InitCore()
{
    assert(!g_isCoreInitialized);

    // Various initializers below depend on CObject-derived classes such as
    // CHashMap, so we have to set to g_isCoreInitialized early on so that
    // CHashMap's inherited constructors (from CObject) did not fail on
    // assert(IsCoreInitialized()). CHashMap's are safe to use as they do not
    // depend on other types and therefore do not introduce intialization fiasco
    // problems.
    g_isCoreInitialized = true;

//#ifdef POLK_WIN
    // Required for ShellExecuteEx(..)
    //CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
//#endif

    __InitMetaObject();
    __InitMetaObjectBase();
    __InitMetaString();
    __InitMetaApplication();
    __InitMetaConsole();
    __InitMetaCoreUtils();
    __InitMetaDateTime();
    __InitMetaRandom();
    __InitMetaStringBuilder();
    __InitMetaFileUtils();
    __InitMetaPath();
    __InitMetaFileSystem();

    __InitThread();
    __InitThreadNative();
    __InitApplication();
}

void DeinitCore()
{
    assert(g_isCoreInitialized);

    __DeinitApplication();
    __DeinitThreadNative();
    __DeinitThread();

    __DeinitMetaObject();

//#ifdef POLK_WIN
    // Paired with CoInitializeEx(..) above.
    //CoUninitialize();
//#endif

    g_isCoreInitialized = false;
}

bool IsCoreInitialized()
{
    return g_isCoreInitialized;
}

} }
