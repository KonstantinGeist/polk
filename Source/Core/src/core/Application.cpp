// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/Application.h>
#include <core/String.h>
#include <core/Contract.h>

#include <core/Thread.h>

#include <reflection/MetaObject.h>

namespace polk { namespace core {

static const CString* g_NewLine = nullptr;
static const CString* g_FileSeparator = nullptr;

// ***********************************
//   INITIALIZATION/DEINITIALIZATION
// ***********************************

void __InitApplication()
{
#ifdef POLK_X
    g_NewLine = CString::FromASCII("\n");
    g_FileSeparator = CString::FromASCII("/");
#endif

#ifdef POLK_WIN
    g_NewLine = CString::FromASCII("\r\n");
    g_FileSeparator = CString::FromASCII("\\");
#endif
}

void __DeinitApplication()
{
    POLK_REQ_PTR(g_NewLine);
    POLK_REQ_PTR(g_FileSeparator);

    g_NewLine->Unref();
    g_FileSeparator->Unref();
    g_NewLine = nullptr;
    g_FileSeparator = nullptr;
}

// ***********************************

} }

namespace polk { namespace core { namespace Application {

const CString* PlatformString(EPlatformString ps)
{
    // Pre-condition.
    POLK_REQ(ps == E_PLATFORMSTRING_NEWLINE
        || ps == E_PLATFORMSTRING_FILE_SEPARATOR,
        polk::core::EC_ILLEGAL_ARGUMENT);

    switch(ps) {
        case E_PLATFORMSTRING_NEWLINE:
            return g_NewLine;

        case E_PLATFORMSTRING_FILE_SEPARATOR:
            return g_FileSeparator;
    }

    return g_NewLine;
}

void Exit(int code)
{
    exit(code);
}

}

// *************************
    // ***************
    //   Reflection.
    // ***************
// *************************

METAMETHODIMPL(Application_Exit_impl)
{
    VERIFY_NARG(1)

    METAVALUE* const exitCodeArg = &args[0];
    VERIFY_ARGTYPE(exitCodeArg, METATYPE_NUMBER)

    METAGUARD_BEGIN
        Application::Exit((int)exitCodeArg->u.asNumber);
    METAGUARD_END
}

METAMETHODIMPL(Application_GetExeFileName_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = STRINGPTR_TO_METAVALUE(Application::GetExeFileName());
    METAGUARD_END
}

METAMETHODIMPL(Application_GetMemoryUsage_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = MetaValueFromNumber(Application::GetMemoryUsage());
    METAGUARD_END
}

METAMETHODIMPL(Application_TickCount_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = MetaValueFromNumber(Application::TickCount());
    METAGUARD_END
}

METAMETHODIMPL(Application_Launch_impl)
{
    if(nArg == 1) {

        METAVALUE* const pathArg = &args[0];

        METAGUARD_BEGIN
            Application::Launch(METAVALUE_TO_STRINGPTR(pathArg),
                                nullptr);
        METAGUARD_END

    } else {

        VERIFY_NARG(2)

        METAVALUE* const pathArg = &args[0];
        METAVALUE* const argArg = &args[1];

        VERIFY_ARGOBJ(pathArg, "String")
        VERIFY_ARGOBJ(argArg, "String")

        METAGUARD_BEGIN
            Application::Launch(METAVALUE_TO_STRINGPTR(pathArg),
                                METAVALUE_TO_STRINGPTR(argArg));
        METAGUARD_END

    }
}

METAMETHODIMPL(Application_GetProcessorCount_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = MetaValueFromNumber(Application::GetProcessorCount());
    METAGUARD_END
}

METAMETHODIMPL(Application_GetCommandLineArgs_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = STRINGPTR_TO_METAVALUE(Application::GetCommandLineArgs());
    METAGUARD_END
}

METAMETHODIMPL(Application_GetOSVersion_impl)
{
    VERIFY_NARG(0)

    METAGUARD_BEGIN
        *out_ret = STRINGPTR_TO_METAVALUE(Application::GetOSVersion());
    METAGUARD_END
}

void __InitMetaApplication()
{
    BEGIN_VTABLE(8)
        DEFINE_VMETHOD("Exit", Application_Exit_impl, 0)
        DEFINE_VMETHOD("GetExeFileName", Application_GetExeFileName_impl, 0)
        DEFINE_VMETHOD("GetMemoryUsage", Application_GetMemoryUsage_impl, 0)
        DEFINE_VMETHOD("TickCount", Application_TickCount_impl, 0)
        DEFINE_VMETHOD("Launch", Application_Launch_impl, 0)
        DEFINE_VMETHOD("GetProcessorCount", Application_GetProcessorCount_impl, 0)
        DEFINE_VMETHOD("GetCommandLineArgs", Application_GetCommandLineArgs_impl, 0)
        DEFINE_VMETHOD("GetOSVersion", Application_GetOSVersion_impl, 0)
    END_VTABLE

    METACLASS metaClass;
    memset(&metaClass, 0, sizeof(METACLASS));
    SET_VTABLE(metaClass)
    metaClass.name = "Application";
    metaClass.flags = METACLASSFLAGS_STATIC;

    RegisterMetaClass(&metaClass);
}

} } 
