// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <collections/polk_ref.h>

#include <core/Object.h>
#include <core/CoreUtils.h>
#include <core/Marshal.h>

namespace polk { namespace collections {
using namespace polk::core;
using namespace polk::core::Marshal;

    // **********
    //   Object
    // **********

bool POLK_EQUALS(const CObject* obj1, const CObject* obj2)
{
    return CoreUtils::AreObjectsEqual(obj1, obj2);
}

    // ***************
    //   const char*
    // ***************

bool POLK_EQUALS(const char* a, const char* b)
{
    if(!a || !b)
        return a == b;
    else
        return strcmp(a, b) == 0;
}

int POLK_HASHCODE(const char* a)
{
    int i = 0;
    int h = 0;
    while(char c = a[i++])
        h = ((h << 5) - h) + c;

    return h;
}

    // **************
    //   polk_char16*
    // **************

bool POLK_EQUALS(const polk_char16* cs1, const polk_char16* cs2)
{
    return polk_wcscmp_16bit(cs1, cs2) == 0;
}

int POLK_HASHCODE(const polk_char16* cs)
{
    int i = 0;
    int h = 0;
    while(polk_char16 c = cs[i++])
        h = ((h << 5) - h) + c;

    return h;
}

} }
