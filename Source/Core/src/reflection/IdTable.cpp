// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <reflection/IdTable.h>
#include <reflection/MetaObject.h>
#include <collections/ArrayList.h>
#include <collections/HashMap.h>
#include <collections/Queue.h>
#include <core/String.h>
#include <io/BinaryReader.h>
#include <io/BinaryWriter.h>

namespace polk { namespace reflection {
using namespace polk::core;
using namespace polk::collections;
using namespace polk::io;

    // ****************
    //    SMetaObject
    // ****************

struct SMetaObject
{
    METACLASS* Class;
    intptr_t IdPtr;

    SMetaObject(int dummy = 0) // for collections
        : Class(nullptr), IdPtr(0)
    {
    }

    SMetaObject(const METAOBJECT& c)
        : Class(c.klass), IdPtr(c.idptr)
    {
    }

    METAOBJECT ToMETAOBJECT() const
    {
        METAOBJECT obj;
        obj.klass = this->Class;
        obj.idptr = this->IdPtr;
        return obj;
    }
};
inline void POLK_REF(const SMetaObject& m) { /* Nothing. */ }
inline void POLK_UNREF(const SMetaObject& m) { /* Nothing. */ }
inline bool POLK_IS_NULL(const SMetaObject& m) { return false; }
inline bool POLK_EQUALS(const SMetaObject& a, const SMetaObject& b)
{
    return (a.Class == b.Class) && (a.IdPtr == b.IdPtr);
}
inline int POLK_HASHCODE(const SMetaObject& m)
{
    // TODO
    return (int)((intptr_t)m.Class + m.IdPtr);
}

    // **********************
    //     SMetaObjectWithId
    // **********************

struct SMetaObjectWithId
{
    SMetaObject Object;
    int Id;

    SMetaObjectWithId()
        : Id(0)
    {
    }

    SMetaObjectWithId(const SMetaObject& object, int id)
        : Object(object), Id(id)
    {
    }
};
inline void POLK_REF(const SMetaObjectWithId& t) { /* Nothing. */ }
inline void POLK_UNREF(const SMetaObjectWithId& t)  { /* Nothing. */ }

    // **********************
    //      CIdTableImpl
    // **********************

static int m_gObjectId = 0; // TODO fix this
class CIdTableImpl final: public CIdTable
{
    friend class CIdTable;

    Auto<CHashMap<SMetaObject, int> > m_objectMap;
    Auto<CHashMap<int, SMetaObject> > m_objectMap2;

public:
    CIdTableImpl()
        : m_objectMap (new CHashMap<SMetaObject, int>()),
          m_objectMap2 (new CHashMap<int, SMetaObject>())
    {
    }

    virtual ~CIdTableImpl()
    {
        SHashMapEnumerator<SMetaObject, int> mapEnum (m_objectMap);
        SMetaObject k;
        while(mapEnum.MoveNext(&k, nullptr)) {
            METAOBJECT metaObject (k.ToMETAOBJECT());
            bool deleted;
            UnrefMetaObject(&metaObject, &deleted);
        }
    }

    virtual intptr_t GetObjectId(const METAOBJECT& metaObject) override
    {
        const SMetaObject obj (metaObject);

        int r;
        if(!m_objectMap->TryGet(obj, &r)) {
            m_gObjectId++;
            r = m_gObjectId;
            m_objectMap->Set(obj, r);
            m_objectMap2->Set(r, obj);

            METAOBJECT metaObjectCopy = metaObject; // have to do it because `metaObject` is const METAOBJECT&
            RefMetaObject(&metaObjectCopy);
        }
        return r;
    }
    
    virtual METAOBJECT ObjectById(intptr_t id)
    {
        return m_objectMap2->Item(id).ToMETAOBJECT();
    }

    virtual void Serialize(SBinaryWriter& bw) override
    {
        CStream* const baseStream = bw.Stream();
        METASTREAM pBaseStream;
        baseStream->ToMetaStream(&pBaseStream);

        const polk_long objectCountPos = baseStream->GetPosition();
        bw.WriteInt(0); // allocates space for the object count, not fully known yet

        const polk_long stringTableOffsetRefPos = baseStream->GetPosition();
        bw.WriteInt(0); // allocates space for the string offset, not fully known yet

        Auto<CQueue<SMetaObjectWithId> > queue (new CQueue<SMetaObjectWithId>());
        int objectCount = 0;

        {
            SHashMapEnumerator<SMetaObject, int> mapEnum (m_objectMap);
            SMetaObject k;
            int v;

            while(mapEnum.MoveNext(&k, &v)) {
                queue->Enqueue(SMetaObjectWithId(k, v));
            }
        }

        Auto<CHashMap<const char*, int> > stringMap (new CHashMap<const char*, int>());
        Auto<CArrayList<const char*> > stringList (new CArrayList<const char*>());

        // Dependency map. Objects referenced by other objects need to be serialized
        // first. So the order we deserialize objects later should be correct.
        // Note that we do not support infinite cycles. TODO detect them?
        Auto<CHashMap<int, CArrayList<int>*> > depMap (new CHashMap<int, CArrayList<int>*>());
        Auto<CHashMap<int, int> > objectRefMap (new CHashMap<int, int>());

        Auto<CIdTableImpl> workingTable (new CIdTableImpl());
        METAIDTABLE pWorkingTable;
        workingTable->ToMetaIdTable(&pWorkingTable);

        while(!queue->IsEmpty()) {

            const SMetaObjectWithId kv (queue->Dequeue());
            const SMetaObject obj (kv.Object);
            const int id = kv.Id;

            // Object ID.
            const int objectRef = (int)baseStream->GetPosition();
            objectRefMap->Set(id, objectRef);
            bw.WriteInt(id);

            // String ID of the class name.
            const char* const typeName = obj.Class->name;
            int typeId;
            if(!stringMap->TryGet(typeName, &typeId)) {
                typeId = stringList->Count();
                stringMap->Set(typeName, typeId);
                stringList->Add(typeName);
            }
            bw.WriteInt(typeId);

            METACLASS* klass = obj.Class;
            if(klass->flags & METACLASSFLAGS_STATIC) {

                // Static class.
                // Do nothing (a static class has no data).

            } else {

                // Normal class.

                METASERIALIZEIMPL serialimpl = nullptr;
                while(klass) { // NOTE looks in the hierarchy
                    if(klass->serialimpl) {
                        serialimpl = klass->serialimpl;
                        break;
                    }
                    klass = klass->parent;
                }
                if(!serialimpl) {
                    POLK_THROW_WITH_MSG(EC_MISSING_MEMBER, "Object has no serialization method (CIdTable::Serialize)");
                }
                METAOBJECT metaObject (obj.ToMETAOBJECT());
                // NOTE A call to serialimpl(..) may eventually try to fetch an ID of
                // an object which is not registered yet. They are added to idTable
                // and will be processed in the next round.
                METARESULT res = serialimpl(&metaObject, &pBaseStream, &pWorkingTable, klass->ctx);
                if(res != METARESULT_SUCCESS) {
                    POLK_THROW_WITH_MSG(EC_EXECUTION_ERROR, "Object serialization failed (CIdTable::Serialize)");
                }

            }

            objectCount++;

            if(workingTable->m_objectMap->Size() > 0) {

                CArrayList<int>* depList;
                if(!depMap->TryGet(id, &depList)) {
                    depList = new CArrayList<int>();
                    depMap->Set(id, depList);
                }
                depList->Unref();

                {
                    SHashMapEnumerator<SMetaObject, int> mapEnum (workingTable->m_objectMap);
                    SMetaObject k;
                    int v;
                    while(mapEnum.MoveNext(&k, &v)) {
                        queue->Enqueue(SMetaObjectWithId(k, v));
                        depList->Add(v);
                    }
                }

                workingTable.SetPtr(new CIdTableImpl());
                workingTable->ToMetaIdTable(&pWorkingTable);

            } else {
                depMap->Set(id, nullptr);
            }
        }

        // Writes the string table.
        const int stringTableOffsetPos = (int)baseStream->GetPosition();
        bw.WriteInt(stringList->Count());
        for(int i = 0; i < stringList->Count(); i++) {
            const char* const cStr = stringList->Array()[i];
            bw.WriteUTF8(cStr, strlen(cStr), true); // lengthHeader=true
        }

        // **************************************************
        // VERSION 2.
        //
        // TopoSort is done in the reading phase (not here).
        // **************************************************
        {
            SHashMapEnumerator<int, int> objectRefMapEnum (objectRefMap);
            int k, v;
            while(objectRefMapEnum.MoveNext(&k, &v)) {
                bw.WriteInt(k);
                bw.WriteInt(v);
            }
        }

        bw.WriteInt(depMap->Size());
        {
            SHashMapEnumerator<int, CArrayList<int>*> depMapEnum (depMap);
            int k;
            CArrayList<int>* v;
            while(depMapEnum.MoveNext(&k, &v)) {

                bw.WriteInt(k);

                if(v == nullptr) {
                    bw.WriteInt(0);
                } else {
                    bw.WriteInt(v->Count());
                    for(int i = 0; i < v->Count(); i++) {
                        bw.WriteInt(v->Array()[i]);
                    }
                }

            }
        }
        // **************************************************

        // Now we know the object count => writes it.
        const polk_long oldPos = baseStream->GetPosition();
        baseStream->SetPosition(objectCountPos);
        bw.WriteInt(objectCount);

        // Writes the offset to the string table while we're at it.
        baseStream->SetPosition(stringTableOffsetRefPos);
        bw.WriteInt(stringTableOffsetPos);

        baseStream->SetPosition(oldPos);
    }
};

static void topoSortRec(int curValue,
                        CHashMap<int, CArrayList<int>*>* depMap,
                        CArrayList<int>* output,
                        CHashMap<int, int>* markedSet)
{
    CArrayList<int>* workingList;
    if(depMap->TryGet(curValue, &workingList)) {
        if(workingList) {
            for(int i = 0; i < workingList->Count(); i++) {
                topoSortRec(workingList->Array()[i], depMap, output, markedSet);
            }

            workingList->Unref();
        }
    }

    if(!markedSet->Contains(curValue)) {
        output->Add(curValue);
        markedSet->Set(curValue, curValue);
    }
}

static CArrayList<int>* topoSort(CHashMap<int, CArrayList<int>*>* depMap)
{
    Auto<CArrayList<int> > output (new CArrayList<int>());
    Auto<CHashMap<int, int> > markedSet (new CHashMap<int, int>());

    // Finds dependencies.
    Auto<CHashMap<int, int> > allDepedencies (new CHashMap<int, int>());
    SHashMapEnumerator<int, CArrayList<int>*> depMapEnum (depMap);
    {
        int k;
        CArrayList<int>* v;
        while(depMapEnum.MoveNext(&k, &v)) {
            if(v) {
                for(int i = 0; i < v->Count(); i++) {
                    const int val = v->Item(i);
                    allDepedencies->Set(val, val);
                }
            }
        }
    }

    // Invokes topoSortRec(..) on objects which are not dependencies.
    depMapEnum.Reset();
    {
        int k;
        CArrayList<int>* v;
        while(depMapEnum.MoveNext(&k, &v)) {
            if(!allDepedencies->Contains(k)) {
                topoSortRec(k, depMap, output, markedSet);
            }
        }
    }

    output->Ref();
    return output;
}

// Required to delete const char's.
struct SStringTableWrapper
{
    CArrayList<const char*>* Wrapped;

    SStringTableWrapper(CArrayList<const char*>* wrapped)
        : Wrapped(wrapped)
    {
        
    }

    ~SStringTableWrapper()
    {
        for(int i = 0; i < this->Wrapped->Count(); i++) {
            char* const cs = (char*)this->Wrapped->Array()[i];
            CString::FreeUtf8(cs);
        }

        this->Wrapped->Unref();
    }
};

CIdTable* CIdTable::Deserialize(SBinaryReader& br)
{
    Auto<CIdTableImpl> r (new CIdTableImpl());
    METAIDTABLE pTable;
    r->ToMetaIdTable(&pTable);

    CStream* const baseStream = br.Stream();
    METASTREAM pBaseStream;
    baseStream->ToMetaStream(&pBaseStream);

    const int objectCount = br.ReadInt();
    const int stringTableOffsetPos = br.ReadInt();

    // Reads the string table.
    SStringTableWrapper stringTable (new CArrayList<const char*>());
    const polk_long oldPos = baseStream->GetPosition();
    baseStream->SetPosition(stringTableOffsetPos);
    const int stringCount = br.ReadInt();
    for(int i = 0; i < stringCount; i++) {
        const char* const str = br.ReadUTF8();
        stringTable.Wrapped->Add(str);
    }

    // Reads the dependency order list.
    // **************************************************
    // VERSION 2.
    //
    // TopoSort is done in the reading phase (here).
    // Why here? We want faster writes, we don't care
    // as much about faster reads.
    // **************************************************
    Auto<CHashMap<int, int> > objectRefMap0 (new CHashMap<int, int>());
    for(int i = 0; i < objectCount; i++) {
        const int objId = br.ReadInt();
        const int objOffset = br.ReadInt();

        objectRefMap0->Set(objId, objOffset);
    }

    const int depMapSize = br.ReadInt();
    Auto<CHashMap<int, CArrayList<int>*> > depMap (new CHashMap<int, CArrayList<int>*>());
    for(int i = 0; i < depMapSize; i++) {
        const int objId = br.ReadInt();
        const int depCount = br.ReadInt();

        Auto<CArrayList<int> > depList (new CArrayList<int>());
        depMap->Set(objId, depList);
        for(int j = 0; j < depCount; j++) {
            const int depId = br.ReadInt();
            depList->Add(depId);
        }
    }

    Auto<CArrayList<int> > sortedDeps (topoSort(depMap));
    assert(sortedDeps->Count() == objectCount);
    Auto<CArrayList<int> > objectRefMap (new CArrayList<int>());
    for(int i = 0; i < sortedDeps->Count(); i++) {
        const int sortedDep = sortedDeps->Array()[i];

        objectRefMap->Add(objectRefMap0->Get(sortedDep));
    }
    // **********************************************

    const polk_long afterIdTableOffset = baseStream->GetPosition();

    // Returns back to the beginning of our object data.
    baseStream->SetPosition(oldPos);

    for(int i = 0; i < objectRefMap->Count(); i++) {
        const int objectRef = objectRefMap->Array()[i];
        baseStream->SetPosition(objectRef);

        const int objectId = br.ReadInt();
        const int typeId = br.ReadInt();

        const char* const klassName = stringTable.Wrapped->Item(typeId);
        METACLASS* klass = GetMetaClassByName(klassName);
        if(!klass) {
            POLK_THROW_WITH_MSG(EC_KEY_NOT_FOUND, "Unknown metaclass (CIdTable::Deserialize)");
        }

        METAVALUE metaValue;

        if(klass->flags & METACLASSFLAGS_STATIC) {

            // Static class.

            // Returns a dummy object, we only use its `klass` field to correctly
            // dispatch its methods.
            metaValue.type = METATYPE_OBJECT;
            metaValue.u.asObject.klass = klass;
            metaValue.u.asObject.idptr = 0;
            metaValue.annotations = 0;

        } else {

            // Normal class.

            METADESERIALIZEIMPL deserialimpl = nullptr;
            while(klass) { // NOTE looks in the hierarchy
                if(klass->deserialimpl) {
                    deserialimpl = klass->deserialimpl;
                    break;
                }
                klass = klass->parent;
            }
            if(!deserialimpl) {
                POLK_THROW_WITH_MSG(EC_MISSING_MEMBER, "Failed to deserialize an object: no serialization method (CIdTable::Deserialize)");
            }
            METARESULT res = deserialimpl(&pBaseStream, &metaValue, &pTable, klass->ctx);
            if(res != METARESULT_SUCCESS) {
                POLK_THROW_WITH_MSG(EC_EXECUTION_ERROR, "Failed to deserialize an object (CIdTable::Deserialize)");
            }

        }

        const SMetaObject obj (metaValue.u.asObject);
        r->m_objectMap->Set(obj, objectId);
        r->m_objectMap2->Set(objectId, obj);
    }

    baseStream->SetPosition(afterIdTableOffset);

    r->Ref();
    return r;
}

CIdTable* CIdTable::CreateInstance()
{
    return new CIdTableImpl();
}

    // **********************
    //     ToMetaIdTable
    // **********************

static METARESULT CIdTable_GetObjectId_wrapper(METAIDTABLE* metaIdTable, METAVALUE* value, intptr_t* out_ret)
{
    if(value->type == METATYPE_OBJECT) {
        auto const idTable = static_cast<CIdTable*>(metaIdTable->ctx);

        *out_ret = idTable->GetObjectId(value->u.asObject);
        return METARESULT_SUCCESS;
    } else {
        *out_ret = 0;
        return METARESULT_TYPE_ERROR;
    }
}

static METARESULT CIdTable_ObjectById_wrapper(METAIDTABLE* metaIdTable, intptr_t id, METAVALUE* out_ret)
{
    auto const idTable = static_cast<CIdTable*>(metaIdTable->ctx);

    try {

        const METAOBJECT metaObject = idTable->ObjectById(id);

        out_ret->type = METATYPE_OBJECT;
        out_ret->u.asObject = metaObject;

        return METARESULT_SUCCESS;

    } catch (const SException& e) { // do not leak outside of the callback, can be custom C code
        *out_ret = MetaValueFromNull();
        return METARESULT_INTERNAL_EXCEPTION;
    }
}

void CIdTable::ToMetaIdTable(METAIDTABLE* out_r)
{
    out_r->getobjectidimpl = CIdTable_GetObjectId_wrapper;
    out_r->objectbyidimpl = CIdTable_ObjectById_wrapper;
    out_r->ctx = static_cast<void*>(this);
}

    // ***************************
    //    CreateFromMetaIdTable
    // ***************************

class METAIDTABLEIdTable final: public CIdTable
{
private:
    METAIDTABLE m_metaIdTable;

public:
    METAIDTABLEIdTable(METAIDTABLE* metaIdTable)
        : m_metaIdTable(*metaIdTable)
    {
    }

    virtual intptr_t GetObjectId(const METAOBJECT& metaObject) override
    {
        METAVALUE value;
        value.type = METATYPE_OBJECT;
        value.u.asObject = metaObject;
        value.annotations = 0;

        intptr_t r;
        METARESULT res = m_metaIdTable.getobjectidimpl(&m_metaIdTable, &value, &r);
        if(res != METARESULT_SUCCESS) {
            POLK_THROW(EC_TYPE_MISMATCH);
        }
        return r;
    }

    virtual METAOBJECT ObjectById(intptr_t id) override
    {
        return METAOBJECT();
    }

    virtual void Serialize(SBinaryWriter& bw) override
    {
        POLK_THROW(EC_NOT_IMPLEMENTED);
    }
};

CIdTable* CIdTable::CreateFromMetaIdTable(METAIDTABLE* metaIdTable)
{
    return new METAIDTABLEIdTable(metaIdTable);
}

} }
