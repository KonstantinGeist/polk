// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <reflection/MetaObject.h>
#include <core/Contract.h>
#include <core/String.h>
#include <collections/HashMap.h>
#include <io/Stream.h>

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#pragma GCC diagnostic ignored "-Wunused-variable"

// If this is defined, then all non-static classes must have serialization/deserialization
// methods.
#define POLK_STRONG_SERIALIZATION_CONTRACT

using namespace polk::core;
using namespace polk::collections;
using namespace polk::io;

    // *****************
    //   Static stuff.
    // *****************

inline void POLK_REF(const METAMETHOD* i) { }
inline void POLK_UNREF(const METAMETHOD* i) { }
inline bool POLK_EQUALS(const METAMETHOD* a, const METAMETHOD* b) { return a == b; }

// Metaclasses are defined early in the process, so this hashmap is created early,
// however this class is not dependent on other CObject-derived classes, so it's
// safe to use here.
static CHashMap<const char*, void*>* g_classMap = nullptr;

METARESULT RefMetaObject(METAOBJECT* obj)
{
    METACLASS* klass = obj->klass;

    while(klass) {

        if(klass->refimpl) {
            return klass->refimpl(obj, klass->ctx);
        }

        klass = klass->parent;
    }

    return METARESULT_SUCCESS;
}

METARESULT UnrefMetaObject(METAOBJECT* obj, bool *out_deleted)
{
    METACLASS* klass = obj->klass;

    while(klass) {

        if(klass->unrefimpl) {
            METARESULT r = klass->unrefimpl(obj, klass->ctx, out_deleted);

            if(*out_deleted) {
                obj->idptr = 0; // just in case
            }

            return r;
        }

        klass = klass->parent;
    }

    *out_deleted = false;
    return METARESULT_SUCCESS;
}

void __InitMetaObject()
{
    g_classMap = new CHashMap<const char*, void*>();
}

void __DeinitMetaObject()
{
    POLK_REQ_PTR(g_classMap);

    SHashMapEnumerator<const char*, void*> mapEnum (g_classMap);
    const char* k;
    void* v;
    while(mapEnum.MoveNext(&k, &v)) {

        METACLASS* klass = static_cast<METACLASS*>(v);

        CHashMap<const char*, METAMETHOD*>* dispatcher
                    = static_cast<CHashMap<const char*, METAMETHOD*>*>(klass->reserved);
        dispatcher->Unref();

        delete klass;
    }

    g_classMap->Unref();
    g_classMap = nullptr;
}

    // *****************

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

static METARESULT static_ctor(int nArg, METAVALUE* args, intptr_t* out_ret, void* ctx)
{
    VERIFY_NARG(0)
    METAGUARD_BEGIN
        *out_ret = 0;
    METAGUARD_END
}

const char* MetaResultToCString(METARESULT r)
{
    switch(r) {
        case METARESULT_SUCCESS: return "METARESULT_SUCCESS";
        case METARESULT_UNKNOWN_CLASS: return "METARESULT_UNKNOWN_CLASS";
        case METARESULT_TYPE_ERROR: return "METARESULT_TYPE_ERROR";
        case METARESULT_PARAMETER_MISMATCH: return "METARESULT_PARAMETER_MISMATCH";
        case METARESULT_INTERNAL_EXCEPTION: return "METARESULT_INTERNAL_EXCEPTION";
        case METARESULT_MISSING_METHOD: return "METARESULT_MISSING_METHOD";
        case METARESULT_MISSING_CTOR: return "METARESULT_MISSING_CTOR";
        case METARESULT_NULL_REF: return "METARESULT_NULL_REF";
        case METARESULT_INVALID_ARGUMENT: return "METARESULT_INVALID_ARGUMENT";
        case METARESULT_ACCESS_DENIED: return "METARESULT_ACCESS_DENIED";
        case METARESULT_NOT_SUPPORTED: return "METARESULT_NOT_SUPPORTED";
        case METARESULT_BUFFER_TOO_SMALL: return "METARESULT_BUFFER_TOO_SMALL";
        default: return "Unknown (TODO)";
    }
}

void RegisterMetaClass(METACLASS* klass)
{
    POLK_REQ_PTR(g_classMap);
    POLK_REQ_PTR(klass->name);

    if(klass->methodCount > 0) {
        POLK_REQ_PTR(klass->vtable);
    }

    for(int i = 0; i < klass->methodCount; i++) {
        METAMETHOD* const method = &klass->vtable[i];

        POLK_REQ_PTR(method->name);
        POLK_REQ_PTR(method->impl);
    }

#ifdef POLK_STRONG_SERIALIZATION_CONTRACT
    if(!(klass->flags & METACLASSFLAGS_STATIC)
    && strcmp(klass->name, "Object") != 0          // one cannot instantiate Object
    && strcmp(klass->name, "EventHandler") != 0)   // EventHandler has special support in polk
    {
        METASERIALIZEIMPL serialimpl = nullptr;
        METADESERIALIZEIMPL deserialimpl = nullptr;
        METACLASS* k = klass;
        while(k) {
            if(k->serialimpl) {
                serialimpl = k->serialimpl;
                deserialimpl = k->deserialimpl;
                break;
            }

            k = k->parent;
        }

        if(!serialimpl || !deserialimpl) {
            printf("Non-static metaclass '%s' doesn't have serialization/deserialization methods (POLK_STRONG_SERIALIZATION_CONTRACT enabled).",
                    klass->name);
            abort();
        }
    }
#endif

    METACLASS* const klassCopy = new METACLASS();
    memcpy(klassCopy, klass, sizeof(METACLASS));

    if(!klass->ctor && (klass->flags & METACLASSFLAGS_STATIC))
        klassCopy->ctor = static_ctor;

    if(!klass->vtable) {
        POLK_REQ_PTR(klass->parent);
        klassCopy->vtable = klass->parent->vtable;
    }

    // Helps to locate methods potentially more quickly, without a linear search.
    klassCopy->reserved = new CHashMap<const char*, METAMETHOD*>();

    g_classMap->Set(klass->name, static_cast<void*>(klassCopy));
}

METACLASS* GetMetaClassByName(const char* name)
{
    POLK_REQ_PTR(g_classMap);

    void* r;
    if(g_classMap->TryGet(name, &r))
        return static_cast<METACLASS*>(r);
    else
        return nullptr;
}

int GetMetaClasses(const char** out_val)
{
    POLK_REQ_PTR(g_classMap);

    if(out_val) {
        int counter = 0;
        SHashMapEnumerator<const char*, void*> mapEnum (g_classMap);
        const char* k;
        void* v;
        while(mapEnum.MoveNext(&k, &v)) {
            out_val[counter] = k;

            counter++;
        }
    }

    return g_classMap->Size();
}

METARESULT NewMetaObject(const char* klassName, int nArg, METAVALUE* args, METAVALUE* out_ret)
{
    POLK_REQ_PTR(g_classMap);

    void* _klass;
    if(g_classMap->TryGet(klassName, &_klass)) {

        METACLASS* klass = static_cast<METACLASS*>(_klass);
        if(!klass->ctor)
            return METARESULT_MISSING_CTOR;

        intptr_t newObj;
        METARESULT r = klass->ctor(nArg, args, &newObj, klass->ctx);

        if(r == METARESULT_SUCCESS) {

            METAVALUE r;
            r.type = METATYPE_OBJECT;
            r.u.asObject.klass = klass;
            r.u.asObject.idptr = newObj;

            *out_ret = r;
            return METARESULT_SUCCESS;

        } else {
            return r;
        }

    } else {
        return METARESULT_UNKNOWN_CLASS;
    }
}

METAVALUE MetaValueFromPtr(const char* klassName, void* ptr)
{
    void* _klass = 0;
    POLK_REQ_PTR(g_classMap);
    g_classMap->TryGet(klassName, &_klass);
    POLK_REQ_WITH_MSG(_klass != nullptr, EC_KEY_NOT_FOUND, "Unknown class.");

    METAVALUE r;
    r.type = METATYPE_OBJECT;
    r.u.asObject.klass = static_cast<METACLASS*>(_klass);
    r.u.asObject.idptr = (intptr_t)ptr;

    return r;
}

METAVALUE MetaValueFromId(const char* klassName, int id)
{
    void* _klass = 0;
    POLK_REQ_PTR(g_classMap);
    g_classMap->TryGet(klassName, &_klass);
    POLK_REQ_PTR(_klass);

    METAVALUE r;
    r.type = METATYPE_OBJECT;
    r.u.asObject.klass = static_cast<METACLASS*>(_klass);
    r.u.asObject.idptr = (intptr_t)id;

    return r;
}

METAVALUE MetaValueFromVal(const char* klassName, void* ptr)
{
    METAVALUE value = MetaValueFromPtr(klassName, ptr);
    METACLASS* klass = value.u.asObject.klass;

    while(klass) {
        if(klass->refimpl) {
            klass->refimpl(&value.u.asObject, klass->ctx);
            break;
        }

        klass = klass->parent;
    }

    return value;
}

METAVALUE MetaValueFromNull()
{
    METAVALUE r;
    memset(&r, 0, sizeof(METAVALUE));
    r.type = METATYPE_NULL;
    return r;
}

METAVALUE MetaValueFromNumber(float number)
{
    METAVALUE r;
    r.type = METATYPE_NUMBER;
    r.u.asNumber = number;
    return r;
}

METAVALUE MetaValueFromBoolean(bool b)
{
    METAVALUE r;
    r.type = METATYPE_BOOL;
    r.u.asBool = b;
    return r;
}

METAVALUE MetaValueFromCString(const char* value)
{
    return STRINGPTR_TO_METAVALUE(CString::FromUtf8(value));
}

char* MetaValueToCString(METAVALUE* metaValue)
{
    METAVALUE strValue = MetaValueToString(metaValue);
    if(strValue.type == METATYPE_NULL) {
        return nullptr;
    }

    char* cString = METAVALUE_TO_STRINGPTR(&strValue)->ToUtf8();
    bool deleted;
    UnrefMetaValue(&strValue, &deleted);
    return cString;
}

void MetaValueFreeCString(char* cs)
{
    CString::FreeUtf8(cs);
}

METARESULT RefMetaValue(METAVALUE* value)
{
    METARESULT r = METARESULT_SUCCESS;

    if(value->type == METATYPE_OBJECT) {

        METAOBJECT* const obj = &value->u.asObject;

        if(obj)
            r = RefMetaObject(obj);

    } else if(value->type == METATYPE_ARRAY) {

        METAARRAY& arr = value->u.asArray;
        POLK_REQ_NOT_NEG(arr.count);

        if(arr.count > 0) {
            POLK_REQ_PTR(arr.items);
        }

        for(int i = 0; i < arr.count; i++) {
            const METARESULT r2 = RefMetaValue(&arr.items[i]);

            if(r == METARESULT_SUCCESS)
                r = r2;
        }

    }

    return r;
}

METARESULT UnrefMetaValue(METAVALUE* value, bool* out_deleted)
{
    METARESULT r = METARESULT_SUCCESS;

    if(value->type == METATYPE_OBJECT) {

        METAOBJECT* const obj = &value->u.asObject;

        if(obj) {
            r = UnrefMetaObject(obj, out_deleted);

            if(*out_deleted) {
                value->u.asObject.idptr = 0;
                value->u.asObject.klass = nullptr;
                value->type = METATYPE_NULL;
            }
        } else {
            *out_deleted = false;
        }

    } else if(value->type == METATYPE_ARRAY) {

        METAARRAY& arr = value->u.asArray;
        POLK_REQ_NOT_NEG(arr.count);

        if(arr.count > 0) {
            POLK_REQ_PTR(arr.items);
        }

        for(int i = 0; i < arr.count; i++) {
            bool tmp;
            const METARESULT r2 = UnrefMetaValue(&arr.items[i], &tmp);

            // Overrides the result only if there were no errors, i.e. reports
            // the first fail. Also note that it tries to unref all metavalues
            // even if an error happened somewhere in the process (to avoid leaks).
            if(r == METARESULT_SUCCESS)
                r = r2;
        }

        delete [] arr.items;
        arr.items = nullptr;

        value->type = METATYPE_NULL;

        *out_deleted = true;

    } else {
        
        value->type = METATYPE_NULL;
        *out_deleted = false;

    }

    return r;
}

METAMETHOD* GetMetaMethod(METACLASS* klass, const char* name)
{
    METAMETHOD* method;
    auto const dispatcher = static_cast<CHashMap<const char*, METAMETHOD*>*>(klass->reserved);

    // Maybe already in the cache?
    if(dispatcher->TryGet(name, &method))
        return method;

    // => not in the cache yet.
    while(klass) {

        for(int i = 0; i < klass->methodCount; i++) {
            method = &klass->vtable[i];

            if(strcmp(method->name, name) == 0) {
                // Puts the result of the search to the cache.
                dispatcher->Set(method->name, method);

                return method;
            }
        }

        klass = klass->parent;
    }

    return nullptr;
}

METARESULT InvokeMetaMethod(METAOBJECT* obj, const char* name, int nArg, METAVALUE* args, METAVALUE* out_ret)
{
    METAMETHOD* const method = GetMetaMethod(obj->klass, name);

    if(method) {
        // To make sure it does not break if the method does not bother to return
        // a value.
        memset(out_ret, 0, sizeof(METAVALUE));

        return method->impl(obj, nArg, args, out_ret, method->ctx);
    } else {
        return METARESULT_MISSING_METHOD;
    }
}

METARESULT InvokeMetaMethodDirect(METAOBJECT* obj,
                                  METAMETHOD* method,
                                  int nArg,
                                  METAVALUE* args,
                                  METAVALUE* out_ret)
{
    // To make sure it does not break if the method does not bother to return
    // a value.
    memset(out_ret, 0, sizeof(METAVALUE));

    return method->impl(obj, nArg, args, out_ret, method->ctx);
}

METARESULT MetaObjectRefImpl(METAOBJECT* obj, void* ctx)
{
    if(obj->idptr) {
        ((CObject*)(void*)obj->idptr)->Ref();
    }

    return METARESULT_SUCCESS;
}

METARESULT MetaObjectUnrefImpl(METAOBJECT* obj, void* ctx, bool* out_deleted)
{
    if(obj->idptr) {
        *out_deleted = ((CObject*)(void*)obj->idptr)->Unref();
    } else {
        *out_deleted = false;
    }

    return METARESULT_SUCCESS;
}

METARESULT VerifyMetaObject(METAVALUE* arg, const char* klassName)
{
    if(arg->type != METATYPE_OBJECT)
        return METARESULT_TYPE_ERROR;

    METACLASS* klass = arg->u.asObject.klass;

    while(klass) {
        if(strcmp(klass->name, klassName) == 0)
            return METARESULT_SUCCESS;

        klass = klass->parent;
    }

    return METARESULT_TYPE_ERROR;
}

bool IsInstanceOf(METAOBJECT* obj, const char* klassName)
{
    METACLASS* klass = obj->klass;

    while(klass) {

        if(strcmp(klass->name, klassName) == 0) {
            return true;
        }

        klass = klass->parent;
    }

    return false;
}

METAVALUE CreateArrayMetaValue(int count)
{
    POLK_REQ_NOT_NEG(count);

    METAVALUE v;
    v.type = METATYPE_ARRAY;
    v.u.asArray.count = count;
    v.u.asArray.items = new METAVALUE[count];

    for(int i = 0; i < count; i++) {
        v.u.asArray.items[i] = MetaValueFromNull();
    }

    return v;
}

METAVALUE MetaValueToString(METAVALUE* metaValue)
{
    if(metaValue->type == METATYPE_OBJECT) {

        METAOBJECT* const obj = &metaValue->u.asObject;
        METAVALUE out_ret = MetaValueFromNull();

        METARESULT r = InvokeMetaMethod(obj, "ToString", 0, nullptr, &out_ret);
        if(r == METARESULT_SUCCESS) {

            if(out_ret.type == METATYPE_OBJECT && IsInstanceOf(&out_ret.u.asObject, "String")) {
                return out_ret;
            } else {
                // Wrong type...
                bool deleted;
                UnrefMetaValue(&out_ret, &deleted);
                return MetaValueFromNull();
            }

        } else {
            return MetaValueFromNull();
        }

    } else {
        // Other types aren't supported because we can't assume formatting rules for numbers, etc.
        return MetaValueFromNull();
    }
}

bool MetaValueEquals(METAVALUE* a, METAVALUE* b)
{
    if(a->type != b->type)
        return false;

    switch(a->type) {
        case METATYPE_BOOL:
            return a->u.asBool == b->u.asBool;

        case METATYPE_NUMBER:
            return a->u.asNumber == b->u.asNumber; //-V550

        case METATYPE_NULL:
            return true;

        case METATYPE_OBJECT:
            {
                METAVALUE out_ret = MetaValueFromNull();

                if(InvokeMetaMethod(&a->u.asObject, "Equals", 1, b, &out_ret) != METARESULT_SUCCESS) {
                    return false;
                }
                if(out_ret.type != METATYPE_BOOL) {
                    bool dummy;
                    UnrefMetaValue(&out_ret, &dummy);
                    return false;
                }

                return out_ret.u.asBool;
            }

        default:
            return false;
    }

    return false; // Silences the compiler.
}

int GetMetaValueHashCode(METAVALUE* metaValue)
{
    switch(metaValue->type) {
        case METATYPE_BOOL:
            return metaValue->u.asBool;

        case METATYPE_NUMBER:
            assert(sizeof(int32_t) == sizeof(float));
        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wstrict-aliasing"
            return (int)(*reinterpret_cast<const int32_t*>(&metaValue->u.asNumber));
        #pragma GCC diagnostic pop

        case METATYPE_NULL:
            return 0;

        case METATYPE_OBJECT:
            {
                METAVALUE out_ret = MetaValueFromNull();

                if(InvokeMetaMethod(&metaValue->u.asObject, "GetHashCode", 0, nullptr, &out_ret) != METARESULT_SUCCESS) {
                    return 0;
                }
                if(out_ret.type != METATYPE_NUMBER) {
                    bool dummy;
                    UnrefMetaValue(&out_ret, &dummy);
                    return 0;
                }

                return out_ret.u.asNumber;
            }

        default:
            return 0;
    }

    return 0; // Silences the compiler.
}

METARESULT PatchMetaMethod(const char* klassName, const char* methodName, METAMETHODIMPL impl)
{
    METACLASS* klass = GetMetaClassByName(klassName);
    if(!klass)
        return METARESULT_UNKNOWN_CLASS;

    for(int i = 0; i < klass->methodCount; i++) {
        METAMETHOD& method = klass->vtable[i];

        if(strcmp(method.name, methodName) == 0) {
            method.impl = impl;
            return METARESULT_SUCCESS;
        }
    }

    return METARESULT_MISSING_METHOD;
}

METARESULT ExceptionCodeToMetaResult(int exceptionCode)
{
    switch(exceptionCode) {
        case EC_OK: return METARESULT_SUCCESS;
        case EC_CUSTOM: return METARESULT_INTERNAL_EXCEPTION;
        case EC_NOT_IMPLEMENTED: return METARESULT_INTERNAL_EXCEPTION;
        case EC_PLATFORM_DEPENDENT: return METARESULT_NOT_SUPPORTED;
        case EC_INVALID_STATE: return METARESULT_INTERNAL_EXCEPTION;
        case EC_MARSHAL_ERROR: return METARESULT_INTERNAL_EXCEPTION;
        case EC_ILLEGAL_ARGUMENT: return METARESULT_INVALID_ARGUMENT;
        case EC_OUT_OF_RANGE: return METARESULT_INTERNAL_EXCEPTION;
        case EC_PATH_NOT_FOUND: return METARESULT_INTERNAL_EXCEPTION;
        case EC_BAD_FORMAT: return METARESULT_INTERNAL_EXCEPTION;
        case EC_EXECUTION_ERROR: return METARESULT_INTERNAL_EXCEPTION;
        case EC_KEY_NOT_FOUND: return METARESULT_INTERNAL_EXCEPTION;
        case EC_TYPE_MISMATCH: return METARESULT_TYPE_ERROR;
        case EC_OUT_OF_RESOURCES: return METARESULT_INTERNAL_EXCEPTION;
        case EC_CONCURRENT_MODIFICATION: return METARESULT_INTERNAL_EXCEPTION;
        case EC_TIMEOUT: return METARESULT_INTERNAL_EXCEPTION;
        case EC_CONNECTION_LOST: return METARESULT_INTERNAL_EXCEPTION;
        case EC_PARAMETER_COUNT_MISMATCH: return METARESULT_PARAMETER_MISMATCH;
        case EC_MISSING_MEMBER: return METARESULT_MISSING_METHOD;
        case EC_WRONG_THREAD: return METARESULT_INTERNAL_EXCEPTION;
        case EC_CONTRACT_UNSATISFIED: return METARESULT_INVALID_ARGUMENT;
        case EC_ACCESS_DENIED: return METARESULT_ACCESS_DENIED;
    }

    return METARESULT_INVALID_ARGUMENT;
}

// **************************
//   Serialization support.
// **************************

inline METARESULT writeByte(METASTREAM* stream, polk_byte byte)
{
    return stream->writeimpl(stream, &byte, sizeof(byte));
}

inline METARESULT writeInt(METASTREAM* stream, int i)
{
    return stream->writeimpl(stream, &i, sizeof(int));
}

inline METARESULT writeNumber(METASTREAM* stream, float number)
{
    return stream->writeimpl(stream, &number, sizeof(float));
}

inline METARESULT writeBool(METASTREAM* stream, bool b)
{
    return stream->writeimpl(stream, &b, sizeof(bool)); // TODO sizeof(bool) is undefined
}

inline METARESULT writeChars(METASTREAM* stream, const char* chars)
{
    int len = strlen(chars);
    const METARESULT r = stream->writeimpl(stream, &len, sizeof(int));
    if(r != METARESULT_SUCCESS)
        return r;

    return stream->writeimpl(stream, &chars[0], len);
}

// NOTE: The first value should be a byte mark to tell what metatype it is.
// It will be decoded during serialization.
METARESULT SerializeMetaValue(METAVALUE* value, METASTREAM* stream, METAIDTABLE* idTable)
{
    METARESULT r;

    switch(value->type) {
        case METATYPE_NULL:
            {
                // Type mark.
                r = writeByte(stream, METATYPE_NULL);
                if(r != METARESULT_SUCCESS)
                    return r;
            }
            return METARESULT_SUCCESS;

        case METATYPE_NUMBER:
            {
                // Type mark.
                r = writeByte(stream, METATYPE_NUMBER);
                if(r != METARESULT_SUCCESS)
                    return r;

                r = writeNumber(stream, value->u.asNumber);
                if(r != METARESULT_SUCCESS)
                    return r;
            }
            return METARESULT_SUCCESS;

        case METATYPE_OBJECT:
            {
                // Type mark.
                r = writeByte(stream, METATYPE_OBJECT);
                if(r != METARESULT_SUCCESS)
                    return r;

                intptr_t objId;
                r = idTable->getobjectidimpl(idTable, value, &objId);
                if(r != METARESULT_SUCCESS)
                    return r;

                r = writeInt(stream, (int)objId);
                if(r != METARESULT_SUCCESS)
                    return r;
            }
            return METARESULT_SUCCESS;

        case METATYPE_BOOL:
            {
                // Type mark.
                r = writeByte(stream, METATYPE_BOOL);
                if(r != METARESULT_SUCCESS)
                    return r;

                r = writeBool(stream, value->u.asBool);
                if(r != METARESULT_SUCCESS)
                    return r;
            }
            return METARESULT_SUCCESS;

        default:
            return METARESULT_TYPE_ERROR; // TODO "not supported"?
    }
}

METARESULT readByte(METASTREAM* stream, polk_byte* out_val)
{
    return stream->readimpl(stream, out_val, sizeof(polk_byte));
}

METARESULT readNumber(METASTREAM* stream, float* out_val)
{
    return stream->readimpl(stream, out_val, sizeof(float));
}

METARESULT readInt(METASTREAM* stream, int* out_i)
{
    return stream->readimpl(stream, out_i, sizeof(int));
}

METARESULT readBool(METASTREAM* stream, bool* out_val)
{
    return stream->readimpl(stream, out_val, sizeof(bool));
}

// Class names aren't supposed to be longer than 128 characters.
// TODO document it? implementation detail?
#define READCHARS_BUFSIZE 128

METARESULT readChars(METASTREAM* stream, char* out_buf)
{
    METARESULT r;

    int len;
    r = stream->readimpl(stream, &len, sizeof(int));
    if(r != METARESULT_SUCCESS)
        return r;
    if((len <= 0) || (len >= READCHARS_BUFSIZE))
        return METARESULT_BUFFER_TOO_SMALL;

    r = stream->readimpl(stream, out_buf, len);
    if(r != METARESULT_SUCCESS)
        return r;

    out_buf[len] = 0; // null termination (not present in the stream)
    return METARESULT_SUCCESS;
}

METARESULT DeserializeMetaValue(METASTREAM* stream, METAVALUE* out_ret, METAIDTABLE* idTable)
{
    METARESULT r;
    polk_byte mark;

    r = readByte(stream, &mark);
    if(r != METARESULT_SUCCESS)
        return r;

    switch(mark) {
        case METATYPE_NULL:
            {
                *out_ret = MetaValueFromNull();
            }
            return METARESULT_SUCCESS;

        case METATYPE_NUMBER:
            {
                float number;
                r = readNumber(stream, &number);
                if(r != METARESULT_SUCCESS)
                    return r;

                *out_ret = MetaValueFromNumber(number);
            }
            return METARESULT_SUCCESS;

        case METATYPE_OBJECT:
            {
                int objId;
                r = readInt(stream, &objId);
                if(r != METARESULT_SUCCESS)
                    return r;

                r = idTable->objectbyidimpl(idTable, objId, out_ret);
                if(r != METARESULT_SUCCESS)
                    return r;

                // ObjectById itself doesn't add a reference.
                r = RefMetaValue(out_ret);
                if(r != METARESULT_SUCCESS)
                    return r;
            }
            return METARESULT_SUCCESS;

        case METATYPE_BOOL:
            {
                bool b;
                r = readBool(stream, &b);
                if(r != METARESULT_SUCCESS)
                    return r;

                *out_ret = MetaValueFromBoolean(b);
            }
            return METARESULT_SUCCESS;

        default:
            return METARESULT_NOT_SUPPORTED;
    }
}

#ifdef __cplusplus
}
#endif /* __cplusplus */
