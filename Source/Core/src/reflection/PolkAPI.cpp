// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <reflection/PolkAPI.h>

void InitializePolkAPI(PolkAPI* polkAPI)
{
    polkAPI->MetaResultToCString = MetaResultToCString;
    polkAPI->RegisterMetaClass = RegisterMetaClass;
    polkAPI->GetMetaClasses = GetMetaClasses;
    polkAPI->GetMetaClassByName = GetMetaClassByName;
    polkAPI->NewMetaObject = NewMetaObject;
    polkAPI->UnrefMetaValue = UnrefMetaValue;
    polkAPI->RefMetaObject = RefMetaObject;
    polkAPI->UnrefMetaObject = UnrefMetaObject;
    polkAPI->MetaValueFromNull = MetaValueFromNull;
    polkAPI->MetaValueFromNumber = MetaValueFromNumber;
    polkAPI->MetaValueFromBoolean = MetaValueFromBoolean;
    polkAPI->MetaValueFromCString = MetaValueFromCString;
    polkAPI->MetaValueFromPtr = MetaValueFromPtr;
    polkAPI->MetaValueFromVal = MetaValueFromVal;
    polkAPI->MetaValueFromId = MetaValueFromId;
    polkAPI->GetMetaMethod = GetMetaMethod;
    polkAPI->InvokeMetaMethod = InvokeMetaMethod;
    polkAPI->InvokeMetaMethodDirect = InvokeMetaMethodDirect;
    polkAPI->VerifyMetaObject = VerifyMetaObject;
    polkAPI->IsInstanceOf = IsInstanceOf;
    polkAPI->RefMetaValue = RefMetaValue;
    polkAPI->MetaValueToString = MetaValueToString;
    polkAPI->MetaValueToCString = MetaValueToCString;
    polkAPI->MetaValueFreeCString = MetaValueFreeCString;
    polkAPI->MetaValueEquals = MetaValueEquals;
    polkAPI->GetMetaValueHashCode = GetMetaValueHashCode;
    polkAPI->PatchMetaMethod = PatchMetaMethod;
    polkAPI->SerializeMetaValue = SerializeMetaValue;
    polkAPI->DeserializeMetaValue = DeserializeMetaValue;
}
