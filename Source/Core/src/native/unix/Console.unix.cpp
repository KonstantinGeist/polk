// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/Console.h>
#include <core/Exception.h>
#include <core/String.h>

#include <stdio.h>

namespace polk { namespace core { namespace Console {
using namespace polk::core;

const CString* ReadLine()
{
#ifdef POLK_ANDROID
    // getline(..) is not supported under Android; it doesn't make sense anyway.
    POLK_THROW(EC_NOT_IMPLEMENTED);
#else
    const CString* r;

    // Assumes GNU C.
    char* buffer = NULL;
    int read;
    size_t len;
    read = getline(&buffer, &len, stdin);
    if (read < 1 || !buffer) {
        r = CString::CreateEmptyString();
    } else {
        len = strlen(buffer);

        // Removes the delimiter.
        if(buffer[len - 1] == '\n')
            buffer[len - 1] = 0;

        r = CString::FromUtf8(buffer);
    }
    free(buffer);

    return r;
#endif
}

void Write(const CString* str)
{
    // Unlike on Windows, it is enough to just use "printf", as the console already supports
    // Unicode.
    Utf8Auto cStr (str->ToUtf8());
    printf("%s", cStr.Ptr());
}

void WriteLine(const polk::core::CString* str)
{
    Utf8Auto cStr (str->ToUtf8());
    printf("%s\n", cStr.Ptr());
}

void SetForeColor(EConsoleColor color)
{
    switch(color) {
        case E_CONSOLECOLOR_RED:
            printf("\x1b[31m");
            break;
        case E_CONSOLECOLOR_YELLOW:
            printf("\x1b[33m");
            break;
        case E_CONSOLECOLOR_GREEN:
            printf("\x1b[32m");
            break;
        case E_CONSOLECOLOR_BLUE:
            printf("\x1b[34m");
            break;
        default:
            printf("\x1b[0m"); // resets the color
            break;
    }
}

} } }
