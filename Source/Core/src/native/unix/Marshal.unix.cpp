// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#include <core/Marshal.h>

namespace polk { namespace core { namespace Marshal {

int polk_wcslen_16bit(const polk_char16* str)
{
    const polk_char16* s = str;
    while (*s) s++;
    return s - str;
}

polk_char16* polk_wcscpy_16bit(polk_char16* dst, const polk_char16* src)
{
   polk_char16 *p = dst;
   while ((*p++ = *src++));
   return dst;
}

polk_char16* polk_wmemcpy_16bit(polk_char16* __restrict__ s1, const polk_char16* __restrict__ s2, int n)
{
    polk_char16* orig_s1 = s1;

    if (s1 == NULL || s2 == NULL || n == 0)
        return orig_s1;

    for (; n > 0; --n )
        *s1++ = *s2++;

    return orig_s1;
}

int polk_wcscmp_16bit(const polk_char16* cs, const polk_char16* ct)
{
    while(*cs == *ct) {
        if(*cs == 0)
            return 0;
        cs++;
        ct++;
    }

    return *cs - *ct;
}

} } }
