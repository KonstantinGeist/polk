// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef IDTABLE_H_INCLUDED
#define IDTABLE_H_INCLUDED

#include <core/Object.h>
#include <reflection/MetaObject.h>

namespace polk { namespace io {
    struct SBinaryWriter;
    struct SBinaryReader;
} }

namespace polk { namespace reflection {

/**
 * An ID table that allows to map ID's to objects and vice versa during
 * serialization/deserialization. Corresponds to METAIDTABLE from MetaObject.h
 *
 * It's supposed to be a temporary object, data is not necesarilly pinned;
 * object ID's are not guaranteed to be unique across separate serialization/deserialization
 * cycles (ID table reuse).
 */
class CIdTable: public polk::core::CObject
{
public:
    static CIdTable* CreateInstance();

    /**
     * Registers an object and returns its ID. If the object was already registered,
     * returns the previously registered ID.
     */
    virtual intptr_t GetObjectId(const METAOBJECT& metaObject) = 0;

    virtual METAOBJECT ObjectById(intptr_t id) = 0;

    virtual void Serialize(polk::io::SBinaryWriter& bw) = 0;

    /**
     * Fills a METAIDTABLE structure with adapters to redirect to this instance.
     */
    void ToMetaIdTable(METAIDTABLE* out_r);

    /**
     * Creates a CIdTable wrapper from a METAIDTABLE.
     */
    static CIdTable* CreateFromMetaIdTable(METAIDTABLE* metaIdTable);

    static CIdTable* Deserialize(polk::io::SBinaryReader& br);
};

} }

#endif // IDTABLE_H_INCLUDED
