// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef METAOBJECT_H_INCLUDED
#define METAOBJECT_H_INCLUDED

#include <stddef.h> // for size_t
#include <stdint.h>
#include <assert.h> // for SET_VTABLE

#ifndef __cplusplus
    #include <stdbool.h>
#endif

/**
 * C interface to create dynamic objects out of C++ objects.
 *
 * This file should not expose CObject-derived objects because this file
 * may be provided for public consumption by third-party plugins etc.
 */

// **************************************
//   Macros.
// **************************************

#define METACTORIMPL(name) static METARESULT name(int nArg, METAVALUE* args, intptr_t* out_ret, void* ctx)
#define METAMETHODIMPL(name) static METARESULT name(METAOBJECT* _self, int nArg, METAVALUE* args, METAVALUE* out_ret, void* ctx)
#define METASERIALIMPL(name) static METARESULT name(METAOBJECT* obj, METASTREAM* stream, METAIDTABLE* idTable, void* ctx)
#define METADESERIALIMPL(name) static METARESULT name(METASTREAM* stream, METAVALUE* out_ret, METAIDTABLE* idTable, void* ctx)

#define VERIFY_NARG(req) if(nArg != req) return METARESULT_PARAMETER_MISMATCH;
#define VERIFY_ARGTYPE(arg, ttype) if((arg)->type != ttype) return METARESULT_TYPE_ERROR;
#define METAGUARD_BEGIN try {
#define METAGUARD_END } catch (polk::core::SException& e) {\
                        return ExceptionCodeToMetaResult((int)e.Code()); \
                      } return METARESULT_SUCCESS;
#define VERIFY_ARGOBJ(arg, klass) if(VerifyMetaObject(arg, klass) != METARESULT_SUCCESS)\
                                    return METARESULT_TYPE_ERROR;

#define BEGIN_VTABLE(nMeth) int methodCount = nMeth; \
                            static METAMETHOD g_vtable[nMeth]; \
                            memset(g_vtable, 0, sizeof(g_vtable)); \
                            int methodIndex = 0;

#define DEFINE_VMETHOD(name_, impl_, flags_) g_vtable[methodIndex].name = name_; \
                                            g_vtable[methodIndex].impl = impl_; \
                                            g_vtable[methodIndex].flags = flags_; \
                                            methodIndex++;

#define END_VTABLE assert(methodIndex == methodCount);

#define SET_VTABLE(metaClass) metaClass.vtable = g_vtable; \
                              metaClass.methodCount = methodCount;

#define METAVALUE_TO_STRINGPTR(metaValue) static_cast<const polk::core::CString*>((const polk::core::CObject*)(void*)((metaValue)->u.asObject.idptr))
#define STRINGPTR_TO_METAVALUE(strPtr) MetaValueFromPtr("String", (void*)static_cast<const polk::core::CObject*>(strPtr))
#define OBJECT_TO_IDPTR(value) ((intptr_t)(void*)(static_cast<polk::core::CObject*>(value)))
// **************************************

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

struct _METACLASS;
struct _METAOBJECT;
struct _METASTREAM;
struct _METAIDTABLE;

/**
 * A metaobject describes a native object in a reflection-friendly way.
 */
typedef struct _METAOBJECT {
    /**
     * The metaclass which describes the class of the wrapped native object.
     */
    struct _METACLASS* klass;

    /**
     * The pointer to the wrapped native object.
     */
    intptr_t idptr;
} METAOBJECT;

// **************************************
//   Subtypes.
// **************************************
#define METATYPE_NULL 0
#define METATYPE_NUMBER 1
#define METATYPE_OBJECT 2
#define METATYPE_BOOL 3
#define METATYPE_ARRAY 4
typedef int METATYPE;
// **************************************

// **************************************
//   Error codes.
// **************************************
#define METARESULT_SUCCESS 0
#define METARESULT_UNKNOWN_CLASS 1
#define METARESULT_TYPE_ERROR 2
#define METARESULT_PARAMETER_MISMATCH 3
#define METARESULT_INTERNAL_EXCEPTION 4
#define METARESULT_MISSING_METHOD 5
#define METARESULT_MISSING_CTOR 6
#define METARESULT_NULL_REF 7
#define METARESULT_INVALID_ARGUMENT 8
#define METARESULT_ACCESS_DENIED 9
#define METARESULT_NOT_SUPPORTED 10
#define METARESULT_BUFFER_TOO_SMALL 11
typedef int METARESULT;
// **************************************

typedef struct _METAARRAY {
    int count;
    struct _METAVALUE* items;
} METAARRAY;

typedef struct _METAVALUE {
    METATYPE type;

    union {
        float asNumber;
        bool asBool;
        struct _METAOBJECT asObject;
        METAARRAY asArray;
    } u;

    /**
     * Here one can add implementation-specific type annonations (flags)
     * if required.
     */
    int annotations;
} METAVALUE;

typedef METARESULT (*METAMETHODIMPL)(METAOBJECT* self, int nArg, METAVALUE* args, METAVALUE* out_ret, void* ctx);
typedef METARESULT (*METACTORIMPL)(int nArg, METAVALUE* args, intptr_t* out_ret, void* ctx);
typedef METARESULT (*METAREFIMPL)(METAOBJECT* obj, void* ctx);
typedef METARESULT (*METAUNREFIMPL)(METAOBJECT* obj, void* ctx, bool* out_deleted);
typedef METARESULT (*METASERIALIZEIMPL)(METAOBJECT* obj, struct _METASTREAM* stream, struct _METAIDTABLE* idTable, void* ctx);
typedef METARESULT (*METADESERIALIZEIMPL)(struct _METASTREAM* stream, METAVALUE* out_ret, struct _METAIDTABLE* idTable, void* ctx);

typedef METARESULT (*METASTREAMWRITEIMPL)(struct _METASTREAM* stream, const void* p, size_t sz);
typedef METARESULT (*METASTREAMREADIMPL)(struct _METASTREAM* stream, void* p, size_t sz);

typedef METARESULT (*METAIDTABLEGETOBJECTIDIMPL)(struct _METAIDTABLE* idTable, METAVALUE* value, intptr_t* out_ret);
typedef METARESULT (*METAIDTABLEOBJECTBYIDIMPL)(struct _METAIDTABLE* idTable, intptr_t id, METAVALUE* out_ret);

typedef int METAMETHODFLAGS;

/**
 * A static method does not require a `self` object to operate on.
 * Not used by the metaobject system; may be useful to implementations.
 */
#define METAMETHODFLAGS_STATIC 1

/**
 * An asynchronous method. Not used by the metaobject system; may be useful to
 * implementations.
 */
#define METAMETHODFLAGS_ASYNC 2

typedef int METACLASSFLAGS;
#define METACLASSFLAGS_STATIC 1

/**
 * Metamethod.
 */
typedef struct {
    const char* name;
    METAMETHODIMPL impl;
    METAMETHODFLAGS flags;

    /**
     * Additional context to access wrapped code; can be accessible in method
     * implementations.
     */
    void* ctx;
} METAMETHOD;

/**
 * Metaclass.
 */
typedef struct _METACLASS {
    METAMETHOD* vtable;
    int methodCount;
    const char* name;
    METACTORIMPL ctor;
    METAREFIMPL refimpl;
    METAUNREFIMPL unrefimpl;
    METASERIALIZEIMPL serialimpl;
    METADESERIALIZEIMPL deserialimpl;
    METACLASSFLAGS flags;
    struct _METACLASS* parent;

    /**
     * Additional context to access wrapped code; can be accessible in
     * constructor/destructor implementations.
     */
    void* ctx;

    void* reserved;
} METACLASS;

/**
 * Metastream. Glue code should provide the metastream object (wrapping some
 * internal stream API, perhaps CStream), so it's their responsibility to
 * correctly construct/free an appropriate METASTREAM object when needed.
 */
typedef struct _METASTREAM {
    METASTREAMWRITEIMPL writeimpl;
    METASTREAMREADIMPL readimpl;

    /**
     * The internal object this interface wraps.
     */
    void* ctx;
} METASTREAM;

/**
 * A table to map ID's to object data during (de)serialization.
 */
typedef struct _METAIDTABLE {
    METAIDTABLEGETOBJECTIDIMPL getobjectidimpl;
    METAIDTABLEOBJECTBYIDIMPL objectbyidimpl;

    /**
     * The internal object this interface wraps.
     */
    void* ctx;
} METAIDTABLE;

/**
 * @note The returned value should not be freed.
 */
const char* MetaResultToCString(METARESULT r);

/**
 * Registers a new metaclass, should be called prior to any code.
 *
 * @warn Not thread-safe.
 */
void RegisterMetaClass(METACLASS* klass);

/**
 * Use this function during a script manager's initialization.
 *
 * @param out_val A list of const char* to return the list of class names into.
 * Can be null if the function is first used to determine the number of registered
 * classes only.
 * @return The number of metaclasses.
 */
int GetMetaClasses(const char** out_val);

METACLASS* GetMetaClassByName(const char* name);

/**
 * Creates a new metaobject:
 *
 * @param nArg number of arguments
 * @param args a list of arguments
 * @param out_ret the returned value
 * @return success or error code
 */
METARESULT NewMetaObject(const char* klassName, int nArg, METAVALUE* args, METAVALUE* out_ret);

/**
 * If the value contains a metaobject, calls the associated destructor and destroys
 * the wrapper. The value itself is cleared to contain NULL.
 */
METARESULT UnrefMetaValue(METAVALUE* value, bool *out_deleted);

METARESULT RefMetaObject(METAOBJECT* obj);
METARESULT UnrefMetaObject(METAOBJECT* obj, bool *out_deleted);

METAVALUE MetaValueFromNull();

METAVALUE MetaValueFromNumber(float number);
METAVALUE MetaValueFromBoolean(bool b);

METAVALUE MetaValueFromCString(const char* value);

/**
 * Allocates a subarray with the given count. WARNING Should be deleted with
 * UnrefMetaValue, or memory will leak.
 */
METAVALUE CreateArrayMetaValue(int count);

/**
 * A convenience function to construct a metaobject (wrapped as a metavalue)
 * from a pointer, given a class. The class should be pre-registered.
 * The function is used in implementations.
 *
 * Does not ref the value.
 */
METAVALUE MetaValueFromPtr(const char* klassName, void* ptr);

/**
 * Same as MetaValueFromPtr except attempts to ref the value.
 */
METAVALUE MetaValueFromVal(const char* klassName, void* ptr);

METAVALUE MetaValueFromId(const char* klassName, int id);

/**
 * Looks for a method in the given metaclass and returns null if nothing found.
 *
 * @warning Not thread-safe because caches the search result internally without
 * locking.
 */
METAMETHOD* GetMetaMethod(METACLASS* klass, const char* name);

/**
 * Invokes a metamethod by its name.
 *
 * @warning Not thread-safe because caches the search result internally without
 * locking.
 */
METARESULT InvokeMetaMethod(METAOBJECT* obj,
                            const char* methodName,
                            int nArg,
                            METAVALUE* args,
                            METAVALUE* out_ret);

/**
 * Invokes a metamethod given a direct pointer to it.
 *
 * @note The call mechanism is thread-safe.
 * @note No verification happens to verify that the metamethod belongs to the
 * specified object. It's expected that the metamethod was returned from a call
 * to GetMetaMethod(..)
 */
METARESULT InvokeMetaMethodDirect(METAOBJECT* obj,
                                  METAMETHOD* method,
                                  int nArg,
                                  METAVALUE* args,
                                  METAVALUE* out_ret);

/**
 * The ref implementation for MetaObjects that wrap CObject-derived objects.
 */
METARESULT MetaObjectRefImpl(METAOBJECT* obj, void* ctx);

/**
 * The unref implementation for MetaObjects that wrap CObject-derived objects.
 */
METARESULT MetaObjectUnrefImpl(METAOBJECT* obj, void* ctx, bool* out_deleted);

/**
 * Verifies that the object is an instance of the given class, or it's an
 * instance of a subclass of the given class.
 */
METARESULT VerifyMetaObject(METAVALUE* arg, const char* klassName);

/**
 * Returns true if the object is an instance of the given class, or it's an
 * instance of a subclass of the given class.
 */
bool IsInstanceOf(METAOBJECT* obj, const char* klassName);

METARESULT RefMetaValue(METAVALUE* metaValue);

/**
 * Tries to call "ToString" metamethod on the metavalue to convert the metavalue
 * to a polk string. If the metavalue does not support the method, or an error occurs,
 * the returned value is null.
 *
 * @note The returned value should be unref'd with UnrefMetaValue(..)
 */
METAVALUE MetaValueToString(METAVALUE* metaValue);

char* MetaValueToCString(METAVALUE* metaValue);
void MetaValueFreeCString(char* cs);

bool MetaValueEquals(METAVALUE* a, METAVALUE* b);
int GetMetaValueHashCode(METAVALUE* metaValue);

/**
 * Patches a method of an existing class with a new implementation.
 * For example, if one wants to redirect the "FileUtils::ReadAllText" metamethod
 * to read from a different source (network, virtual file system etc.), this function is
 * useful in patching an existing method.
 */
METARESULT PatchMetaMethod(const char* klassName, const char* methodName, METAMETHODIMPL impl);

METARESULT ExceptionCodeToMetaResult(int exceptionCode);

// **************************
//   Serialization support.
// **************************

METARESULT SerializeMetaValue(METAVALUE* value, METASTREAM* stream, METAIDTABLE* idTable);
METARESULT DeserializeMetaValue(METASTREAM* stream, METAVALUE* out_ret, METAIDTABLE* idTable);

 #ifdef __cplusplus
}
#endif /* __cplusplus */

// Do not call directly.
void __InitMetaObject();
void __DeinitMetaObject();

#endif // METAOBJECT_H_INCLUDED

