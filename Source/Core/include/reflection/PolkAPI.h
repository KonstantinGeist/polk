// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef POLKAPI_H_INCLUDED
#define POLKAPI_H_INCLUDED

#include "MetaObject.h"

typedef const char* (*MetaResultToCStringFunc)(METARESULT r);
typedef void (*RegisterMetaClassFunc)(METACLASS* klass);
typedef int (*GetMetaClassesFunc)(const char** out_val);
typedef METACLASS* (*GetMetaClassByNameFunc)(const char* name);
typedef METARESULT (*NewMetaObjectFunc)(const char* klassName, int nArg, METAVALUE* args, METAVALUE* out_ret);
typedef METARESULT (*UnrefMetaValueFunc)(METAVALUE* value, bool *out_deleted);
typedef METARESULT (*RefMetaObjectFunc)(METAOBJECT* obj);
typedef METARESULT (*UnrefMetaObjectFunc)(METAOBJECT* obj, bool *out_deleted);
typedef METAVALUE (*MetaValueFromNullFunc)();
typedef METAVALUE (*MetaValueFromNumberFunc)(float number);
typedef METAVALUE (*MetaValueFromBooleanFunc)(bool b);
typedef METAVALUE (*MetaValueFromCStringFunc)(const char* value);
typedef METAVALUE (*MetaValueFromPtrFunc)(const char* klassName, void* ptr);
typedef METAVALUE (*MetaValueFromValFunc)(const char* klassName, void* ptr);
typedef METAVALUE (*MetaValueFromIdFunc)(const char* klassName, int id);
typedef METAMETHOD* (*GetMetaMethodFunc)(METACLASS* klass, const char* name);
typedef METARESULT (*InvokeMetaMethodFunc)(METAOBJECT* obj,
                            const char* methodName,
                            int nArg,
                            METAVALUE* args,
                            METAVALUE* out_ret);
typedef METARESULT (*InvokeMetaMethodDirectFunc)(METAOBJECT* obj,
                                  METAMETHOD* method,
                                  int nArg,
                                  METAVALUE* args,
                                  METAVALUE* out_ret);
typedef METARESULT (*VerifyMetaObjectFunc)(METAVALUE* arg, const char* klassName);
typedef bool (*IsInstanceOfFunc)(METAOBJECT* obj, const char* klassName);
typedef METARESULT (*RefMetaValueFunc)(METAVALUE* metaValue);
typedef METAVALUE (*MetaValueToStringFunc)(METAVALUE* metaValue);
typedef char* (*MetaValueToCStringFunc)(METAVALUE* metaValue);
typedef void (*MetaValueFreeCStringFunc)(char* cs);
typedef bool (*MetaValueEqualsFunc)(METAVALUE* a, METAVALUE* b);
typedef int (*GetMetaValueHashCodeFunc)(METAVALUE* metaValue);
typedef METARESULT (*PatchMetaMethodFunc)(const char* klassName, const char* methodName, METAMETHODIMPL impl);
typedef METARESULT (*SerializeMetaValueFunc)(METAVALUE* value, METASTREAM* stream, METAIDTABLE* idTable);
typedef METARESULT (*DeserializeMetaValueFunc)(METASTREAM* stream, METAVALUE* out_ret, METAIDTABLE* idTable);

/**
 * A context for late-bound calls into the MetaObject system. This makes it possible for
 * plugins to call into the engine without statically linking to it, provided they acquire
 * a pointer to this context. See the corresponding functions in MetaObject.h for more info.
 */
struct PolkAPI
{
    MetaResultToCStringFunc MetaResultToCString;
    RegisterMetaClassFunc RegisterMetaClass;
    GetMetaClassesFunc GetMetaClasses;
    GetMetaClassByNameFunc GetMetaClassByName;
    NewMetaObjectFunc NewMetaObject;
    UnrefMetaValueFunc UnrefMetaValue;
    RefMetaObjectFunc RefMetaObject;
    UnrefMetaObjectFunc UnrefMetaObject;
    MetaValueFromNullFunc MetaValueFromNull;
    MetaValueFromNumberFunc MetaValueFromNumber;
    MetaValueFromBooleanFunc MetaValueFromBoolean;
    MetaValueFromCStringFunc MetaValueFromCString;
    MetaValueFromPtrFunc MetaValueFromPtr;
    MetaValueFromValFunc MetaValueFromVal;
    MetaValueFromIdFunc MetaValueFromId;
    GetMetaMethodFunc GetMetaMethod;
    InvokeMetaMethodFunc InvokeMetaMethod;
    InvokeMetaMethodDirectFunc InvokeMetaMethodDirect;
    VerifyMetaObjectFunc VerifyMetaObject;
    IsInstanceOfFunc IsInstanceOf;
    RefMetaValueFunc RefMetaValue;
    MetaValueToStringFunc MetaValueToString;
    MetaValueToCStringFunc MetaValueToCString;
    MetaValueFreeCStringFunc MetaValueFreeCString;
    MetaValueEqualsFunc MetaValueEquals;
    GetMetaValueHashCodeFunc GetMetaValueHashCode;
    PatchMetaMethodFunc PatchMetaMethod;
    SerializeMetaValueFunc SerializeMetaValue;
    DeserializeMetaValueFunc DeserializeMetaValue;
};

void InitializePolkAPI(PolkAPI* polkAPI);

#endif // POLKAPI_H_INCLUDED
