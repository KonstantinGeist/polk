// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef MEMORYSTREAM_H_INCLUDED
#define MEMORYSTREAM_H_INCLUDED

#include <io/Stream.h>
#include <io/ByteBuffer.h>

namespace polk { namespace io {

/**
 * An implementation of CStream which reads/writes from/to memory.
 */
class CMemoryStream: public CStream
{
private:
    polk::core::Auto<CByteBuffer> m_bb;
    polk_long m_position;

    void ensureBuffer(polk_long end);

public:
    CMemoryStream(CByteBuffer* bb = nullptr);

    virtual bool CanRead() const override;
    virtual bool CanWrite() const override;
    virtual bool CanSeek() const override;
    virtual polk_long Read(char* buf, polk_long count) override;
    virtual polk_long Write(const char* buf, polk_long count) override;
    virtual void SetPosition(polk_long pos) override;
    virtual polk_long GetPosition() const override;
    virtual polk_long Size() const override;

    /**
     * Pointer to the memory block at the current position.
     */
    polk_byte* CurrentBytes() const;

    /**
     * Clears the underlying byte buffer.
     */
    void Clear();
};

} }

#endif // MEMORYSTREAM_H_INCLUDED
