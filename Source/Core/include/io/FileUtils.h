// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef FILEUTILS_H_INCLUDED
#define FILEUTILS_H_INCLUDED

#include <collections/ArrayList.h>
#include <core/String.h>
#include <io/Stream.h>

namespace polk { namespace io { namespace FileUtils {

/**
 * Opens a text file by the given path, reads all lines of the file into a
 * string array, and then closes the file.
 */
polk::collections::CArrayList<const polk::core::CString*>*
        ReadAllLines(const polk::core::CString* path);

/**
 * Reads all lines from the stream into a string array.
 */
polk::collections::CArrayList<const polk::core::CString*>*
        ReadAllLines(polk::io::CStream* stream);

/**
 * Opens a text file, reads all lines of the file into a string, and then closes
 * the file.
 */
const polk::core::CString* ReadAllText(const polk::core::CString* path);

/**
 * Reads all lines from the stream into a string.
 */
const polk::core::CString* ReadAllText(polk::io::CStream* stream);

/**
 * Creates a new file, writes one or more strings to the file, and then closes
 * the file.
 */
void WriteAllLines(const polk::core::CString* path,
                   polk::collections::CArrayList<const polk::core::CString*>* lines);

/**
 * Reads all bytes in the stream (depends on stream::Size())
 * Delete the output with delete []
 */
polk_byte* ReadAllBytes(polk::io::CStream* stream, polk_long* out_size = 0);

}

// Do not call directly.
void __InitMetaFileUtils();

} }

#endif // FILEUTILS_H_INCLUDED
