// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef BYTEBUFFER_H_INCLUDED
#define BYTEBUFFER_H_INCLUDED

#include <core/Object.h>

namespace polk { namespace io {

/** Default byte buffer capacity. */
#define POLK_DEF_BYTEBUFFER_CAP 512

/** Default grow factor. */
#define POLK_BYTEBUFFER_GROW_FACTOR 0.75f

/**
 * An expandable or fixed byte buffer.
 */
class CByteBuffer: public polk::core::CObject
{
private:
    polk_long m_size;
    polk_long m_cap;
    polk_long m_initCap; // Initial capacity (restored to it when trimmed down).
    polk_byte* m_bytes;
    bool m_isFixed;

    void appendBytesGeneric(const polk_byte* bytes, polk_long count);

public:
    /**
     * Constructor.
     *
     * @param cap The initial capacity. Optional.
     */
    explicit CByteBuffer(polk_long cap = 0);

    virtual ~CByteBuffer();

    /**
     * Appends bytes to the byte buffer.
     *
     * @note If bytes is NULL, increases the buffer anyway, but the content of
     * the newly appended portion is undefined.
     * @param bytes Array to append.
     * @param count Count of bytes.
     * @throws EC_INVALID_STATE if the buffer is fixed
     */
    void AppendBytes(const polk_byte* bytes, polk_long count);

    /**
     * Appends one byte to the byte buffer.
     *
     * @param byte the byte to write to the buffer
     * @throws EC_INVALID_STATE if the buffer is fixed
     */
    void AppendByte(polk_byte byte);

    /**
     * Clears the buffer by setting its size to 0.
     *
     * @throws EC_INVALID_STATE if the buffer is fixed
     */
    void Clear();

    /**
     * Gets the size of the buffer.
     */
    inline polk_long Size() const
    {
        return m_size;
    }

    /**
     * Direct access to the data.
     */
    inline polk_byte* Bytes() const
    {
        return m_bytes;
    }

    /**
     * Makes the buffer fixed.
     */
    void SetFixed(bool b);
};

} }

#endif // BYTEBUFFER_H_INCLUDED
