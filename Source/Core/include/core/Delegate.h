// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef DELEGATE_H_INCLUDED
#define DELEGATE_H_INCLUDED

#include <collections/ArrayList.h>
#include <core/String.h>

namespace polk { namespace core {

/**
 * Represents the base struct for structs that contain event data.
 *
 * @note This class serves as the base struct for all structs that represent
 * event data. For example, a SMouseEventArgs struct would derive
 * from SEventArgs and could be used to hold data for mouse events. To create a
 * custom event data struct, create a struct that derives from the SEventArgs
 * struct and provide the properties to store the necessary data. The name of
 * your custom event data struct should end with EventArgs.
 * @note SEventArgs-derived structs should not acquire references to
 * reference-counted objects because SEventArgs-derived structs are short-lived
 * and they always live on the stack and there's always someone up the stack
 * owning the reference to the object in question and therefore keeping the
 * object from being disposed.
 */
struct SEventArgs
{
    /**
     * Reserved. Do not use.
     */
    void* Reserved;

    SEventArgs()
        : Reserved(0)
    {
    }
};

/**
 * A base delegate class, which is a data structure that refers to a static
 * function or to a class instance and an instance method (member function) of
 * that class.
 *
 * @note Not to be used directly, use CInstanceDelegate or CStaticDelegate instead.
 */
template <class E>
class CDelegate: public CObject
{
public:

    /**
     * The method body of the delegate.
     */
    virtual void Invoke(const CObject* sender, E& e) = 0;
};

/**
 * Represents an instance delegate closed over an instance of class T
 * with parameter information provided by E.
 *
 * @warning Instance delegates do not Ref()/Unref() objects they're bound to
 * (to avoid cyclic references because objects often contain delegates bound to
 * them). Ref() both a delegate and a context, to keep their objects alive.
 *
 * @param T The type of the instance the delegate is bound to.
 * @param E Parameter list information; E should derive from SEventArgs.
 */
template <class T, class E>
class CInstanceDelegate: public CDelegate<E>
{
private:
    T* m_target;
    void(T::*m_impl)(const CObject* sender, E& e);

public:
    /**
     * @param target The instance object the delegate is to be bound to.
     * @param impl A pointer to the member function this delegate wraps.
     */
    CInstanceDelegate(T* target, void(T::*impl)(const CObject* sender, E& e))
        : m_target(target), m_impl(impl) { }

    virtual void Invoke(const CObject* sender, E& e) override
    {
        (m_target->*m_impl)(sender, e);
    }
};

/**
 * Represents a static delegate closed over a static function.
 *
 * @param E Parameter list information; E should derive from SEventArgs.
 */
template <class E>
class CStaticDelegate: public CDelegate<E>
{
private:
    void(*m_simpl)(const CObject* sender, E& e);

public:
    /**
     * @param simpl A pointer to the static function.
     */
    explicit CStaticDelegate(void(*simpl)(const CObject* sender, E& e))
        : m_simpl(simpl) { }

    virtual void Invoke(const CObject* sender, E& e) override
    {
        m_simpl(sender, e);
    }
};

/**
 * Events enable an object to notify other objects when something of interest
 * occurs. The object that sends (or raises) the event is called the sender and
 * the objects that receive (or handle) the event are called handlers.
 */
template <class E>
class CEvent: public CObject
{
protected:
    polk::core::Auto<polk::collections::CArrayList<CDelegate<E>*> > m_dels;

public:
    CEvent()
        : m_dels(new polk::collections::CArrayList<CDelegate<E>*>())
    {
    }

    /**
     * Adds a new handler for the event.
     *
     * @param del The new handler.
     */
    void AddHandler(CDelegate<E>* del)
    {
        m_dels->Add(del);
    }

    /**
     * Removes the handler.
     *
     * @param del The handler to be removed from the notification list.
     */
    void RemoveHandler(CDelegate<E>* del)
    {
        m_dels->Remove(del);
    }

    /**
     * Fires the event notifying all the handlers something of interest has
     * occured.
     *
     * @param sender The object that has raised the event. Typically, it is "this".
     * @param e Event data.
     */
    void Fire(const CObject* sender, E& e)
    {
        for(int i = 0; i < m_dels->Count(); i++) {
            (m_dels->Array()[i])->Invoke(sender, e);
        }
    }

   /**
    * Clears the list of the handlers.
    *
    * @warning Call only inside destructors when the order of destruction is
    * important. Otherwise, it is dangerous to clear events, because program
    * logic relies on subscribed delegates.
    */
    void DangerousClear()
    {
        m_dels->Clear();
    }

    /**
     * Returns the number of handlers for this event.
     */
    inline int Count() const
    {
        return m_dels->Count();
    }

    inline CDelegate<E>* Item(int index) const
    {
        return m_dels->Item(index);
    }
};

} }

#endif // DELEGATE_H_INCLUDED
