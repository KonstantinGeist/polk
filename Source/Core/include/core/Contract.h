// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef CONTRACT_H_INCLUDED
#define CONTRACT_H_INCLUDED

/*
 * Contract macros to verify arguments.
 *
 * The good thing about these contract macros is that they can be defined to
 * no-op for release mode. They always are in debug mode (if POLK_DEBUG_MODE is on).
 * You can enable contracts for release mode by enabling the POLK_CONTRACT define.
 */

#include <core/Exception.h>

// Better leave contract everywhere including releases, for better remote
// diagnostics & safer code.
#define POLK_CONTRACT

#ifdef POLK_CONTRACT

    // ****************************************************************************************
    //   These can be redefined to either throw exceptions (to try fix an error dynamically),
    //   use asserts if we're more pessimistic, or to no-op in release.
    // ****************************************************************************************

    // The contracts' implementations tell for themselves.

    #define POLK_REQ(x, ec) if(!(x)) { POLK_THROW(ec); }
    #define POLK_REQ_NOT(x, ec) if(x) { POLK_THROW(ec); }
    #define POLK_REQ_WITH_MSG(x, ec, msg) if(!(x)) { POLK_THROW_WITH_MSG(ec, msg); }
    #define POLK_REQ_NOT_NEG(x) POLK_REQ_WITH_MSG(x >= 0, polk::core::EC_ILLEGAL_ARGUMENT, "Input cannot be negative.")
    #define POLK_REQ_POS(x) POLK_REQ_WITH_MSG(x > 0, polk::core::EC_ILLEGAL_ARGUMENT, "Input must be positive.")
    #define POLK_REQ_EQUALS(x, y) if((x) != (y)) { POLK_THROW_WITH_MSG(polk::core::EC_CONTRACT_UNSATISFIED, "Equality condition unsatisfied."); }
    #define POLK_REQ_NOT_EQUALS(x, y) if((x) == (y)) { POLK_THROW_WITH_MSG(polk::core::EC_CONTRACT_UNSATISFIED, "Inequality condition unsatisfied."); }
    #define POLK_REQ_PTR(x) POLK_REQ_WITH_MSG(x, polk::core::EC_ILLEGAL_ARGUMENT, "Input cannot be null pointer.")

#else

    // **********
    //   No-op.
    // **********

    #define POLK_REQ(x, ec)
    #define POLK_REQ_NOT(x, ec)
    #define POLK_REQ_WITH_MSG(x, ec, msg)
    #define POLK_REQ_NOT_NEG(x)
    #define POLK_REQ_POS(x)
    #define POLK_REQ_EQUALS(x, y)
    #define POLK_REQ_NOT_EQUALS(x, y)
    #define POLK_REQ_PTR(x)

#endif

// Suffix _D ('D" for "Dynamic") means that it should always be a dynamic
// exception, even if the compilation mode for contracts is "no-op" (unless dynamic
// checks are forced to be no-op as well). Important for collections.
#define POLK_REQ_RANGE_D(value, minIncl, maxExcl) \
                POLK_REQ((value) >= (minIncl) && (value) < (maxExcl), polk::core::EC_OUT_OF_RANGE)

#define POLK_REQ_NEVER POLK_THROW_WITH_MSG(polk::core::EC_CONTRACT_UNSATISFIED, "Should never be reached.");

#endif // CONTRACT_H_INCLUDED
