// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef BASEDEFS_H_INCLUDED
#define BASEDEFS_H_INCLUDED

#include <new>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// Atomic is not implemented for JS because it's single-threaded.
#ifndef POLK_JS
	#include <atomic>
#endif

#ifdef POLK_WIN
  #define UNICODE
  #define _UNICODE
  #define WINVER 0x0501
  #define WIN32_LEAN_AND_MEAN // Excludes winsock v1 and a lot of other garbage.
  #include <windows.h>

  // FIX for O3+vectorization, non-16-byte aligned stack (which can happen on
  // Windows) can wreck havoc when using SSE. Used mostly for OS callbacks.
  #define POLK_FORCE_ALIGNMENT __attribute__((force_align_arg_pointer))
#endif

#ifdef POLK_X
  // Linux complains about it. It doesn't matter, as __cdecl is by default on Linux.
  #define __cdecl

  // No need to force alignment under Linux.
  #define POLK_FORCE_ALIGNMENT
#endif

// ************************************
//   System-indepedent integer values.
// ************************************

typedef unsigned char polk_char8;
typedef unsigned char polk_byte;
typedef uint16_t polk_char16;
typedef uint16_t polk_uint16;
typedef uint32_t polk_uint32;
typedef int64_t polk_long;
typedef uint64_t polk_uint64;

// ************************************

/**
 * The size of boolean is not stable across frameworks. We standardize it to be
 * same as "int" for better interop with scripting languages such as C#.
 */
typedef int polk_bool; // For interop.

/**
 * The minimum allowed in32 value.
 */
#define POLK_INT32_MIN (-2147483647 - 1)

/**
 * The maximum allowed in32 value.
 */
#define POLK_INT32_MAX 2147483647

/**
 * The maximum allowed in16 value.
 */
#define POLK_UINT16_MAX 65535

/**
 * Atomic int.
 */
#ifdef POLK_JS
	typedef int polk_atomic_int; // Atomic is not implemented for JS because it's single-threaded.
#else
	typedef std::atomic_int polk_atomic_int;
#endif

/**
 * Converts a C literal to a polk char16 literal.
 */
#define POLK_CHAR(x) ((polk_char16)(L ## x)) // FIXME warn about data loss?

#endif // BASEDEFS_H_INCLUDED
