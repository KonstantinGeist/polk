// *****************************************************************************
//
//  Copyright (c) Konstantin Geist. All rights reserved.
//
//  The use and distribution terms for this software are contained in the file
//  named License.txt, which can be found in the root of this distribution.
//  By using this software in any fashion, you are agreeing to be bound by the
//  terms of this license.
//
//  You must not remove this notice, or any other, from this software.
//
// *****************************************************************************

#ifndef POLK_REF_H_INCLUDED
#define POLK_REF_H_INCLUDED

#include <core/basedefs.h>
#include <core/Variant.h>
#include <core/Object.h>

namespace polk { namespace collections {

// Special functions that can be used by containers.
// For CObjects, calls Ref() & Unref().
// For other types, a no-op.
//
// Supported primitive types: int, const char*, void*, CObject*, polk_char16* & others.

    // **********
    //   Object
    // **********

bool POLK_EQUALS(const polk::core::CObject* obj1, const polk::core::CObject* obj2);

inline int POLK_HASHCODE(const polk::core::CObject* obj)
{
    return obj->GetHashCode();
}

inline void POLK_REF(const polk::core::CObject* obj)
{
    if(obj)
        obj->Ref();
}

inline void POLK_UNREF(const polk::core::CObject* obj)
{
    if(obj)
        obj->Unref();
}

inline bool POLK_IS_NULL(const polk::core::CObject* obj)
{
    return !obj;
}

    // *****
    //  int
    // *****

inline void POLK_REF(const int i) { }
inline void POLK_UNREF(const int i) { }

inline bool POLK_IS_NULL(const int i)
{
    return false;
}

inline bool POLK_EQUALS(const int a, const int b)
{
    return a == b;
}

inline int POLK_HASHCODE(const int i)
{
    return i;
}

    // ************
    //  polk_uint32
    // ************

inline void POLK_REF(const polk_uint32 i) { }
inline void POLK_UNREF(const polk_uint32 i) { }

inline bool POLK_IS_NULL(const polk_uint32 i)
{
    return false;
}

inline bool POLK_EQUALS(const polk_uint32 a, const polk_uint32 b)
{
    return a == b;
}

inline int POLK_HASHCODE(const polk_uint32 i)
{
    return (int)i;
}

    // ************
    //  polk_uint16
    // ************

inline void POLK_REF(const polk_uint16 i) { }
inline void POLK_UNREF(const polk_uint16 i) { }

inline bool POLK_IS_NULL(const polk_uint16 i)
{
    return false;
}

inline bool POLK_EQUALS(const polk_uint16 a, const polk_uint16 b)
{
    return a == b;
}

inline int POLK_HASHCODE(const polk_uint16 i)
{
    return (int)i;
}

    // *******
    //  void*
    // *******

inline void POLK_REF(const void* i) { }
inline void POLK_UNREF(const void* i) { }
inline bool POLK_EQUALS(const void* a, const void* b) { return a == b; }
inline bool POLK_IS_NULL(const void* i) { return !i; }

inline int POLK_HASHCODE(const void* i)
{
    // NOTE Shifts by 3 because pointers are often aligned and have 0's at the end;
    // focused on 64 bit platforms.
    return (size_t)(i) >> 3;
}

    // ***************
    //   const char*
    // ***************

inline void POLK_REF(const char* i) { }
inline void POLK_UNREF(const char* i) { }

bool POLK_EQUALS(const char* a, const char* b);
int POLK_HASHCODE(const char* i);

inline bool POLK_IS_NULL(const char* c)
{
    return !c;
}

    // **************
    //   polk_char16*
    // **************

inline void POLK_REF(const polk_char16* cs) { }
inline void POLK_UNREF(const polk_char16* cs) { }

bool POLK_EQUALS(const polk_char16* cs1, const polk_char16* cs2);
int POLK_HASHCODE(const polk_char16* cs);

inline bool POLK_IS_NULL(const polk_char16* c)
{
    return !c;
}

    // ************
    //   SVariant
    // ************

inline void POLK_REF(const polk::core::SVariant& v) { }
inline void POLK_UNREF(const polk::core::SVariant& v) { }

inline bool POLK_EQUALS(const polk::core::SVariant& v1, const polk::core::SVariant& v2)
{
    return v1.Equals(v2);
}

inline int POLK_HASHCODE(const polk::core::SVariant& v1)
{
    return v1.GetHashCode();
}

inline bool POLK_IS_NULL(const polk::core::SVariant& v)
{
    return false;
}

    // ***********
    //   polk_byte
    // ***********

inline void POLK_REF(const polk_byte b) { }
inline void POLK_UNREF(const polk_byte b) { }
inline bool POLK_EQUALS(const polk_byte b1, const polk_byte b2) { return b1 == b2; }
inline int POLK_HASHCODE(const polk_byte b) { return b; }
inline bool POLK_IS_NULL(const polk_byte b) { return false; }

    // *********
    //   float
    // *********

inline void POLK_REF(const float i) { }
inline void POLK_UNREF(const float i) { }

// NOTE Do not define POLK_EQUALS for floats, because using floats as dictionary
// keys is a bad idea.

inline int POLK_HASHCODE(const float i)
{
    int r;
    memcpy(&r, &i, sizeof(int) < sizeof(float)? sizeof(int): sizeof(float));
    return r;
}

inline bool POLK_IS_NULL(float i)
{
    return false;
}

    // ***********
    //   polk_long
    // ***********

inline void POLK_REF(const polk_long i) { }
inline void POLK_UNREF(const polk_long i) { }
inline bool POLK_EQUALS(const polk_long a, const polk_long b) { return a == b; }
inline bool POLK_IS_NULL(const polk_long i) { return false; }
inline int POLK_HASHCODE(const polk_long i) { return (int)i; } // TODO ?

} }

#endif // POLK_REF_H_INCLUDED
