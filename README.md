**Polk** (Russian for *regiment*) is a lightweight scripting engine for soft realtime multiple agent systems (that is, video games).
*Disclaimer:* the project is experimental and was made for esoteric purposes.

A typical video game has the following characteristics:

1) Multiple AI agents (NPC's), each with their own logic & data running in parallel.
2) Soft realtime: we want each frame to complete in a predictable amount of time.
3) Game state can be saved/loaded (preferrably at any time -- not only at checkpoints).
4) Most of the code is implemented in C++; only high level abstractions that are of interest to level designers (NPC control, environment scripts) are exposed to a scripting language.
5) Often needs a domain-specific language to more easily express game mechanics.

Many of the existing programming languages lack one of the following features that are of use in game development:

1) Lightweight green threads, both cooperative and preemptive.
2) Rejection of GC to avoid unpredictable pauses.
3) Ability to serialize full thread state on disk, for a later reload.
4) Simple and low overhead communication between host code (C/C++) and the scripting language.
5) Language extensibility for custom game DSL's.

Polk aims to implement all these features:

1) We make use of lightweight "tracks" (both preemptable and cooperative) to simulate hundreds of NPC's on the same logic thread. Tracks can also be gracefully cancelled for a better transition between NPC behaviors.
2) Simple reference counting under the hood instead of a generational GC to avoid pauses. There's no global runtime object: all tracks are independent of each other so they can be created/released gradually one by one.
3) Full track state can be serialized/deserialized at any point (all globals and all stack variables including full object trees they reference).
4) Low overhead communication with host code.
5) Simple grammar and a simple implementation that allow to easily extend the engine with new syntactical structures and new opcodes.

Polk does not make decisions for you and only provides the needed primitives. For example, it doesn't implement thread scheduling heuristics, although it allows to set an individual thread's "quantum" (for how long it should run before it is preempted to give time to other tracks), because there's no one-fits-all algorithm. Polk implements only the most basic classes such as "string", "list" etc. -- it's expected that the rest will be provided by host code.

## How to compile

I have tested it on Windows and Lubuntu.

Requirements for compilation (the tools the project was succesfully compiled with):

    CMake 3.4.1 (requires bash tools for Windows)
    GCC 5.1 (MinGW TDM supported for Windows)
	
For example, to compile the polk interpreter (example project), go to Examples/psi and call cmake_build_win64r.bat (for Windows). Note that on Windows, the file has to be launched twice for some reason.

## Syntax

See Documentation/Syntax.md

## Class library

The standard class library includes the most basic functionality: 

	Application, Class, Console, CoreUtils, DateTime, FileSystem, FileUtils, List, Map, Method, Object, Path, Random, Reflection, String, StringBuilder.

The class library will grow to cover all internals of the runtime.

## Performance considerations

As it is expected that the scripting engine is only used for the highest level abstractions, with the rest being implemented in C++, I did not attempt to make it a very performant implementation.

## Note on C++

See Documentation/OnImplementation.md

## What else needs to be done

* Expose more of the internal classes to Polk (Math, Track, etc.)
* More tests. Implement tests for Linux.
* Macros to make some of the aspects of the language less clumsy.
* More examples.
* Fix the thread pool.
etc.